/**
 * Created by Tech N Choice on 6/16/2017.
 */
(function(){
  angular
    .module('fuse')
    .factory('AuthenticateFactory' , AuthenticateFactory);
})();

function AuthenticateFactory($q, $state, $sessionStorage)
{
  function getUserToken()
  {

    return sessionStorage.getItem('ngStorage-login');
  }

  function isAuthenticated()
  {

    var deferred = $q.defer();
    var user = sessionStorage.getItem('ngStorage-login') || sessionStorage.getItem('ngStorage-login');
    user && deferred.resolve(authentication.OK) || deferred.reject(authentication.UNAUTHORIZED);
    return deferred.promise;

  }
  var authentication = {
    OK: 200,
    UNAUTHORIZED : 401,
    isAuthenticated : isAuthenticated,
    getUserToken    : getUserToken
    }

  return authentication;
};

