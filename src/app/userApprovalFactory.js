/**
 * Created by Tech N Choice on 5/29/2017.
 */
/**
 * Created by Tech N Choice on 5/27/2017.
 */
(function(){
  angular
    .module('fuse')
    .factory('userApprovalFactory' , userApprovalFactory);
})();
userApprovalFactory.$inject = ['api', 'XmlJson'];
function userApprovalFactory(api, XmlJson){

  var service = {
    GetSavedApprovalsByModule : GetSavedApprovalsByModule,
    GetUserApprovalRights     : GetUserApprovalRights,
    SetDealApprovals          : SetDealApprovals,
    GetApprovalHistory        : GetApprovalHistory
  }

  function GetSavedApprovalsByModule(module, callback){
    api.GetSavedApprovalsByModule(module).then(success)
    function success(savedApprovals){
     callback(savedApprovals['output']['adm_web_approval_policy']);
    }
  }

  function GetUserApprovalRights(deal, callback){
    api.GetUserApprovalRights(deal.id, deal.module).then(success)

    function success(userRights){
      callback(userRights)
    }
  }

  function SetDealApprovals(deal, callback){
    api.SetDealApproval(deal.id).then(success)
    function success(dealApprovals){
      callback(dealApprovals)
    }
  }
    function  GetApprovalHistory(deal, callback){
      api.GetApprovalHistory(deal.id).then(success)
      function success(approvalHistory){
        callback(approvalHistory)
      }
    }





  function convertDate(dt){
    var date = new Date(dt);
    return date.getFullYear() + '-' + ('0' + (date.getMonth()+1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
  }



  function convertDataToJson(xml, name){
    var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
    var actualJson = XmlJson.xmlToJson(xmlDOM);
    return actualJson.output[name];
  }

  return service;
}
