(function ()
{
    'use strict';

    angular
        .module('fuse')
        .controller('IndexController', IndexController);

    /** @ngInject */
    function IndexController(api, XmlJson, fuseTheming, $q, $rootScope, $window, $scope, $localStorage, $sessionStorage, $state)
    {
        var vm = this;
        vm.themes = fuseTheming.themes;

        /*
        * Global API constant/values
        * */

   if($sessionStorage.login)
   {
     api.GetEntity().then(function(data){
       $rootScope.entity = data.output;
     });
   }

        api.GetEntityWebDefaults().then(function(data){
          $rootScope.entityWebDefaults = data.output.web_default;
        });
        api.GetDefaultDate().then(function(data){
          $rootScope.defaultDate = data;
        });

        /*$scope.$on('onBeforeUnload', function (e, confirmation) {
          confirmation.message = "All data will be lost.";
        });

      $scope.$on('onUnload', function (e) {
        console.log('leaving page'); // Use 'Preserve Log' option in Console
      });*/
    }
})();
