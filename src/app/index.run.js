(function ()
{
  'use strict';

  angular
    .module('fuse')
    .run(runBlock);

  /** @ngInject */
  function runBlock(AuthenticateFactory, $rootScope, $timeout, $state, $localStorage, msNavigationService, $sessionStorage,$location, $mdToast, $interval, soap_api, ngTableDefaults, cryptoService, beforeUnload)
  {

    localStorage.removeItem('ngStorage-login');
    // window.onbeforeunload = function() {
    //   // localStorage.removeItem('ngStorage-rememberMe');
    //
    //   localStorage.removeItem('ngStorage-login');
    //   $state.go('app.pages_auth_login')
    // };
    // window.onbeforeunload = function(e)
    // {
    //
    //   // localStorage.myPageDataArr=undefined;
    //   alert("Okay")
    //   e = e || window.event;
    //   e.preventDefault = true;
    //   e.cancelBubble = true;
    //   console.log(localStorage.getItem('ngStorage-rememberMe'))
    //   e.returnValue = 'test';
    // }

    // beforeUnload.clearUserBrowserClose();
    // window.unload(function(){
    //   // localStorage.removeItem(key);
    //   console.log("clear")
    // });
    // window.onbeforeunload = function (e)
    // {
    //
    //   e = e || window.event;
    //   var y = e.pageY || e.clientY;
    //   if (y < 0){
    //     return "Do You really Want to Close the window ?"
    //   }
    //   else {
    //     return "Refreshing this page can result in data loss.";
    //   }
    //
    // }
    var vm = {};
    vm.navigationList = msNavigationService.getFlatNavigation();
    vm.navsToDelete = [];
    vm.parseBoolean = function(str){
      return str == 'true' ? true: false;
    };
    vm.parseFloat = function(number){
      return parseFloat(number);
    };
    vm.parseInt = function(number){
      return parseInt(number);
    };
    vm.toLowerCase = function(str){
      str = str || '';
      return str.toLowerCase();
    };
    vm.toUpperCase = function(str){
      str = str || '';
      return str.toUpperCase();
    };
    vm.isXML = function(xml){
      try{
        var xmlDoc = jQuery.parseXML(xml);
        return true;
      }catch(error){
        return false;
      }
    };
    vm.includes = function(string, substring){
      string = string || '';
      substring = substring || ''
      if( string.toLowerCase().indexOf(substring.toLowerCase()) !== -1){
        return true;
      }else{
        return false;
      }
    };
    vm.replaceWith = function(string, separator){
      string = string || '';
      return string.replace(/ +/g, separator);
    };
    vm.floor = function(expression){
      return Math.floor(expression);
    };
    vm.protectAmount = function(event){
      var input = event.key;
      var re = /\d/i;
      if(input != 'Backspace' && input != '.' && input != 'Delete' && input != 'Del' && input != 'Decimal' && input != 'Left' && input != 'Right' && input != 'Tab' && input != 'ArrowLeft' && input != 'ArrowRight'){
        if(input.match(re) == null){
          event.preventDefault();
          return false;
        }
      }else{
        if(input == '.' || input == 'Decimal'){
          re = /\./g;
          if(event.target.value.length > 0){
            if(event.target.value.match(/\./g) != null){
              if(event.target.value.match(/\./g).length > 0){
                event.preventDefault();
                return false;
              }
            }
          }
        }
      }
    };
    vm.onlyWords = function(event){
      var input = event.key;
      var re = /^[a-zA-Z\s]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.WordsAndNumbers = function(event){
      var input = event.key;
      var re = /^[a-zA-Z0-9\s]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.OnlyContacts = function(event){
      var input = event.key;
      var re = /^[0-9\s\-]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.noSpecialCharactersFunction = function(event){
      var input = event.key;
      var re = /^[a-zA-Z0-9\-\s]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.validateEmail = function(event){
      var input = event.key;
      var re = /^[a-zA-Z0-9@.]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.parseDate = function parseDate(input) {
      console.log(typeof input);
      var parts = input.split('-');
      // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
      return new Date(parts[0], parts[1]-1, parts[2]); // Note: months are 0-based
    };
    vm.pasteLimit = function(event, limit){
      var length = event.originalEvent.clipboardData.getData('text/plain').length + event.originalEvent.target.value.length;
      if(length > limit){
        event.preventDefault();
      }
    };
    vm.limitLength = function(event, limit){
      if(event.target.value.length == limit){
        event.preventDefault();
      }
    };
    vm.difference = function arr_diff (a1, a2) {

      var a = [], diff = [];

      for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
      }

      for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
          delete a[a2[i]];
        } else {
          a[a2[i]] = true;
        }
      }

      for (var k in a) {
        diff.push(k);
      }

      return diff;
    };
    //restrict modules list
    if(typeof $localStorage.login !== 'undefined'){
      vm.GetUserRights = function(){
        return soap_api.GetUserRights().then(function(success){
          if(vm.isXML(success.data)){
            var row = [];
            var response = jQuery.parseXML(success.data);
            jQuery(response).find('OUTPUT USER_PERMISSION').each(function(i,e){
              var temp = {
                module_id: jQuery(e).find('MODULE_ID').text(),
                module_name: jQuery(e).find('MODULE_NAME').text(),
                module_short_name: jQuery(e).find('MODULE_SHORT_NAME').text(),
                module_parentid: jQuery(e).find('MODULE_PARENTID').text(),
                module_table_name: jQuery(e).find('MODULE_TABLE_NAME').text(),
                module_tag: jQuery(e).find('MODULE_TAG').text(),
                module_id_column: jQuery(e).find('MODULE_ID_COLUMN').text(),
                module_trans_date_column: jQuery(e).find('MODULE_TRANS_DATE_COLUMN').text(),
                module_display_column: jQuery(e).find('MODULE_DISPLAY_COLUMN').text(),
                module_control_name: jQuery(e).find('MODULE_CONTROL_NAME').text(),
                FIScAL_SENSETIVE: jQuery(e).find('FISCAL_SENSETIVE').text(),
                is_dashboard_item: jQuery(e).find('IS_DASHBOARD_ITEM').text(),
                is_menu_element: jQuery(e).find('IS_MENU_ELEMENT').text(),
                module_icon_filename: jQuery(e).find('MODULE_ICON_FILENAME').text(),
                module_icon_position: jQuery(e).find('MODULE_ICON_POSITION').text(),
                module_largeicon_filename: jQuery(e).find('MODULE_LARGEICON_FILENAME').text(),
                sort_order: jQuery(e).find('SORT_ORDER').text(),
                module_type: jQuery(e).find('MODULE_TYPE').text(),
                created_on: jQuery(e).find('CREATED_ON').text(),
                created_by: jQuery(e).find('CREATED_BY').text(),
                updated_on: jQuery(e).find('UPDATED_ON').text(),
                updated_by: jQuery(e).find('UPDATED_BY').text(),
                is_deleted: jQuery(e).find('IS_DELETED').text(),
                version_no: jQuery(e).find('VERSION_NO').text()
              };
              row.push(temp);
            });
            return row;
          }
        });
      };
      vm.GetUserRights().then(function(data){
        var rights = [];
        for(var i=0; i<data.length; i++){
          rights.push(vm.parseInt(data[i].module_id));
        }
        var navs = [];
        for(var i=0; i<vm.navigationList.length; i++){
          if(typeof vm.navigationList[i].module_id !== 'undefined')
            navs.push(vm.navigationList[i].module_id);
        }
        vm.navsToDelete = vm.difference(navs, rights);
        for(var i=0; i<vm.navsToDelete.length; i++){
          for(var j=0; j<vm.navigationList.length; j++){
            if(vm.navsToDelete[i] == vm.navigationList[j].module_id){
              msNavigationService.deleteItem(vm.navigationList[j]._path);
            }
          }
        }
        // vm.navigationList = msNavigationService.getNavigation();
        //console.log(vm.navigationList);
        //for(var i=0; i<vm.navigationList.length; i++){
        //console.log(vm.navigationList[i]);
        /*if(vm.navigationList[i].child_count > 0 && vm.navigationList[i].children.length == 0){
         msNavigationService.deleteItem(vm.navigationList[i]._path);
         }*/
        //}
      });
    }
    // Activate loading indicator
    var check = true;
    var pathName = document.location.pathname;
    var search = document.location.search;
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

      if (toState.name !== 'app.pages_auth_login' && !AuthenticateFactory.getUserToken() && check) {
        event.preventDefault();
        if(toState.name === 'app.pages_auth_reset-password')
        {
          check = false;
          return $state.go(toState.name, {token : search});
        }
        check = false
        return $state.go(toState.name);
      }

      else if( (toState.name === 'app.pages_auth_login' || toState.name === 'app.pages_auth_reset-password' || toState.name === 'app.pages_auth_forgot_password') && AuthenticateFactory.getUserToken()) {

         event.preventDefault();
        $state.go('app.dashboard');
      }
    })


    // var stateChangeStartEvent = $rootScope.$on('$stateChangeStart', function (event,toState)
    // {
    //   ngTableDefaults.params.count = 10;
    //   ngTableDefaults.settings.counts = [];
    //
    //
    //   // if(typeof $localStorage.rememberMe != 'undefined'){
    //   //
    //   //   var rememberMe = angular.fromJson($localStorage.rememberMe);
    //   //   if(rememberMe.remember == false) {
    //   //     var storage = $sessionStorage;
    //   //   }else{
    //   //     var storage = $localStorage;
    //   //   }
    //   //
    //   //   /*if(typeof storage.previousLogin !== 'undefined'){
    //   //    var decrypt = cryptoService.crypto.AES.decrypt(storage.previousLogin, storage.passphrase);
    //   //    var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));
    //   //    }*/
    //   //
    //   //   if(typeof storage.login == 'undefined' && toState.name != 'app.pages_auth_login' && toState.name != 'app.pages_auth_forgot_password'){
    //   //     $location.url('/auth/login');
    //   //     $mdToast.show({
    //   //       template: '<md-toast class="md-toast info">Please login first.</md-toast>',
    //   //       hideDelay: 4000,
    //   //       position: 'bottom right'
    //   //     });
    //   //   }
    //   //
    //   //   /*if(typeof storage.login == 'string' && toState.name == 'app.pages_auth_login'){
    //   //    //$rootScope.loginForm = login;
    //   //    $location.url('/dashboard');
    //   //    $mdToast.show({
    //   //    template: '<md-toast class="md-toast info">Welcome back</md-toast>',
    //   //    hideDelay: 5000,
    //   //    position: 'bottom right'
    //   //    });
    //   //    }*/
    //   //   /*
    //   //    if(typeof storage.login == 'string'){
    //   //    //check if user can be logged in in this time
    //   //    $rootScope.timeValidateUser = $interval(function(){
    //   //    if(typeof storage.login == 'string'){
    //   //    soap_api.ValidateUserOnlineTradingHours(new Date()).then(function(success){
    //   //    var response = jQuery.parseXML(success.data);
    //   //    var temp = {};
    //   //    jQuery(response).find('OUTPUT RESULT').each(function(i,e){
    //   //    temp = {
    //   //    status: jQuery(e).find('STATUS').text(),
    //   //    error: jQuery(e).find('ERROR').text()
    //   //    };
    //   //    });
    //   //    if(temp.status === 'FALSE'){
    //   //    delete storage.login;
    //   //    $location.url('/auth/login');
    //   //    $mdToast.show({
    //   //    template: '<md-toast class="md-toast error">You are not allowed to login at this time.</md-toast>',
    //   //    hideDelay: 4000,
    //   //    position: 'bottom right'
    //   //    });
    //   //    }
    //   //    }, function(error){
    //   //    console.error(error.statusText);
    //   //    });
    //   //    }
    //   //    }, 1000);
    //   //    }
    //   //    if(typeof storage.login == 'undefined'){
    //   //    $interval.cancel($rootScope.timeValidateUser);
    //   //    }
    //   //    */
    //   //
    //   //   //console.log($sessionStorage);
    //   //   //console.log($localStorage);
    //   // }
    //     if( (!$localStorage.rememberMe && check) && (toState.name === 'app.pages_auth_login' || toState.name === 'app.pages_auth_reset-password') )
    //     {
    //       var state, params;
    //       event.preventDefault();
    //       if(pathName === '/auth/forgot-password' || pathName === '/auth/reset-password' && !$localStorage.rememberMe )
    //       {
    //
    //         state = "app.pages" + pathName .split("/").join("_");
    //         if(pathName === '/auth/reset-password')
    //         {
    //           params = search;
    //           check = false;
    //           return $state.go(state, {token : search});
    //
    //         }
    //
    //       }
    //
    //
    //       // else
    //       // {
    //       //   state = 'app.pages_auth_login';
    //       //   check = false;
    //       // }
    //       // updateState(state);
    //
    //      else
    //       {
    //         check = false;
    //         return $state.go(toState.name);
    //
    //       }
    //
    //
    //
    //     }
    //     else if( $localStorage.rememberMe && check)
    //     {
    //       if(toState.name === 'app.pages_auth_login' || toState.name === 'app.pages_auth_forgot-password' || toState.name === 'app.pages_auth_reset-password')
    //       {
    //         state = 'app.dashboard';
    //       }
    //       else
    //       {
    //         state = toState.name;
    //       }
    //
    //       check = false;
    //       $state.go(state);
    //       // updateState(state);
    //
    //     }
    //
    //   $rootScope.loadingProgress = true;
    // });
    function updateState(state)
    {
      $state.go(state);
    }

    // De-activate loading indicator
    var stateChangeSuccessEvent = $rootScope.$on('$stateChangeSuccess', function ()
    {
      $timeout(function ()
      {
        $rootScope.loadingProgress = false;
      });
    });

    // Store state in the root scope for easy access
    $rootScope.state = $state;
    $rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
      debugger;
      switch (error) {

        case AuthenticateFactory.UNAUTHORIZED:
          $state.go("app.pages_auth_login");
          break;

        default:
          console.log("$stateChangeError event catched");
          // $state.go("app.pages_auth_login");
          break;

      }
    })

    // Cleanup
    $rootScope.$on('$destroy', function ()
    {
      stateChangeStartEvent();
      stateChangeSuccessEvent();
    });
  }
})();
