(function(){
  angular
    .module('fuse')
    .factory('QueryFactory' , QueryFactory);
})();
QueryFactory.$inject = ['$http' , '$q' , '$localStorage', '$sessionStorage', 'cryptoService' ];
function QueryFactory($http, $q, $localStorage, $sessionStorage, cryptoService){


  var decryptt;
  var loginn;


  // if(typeof $localStorage.rememberMe === 'string'){
  //   var rememberMe = angular.fromJson($localStorage.rememberMe);
  //   if(rememberMe.remember) {
  //     this.storage = $sessionStorage;
  //   }else{
  //     this.storage = $localStorage;
  //   }
  // };


  this.storage = $sessionStorage;
  function dcryptUserInfo()
  {
    if($sessionStorage.login)
    {
      var decrypt = cryptoService.crypto.AES.decrypt($sessionStorage.login, $sessionStorage.passphrase);
      var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));
      return login;
    }

  }



  var rememberMe = angular.fromJson($localStorage.rememberMe);

  function query(method, input, userData, forgotPassword){
    var login;
    // this.storage = $sessionStorage;
    if($sessionStorage && $sessionStorage.login){
      login = dcryptUserInfo();
    }
    var deferred = $q.defer();
    if(!forgotPassword && !userData)
    {

      if($sessionStorage.login)
      {
        login = dcryptUserInfo();
      }
      else
      {
          deferred.reject("User is not login");
          return deferred.promise;
      }

    }

    // else if(!login && !forgotPassword && !userData )
    // {
    //   deferred.reject("User is not login");
    //   return deferred.promise;
    // }
    // else
    //   if(!forgotPassword && !userData)
    //   {
    //     var decrypt = cryptoService.crypto.AES.decrypt($sessionStorage.login, $sessionStorage.passphrase);
    //     login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));
    //   }

    var headers;
      if(userData !== "" && userData)
      {
        headers = setHeaders(userData);
      }
      else if(login)
      {
        headers = setHeaders(login);
      }

      // {
      //   username: login && login.username || userData.name ||  "",
      //   password: login && login.password || "",
      //   OrgID:    login && login.OrgID ||   "",
      //   CustID:   login && login.CustID || userData.custId || "",
      //   email :   login  &&  "" ||  userData && userData.email
      // };
    var body = {
      method : method,
      input  : input || ""
    };

    var data = {
      'data': {
        'headers': headers || "",
        'body': body
      }
    };

    var opt = {
      method : 'POST',
      url: 'http://68.68.5.189:7070/fxoffice.asmx/CallService',
      data: data || {},
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    };
    $http(opt).then(function(d) {
      if (!d.config) {
        console.log('Server error occured.');
      }
      deferred.resolve(d);
    },function(error) {
      deferred.reject(error);
    });
    return deferred.promise;
  }
  var loggin = sessionStorage.getItem("ngStorage-login");
  if(loggin)
  {
    decryptt = cryptoService.crypto.AES.decrypt($sessionStorage.login, $sessionStorage.passphrase);
    loginn = angular.fromJson(decryptt.toString(cryptoService.crypto.enc.Utf8));

  }
  var service ={
    query : query,
    login : loginn || dcryptUserInfo() || {}

  }


  function setHeaders(userData)
  {
    return {
      username: userData.username,
      password: userData.password || "",
      OrgID:    userData.OrgID ||   "",
      CustID:   userData.CustID || "",
      email :   userData.email || "",
    }
  }
  return service;
}
