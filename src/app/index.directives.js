(function ()
{
  'use strict';

  var fuse = angular.module('fuse');
  fuse.directive('modernTable', function(){
    return {
      scope: true,
      restrict: 'AE',
      replace: true,
      templateUrl: 'app/main/templates/modern-datatable.html',
      link: function(scope, elem, attrs){
        console.log(scope);
      }
    };
  });
})();
