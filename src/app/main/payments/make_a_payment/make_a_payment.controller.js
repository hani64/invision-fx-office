(function () {
  'use strict';

  angular
    .module('app.payments.make_a_payment')
    .controller('MakeAPaymentController', MakeAPaymentController);

  /** @ngInject */
  function MakeAPaymentController(api, $state, $filter, $rootScope, $mdDialog, $scope, $interval, $mdToast, soap_api, $window, $q, $location, QueryFactory, messageService) {
    var vm = this;
    //hide div when clicked outside
    convertCurrency();
    vm.loader = true;
    vm.module_id = 12280;
    $rootScope.headerTitle = 'Make A Payment';
    vm.step1Invalid = false;
    vm.entityWebDefaults = [];
    vm.disableFunding = false;
    vm.convertMethods = convertObjects;
    vm.convertName = convertObjects;

    function convertObjects(objectOrStringValue){
      // debugger
      if(typeof objectOrStringValue === 'object')
      {
        return objectOrStringValue['#text'].join(" ");
      }
      return objectOrStringValue
    }
    vm.GetDefaultDate = function(){
      return soap_api.GetDefaultDate().then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.GetDefaultDate().then(function(data){
      if(data[0].status == 'TRUE'){
        vm.getDefaultDate = data[0];
        vm.defaultValueDate = new Date(Date.parse(data[0].recordid));
        vm.defaultValueDate = ('0' + (vm.defaultValueDate.getMonth()+1)).slice(-2) + '/' + ('0' + vm.defaultValueDate.getDate()).slice(-2) + '/' + vm.defaultValueDate.getFullYear();
      }
    });
    vm.GetUserApprovalRights = function(deal, module){
      return soap_api.GetUserApprovalRights(deal, module).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.GetEntity = function(){
      return soap_api.GetEntity().then(function(success){
        var row = [];
        var temp1 = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT ENTITY').each(function(i,e){
          var temp = {
            name: jQuery(e).find('NAME').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            entity_no: jQuery(e).find('ENTITY_NO').text(),
            entity_type_id: jQuery(e).find('ENTITY_TYPE_ID').text(),
            type_name: jQuery(e).find('TYPE_NAME').text(),
            status: jQuery(e).find('STATUS').text(),
            value: jQuery(e).find('VALUE').text(),
            entity_class_id: jQuery(e).find('ENTITY_CLASS_ID').text(),
            name1: jQuery(e).find('NAME1').text(),
            entity_org_id: jQuery(e).find('ENTITY_ORG_ID').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            relationship_manager_id: jQuery(e).find('RELATIONSHIP_MANAGER_ID').text(),
            relationship_manager_name: jQuery(e).find('RELATIONSHIP_MANAGER_NAME').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            version_no: jQuery(e).find('VERSION_NO').text(),
            is_deleted1: jQuery(e).find('IS_DELETED1').text()
          };
          temp1['entity'] = temp;
        });
        jQuery(response).find('OUTPUT ADDRESS').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            parent_type: jQuery(e).find('PARENT_TYPE').text(),
            address_type: jQuery(e).find('ADDRESS_TYPE').text(),
            address_type_value: jQuery(e).find('ADDRESS_TYPE_VALUE').text(),
            priority: jQuery(e).find('PRIORITY').text(),
            street_address: jQuery(e).find('STREET_ADDRESS').text(),
            post_code: jQuery(e).find('POST_CODE').text(),
            city: jQuery(e).find('CITY').text(),
            province: jQuery(e).find('PROVINCE').text(),
            countryid: jQuery(e).find('COUNTRYID').text(),
            country_value: jQuery(e).find('COUNTRY_VALUE').text(),
            is_default: jQuery(e).find('IS_DEFAULT').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            is_approved: jQuery(e).find('IS_APPROVED').text(),
            is_web_approved: jQuery(e).find('IS_WEB_APPROVED').text(),
            version_no: jQuery(e).find('VERSION_NO').text(),
            country_code: jQuery(e).find('COUNTRY_CODE').text()
          };
          temp1['address'] = temp;
        });
        jQuery(response).find('OUTPUT PHONE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            parent_type: jQuery(e).find('PARENT_TYPE').text(),
            phone_type: jQuery(e).find('PHONE_TYPE').text(),
            phone_type_value: jQuery(e).find('PHONE_TYPE_VALUE').text(),
            phone_number: jQuery(e).find('PHONE_NUMBER').text(),
            priority: jQuery(e).find('PRIORITY').text(),
            is_default: jQuery(e).find('IS_DEFAULT').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            is_approved: jQuery(e).find('IS_APPROVED').text(),
            version_no: jQuery(e).find('VERSION_NO').text(),
            is_web_approved: jQuery(e).find('IS_WEB_APPROVED').text()
          };
          temp1['phone'] = temp;
        });
        var temp2 = [];
        jQuery(response).find('OUTPUT WEB').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            PARENT_TYPE_VALUE: jQuery(e).find('PARENT_TYPE_VALUE').text(),
            PARENT_TYPE: jQuery(e).find('PARENT_TYPE').text(),
            WEB_TYPE: jQuery(e).find('WEB_TYPE').text(),
            WEB_TYPE_VALUE: jQuery(e).find('WEB_TYPE_VALUE').text(),
            WEB_ADDRESS: jQuery(e).find('WEB_ADDRESS').text(),
            PRIORITY: jQuery(e).find('PRIORITY').text(),
            IS_DEFAULT: jQuery(e).find('IS_DEFAULT').text(),
            CREATED_ON: jQuery(e).find('CREATED_ON').text(),
            CREATED_BY: jQuery(e).find('CREATED_BY').text(),
            UPDATED_ON: jQuery(e).find('UPDATED_ON').text(),
            UPDATED_BY: jQuery(e).find('UPDATED_BY').text(),
            IS_DELETED: jQuery(e).find('IS_DELETED').text(),
            IS_APPROVED: jQuery(e).find('IS_APPROVED').text(),
            VERSION_NO: jQuery(e).find('VERSION_NO').text(),
            IS_WEB_APPROVED: jQuery(e).find('IS_WEB_APPROVED').text()
          };
          temp2.push(temp);
        });
        temp1['web'] = temp2;
        row.push(temp1);
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.GetEntity().then(function (data) {
      vm.entity = data;
    });
    vm.GetEntityWebDefaults = function(){
      return soap_api.GetEntityWebDefaults().then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT WEB_DEFAULT').each(function(i,e){
          var temp = {
            ID: jQuery(e).find('ID').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            allow_forward_deal: jQuery(e).find('ALLOW_FORWARD_DEAL').text(),
            trader_id: jQuery(e).find('TRADER_ID').text(),
            ordertype_id: jQuery(e).find('ORDERTYPE_ID').text(),
            funding_method_id: jQuery(e).find('FUNDING_METHOD_ID').text(),
            is_single_ccy_fee: jQuery(e).find('IS_SINGLE_CCY_FEE').text(),
            fee_cur_id: jQuery(e).find('FEE_CUR_ID').text(),
            fundingpaymentmethod: jQuery(e).find('FundingPaymentMethod').text(),
            payment_method_id: jQuery(e).find('PAYMENT_METHOD_ID').text(),
            paymentmethod: jQuery(e).find('PaymentMethod').text(),
            funding_template_id: jQuery(e).find('FUNDING_TEMPLATE_ID').text(),
            fundingtemplatename: jQuery(e).find('FundingTemplateName').text(),
            payment_template_id: jQuery(e).find('PAYMENT_TEMPLATE_ID').text(),
            paymenttemplatename: jQuery(e).find('PaymentTemplateName').text(),
            funding_cur_id: jQuery(e).find('FUNDING_CUR_ID').text(),
            fundingcurrency: jQuery(e).find('FundingCurrency').text(),
            payment_cur_id: jQuery(e).find('PAYMENT_CUR_ID').text(),
            paymentcurrency: jQuery(e).find('PaymentCurrency').text(),
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.multiplePayees = [];
    vm.accounts = [];
    vm.showMultiplePayment = function(event){
      event.preventDefault();
      if(vm.step1.currency !== null && vm.step1.method !== null){
        vm.GetWebPaymentMethods(12280).then(function(data){
          vm.methods = data;
          var m = [];
          for(var i=0; i<data.length; i++){
            m.push(data[i].id);
          }
          vm.payments_methods_joined = m.join('|');
        });
        $mdDialog.show({
          fullscreen: vm.customFullscreen,
          clickOutsideToClose: true,
          scope: $scope,
          preserveScope: true,
          templateUrl: 'app/main/payments/make_a_payment/multiple_payment.html',
          parent: angular.element(document.body),
          controller: function dialogController($scope, $mdDialog){
            $scope.closeDialog = function() {
              $mdDialog.hide();
              vm.searchKeys = {
                payee: null,
                method: null,
                currency: null,
                account: null
              };
            };
            $scope.clearFilters = function(){
              vm.searchKeys = {
                payee: null,
                method: null,
                currency: null,
                account: null
              };
            };
            $scope.single = {
              currency: null,
              payee_: null,
              amount: null,
              payment_reference: null,
              internal_payment_reference: null,
              payee_notification: null,
              purpose_of_payment: null,
              method: null,
              account: null
            };
            $scope.multipleLoader = false;
            vm.GetPaymentsByCurreniesAndMethodsInMakeAPaymentMultiplePayee(vm.payments_currencies_joined, vm.payments_methods_joined).then(function(data){
              $scope.payees = data;
              vm.multiplePayees = data;
              $scope.multipleLoader = true;
            });
            $scope.feePromises = [];
            $scope.counter = 0;
            $scope.expand = false;
            $scope.showMore = function(data, event){
              var tr = angular.element(event.target).closest('tr');
              var td = tr.find('button.show-more-btn');
              var next = tr.next();
              if(next.hasClass('hide')){
                td.empty();
                td.html('<i class="material-icons">remove</i>');
                next.removeClass('hide').addClass('show');
              }else{
                td.empty();
                td.html('<i class="material-icons">add</i>');
                next.removeClass('show').addClass('hide');
              }
            };
            $scope.checkMyRow = function(data){
              var tr = angular.element('tr#payee-'+data.id);
              var next = tr.next();
              if(data.amount && data.amount !== null){
                tr.addClass('completed');
                next.addClass('completed');
              }
              if(data.amount && (vm.parseFloat(data.amount) === 0 || data.amount < 0)){
                tr.removeClass('completed');
                next.removeClass('completed');
              }
            };
            $scope.payeeFilter = function(payee){
              if(vm.searchKeys){
                if(
                  (payee.name.toLowerCase().indexOf(vm.searchKeys.payee || '') !== -1)
                ){
                  return true;
                }else{
                  return false;
                }
              }else{
                return true;
              }
            };
            $scope.methodFilter = function(payee){
              if(vm.searchKeys){
                if(
                  (payee.type.toLowerCase().indexOf(vm.searchKeys.method || '') !== -1)
                ){
                  return true;
                }else{
                  return false;
                }
              }else{
                return true;
              }
            };
            $scope.currencyFilter = function(payee){
              if(vm.searchKeys){
                if(
                  (payee.currency_name.toLowerCase().indexOf(vm.searchKeys.currency || '') !== -1)
                ){
                  return true;
                }else{
                  return false;
                }
              }else{
                return true;
              }
            };
            $scope.accountFilter = function(payee){
              if(vm.searchKeys){
                if(
                  (payee.beneficiary_accountcode.toLowerCase().indexOf(vm.searchKeys.account || '') !== -1) ||
                  (payee.beneficiarybank_name.toLowerCase().indexOf(vm.searchKeys.account || '') !== -1)
                ){
                  return true;
                }else{
                  return false;
                }
              }else{
                return true;
              }
            };
            $scope.single = {
              currency: null,
              payee_: null,
              amount: null,
              payment_reference: null,
              internal_payment_reference: null,
              payee_notification: null,
              payment_purpose: null,
              account: null,
              method: null,
              fee: null,
              value_date: null
            };
            $scope.record = {};
            $scope.tempRows = [];
            $scope.tempFee = {};
            $scope.addMultiplePayee = function(data){
              $scope.multipleLoader = false;
              var records = [];
              angular.copy(data, records);
              if(records.length > 0){
                $scope.feePromises = [];
                $scope.tempRows = [];
                var holiday_rows = [];
                var rows_count = 0;
                for(var i=0; i<records.length; i++){
                  if(records[i].amount != null && typeof records[i].amount == 'string' && vm.parseFloat(records[i].amount) > 0){
                    if(records[i].value_date != null || records[i].value_date != ''){
                        rows_count++;
                        holiday_rows.push(vm.ValidateBusinessDate(records[i].value_date));
                    }else{
                      $mdToast.show({
                          template: '<md-toast class="md-toast error">Value date is mandatory. Some rows missing this value.</md-toast>',
                          hideDelay: 4000,
                          position: 'bottom right'
                      });
                      $scope.multipleLoader = true;
                      return true;
                    }
                  }
                }
                $q.all(holiday_rows).then(function(data){
                    var temp_rows_count = 0;
                    for(var i=0; i<data.length; i++){
                        if(data[i][0].status == 'TRUE'){
                            temp_rows_count++;
                        }else{
                            $mdToast.show({
                                template: '<md-toast class="md-toast error">'+data[i][0].error+'</md-toast>',
                                hideDelay: 4000,
                                position: 'bottom right'
                            });
                            $scope.multipleLoader = true;
                            return true;
                        }
                    }
                    if(temp_rows_count == rows_count){
                        for(var i=0; i<records.length; i++){
                            if(records[i].amount != null && typeof records[i].amount == 'string' && vm.parseFloat(records[i].amount) > 0){
                                if(vm.popMandate == true && records[i].payment_purpose == null){
                                    $mdToast.show({
                                        template: '<md-toast class="md-toast error">Purpose Of Payment Is Mandatory. Some Rows Missing Purpose Of Payment Value.</md-toast>',
                                        hideDelay: 4000,
                                        position: 'bottom right'
                                    });
                                    $scope.multipleLoader = true;
                                    return;
                                }
                                $scope.multipleLoader = false;
                                $scope.feePromises.push(vm.GetFeeByMethodByID(records[i].paymentmethod));
                                $scope.tempRows.push(records[i]);
                            }
                        }
                        $q.all($scope.feePromises).then(function(data){
                            for(var i=0; i<data.length; i++){
                                var record = {};
                                var identifier = vm.payeeEntries.length + 1;
                                record.current_id = identifier;
                                record.currency = vm.toLowerCase($scope.tempRows[i].currency_id);
                                record.payee_ = $scope.tempRows[i].name;
                                record.payee_old_id = $scope.tempRows[i].id;
                                record.payee_id = $scope.tempRows[i].id;
                                record.amount = vm.parseFloat($scope.tempRows[i].amount);
                                record.payment_reference = $scope.record.payment_reference == null ? null : $scope.tempRows[i].payment_reference;
                                record.payment_purpose = $scope.tempRows[i].payment_purpose == null ? null : $scope.tempRows[i].payment_purpose;
                                record.internal_payment_reference = $scope.tempRows[i].internal_payment_reference == null ? null : $scope.tempRows[i].internal_payment_reference;
                                record.payee_notification = $scope.tempRows[i].payee_notification == null ? null : $scope.tempRows[i].payee_notification;
                                record.method = vm.toLowerCase($scope.tempRows[i].type);
                                record.value_date = $scope.tempRows[i].value_date == null ? new Date() : $scope.tempRows[i].value_date;
                              /*if(typeof data[i].id == 'string'){
                               if(record.currency == vm.step1.currency){
                               record.fee = data[i].non_instrument_cost == '' ? 0 : vm.parseFloat(data[i].non_instrument_cost);
                               }else{
                               record.fee = vm.parseFloat(data[i].instrument_cost);
                               }
                               }*/
                                if(record.currency == vm.step1.currency){
                                    //if we have same currencies
                                    if(data[i].same_currency_fee.length > 0){
                                        if(data[i].same_currency_fee[0].currency_id == record.currency){
                                            record.fee = vm.parseFloating(data[i].same_currency_fee[0].instrument_cost);
                                            //console.log(record.fee);
                                        }else{
                                            record.fee = vm.parseFloating(data[i].fee[0].non_instrument_cost);
                                            //console.log(record.fee);
                                        }
                                    }else{
                                        record.fee = vm.parseFloating(data[i].fee[0].non_instrument_cost);
                                        //console.log(record.fee);
                                    }
                                }else{
                                    //else we have different currencies
                                    if(data[i].instrument_fee.length > 0){
                                        var found = false;
                                        var instrument_fee = {};
                                        for(var j=0; j<data[i].instrument_fee.length; j++){
                                            if(data[i].instrument_fee[j].currency_id == 2){
                                                found = true;
                                                instrument_fee = data[i].instrument_fee[j];
                                            }
                                        }
                                        if(found){
                                            record.fee = vm.parseFloating(instrument_fee.instrument_cost);
                                            //console.log(record.fee);
                                        }else{
                                            record.fee = vm.parseFloating(data[i].fee[0].instrument_cost);
                                            //console.log(record.fee);
                                        }
                                    }else{
                                        record.fee = vm.parseFloating(data[i].fee[0].instrument_cost);
                                        //console.log(record.fee);
                                    }
                                }
                                record = vm.mergeObjects(record, $scope.tempRows[i]);
                                vm.payeeEntries.push(record);
                                //console.log(record);
                                //console.log(record);
                                vm.showPaymentHistory = true;
                                vm.showNext = true;
                            }
                        }).finally(function(){
                            $scope.multipleLoader = true;
                            vm.showCircleLoader = true;
                            $scope.closeDialog();
                        });
                    }else{
                        $scope.multipleLoader = true;
                        return true;
                    }
                });
              }
            };
          }
        });
      }else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please fill required fields.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        vm.step1Invalid = true;
      }
    };
    vm.GetFundingCurrencies = function(){
      return soap_api.GetFundingCurrencies().then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        //console.log(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            buy_item_id: jQuery(e).find('BUY_ITEM_ID').text(),
            item_name: jQuery(e).find('ITEM_NAME').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            curr_rate_conversion_type: jQuery(e).find('CURR_RATE_CONVERSION_TYPE').text(),
            currency_price_rounding: jQuery(e).find('CURRENCY_PRICE_ROUNDING').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetFundingCurrenciesFilterByCurrency = function(currency_id){

      return soap_api.GetFundingCurrenciesFilterByCurrency(currency_id).then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            buy_item_id: jQuery(e).find('BUY_ITEM_ID').text(),
            item_name: jQuery(e).find('ITEM_NAME').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            curr_rate_conversion_type: jQuery(e).find('CURR_RATE_CONVERSION_TYPE').text(),
            currency_price_rounding: jQuery(e).find('CURRENCY_PRICE_ROUNDING').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    var accountId;
    vm.showAccount = function(event){
      vm.accounts = [];
      event.preventDefault();
      $mdDialog.show({
        fullscreen: true,
        clickOutsideToClose: true,
        scope: $scope,
        preserveScope: true,
        templateUrl: 'app/main/payments/make_a_payment/account-popup.html',
        parent: angular.element(document.body),
        controller: function dialogController($scope, $mdDialog){
          $scope.dialogLoader = false;
          if(vm.step1.method && vm.step1.currency){
            api.GetPaymentsByCurrencyAndMethodInMakeAPaymentTop(vm.step1.method, vm.step1.currency).then(function(data){
             if(data.output.table1){
               if(angular.isObject(data.output.table1)){
                 if(_.isArray(data.output.table1)){
                   vm.accounts = data.output.table1;
                 }else{
                   vm.accounts.push(data.output.table1);
                 }
               }
               $scope.dialogLoader = true;
             }else{
               vm.accounts = [];
               $scope.dialogLoader = true;
             }
            });
          }
          $scope.selectAccount = function(account){
            accountId = account.id;
            if(account.paymentmethod === '5'){
              var balance = $filter('currency')(account.balance, '',2);
              vm.step1.account = account.name + ' ' + balance;
            }else{
              vm.step1.account = account.name;
            }
            $scope.closeDialog();
          };
          $scope.closeDialog = function() {
            $mdDialog.hide();
          };
          $scope.showFilter = true;
          $scope.toggleFilter = function(){
            $scope.showFilter = !$scope.showFilter;
          };
        }
      });
    };
    vm.deselectAccount = function(){
      accountId = null;
      vm.step1.account = null;
    };
    vm.hideTableFunc = function(event){
      vm.showTable = true;
    };
    vm.GetPaymentCurrencies = function(){

      return soap_api.GetPaymentCurrencies().then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            buy_item_id: jQuery(e).find('BUY_ITEM_ID').text(),
            item_name: jQuery(e).find('ITEM_NAME').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            curr_rate_conversion_type: jQuery(e).find('CURR_RATE_CONVERSION_TYPE').text(),
            currency_price_rounding: jQuery(e).find('CURRENCY_PRICE_ROUNDING').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetPaymentCurrenciesFilterByCurrency = function(currency_id){

      return soap_api.GetPaymentCurrenciesFilterByCurrency(currency_id).then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            buy_item_id: jQuery(e).find('BUY_ITEM_ID').text(),
            item_name: jQuery(e).find('ITEM_NAME').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            curr_rate_conversion_type: jQuery(e).find('CURR_RATE_CONVERSION_TYPE').text(),
            currency_price_rounding: jQuery(e).find('CURRENCY_PRICE_ROUNDING').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetPaymentCurrenciesWithoutFilter = function(){
      return soap_api.GetPaymentCurrenciesWithoutFilter().then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            buy_item_id: jQuery(e).find('BUY_ITEM_ID').text(),
            item_name: jQuery(e).find('ITEM_NAME').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            curr_rate_conversion_type: jQuery(e).find('CURR_RATE_CONVERSION_TYPE').text(),
            currency_price_rounding: jQuery(e).find('CURRENCY_PRICE_ROUNDING').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    api.GetFundingCurrenciesWithoutFilter().then(function(data){
      vm.fundingCurrencies = data.output.table1;
      var m = [];
      for(var i=0; i<data.output.table1.length; i++){
        m.push(data.output.table1[i].currency_id);
      }
      vm.funding_currencies_joined = m.join('|');
    });
    vm.GetEntityWebDefaults().then(function(data){

      vm.entityWebDefaults = data[0];
      vm.showCircleLoader = false;
      vm.GetPaymentCurrenciesWithoutFilter().then(function(data){
        // console.log(data)
        vm.paymentCurrencies = data;
        for(var i=0; i<vm.paymentCurrencies.length; i++){
          if(vm.paymentCurrencies[i].currency_id == vm.entityWebDefaults.payment_cur_id){
            vm.step1.currency = vm.paymentCurrencies[i].currency_id;
          }
        }
        var m = [];
        for(var i=0; i<data.length; i++){
          m.push(data[i].currency_id);
        }
        vm.payments_currencies_joined = m.join('|');
        vm.showCircleLoader = true;
      });
    });
    vm.getPaymentCurrencyName = function(currency_id){
     if(typeof vm.paymentCurrencies !== 'undefined'){
       for(var i=0; i<vm.paymentCurrencies.length; i++){
         if(vm.paymentCurrencies[i].currency_id == currency_id){
           return vm.paymentCurrencies[i].currency_name;
         }
       }
     }
    };
    vm.getFundingCurrencyName = function(currency_id){
     if(typeof vm.fundingCurrencies !== 'undefined'){
       for(var i=0; i<vm.fundingCurrencies.length; i++){
         if(vm.fundingCurrencies[i].currency_id == currency_id){
           return vm.fundingCurrencies[i].currency_name;
         }
       }
     }
    };
    vm.getFundingsMethodName = function(method_id){

      if(typeof vm.fundingMethods !== 'undefined'){
        for(var i=0; i<vm.fundingMethods.length; i++){
          if(vm.fundingMethods[i].id == method_id){
            return vm.fundingMethods[i].name;
          }
        }
      }
    };
    vm.getAccountName = function(account_id){


      if(account_id != null){
        if(typeof vm.accounts !== 'undefined'){
          for(var i=0; i<vm.accounts.length; i++){
            if(vm.accounts[i].id == account_id){
              return vm.accounts[i].name;
            }
          }
        }
      }else{
        return 'N/A';
      }
    };
    vm.GetFundingMethod = function(module){
      return soap_api.GetFundingMethod(module).then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT FUNDING_METHOD').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            allow_funding_picker: jQuery(e).find('ALLOW_FUNDING_PICKER').text(),
            allow_payment_picker: jQuery(e).find('ALLOW_PAYMENT_PICKER').text(),
            allow_payment_edit: jQuery(e).find('ALLOW_PAYMENT_EDIT').text(),
            allow_funding_edit: jQuery(e).find('ALLOW_FUNDING_EDIT').text(),
            free_instruments: jQuery(e).find('FREE_INSTRUMENTS').text(),
            instrument_cost: jQuery(e).find('INSTRUMENT_COST').text(),
            revenue_type_id: jQuery(e).find('REVENUE_TYPE_ID').text(),
            is_funding: jQuery(e).find('IS_FUNDING').text(),
            source_id: jQuery(e).find('SOURCE_ID').text(),
          };
          result.push(temp);
        });
        return result;
      });
    };
    api.GetEntityWebDefaults().then(function(data){
      vm.entityWebDefaults = data.output.web_default;
    });
    api.GetFundingMethod(vm.module_id).then(function(data){
      vm.fundingMethods = data.output.funding_method;
      for(var i=0; i<vm.fundingMethods.length; i++){
        if(vm.entityWebDefaults.payment_method_id && vm.fundingMethods[i].id === vm.entityWebDefaults.payment_method_id){
          vm.step1.method = vm.fundingMethods[i].id;
        }
      }
      var m = [];
      for(i=0; i<vm.fundingMethods.length; i++){
        m.push(vm.fundingMethods[i].id);
      }
      vm.funding_methods_joined = m.join('|');
    });
    vm.isXML = function(xml){
      try{
        var xmlDoc = jQuery.parseXML(xml);
        return true;
      }catch(error){
        return false;
      }
    };
    vm.capitalizeFirstLetter = function(str) {
      str = str.toLowerCase().split(' ');                // will split the string delimited by space into an array of words

      for(var i = 0; i < str.length; i++){               // str.length holds the number of occurrences of the array...
        str[i] = str[i].split('');                    // splits the array occurrence into an array of letters
        str[i][0] = str[i][0].toUpperCase();          // converts the first occurrence of the array to uppercase
        str[i] = str[i].join('');                     // converts the array of letters back into a word.
      }
      return str.join(' ');                              //  converts the array of words back to a sentence.
    };
    vm.uniqueCountries = function(array){
      var flags = [], output = [], l = array.length, i;
      for( i=0; i<l; i++) {
        if( flags[array[i].country_name]) continue;
        flags[array[i].country_name] = true;
        output.push(array[i]);
      }
      return output;
    };
    vm.uniqueCurrencies = function(array){
      var flags = [], output = [], l = array.length, i;
      for( i=0; i<l; i++) {
        if( flags[array[i].currency_name]) continue;
        flags[array[i].currency_name] = true;
        output.push(array[i]);
      }
      return output;
    };
    vm.GetAllCurrencies = function(){
      QueryFactory.query('GetAllCurrencies','').then(success, err)
      function success(data){
        //console.log(data)
      }
      function err(er){
        console.log(er)
      }
      return soap_api.GetAllCurrencies().then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT CURRENCY').each(function(i,e){
          var temp = {
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text(),
          };
          row.push(temp);
        });
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.GetAllElectronicMethods = function(){
      return soap_api.GetAllElectronicMethods().then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            is_template: jQuery(e).find('IS_TEMPLATE').text(),
            is_routing_type: jQuery(e).find('IS_ROUTING_TYPE').text(),
            is_bank_code_enabled: jQuery(e).find('IS_BANK_CODE_ENABLED').text(),
            is_electronic: jQuery(e).find('IS_ELECTRONIC').text(),
            is_bank_code_required: jQuery(e).find('IS_BANK_CODE_REQUIRED').text()
          };
          row.push(temp);
        });
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.GetPayeesByCurrencyAndPaymentMethod = function(currency, method){
      var row = [];
      soap_api.GetPayeesByCurrencyAndPaymentMethod(currency, method).then(function(success){
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT Table1').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              name: jQuery(e).find('NAME').text(),
              is_template: jQuery(e).find('IS_TEMPLATE').text(),
              is_routing_type: jQuery(e).find('IS_ROUTING_TYPE').text(),
              is_bank_code_enabled: jQuery(e).find('IS_BANK_CODE_ENABLED').text(),
              is_electronic: jQuery(e).find('IS_ELECTRONIC').text(),
              is_bank_code_required: jQuery(e).find('IS_BANK_CODE_REQUIRED').text()
            };
            row.push(temp);
          });
          jQuery(response).find('OUTPUT RESULT').each(function(i,e){
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              recordid: jQuery(e).find('RECORDID').text(),
              error: jQuery(e).find('ERROR').text()
            };
            row.push(temp);
          });
        }else{
          console.log(success.data);
        }
      }, function(error){
        console.error(error.statusText);
      });
      return row;
    };
    vm.GetAllPayeeTemplates = function(){
      vm.showCircleLoader = false;
      return soap_api.GetAllPayeeTemplates().then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT Table1').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              name: jQuery(e).find('NAME').text(),
              parent_id: jQuery(e).find('PARENT_ID').text(),
              entity: jQuery(e).find('ENTITY').text(),
              type: jQuery(e).find('TYPE').text(),
              is_non_third_party: jQuery(e).find('IS_NON_THIRD_PARTY').text(),
              payment_type_name: jQuery(e).find('PAYMENT_TYPE_NAME').text(),
              payment_type: jQuery(e).find('PAYMENT_TYPE').text(),
              act_seg_id: jQuery(e).find('ACT_SEG_ID').text(),
              paymentmethod_name: jQuery(e).find('PAYMENTMETHOD_NAME').text(),
              paymentmethod: jQuery(e).find('PAYMENTMETHOD').text(),
              currency_id: jQuery(e).find('CURRENCY_ID').text(),
              currency_name: jQuery(e).find('CURRENCY_NAME').text(),
              beneficiary_accountcode: jQuery(e).find('BENEFICIARY_ACCOUNTCODE').text(),
              country_name: jQuery(e).find('COUNTRY_NAME').text(),
              beneficiary_email: jQuery(e).find('BENEFICIARY_EMAIL').text(),
              beneficiary_instructions: jQuery(e).find('BENEFICIARY_INSTRUCTIONS').text(),
              beneficiarybank_name: jQuery(e).find('BENEFICIARYBANK_NAME').text(),
              purpose: jQuery(e).find('PURPOSE').text(),
              purpose_of_payment: jQuery(e).find('PURPOSE_OF_PAYMENT').text(),
              currency_item_id: jQuery(e).find('CURRENCY_ITEM_ID').text(),
            };
            row.push(temp);
          });
          return row;
        }else{
          console.log(success.data);
        }
      });
    };
    vm.GetCountries = function(){
      return soap_api.GetCountries().then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT COUNTRY').each(function(i,e){
          var temp = {
            country_id: jQuery(e).find('COUNTRY_ID').text(),
            country_name: jQuery(e).find('COUNTRY_NAME').text(),
            continent_id: jQuery(e).find('CONTINENT_ID').text(),
            country_code: jQuery(e).find('COUNTRY_CODE').text(),
            is_watch_list: jQuery(e).find('IS_WATCH_LIST').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            is_approved: jQuery(e).find('IS_APPROVED').text(),
            version_no: jQuery(e).find('VERSION_NO').text()
          };
          row.push(temp);
        });
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.GetPurposeOfPayments = function(){
      return soap_api.GetPurposeOfPayments().then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT PURPOSE_OF_PAYMENT').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            description: jQuery(e).find('DESCRIPTION').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            is_approved: jQuery(e).find('IS_APPROVED').text(),
            version_no: jQuery(e).find('VERSION_NO').text()
          };
          row.push(temp);
        });
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.mergeObjects = function(obj1,obj2){
      var obj3 = {};
      for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
      for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
      return obj3;
    };
    vm.GetFeeByMethodByID = function(ID){
      return soap_api.GetFeeByMethodByID(ID).then(function(success){
        var fee = [];
        var instrument_fee = [];
        var same_currency_fee = [];
        var error = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT FEE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            free_instruments: jQuery(e).find('FREE_INSTRUMENTS').text(),
            instrument_cost: jQuery(e).find('INSTRUMENT_COST').text(),
            non_instrument_cost: jQuery(e).find('NON_INSTRUMENT_COST').text(),
            inv_act_type_id: jQuery(e).find('INV_ACT_TYPE_ID').text(),
            inv_act_type: jQuery(e).find('INV_ACT_TYPE').text(),
            charge_id: jQuery(e).find('CHARGE_ID').text(),
            charge_type: jQuery(e).find('CHARGE_TYPE').text(),
            is_allowed: jQuery(e).find('IS_ALLOWED').text(),
            source_id: jQuery(e).find('SOURCE_ID').text(),
          };
          fee.push(temp);
        });
        jQuery(response).find('OUTPUT INSTRUMENT_FEE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            currency_id: jQuery(e).find('Currency_ID').text(),
            instrument_cost: jQuery(e).find('Instrument_Cost').text(),
            cost_type: jQuery(e).find('COST_TYPE').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            cost_type_name: jQuery(e).find('COST_TYPE_NAME').text()
          };
          instrument_fee.push(temp);
        });
        jQuery(response).find('OUTPUT SAME_CURRENCY_FEE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            currency_id: jQuery(e).find('Currency_ID').text(),
            instrument_cost: jQuery(e).find('Instrument_Cost').text(),
            cost_type: jQuery(e).find('COST_TYPE').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            cost_type_name: jQuery(e).find('COST_TYPE_NAME').text()
          };
          same_currency_fee.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          error.push(temp);
        });
        var result = {
          fee : fee,
          instrument_fee: instrument_fee,
          same_currency_fee: same_currency_fee,
          error: error
        }
        return result;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.GetWebModules = function(){
      var row = [];
      soap_api.GetWebModules().then(function(success){
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            module_id: jQuery(e).find('MODULE_ID').text(),
            module_name: jQuery(e).find('MODULE_NAME').text(),
            module_parentid: jQuery(e).find('MODULE_PARENTID').text(),
            module_tag: jQuery(e).find('MODULE_TAG').text(),
            module_id_column: jQuery(e).find('MODULE_ID_COLUMN').text(),
            module_tree_column: jQuery(e).find('MODULE_TREE_COLUMN').text(),
            module_control_name: jQuery(e).find('MODULE_CONTROL_NAME').text(),
            fiscal_sensetive: jQuery(e).find('FISCAL_SENSETIVE').text(),
            is_dashboard_item: jQuery(e).find('IS_DASHBOARD_ITEM').text(),
            is_menu_element: jQuery(e).find('IS_MENU_ELEMENT').text(),
            module_icon_filename: jQuery(e).find('MODULE_ICON_FILENAME').text(),
            module_largeicon_filename: jQuery(e).find('MODULE_LARGEICON_FILENAME').text(),
            sort_order: jQuery(e).find('SORT_ORDER').text(),
            module_type: jQuery(e).find('MODULE_TYPE').text(),
            module_org_type: jQuery(e).find('MODULE_ORG_TYPE').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            version_no: jQuery(e).find('VERSION_NO').text(),
          };
          row.push(temp);
        });
      }, function(error){
        console.error(error.statusText);
      });
      return row;
    };
    vm.GetWebPaymentMethods = function(module){
      return soap_api.GetWebPaymentMethods(module).then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT PAYMENT_METHOD').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            allow_funding_picker: jQuery(e).find('ALLOW_FUNDING_PICKER').text(),
            allow_payment_picker: jQuery(e).find('ALLOW_PAYMENT_PICKER').text(),
            allow_payment_edit: jQuery(e).find('ALLOW_PAYMENT_EDIT').text(),
            allow_funding_edit: jQuery(e).find('ALLOW_FUNDING_EDIT').text(),
            free_instruments: jQuery(e).find('FREE_INSTRUMENTS').text(),
            instrument_cost: jQuery(e).find('INSTRUMENT_COST').text(),
            revenue_type_id: jQuery(e).find('REVENUE_TYPE_ID').text(),
            is_funding: jQuery(e).find('IS_FUNDING').text(),
            source_id: jQuery(e).find('SOURCE_ID').text(),
            charge_id: jQuery(e).find('CHARGE_ID').text(),
            charge_type: jQuery(e).find('CHARGE_TYPE').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetSettlementsPayees = function(methods){
      vm.showCircleLoader = false;
      return soap_api.GetSettlementsPayees(methods).then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('DATASET_OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            entity: jQuery(e).find('ENTITY').text(),
            type: jQuery(e).find('TYPE').text(),
            is_non_third_party: jQuery(e).find('IS_NON_THIRD_PARTY').text(),
            payment_type_name: jQuery(e).find('PAYMENT_TYPE_NAME').text(),
            payment_type: jQuery(e).find('PAYMENT_TYPE').text(),
            act_seg_id: jQuery(e).find('ACT_SEG_ID').text(),
            paymentmethod_name: jQuery(e).find('PAYMENTMETHOD_NAME').text(),
            paymentmethod: jQuery(e).find('PAYMENTMETHOD').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            beneficiary_accountcode: jQuery(e).find('BENEFICIARY_ACCOUNTCODE').text(),
            country_name: jQuery(e).find('COUNTRY_NAME').text(),
            beneficiary_email: jQuery(e).find('BENEFICIARY_EMAIL').text(),
            beneficiary_instructions: jQuery(e).find('BENEFICIARY_INSTRUCTIONS').text(),
            internal_reference: jQuery(e).find('INTERNAL_REFERENCE').text(),
            beneficiarybank_name: jQuery(e).find('BENEFICIARYBANK_NAME').text(),
            purpose: jQuery(e).find('PURPOSE').text(),
            purpose_of_payment: jQuery(e).find('PURPOSE_OF_PAYMENT').text(),
            currency_item_id: jQuery(e).find('CURRENCY_ITEM_ID').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetWebPaymentMethods(12280).then(function(data){
      vm.methods = data;
      var m = [];
      for(var i=0; i<data.length; i++){
        m.push(data[i].id);
      }
      vm.payments_methods_joined = m.join('|');
    });
    vm.GetSavedApprovalsByModule = function(module){
      return soap_api.GetSavedApprovalsByModule(module).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT ADM_WEB_APPROVAL_POLICY').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              entity_name: jQuery(e).find('ENTITY_NAME').text(),
              entity_id: jQuery(e).find('ENTITY_ID').text(),
              module_id: jQuery(e).find('MODULE_ID').text(),
              approval_level: jQuery(e).find('APPROVAL_LEVEL').text(),
              created_on: jQuery(e).find('CREATED_ON').text(),
              created_by: jQuery(e).find('CREATED_BY').text(),
              updated_on: jQuery(e).find('UPDATED_ON').text(),
              updated_by: jQuery(e).find('UPDATED_BY').text(),
              is_deleted: jQuery(e).find('IS_DELETED').text(),
              is_approved: jQuery(e).find('IS_APPROVED').text(),
              version_no: jQuery(e).find('VERSION_NO').text()
            };
            row.push(temp);
          });
          jQuery(response).find('OUTPUT ADM_WEB_USER_APPROVAL').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              approval_policy_id: jQuery(e).find('APPROVAL_POLICY_ID').text(),
              user_id: jQuery(e).find('USER_ID').text(),
              user_name: jQuery(e).find('USER_NAME').text(),
              approval_type_id: jQuery(e).find('APPROVAL_TYPE_ID').text(),
              approval_type_name: jQuery(e).find('APPROVAL_TYPE_NAME').text(),
              created_on: jQuery(e).find('CREATED_ON').text(),
              created_by: jQuery(e).find('CREATED_BY').text(),
              updated_on: jQuery(e).find('UPDATED_ON').text(),
              updated_by: jQuery(e).find('UPDATED_BY').text(),
              is_deleted: jQuery(e).find('IS_DELETED').text(),
              is_approved: jQuery(e).find('IS_APPROVED').text(),
              version_no: jQuery(e).find('VERSION_NO').text()
            };
            row.push(temp);
          });
        }else{
          console.log(success.data);
        }
        return row;
      });
    };
    vm.GetSavedApprovalsByModule(12280).then(function(data){
      vm.approvalPolicy = data;
      if(data[0].approval_level == 0 || data[0].approval_level == ''){
        vm.skipApprovalStep = true;
      }else{
        vm.skipApprovalStep = false;
      }
    });
    vm.GetFeeForSettlements = function(input_xml){
      return soap_api.GetFeeForSettlements(input_xml).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT PAYMENT').each(function(i,e){
            var temp = {
              buy_amount: jQuery(e).find('BUY_AMOUNT').text(),
              buy_currency_id: jQuery(e).find('BUY_CURRENCY_ID').text(),
              buy_currency: jQuery(e).find('BUY_CURRENCY').text()
            };
            row.push(temp);
          });
          jQuery(response).find('OUTPUT SETTLEMENT').each(function(i,e){
            var temp = {
              fee: jQuery(e).find('FEE').text(),
              fee_type: jQuery(e).find('FEE_TYPE').text(),
              fee_currency_id: jQuery(e).find('FEE_CURRENCY_ID').text(),
              fee_currency: jQuery(e).find('FEE_CURRENCY').text(),
              action_id: jQuery(e).find('ACTION_ID').text(),
              action_name: jQuery(e).find('ACTION_NAME').text(),
              sell_currency_id: jQuery(e).find('SELL_CURRENCY_ID').text(),
              sell_currency: jQuery(e).find('SELL_CURRENCY').text(),
              sell_amount: jQuery(e).find('SELL_AMOUNT').text(),
              rate: jQuery(e).find('RATE').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetSystemConfiguration = function(){
      return soap_api.GetSystemConfiguration().then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT SYSTEM_SETUP').each(function(i,e){
            var temp = {
              fx_fwd_days: jQuery(e).find('FX_FWD_DAYS').text(),
              web_quotation_expiry: jQuery(e).find('WEB_QUOTATION_EXPIRY').text(),
              rfq_expiry: jQuery(e).find('RFQ_EXPIRY').text(),
              trader_quotation_expiry: jQuery(e).find('TRADER_QUOTATION_EXPIRY').text(),
              allow_online_trading: jQuery(e).find('ALLOW_ONLINE_TRADING').text(),
              default_web_page_id: jQuery(e).find('DEFAULT_WEB_PAGE_ID').text(),
              rfq_quotation_mode: jQuery(e).find('RFQ_QUOTATION_MODE').text(),
              show_all_web_methods: jQuery(e).find('SHOW_ALL_WEB_METHODS').text(),
              max_online_forward_days: jQuery(e).find('MAX_ONLINE_FORWARD_DAYS').text(),
              allow_secure_ques_on_login: jQuery(e).find('ALLOW_SECURE_QUES_ON_LOGIN').text(),
              enforce_term_condition: jQuery(e).find('ENFORCE_TERM_CONDITION').text(),
              password_reset_type: jQuery(e).find('PASSWORD_RESET_TYPE').text(),
              allow_email_approval: jQuery(e).find('ALLOW_EMAIL_APPROVAL').text(),
              allow_1to1_deal: jQuery(e).find('ALLOW_1TO1_DEAL').text(),
              is_fee_fx_based: jQuery(e).find('IS_FEE_FX_BASED').text(),
              is_pop_mandatory: jQuery(e).find('IS_POP_MANDATORY').text()
            };
            row.push(temp);
          });
          jQuery(response).find('OUTPUT ALLOWED_TIMES').each(function(i,e){
            var temp = {
              day: jQuery(e).find('DAY').text(),
              is_allowed: jQuery(e).find('IS_ALLOWED').text(),
              from_time: jQuery(e).find('FROM_TIME').text(),
              to_time: jQuery(e).find('TO_TIME').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetSystemConfiguration().then(function(data){
      vm.systemConfig = data;
      if(vm.systemConfig[0].is_pop_mandatory == 'false'){
        vm.popMandate = false;
      }else{
        vm.popMandate = true;
      }
    });
    vm.GetWebDeals = function(module){
      return soap_api.GetWebDeals(module).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT WEB_DEAL').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              deal_number_value: jQuery(e).find('DEAL_NUMBER_VALUE').text(),
              deal_number: jQuery(e).find('DEAL_NUMBER').text(),
              entry_date: jQuery(e).find('ENTRY_DATE').text(),
              value_date: jQuery(e).find('VALUE_DATE').text(),
              type_id: jQuery(e).find('TYPE_ID').text(),
              type: jQuery(e).find('TYPE').text(),
              buy_cur_name: jQuery(e).find('BUY_CUR_NAME').text(),
              sell_amount: jQuery(e).find('SELL_AMOUNT').text(),
              exchange_rate: jQuery(e).find('EXCHANGE_RATE').text(),
              buy_balance: jQuery(e).find('BUY_BALANCE').text(),
              sell_cur_name: jQuery(e).find('SELL_CUR_NAME').text(),
              buy_amount: jQuery(e).find('BUY_AMOUNT').text(),
              sell_balance: jQuery(e).find('SELL_BALANCE').text(),
              booker: jQuery(e).find('BOOKER').text(),
              is_batch: jQuery(e).find('IS_BATCH').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetWebDealPaymentAndSettlement = function(deal){
      return soap_api.GetWebDealPaymentAndSettlement(deal).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT WEB_DLG_DEAL_SETTLEMENT').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              settlement_id: jQuery(e).find('SETTLEMENT_ID').text(),
              deal_id: jQuery(e).find('DEAL_ID').text(),
              internal_reference: jQuery(e).find('INTERNAL_REFERENCE').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetWireReport = function(settlement_id){
      return soap_api.GetWireReport(settlement_id).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function(i,e){
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              recordid: jQuery(e).find('RECORDID').text(),
              error: jQuery(e).find('ERROR').text(),
              value: jQuery(e).find('VALUE').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetDealReport = function(deal){
      return soap_api.GetDealReport(deal).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function(i,e){
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              recordid: jQuery(e).find('RECORDID').text(),
              error: jQuery(e).find('ERROR').text(),
              value: jQuery(e).find('VALUE').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetAllElectronicMethods = function(){
      return soap_api.GetAllElectronicMethods().then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            is_template: jQuery(e).find('IS_TEMPLATE').text(),
            is_routing_type: jQuery(e).find('IS_ROUTING_TYPE').text(),
            is_bank_code_enabled: jQuery(e).find('IS_BANK_CODE_ENABLED').text(),
            is_electronic: jQuery(e).find('IS_ELECTRONIC').text(),
            is_bank_code_required: jQuery(e).find('IS_BANK_CODE_REQUIRED').text()
          };
          row.push(temp);
        });
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.GetAllElectronicMethods().then(function(data){
      vm.allElectronicMethods = data;
    });
    vm.ValidateBusinessDate = function(date){
          return soap_api.ValidateBusinessDate(date).then(function(success){
              if(vm.isXML(success.data)){
                  var row = [];
                  var response = jQuery.parseXML(success.data);
                  jQuery(response).find('OUTPUT RESULT').each(function(i,e){
                      var temp = {
                          status: jQuery(e).find('STATUS').text(),
                          recordid: jQuery(e).find('RECORDID').text(),
                          error: jQuery(e).find('ERROR').text()
                      };
                      row.push(temp);
                  });
                  return row;
              }
          });
      };


    vm.protectAmount = function(event){
      var input = event.key;
      var re = /\d/i;
      if(input != 'Backspace' && input != '.' && input != 'Delete' && input != 'Del' && input != 'Decimal' && input != 'Left' && input != 'Right' && input != 'Tab' && input != 'ArrowLeft' && input != 'ArrowRight'){
        if(input.match(re) == null){
          event.preventDefault();
          return false;
        }
      }else{
        if(input == '.' || input == 'Decimal'){
          re = /\./g;
          if(event.target.value.length > 0){
            if(event.target.value.match(/\./g) != null){
              if(event.target.value.match(/\./g).length > 0){
                event.preventDefault();
                return false;
              }
            }
          }
        }
      }
    };
    vm.noSpecialCharactersFunction = function(event){
      var input = event.key;
      //console.log(event);
      var re = /^[a-zA-Z0-9\-\s]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.parseFloat = function(number){
      var parsed = parseFloat(number);
      return parsed;
    };
    vm.parseInt = function(number){
      return parseInt(number);
    };
    vm.toLowerCase = function(str){
      str = str || '';
      return str.toLowerCase();
    };
    vm.toUpperCase = function(str){
      str = str || '';
      return str.toUpperCase();
    };
    vm.includes = function(string, substring){
      string = string || '';
      substring = substring || ''
      if( string.toLowerCase().indexOf(substring.toLowerCase()) !== -1){
        return true;
      }else{
        return false;
      }
    };
    vm.replaceWith = function(string, separator){
      string = string || '';
      return string.replace(/ +/g, separator);
    };
    vm.limitLength = function(event, limit){
      if(event.target.value.length == limit){
        event.preventDefault();
      }
    };
    vm.stopInputing = function(event){
      event.preventDefault();
    };
    vm.doneFunc = function(){
      $state.reload();
    };
    vm.pasteLimit = function(event, limit){
      var length = event.originalEvent.clipboardData.getData('text/plain').length + event.originalEvent.target.value.length;
      if(length > limit){
        event.preventDefault();
      }
    };

    /* Make A Payment Logic */
    vm.modules = vm.GetWebModules();
    vm.selectedIndex = 0;
    vm.max = 3;
    vm.activated = true;
    //step 1
    vm.step1 = {
      currency: null,
      method: null,
      account: null,
    };
    vm.dealPayment = [];
    vm.payeeEntries = [];
    $scope.$watchCollection("vm.payeeEntries", function(newValue, oldValue){
      if(newValue.length > 0){
        vm.disableFunding = true;
      }
    });
    vm.temporaryPayeeEntries = [];
    vm.GetCountries().then(function(data){
      vm.countries = vm.uniqueCountries(data);
    });
    vm.GetAllCurrencies().then(function(data){

      vm.currencies = data;
    });
    vm.GetPurposeOfPayments().then(function(data){
      vm.purpose_of_payment = data;
    });
    vm.SendPayments = function(deal, value_date, payments){
      return soap_api.SendPayments(deal, value_date, payments).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text()
          };
          row.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.SendSettlements = function(deal, value_date, settlements){
      return soap_api.SendSettlements(deal, value_date, settlements).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text()
          };
          row.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.getAccountsList = function(data){
      vm.step1.account = "";
      vm.makePaymentAccounts = [];
      vm.showCircleLoader = false;

      if(data.currency != null && data.method != null){
        vm.GetPaymentsByCurrenyAndMethodInMakeAPaymentTop(data.currency, data.method).then(function(data){
          if(data.length > 0){
            vm.accounts = data;

          }
          hideCircularLoader()

        });
      }
    };
    function hideCircularLoader(){
   vm.showCircleLoader = true;
 }
    vm.uniqueAccounts = function(array){
      var flags = [], output = [], l = array.length, i;
      for( i=0; i<l; i++) {
        if( flags[array[i].id]) continue;
        flags[array[i].id] = true;
        output.push(array[i]);
      }
      console.log(output);
      return output;
    };
    vm.GetPurposeOfPayments().then(function(data){
      vm.purposes_of_payment = data;
    });
    vm.CreateWebDeal = function(client, trader, notes, internal_remarks, value_date, is_bach, deal_transaction){
      return soap_api.CreateWebDeal(client, trader, notes, internal_remarks, value_date, is_bach, deal_transaction).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text()
          };
          row.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text(),
            recordnumber: jQuery(e).find('RECORDNUMBER').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.updateWebDealWithPayments = function(deal, value_date, deal_transaction, payments){
      return soap_api.updateWebDealWithPayments(deal, value_date, deal_transaction, payments).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text()
          };
          row.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text(),
            recordnumber: jQuery(e).find('RECORDNUMBER').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.updateWebDealWithSettlementsPaymentsTransactions = function(deal, value_date, deal_transaction, payments, settlements){
      return soap_api.updateWebDealWithSettlementsPaymentsTransactions(deal, value_date, deal_transaction, payments, settlements).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text()
          };
          row.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text(),
            recordnumber: jQuery(e).find('RECORDNUMBER').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.PostWebDeal = function(deal){
      return soap_api.PostWebDeal(deal).then(function (success) {
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT RESULT').each(function(i,e) {
          var temp = {
            recordtype: jQuery(e).find('RecordType').text(),
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text(),
            recordnumber: jQuery(e).find('RECORDNUMBER').text()
          };
          row.push(temp);
        });
        return row;
      });

    };
    vm.FinalizeDealPaymentSettlement = function (deal) {
      return soap_api.FinalizeDealPaymentSettlement(deal).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            recordtype: jQuery(e).find('RecordType').text(),
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text(),
            recordnumber: jQuery(e).find('RECORDNUMBER').text()
          };
          row.push(temp);
        });
        return row;
      });
    }
    vm.GetPaymentsByCurrenyAndMethodInMakeAPaymentTop = function(currency_id, method_id){
      return soap_api.GetPaymentsByCurrenyAndMethodInMakeAPaymentTop(currency_id, method_id).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT Table1').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              name: jQuery(e).find('NAME').text(),
              parent_id: jQuery(e).find('PARENT_ID').text(),
              entity: jQuery(e).find('ENTITY').text(),
              type: jQuery(e).find('TYPE').text(),
              is_non_third_party: jQuery(e).find('IS_NON_THIRD_PARTY').text(),
              payment_type_name: jQuery(e).find('PAYMENT_TYPE_NAME').text(),
              payment_type: jQuery(e).find('PAYMENT_TYPE').text(),
              act_seg_id: jQuery(e).find('ACT_SEG_ID').text(),
              paymentmethod_name: jQuery(e).find('PAYMENTMETHOD_NAME').text(),
              paymentmethod: jQuery(e).find('PAYMENTMETHOD').text(),
              currency_id: jQuery(e).find('CURRENCY_ID').text(),
              currency_name: jQuery(e).find('CURRENCY_NAME').text(),
              beneficiary_accountcode: jQuery(e).find('BENEFICIARY_ACCOUNTCODE').text(),
              country_name: jQuery(e).find('COUNTRY_NAME').text(),
              beneficiary_email: jQuery(e).find('BENEFICIARY_EMAIL').text(),
              beneficiary_instructions: jQuery(e).find('BENEFICIARY_INSTRUCTIONS').text(),
              beneficiarybank_name: jQuery(e).find('BENEFICIARYBANK_NAME').text(),
              purpose: jQuery(e).find('PURPOSE').text(),
              purpose_of_payment: jQuery(e).find('PURPOSE_OF_PAYMENT').text(),
              currency_item_id: jQuery(e).find('CURRENCY_ITEM_ID').text(),
              balance: jQuery(e).find('BALANCE').text(),
            };
            row.push(temp);
          });
          return row;
        }else{
          console.log(success.data);
        }
      });
    };
    vm.GetPaymentsByCurrenyAndMethodsInMakeAPaymentSinglePayee = function(currency_id, methods){
      return soap_api.GetPaymentsByCurrenyAndMethodsInMakeAPaymentSinglePayee(currency_id, methods).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT Table1').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              name: jQuery(e).find('NAME').text(),
              parent_id: jQuery(e).find('PARENT_ID').text(),
              entity: jQuery(e).find('ENTITY').text(),
              type: jQuery(e).find('TYPE').text(),
              is_non_third_party: jQuery(e).find('IS_NON_THIRD_PARTY').text(),
              payment_type_name: jQuery(e).find('PAYMENT_TYPE_NAME').text(),
              payment_type: jQuery(e).find('PAYMENT_TYPE').text(),
              act_seg_id: jQuery(e).find('ACT_SEG_ID').text(),
              paymentmethod_name: jQuery(e).find('PAYMENTMETHOD_NAME').text(),
              paymentmethod: jQuery(e).find('PAYMENTMETHOD').text(),
              currency_id: jQuery(e).find('CURRENCY_ID').text(),
              currency_name: jQuery(e).find('CURRENCY_NAME').text(),
              beneficiary_accountcode: jQuery(e).find('BENEFICIARY_ACCOUNTCODE').text(),
              country_name: jQuery(e).find('COUNTRY_NAME').text(),
              beneficiary_email: jQuery(e).find('BENEFICIARY_EMAIL').text(),
              beneficiary_instructions: jQuery(e).find('BENEFICIARY_INSTRUCTIONS').text(),
              beneficiarybank_name: jQuery(e).find('BENEFICIARYBANK_NAME').text(),
              purpose: jQuery(e).find('PURPOSE').text(),
              purpose_of_payment: jQuery(e).find('PURPOSE_OF_PAYMENT').text(),
              currency_item_id: jQuery(e).find('CURRENCY_ITEM_ID').text(),
              beneficiarybank_address1: jQuery(e).find('BENEFICIARYBANK_ADDRESS1').text(),
              beneficiarybank_address2: jQuery(e).find('BENEFICIARYBANK_ADDRESS2').text(),
              beneficiarybank_city: jQuery(e).find('BENEFICIARYBANK_CITY').text(),
              beneficiarybank_province: jQuery(e).find('BENEFICIARYBANK_PROVINCE').text(),
              beneficiarybank_country: jQuery(e).find('BENEFICIARYBANK_COUNTRY').text(),
              balance: jQuery(e).find('BALANCE').text()
            };
            row.push(temp);
          });
          return row;
        }else{
          console.log(success.data);
        }
      });
    };
    vm.GetPaymentsByCurreniesAndMethodsInMakeAPaymentMultiplePayee = function(currencies, methods){
      return soap_api.GetPaymentsByCurreniesAndMethodsInMakeAPaymentMultiplePayee(currencies, methods).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT Table1').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              name: jQuery(e).find('NAME').text(),
              balance: jQuery(e).find('BALANCE').text(),
              parent_id: jQuery(e).find('PARENT_ID').text(),
              entity: jQuery(e).find('ENTITY').text(),
              type: jQuery(e).find('TYPE').text(),
              is_non_third_party: jQuery(e).find('IS_NON_THIRD_PARTY').text(),
              payment_type_name: jQuery(e).find('PAYMENT_TYPE_NAME').text(),
              payment_type: jQuery(e).find('PAYMENT_TYPE').text(),
              act_seg_id: jQuery(e).find('ACT_SEG_ID').text(),
              paymentmethod_name: jQuery(e).find('PAYMENTMETHOD_NAME').text(),
              paymentmethod: jQuery(e).find('PAYMENTMETHOD').text(),
              currency_id: jQuery(e).find('CURRENCY_ID').text(),
              currency_name: jQuery(e).find('CURRENCY_NAME').text(),
              beneficiary_accountcode: jQuery(e).find('BENEFICIARY_ACCOUNTCODE').text(),
              country_name: jQuery(e).find('COUNTRY_NAME').text(),
              beneficiary_email: jQuery(e).find('BENEFICIARY_EMAIL').text(),
              beneficiary_instructions: jQuery(e).find('BENEFICIARY_INSTRUCTIONS').text(),
              beneficiarybank_name: jQuery(e).find('BENEFICIARYBANK_NAME').text(),
              purpose: jQuery(e).find('PURPOSE').text(),
              purpose_of_payment: jQuery(e).find('PURPOSE_OF_PAYMENT').text(),
              currency_item_id: jQuery(e).find('CURRENCY_ITEM_ID').text(),
              beneficiarybank_address1: jQuery(e).find('BENEFICIARYBANK_ADDRESS1').text(),
              beneficiarybank_address2: jQuery(e).find('BENEFICIARYBANK_ADDRESS2').text(),
              beneficiarybank_city: jQuery(e).find('BENEFICIARYBANK_CITY').text(),
              beneficiarybank_province: jQuery(e).find('BENEFICIARYBANK_PROVINCE').text(),
              beneficiarybank_country: jQuery(e).find('BENEFICIARYBANK_COUNTRY').text()
            };
            row.push(temp);
          });
          return row;
        }else{
          console.log(success.data);
        }
      });
    };

    vm.step2 = {};
    vm.step3 = {};
    vm.step4 = {};
    vm.b64toBlob = function (b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    };
    vm.showNext = true;
    vm.showPaymentHistory = true;
    vm.CreateWebDeal = function(client, trader, notes, internal_remarks, value_date, is_bach, deal_transaction){
      return soap_api.CreateWebDeal(client, trader, notes, internal_remarks, value_date, is_bach, deal_transaction).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text()
          };
          row.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text(),
            recordnumber: jQuery(e).find('RECORDNUMBER').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.GetEntity = function(){
      return soap_api.GetEntity().then(function(success){
        var row = [];
        var temp1 = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT ENTITY').each(function(i,e){
          var temp = {
            name: jQuery(e).find('NAME').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            entity_no: jQuery(e).find('ENTITY_NO').text(),
            entity_type_id: jQuery(e).find('ENTITY_TYPE_ID').text(),
            type_name: jQuery(e).find('TYPE_NAME').text(),
            status: jQuery(e).find('STATUS').text(),
            value: jQuery(e).find('VALUE').text(),
            entity_class_id: jQuery(e).find('ENTITY_CLASS_ID').text(),
            name1: jQuery(e).find('NAME1').text(),
            entity_org_id: jQuery(e).find('ENTITY_ORG_ID').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            relationship_manager_id: jQuery(e).find('RELATIONSHIP_MANAGER_ID').text(),
            relationship_manager_name: jQuery(e).find('RELATIONSHIP_MANAGER_NAME').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            version_no: jQuery(e).find('VERSION_NO').text(),
            is_deleted1: jQuery(e).find('IS_DELETED1').text()
          };
          temp1['entity'] = temp;
        });
        jQuery(response).find('OUTPUT ADDRESS').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            parent_type: jQuery(e).find('PARENT_TYPE').text(),
            address_type: jQuery(e).find('ADDRESS_TYPE').text(),
            address_type_value: jQuery(e).find('ADDRESS_TYPE_VALUE').text(),
            priority: jQuery(e).find('PRIORITY').text(),
            street_address: jQuery(e).find('STREET_ADDRESS').text(),
            post_code: jQuery(e).find('POST_CODE').text(),
            city: jQuery(e).find('CITY').text(),
            province: jQuery(e).find('PROVINCE').text(),
            countryid: jQuery(e).find('COUNTRYID').text(),
            country_value: jQuery(e).find('COUNTRY_VALUE').text(),
            is_default: jQuery(e).find('IS_DEFAULT').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            is_approved: jQuery(e).find('IS_APPROVED').text(),
            is_web_approved: jQuery(e).find('IS_WEB_APPROVED').text(),
            version_no: jQuery(e).find('VERSION_NO').text(),
            country_code: jQuery(e).find('COUNTRY_CODE').text()
          };
          temp1['address'] = temp;
        });
        jQuery(response).find('OUTPUT PHONE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            parent_type: jQuery(e).find('PARENT_TYPE').text(),
            phone_type: jQuery(e).find('PHONE_TYPE').text(),
            phone_type_value: jQuery(e).find('PHONE_TYPE_VALUE').text(),
            phone_number: jQuery(e).find('PHONE_NUMBER').text(),
            priority: jQuery(e).find('PRIORITY').text(),
            is_default: jQuery(e).find('IS_DEFAULT').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            is_approved: jQuery(e).find('IS_APPROVED').text(),
            version_no: jQuery(e).find('VERSION_NO').text(),
            is_web_approved: jQuery(e).find('IS_WEB_APPROVED').text()
          };
          temp1['phone'] = temp;
        });
        var temp2 = [];
        jQuery(response).find('OUTPUT WEB').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            PARENT_TYPE_VALUE: jQuery(e).find('PARENT_TYPE_VALUE').text(),
            PARENT_TYPE: jQuery(e).find('PARENT_TYPE').text(),
            WEB_TYPE: jQuery(e).find('WEB_TYPE').text(),
            WEB_TYPE_VALUE: jQuery(e).find('WEB_TYPE_VALUE').text(),
            WEB_ADDRESS: jQuery(e).find('WEB_ADDRESS').text(),
            PRIORITY: jQuery(e).find('PRIORITY').text(),
            IS_DEFAULT: jQuery(e).find('IS_DEFAULT').text(),
            CREATED_ON: jQuery(e).find('CREATED_ON').text(),
            CREATED_BY: jQuery(e).find('CREATED_BY').text(),
            UPDATED_ON: jQuery(e).find('UPDATED_ON').text(),
            UPDATED_BY: jQuery(e).find('UPDATED_BY').text(),
            IS_DELETED: jQuery(e).find('IS_DELETED').text(),
            IS_APPROVED: jQuery(e).find('IS_APPROVED').text(),
            VERSION_NO: jQuery(e).find('VERSION_NO').text(),
            IS_WEB_APPROVED: jQuery(e).find('IS_WEB_APPROVED').text()
          };
          temp2.push(temp);
        });
        temp1['web'] = temp2;
        row.push(temp1);
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.GetEntity().then(function (data) {
      vm.entity = data[0];
    });
    vm.tempPromises = [];
    vm.tempPayees = [];
    vm.deal = {};
    vm.finalEntries = [];
    vm.finalPromises = [];
    vm.tempState = null;
    vm.hideQuoteScreens = false;
    vm.sendMakeAPaymentRecord = function(){
      vm.GetEntity().then(function (data) {
        vm.entity = data[0];
        vm.GetEntityWebDefaults().then(function(data){
          vm.entityWebDefaults = data[0];
          var totalRows = [];
          var promises = [];
          var payees = [];
          if(vm.payeeEntries.length > 0){
            for(var i=0; i<vm.payeeEntries.length; i++){
              if(vm.payeeEntries[i].currency == 'n/a' || vm.payeeEntries[i].currency == ''){
                $mdToast.show({
                  template: '<md-toast class="md-toast error">Some payments have invalid currencies.</md-toast>',
                  hideDelay: 5000,
                  position: 'bottom right'
                });
                vm.showCircleLoader = true;
                return;
              }
              payees.push(vm.payeeEntries[i]);
            }
            for(var i=0; i<payees.length; i++){
              var single = {
                currency: payees[i].currency,
                amount: 0,
                count: 0,
                fee: payees[i].fee,
                cost: 0
              };
              for(var j=0; j<payees.length; j++){
                if(payees[i].currency == payees[j].currency){
                  single.amount += vm.parseFloat(payees[j].amount);
                  single.count++;
                  single.cost = vm.parseFloat(single.amount);
                }
              }
              totalRows.push(single);
            }
            vm.temporaryBeneficiaries = _.unique(totalRows, function(p){ return p.currency; });
            var client = vm.entity.entity.name;
            var trader = vm.entityWebDefaults.trader_id;
            var notes = '';
            var internal_remarks = '';
            var value_date = vm.defaultValueDate == '' ? new Date() : new Date(Date.parse(vm.defaultValueDate));
            value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
            var is_bach = 12280;
            var deal_transaction = '';
            var buy_currency = _.filter(vm.paymentCurrencies, {currency_id: vm.step1.currency});
            var buy_currency_id = buy_currency[0].currency_id;
            buy_currency = buy_currency[0].currency_name;
            for(var i=0; i<vm.temporaryBeneficiaries.length; i++){
              vm.tempPayees.push(vm.temporaryBeneficiaries[i]);
            }
            for(var i=0; i<vm.tempPayees.length; i++){
              var sell_currency = _.filter(vm.paymentCurrencies, {currency_id: vm.tempPayees[i].currency});
              var sell_currency_id = sell_currency[0].currency_id;
              var sell_currency_name = sell_currency[0].currency_name;
              var sell_amount = vm.parseFloat(vm.tempPayees[i].cost);
              if(vm.toLowerCase(sell_currency_name) == vm.toLowerCase(buy_currency)){
                deal_transaction += '<DEAL_TRANSACTION>';
                deal_transaction += '<BUY_CURRENCY><ID>'+buy_currency_id+'</ID></BUY_CURRENCY>';
                deal_transaction += '<BUY_AMOUNT>0</BUY_AMOUNT>';
                deal_transaction += '<EXCHANGE_RATE>1</EXCHANGE_RATE>';
                deal_transaction += '<ACTUAL_MARKET_RATE>1</ACTUAL_MARKET_RATE>';
                deal_transaction += '<MARKET_RATE>1</MARKET_RATE>';
                deal_transaction += '<IS_EQUIVALENT>1</IS_EQUIVALENT>';
                deal_transaction += '<SELL_CURRENCY><ID>'+sell_currency_id+'</ID></SELL_CURRENCY>';
                deal_transaction += '<SELL_AMOUNT>'+sell_amount+'</SELL_AMOUNT>';
                deal_transaction += '</DEAL_TRANSACTION>';
              }else{
                deal_transaction += '<DEAL_TRANSACTION>';
                deal_transaction += '<BUY_CURRENCY><ID>'+buy_currency_id+'</ID></BUY_CURRENCY>';
                deal_transaction += '<BUY_AMOUNT>0</BUY_AMOUNT>';
                deal_transaction += '<EXCHANGE_RATE>0</EXCHANGE_RATE>';
                deal_transaction += '<ACTUAL_MARKET_RATE>0</ACTUAL_MARKET_RATE>';
                deal_transaction += '<MARKET_RATE>0</MARKET_RATE>';
                deal_transaction += '<IS_EQUIVALENT>0</IS_EQUIVALENT>';
                deal_transaction += '<SELL_CURRENCY><ID>'+sell_currency_id+'</ID></SELL_CURRENCY>';
                deal_transaction += '<SELL_AMOUNT>'+sell_amount+'</SELL_AMOUNT>';
                deal_transaction += '</DEAL_TRANSACTION>';
              }
            }
            $mdToast.show({
              template: '<md-toast class="md-toast info">Saving Deal.</md-toast>',
              hideDelay: 1000,
              position: 'bottom right'
            });
            vm.CreateWebDeal(client, trader, notes, internal_remarks, value_date, is_bach, deal_transaction).then(function(data){

              if(data[0].status == 'FALSE'){
                $mdToast.show({
                  template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                var circular_timer = angular.element('#circular-timer-box #circular-timer');
                circular_timer.TimeCircles().stop();
                $interval.cancel(vm.interval_func);
                vm.interval_func = null;
                $interval.cancel(vm.paymentsRateInterval);
                vm.paymentsRateInterval = null;
                //vm.acceptQuoteDisable = false;
                //vm.declineQuoteDisable = false;
                vm.disableMe = false;
                vm.showCircleLoader = true;
                vm.previousStep();
              }else{
                vm.deal = data[0];
                if(typeof vm.deal.id == 'string'){
                  //now adding settlements
                  if(vm.payeeEntries.length > 0){
                    $mdToast.show({
                      template: '<md-toast class="md-toast info">Saving Payments.</md-toast>',
                      hideDelay: 1000,
                      position: 'bottom right'
                    });
                    var settlements = '';
                    var feesPromises = [];
                    var payeesList = [];

                    for(var i=0; i<vm.payeeEntries.length; i++) {
                      feesPromises.push(vm.GetFeeByMethodByID(vm.payeeEntries[i].paymentmethod));
                      payeesList.push(vm.payeeEntries[i]);
                    }
                    $q.all(feesPromises).then(function(data){
                      for(var i=0; i<data.length; i++) {
                        //console.log(data[i]);
                        //console.log(payeesList[i]);
                        var value_date = payeesList[i].value_date == '' ? new Date( Date.parse(vm.defaultValueDate)) : new Date(Date.parse(payeesList[i].value_date));
                        value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                        if(payeesList[i].payment_purpose != null){
                          var purpose_of_payment = payeesList[i].payment_purpose;
                        }else{
                          var purpose_of_payment = '0';
                        }
                        var payment_reference = payeesList[i].payment_reference == null ? '' : payeesList[i].payment_reference;
                        var internal_payment_reference = payeesList[i].internal_payment_reference == null ? '' : payeesList[i].internal_payment_reference;
                        if(payeesList[i].payee_notification != null){
                          var payee_notification = payeesList[i].payee_notification;
                        }else{
                          var payee_notification = '';
                        }
                        var fee = payeesList[i].fee;
                        // if(typeof payeesList[i].fee == ''){
                        //   var fee = 0;
                        // }else{
                        //   var fee = payeesList[i].fee;
                        // }
                        settlements += '<SETTLEMENTS>';
                        settlements += '<ACTION><ID>'+payeesList[i].paymentmethod+'</ID></ACTION>';
                        settlements += '<CURRENCY><Value>'+payeesList[i].currency_name+'</Value></CURRENCY>';
                        settlements += '<AMOUNT>'+vm.parseFloat(payeesList[i].amount)+'</AMOUNT>';
                        settlements += '<VALUE_DATE>'+value_date+'</VALUE_DATE>';
                        if(payeesList[i].paymentmethod == 5){
                          if(payeesList[i].id != null)
                            settlements += '<ACCOUNT><ID>'+payeesList[i].id+'</ID></ACCOUNT>';
                          settlements += '<ACT_TYPE>E</ACT_TYPE>';
                        }else{
                          if(payeesList[i].id != null)
                            settlements += '<PAYEE><ID>'+payeesList[i].id+'</ID></PAYEE>';
                        }
                        settlements += '<REFERENCE>'+payment_reference+'</REFERENCE>';
                        settlements += '<BENEFICIARY_REFERENCE>'+payment_reference+'</BENEFICIARY_REFERENCE>';
                        settlements += '<NOTIFY_MY_RECEIPIENT></NOTIFY_MY_RECEIPIENT>';
                        settlements += '<BENEFICIARY_REMARKS></BENEFICIARY_REMARKS>';
                        settlements += '<REMITTENCE_REMARKS></REMITTENCE_REMARKS>';
                        settlements += '<PURPOSE_OF_PAYMENT>'+purpose_of_payment+'</PURPOSE_OF_PAYMENT>';
                        settlements += '<IS_EQUIVALENT>0</IS_EQUIVALENT>';
                        settlements += '<INTERNAL_REFERENCE>'+internal_payment_reference+'</INTERNAL_REFERENCE>';
                        settlements += '<FEE>'+fee+'</FEE>';
                        //                      console.log(data[i]);
                        settlements += '<FEE_TYPE><ID>'+data[i].fee[0].charge_id+'</ID></FEE_TYPE>';
                        settlements += '<FEE_CURRENCY><ID>'+vm.step1.currency+'</ID></FEE_CURRENCY>';
                        settlements += '</SETTLEMENTS>';
                      }
                      var date = new Date(Date.parse(vm.defaultValueDate)) || new Date();
                      vm.SendSettlements(vm.deal.deal_number, date, settlements).then(function(data){
                        if(data[0].status == 'FALSE'){
                          $mdToast.show({
                            template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                            hideDelay: 5000,
                            position: 'bottom right'
                          });
                          vm.showCircleLoader = true;
                        }else{
                          vm.showCircleLoader = true;
                          vm.showNext = false;
                          vm.showPaymentHistory = false;
                          $mdToast.show({
                            template: '<md-toast class="md-toast success">Deal Saved.</md-toast>',
                            hideDelay: 1000,
                            position: 'bottom right'
                          });
                        }
                      });
                    });
                  }
                }else{
                  vm.showCircleLoader = true;
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Deal Not Saved.</md-toast>',
                    hideDelay: 5000,
                    position: 'bottom right'
                  });
                }
              }
            });
          }
        });
      });
    };
    vm.getTempEntries = function(){
      var row = [];
      for(var i=0; i<vm.payeeEntries.length; i++){
        var single = {
          currency: vm.payeeEntries[i].currency,
          amount: 0,
          count: 0,
          fee: 0,
          rate: 0,
          cost: 0
        };
        for(var j=0; j<vm.payeeEntries.length; j++){
          if(vm.payeeEntries[i].currency == vm.payeeEntries[j].currency){
            //single.cost += vm.parseFloat(vm.parseFloat(payees[j].amount) * vm.parseFloat(data[j][0].exchange_rate));
            //single.cost += vm.parseFloat(vm.parseFloat(payees[j].amount) + vm.parseFloat(payees[j].fee));
            single.amount += vm.parseFloat(vm.payeeEntries[j].amount);
            single.count++;
            //single.cost = vm.parseFloat(single.cost);
            //single.cost = vm.parseFloat(single.amount);
          }
        }
        row.push(single);
      }
      return _.unique(row, function(p){ return p.currency; });
    };
    vm.fetchRatesForRows = function(){
      var entries = [];
      if(vm.payeeEntries.length > 0){
        for(var i=0; i<vm.payeeEntries.length; i++){
          var single = {
            currency: vm.payeeEntries[i].currency,
            amount: 0,
            count: 0,
            fee: 0,
            rate: 0,
            cost: 0,
            market_rate : 0,
            actual_market_rate : 0
          };
          for(var j=0; j<vm.payeeEntries.length; j++){
            if(vm.payeeEntries[i].currency == vm.payeeEntries[j].currency){
              single.amount += vm.parseFloat(vm.payeeEntries[j].amount);
              single.count++;
            }
          }
          entries.push(single);
        }
        vm.finalEntries = _.unique(entries, function(p){ return p.currency; });
        if(typeof vm.finalEntries == 'object'){
          vm.paymentsRateInterval = $interval(function(){
            console.log("Here and there!");

            if(typeof vm.entity != 'undefined'){
              vm.finalPromises = [];
              for(var i=0; i< vm.finalEntries.length; i++){
                var value_date = vm.payeeEntries[i].value_date == null ? new Date() : vm.payeeEntries[i].value_date;
                vm.finalPromises.push(vm.GetRateByIDS(vm.step1.currency, 0, vm.finalEntries[i].currency, vm.finalEntries[i].amount, value_date, vm.entity.entity.name));
              }
              $q.all(vm.finalPromises).then(function(data){
                for(var i=0; i< data.length; i++){
                  if(typeof data[i][0].buy_amount !== 'undefined')
                    vm.finalEntries[i].cost = vm.parseFloat(data[i][0].buy_amount);
                    vm.finalEntries[i].rate = vm.parseFloat(data[i][0].exchange_rate);
                    vm.finalEntries[i].market_rate = vm.parseFloat(data[i][0].market_rate);
                    vm.finalEntries[i].actual_market_rate = vm.parseFloat(data[i][0].actual_market_rate);
                }
                vm.getTotalCost();
              }).finally(function(){
                if(vm.tempState == 'approve'){
                  //start timer and other things
                  var circular_timer = angular.element('#circular-timer-box');
                  circular_timer.empty();
                  circular_timer.append('<div id="circular-timer" data-timer="60"></div>');
                  var timer = circular_timer.find('#circular-timer');
                  timer.TimeCircles({
                    "animation": "smooth",
                    "bg_width": 1,
                    "fg_width": 0.21,
                    "circle_bg_color": "#AAAAAA",
                    "direction":'Counter-clockwise',
                    "start_angle": 160,
                    "time": {
                      "Days": {
                        "text": "Days",
                        "color": "#FFCC66",
                        "show": false
                      },
                      "Hours": {
                        "text": "Hours",
                        "color": "#99CCFF",
                        "show": false
                      },
                      "Minutes": {
                        "text": "Minutes",
                        "color": "#BBFFBB",
                        "show": false
                      },
                      "Seconds": {
                        "text": "Expires in",
                        "color": "#FFA834",
                        "show": true
                      }
                    }
                  }).addListener(function(unit, value, total){
                    if( total == -1){
                      vm.step1.amount = null;
                      vm.step1.currency_to_buy = null;
                      vm.step1.currency_to_sell = null;
                      vm.step1.value_date = null;
                      vm.quote = [];
                      vm.quote_copy = [];
                      timer.TimeCircles().stop();
                      $mdToast.show({
                        template: '<md-toast class="md-toast info">Quote timeout.</md-toast>',
                        hideDelay: 2000,
                        position: 'bottom right'
                      });
                      vm.showCircleLoader = true;
                      $interval.cancel(vm.interval_func);
                      vm.interval_func = null;
                      $interval.cancel(vm.paymentsRateInterval);
                      vm.paymentsRateInterval = null;
                      var circular_timer = angular.element('#circular-timer-box #circular-timer');
                      circular_timer.TimeCircles().stop();
                      vm.previousStep();
                    }else{
                      if(unit == 'Seconds'){
                        var text_box = timer.find('.textDiv_Seconds');
                        text_box.find('.title').remove();
                        var new_value = value + 'Secs';
                        text_box.append('<span class="title">'+new_value+'</span>');
                      }
                      if(vm.payeeEntries.length == 1){
                        if(vm.payeeEntries[0].currency_id == vm.step1.currency){
                          $interval.cancel(vm.interval_func);
                          vm.interval_func = null;
                          $interval.cancel(vm.paymentsRateInterval);
                          vm.paymentsRateInterval = null;
                        }
                      }
                    }
                  });
                  vm.timer();
                  vm.nextStep();
                  vm.tempState = null;
                  vm.showCircleLoader = true;
                }
              });
            }else{
              $mdToast.show({
                template: '<md-toast class="md-toast error">Entity record missing.</md-toast>',
                hideDelay: 4000,
                position: 'bottom right'
              });
              vm.showCircleLoader = true;
              $interval.cancel(vm.paymentsRateInterval);
              vm.paymentsRateInterval = null;
              //vm.previousStep();
            }
          }, 1000);
        }
      }else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">Payments record not found.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        vm.showCircleLoader = true;
      }
    };
    vm.savePaymentDialog = function(){
      vm.showCircleLoader = false;
      vm.disableFunding = true;
      vm.sendMakeAPaymentRecord();
    };
    vm.ApproveFunc = function(){
      console.log("Heree and there");

      vm.showCircleLoader = false;
      vm.tempState = 'approve';
      var count = 0;
      for(var i=0; i<vm.payeeEntries.length; i++){
        if(vm.payeeEntries[i].currency_id == vm.step1.currency){
          count++;
        }
      }
      if(count == vm.payeeEntries.length){
        var queue = [];
        for(var j=0; j<vm.payeeEntries.length; j++){
          var value_date = vm.payeeEntries[j].value_date == null ? new Date() : vm.payeeEntries[j].value_date;
          queue.push(vm.GetRateByIDS(vm.step1.currency, 0, vm.payeeEntries[j].currency, vm.payeeEntries[j].amount, value_date, vm.entity.entity.name));
        }
        $q.all(queue).then(function(data){
          var entries = [];
          for(var i=0; i<vm.payeeEntries.length; i++){
            var single = {
              currency: vm.payeeEntries[i].currency,
              amount: 0,
              count: 0,
              fee: 0,
              rate: 0,
              cost: 0,
              market_rate : 0,
              actual_market_rate : 0
            };
            for(var j=0; j<vm.payeeEntries.length; j++){
              if(vm.payeeEntries[i].currency == vm.payeeEntries[j].currency){
                single.amount += vm.parseFloat(vm.payeeEntries[j].amount);
                single.count++;
              }
            }
            entries.push(single);
          }
          vm.finalEntries = _.unique(entries, function(p){ return p.currency; });
          for(var i=0; i< data.length; i++){
            if(typeof data[i][0].buy_amount !== 'undefined')
              vm.finalEntries[0].cost += vm.parseFloat(data[i][0].buy_amount);
            vm.finalEntries[0].rate = vm.parseFloat(data[i][0].exchange_rate);
            vm.finalEntries[0].market_rate = vm.parseFloat(data[i][0].market_rate);
            vm.finalEntries[0].actual_market_rate = vm.parseFloat(data[i][0].actual_market_rate);
          }
          vm.getTotalCost();
          vm.hideQuoteScreens = true;
          vm.showCircleLoader = true;
          vm.nextStep();
        });
      }else{
        vm.fetchRatesForRows();
      }
    };
    vm.acceptDealPayment = function(data){
      vm.showCircleLoader = false;
      var temp = {};
      angular.copy(data, temp);
      if(temp.method != null && temp.currency != null){
        vm.GetUserApprovalRights(vm.deal, 12280).then(function(data){
          if(data[0].status == 'TRUE'){
            vm.skipApprovalStep = false;
          }else{
            vm.skipApprovalStep = true;
          }
          vm.showCircleLoader = true;
          vm.nextStep();
        });
      }else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please fill required fields.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
      }
    };
    vm.showPopOver = false;
    vm.popover = {
      title: 'List of Approvals',
    };
    vm.approvals = [];
    vm.ApprovalNext = false;
    vm.approvalCounter = 0;
    vm.showPopOverFunc = function(){
      vm.showPopOver = true;
    };
    vm.showPopLeaveFunc = function(){
      vm.showPopOver = false;
    };
    vm.SetDealApproval = function(deal){
      return soap_api.SetDealApproval(deal).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function(i,e){
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              error: jQuery(e).find('ERROR').text()
            };
            row.push(temp);
          });
        }else{
          console.log(success.data);
        }
        return row;
      });
    };
    vm.GetApprovalHistory = function(deal){
      return soap_api.GetApprovalHistory(deal).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT APPROVAL_HISTORY').each(function(i,e){
            var temp = {
              trans_approval_id: jQuery(e).find('TRANS_APPROVAL_ID').text(),
              org_approval_id: jQuery(e).find('ORG_APPROVAL_ID').text(),
              transaction_id: jQuery(e).find('TRANSACTION_ID').text(),
              user_id: jQuery(e).find('USER_ID').text(),
              user_name: jQuery(e).find('USER_NAME').text(),
              approval_num: jQuery(e).find('APPROVAL_NUM').text(),
              approval_date: jQuery(e).find('APPROVAL_DATE').text(),
              approval_level: jQuery(e).find('APPROVAL_LEVEL').text(),
              created_on: jQuery(e).find('CREATED_ON').text(),
              created_by: jQuery(e).find('CREATED_BY').text(),
              updated_on: jQuery(e).find('UPDATED_ON').text(),
              updated_by: jQuery(e).find('UPDATED_BY').text(),
              version_no: jQuery(e).find('VERSION_NO').text(),
              is_deleted: jQuery(e).find('IS_DELETED').text()
            };
            row.push(temp);
          });
        }else{
          console.log(success.data);
        }
        return row;
      });
    };
    vm.approveNow = function(){
      if(vm.ApprovalNext == false){
        vm.showCircleLoader = false;
        vm.GetUserApprovalRights(vm.deal, 12280).then(function(data){
          if(data[0].status == 'TRUE'){
            vm.SetDealApproval(vm.deal.deal_number).then(function(data){
              if(data[0].status == 'TRUE'){
                $mdToast.show({
                  template: '<md-toast class="md-toast info">Deal Approved.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                vm.GetApprovalHistory(vm.deal).then(function(data){
                  /*var user = data[0];
                  var user_row = {
                    name: user.user_name,
                    date: user.approval_date
                  };*/
                  vm.approvalCounter++;
                  vm.approvals = data;
                  if(vm.approvals.length == vm.approvalPolicy[0].approval_level){
                    vm.ApprovalNext = true;
                  }
                  vm.showCircleLoader = true;
                });
              }else{
                $mdToast.show({
                  template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                  hideDelay: 2000,
                  position: 'bottom right'
                });
                vm.showCircleLoader = true;
              }
            });
          }else{
            $mdToast.show({
              template: '<md-toast class="md-toast error">You cannot approve this deal.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
          }
          vm.showCircleLoader = true;
        });
      }
    };
    vm.customFullscreen = true;
    vm.parseFloating = function(value){
      var the_amount = value;
      if(typeof value == 'string'){
        the_amount = value.split(',').join('');
        the_amount = vm.parseFloat(the_amount);
      }
      if(typeof value == 'number'){
        the_amount = vm.parseFloat(the_amount);
      }
      return the_amount;
    };
    vm.showPayeeTable = true;
    vm.showSinglePayment = function(event){
      if(vm.step1.currency !== null && vm.step1.method !== null){
        //show single payment dialog
        $mdDialog.show({
          fullscreen: vm.customFullscreen,
          clickOutsideToClose: true,
          scope: $scope,
          preserveScope: true,
          templateUrl: 'app/main/payments/make_a_payment/single_payment.html',
          parent: angular.element(document.body),
          controller: function dialogController($scope, $mdDialog){
            $scope.showPayeeTable = true;
            $scope.isDisabled = true;
            $scope.showSinglePayeeTable = true;
            $scope.data = {
              expanded: false
            };
            $scope.payeeTable = {
              routing: null,
              payee_payor: null,
              bank_name: null,
              country: null
            };
            $scope.singlePayeeLoader = true;
            $scope.routingFilter = function(payee){
              if(vm.payeeTable){
                if(
                  (payee.beneficiary_accountcode.toLowerCase().indexOf(vm.payeeTable.routing || '') !== -1) ||
                  (payee.beneficiarybank_name.toLowerCase().indexOf(vm.payeeTable.routing || '') !== -1)
                ){
                  return true;
                }else{
                  return false;
                }
              }else{
                return true;
              }
            };
            $scope.payeeFilter = function (payee) {
              if(vm.payeeTable){
                if(
                  (payee.name.toLowerCase().indexOf(vm.payeeTable.payee_payor || '') !== -1)
                ){
                  return true;
                }else{
                  return false;
                }
              }else{
                return true;
              }
            };
            $scope.bankFilter = function (payee) {
              if(vm.payeeTable){
                if(
                  (payee.beneficiarybank_name.toLowerCase().indexOf(vm.payeeTable.bank_name || '') !== -1)
                ){
                  return true;
                }else{
                  return false;
                }
              }else{
                return true;
              }
            };
            $scope.countryFilter = function (payee) {
              if(vm.payeeTable){
                if(
                  (payee.country_name.toLowerCase().indexOf(vm.payeeTable.country || '') !== -1)
                ){
                  return true;
                }else{
                  return false;
                }
              }else{
                return true;
              }
            };
            $scope.methodFilter = function (payee) {
              if(vm.payeeTable){
                if(
                  (payee.type.toLowerCase().indexOf(vm.payeeTable.method || '') !== -1)
                ){
                  return true;
                }else{
                  return false;
                }
              }else{
                return true;
              }
            };
            $scope.payeeSelectFunc = function(event){
              $scope.singlePayeeLoader = false;
              var element = event.target;
              if(element.nodeName.toLowerCase() == 'td'){
                var tr = angular.element(element).closest('tr');
                var payee_id = tr.attr('id');
                var payee_object = _.filter($scope.payeesList, {id: payee_id});
                vm.GetFeeByMethodByID(payee_object[0].paymentmethod).then(function(data){
                  $scope.singlePayeeLoader = true;
                  if(payee_object[0].currency_id == vm.step1.currency){
                    //if we have same currencies
                    if(data.same_currency_fee.length > 0){
                      if(data.same_currency_fee[0].currency_id == payee_object[0].currency_id){
                        $scope.single.fee = vm.parseFloating(data.same_currency_fee[0].instrument_cost);
                        console.log($scope.single.fee);
                      }else{
                        $scope.single.fee = vm.parseFloating(data.fee[0].non_instrument_cost);
                        console.log($scope.single.fee);
                      }
                    }else{
                        $scope.single.fee = vm.parseFloating(data.fee[0].non_instrument_cost);
                        console.log($scope.single.fee);
                    }
                  }else{
                    //else we have different currencies
                    if(data.instrument_fee.length > 0){
                      var found = false;
                      var instrument_fee = {};
                      for(var i=0; i<data.instrument_fee.length; i++){
                          if(data.instrument_fee[i].currency_id == 2){
                              found = true;
                              instrument_fee = data.instrument_fee[i];
                          }
                      }
                      if(found){
                        $scope.single.fee = vm.parseFloating(instrument_fee.instrument_cost);
                      }else{
                        $scope.single.fee = vm.parseFloating(data.fee[0].instrument_cost);
                      }
                    }else{
                      $scope.single.fee = vm.parseFloating(data.fee[0].instrument_cost);
                    }
                  }
                  $scope.showPayeeTable = true;
                  vm.showPayeeTable = true;

                  // var balance = parseFloat(payee_object[0].balance);
                  if(payee_object[0].paymentmethod == 5){
                    $scope.single.payee_ = addBalance(payee_object[0]);
                  }else{
                    $scope.single.payee_ = payee_object[0].name;
                  }
                  // $scope.single.payee_balance = payee_object[0].balance;
                  $scope.single.payee_id = payee_object[0].id;
                  $scope.single.payee_old_id = payee_object[0].id;
//                  console.log(payee_object[0]);
                  if(payee_object[0].purpose != 0)
                    $scope.single.payment_purpose = payee_object[0].purpose;
                  vm.singlePayeeLoader = true;
                });
              }
            };

            $scope.hidePayeeTableFunc = function(){
              //$scope.showPayeeTable = true;
              vm.showPayeeTable = true;
              vm.payeeTable = {
                routing: null,
                payee_payor: null,
                bank_name: null,
                country: null
              };
            };
            $scope.showPayeeTableFunc = function($event){
              $scope.singlePayeeLoader = false;
              if(vm.showPayeeTable == true){
                vm.showPayeeTable = false;
                if($scope.single.currency != null){
                  vm.GetPaymentsByCurrenyAndMethodsInMakeAPaymentSinglePayee($scope.single.currency, vm.payments_methods_joined).then(function(data){
                    $scope.payeesList = data;
                    $scope.singlePayeeLoader = true;
                    vm.payeeTable = {
                      routing: null,
                      payee_payor: null,
                      bank_name: null,
                      country: null
                    };
                  });
                }
              }else{
                $scope.singlePayeeLoader = true;
              }
            };
            $scope.closeDialog = function() {
              $mdDialog.hide();
              vm.payeeTable = {
                routing: null,
                payee_payor: null,
                bank_name: null,
                country: null
              };
            };
            $scope.collapseAll = function(data) {
              for(var i in $scope.accordianData) {
                if($scope.accordianData[i] != data) {
                  $scope.accordianData[i].expanded = false;
                  $scope.showHelloMe = false
                }
              }
              data.expanded = !data.expanded;
              $scope.showHelloMe= true
            };
            $scope.single = {
              currency: null,
              payee_: null,
              amount: null,
              payment_reference: null,
              internal_payment_reference: null,
              payee_notification: null,
              payment_purpose: null,
              account: null,
              method: null,
              fee: null,
              value_date: vm.defaultValueDate || null
            };
            vm.GetFundingCurrenciesFilterByCurrency(vm.step1.currency).then(function(data){

              $scope.fundingCurrencies = data;
              var default_currency = _.filter($scope.fundingCurrencies, {currency_id: vm.entityWebDefaults.funding_cur_id});
              if(default_currency.length > 0){
                $scope.single.currency  = default_currency[0].currency_id;
                vm.GetPaymentsByCurrenyAndMethodsInMakeAPaymentSinglePayee($scope.single.currency, vm.payments_methods_joined).then(function(data){
                  $scope.payeesList = data;
                  var default_payee = _.filter($scope.payeesList, {id: vm.entityWebDefaults.funding_template_id});
                  //console.log(default_payee);
                  if(default_payee.length > 0){
                    // var balance = parseFloat(default_payee[0].balance);
                    // $scope.single.payee_ = default_payee[0].balance && (default_payee[0].name + " " + (balance).formatMoney(2, '.', ',') ) || default_payee[0].name ;
                    $scope.single.payee_ = default_payee[0].name;
                    $scope.single.payee_id = default_payee[0].id;
                    $scope.single.payee_old_id = default_payee[0].id;
                    //console.log(default_payee[0].currency_id);
                    //console.log(vm.step1.currency);
                    vm.GetFeeByMethodByID(default_payee[0].paymentmethod).then(function(data){
                      if(default_payee[0].currency_id === vm.step1.currency){
                        //if we have same currencies
                        if(data.same_currency_fee.length > 0){
                          if(data.same_currency_fee[0].currency_id == default_payee[0].currency){
                            $scope.single.fee = vm.parseFloating(data.same_currency_fee[0].instrument_cost);
                            //console.log($scope.single.fee);
                          }else{
                            $scope.single.fee = vm.parseFloating(data.fee[0].non_instrument_cost);
                            //console.log($scope.single.fee);
                          }
                        }else{
                          $scope.single.fee = vm.parseFloating(data.fee[0].non_instrument_cost);
                          //console.log($scope.single.fee);
                        }
                      }else{
                        //else we have different currencies
                        if(data.instrument_fee.length > 0){
                          var found = false;
                          var instrument_fee = {};
                          for(var i=0; i<data.instrument_fee.length; i++){
                            if(data.instrument_fee[i].currency_id == 2){
                              found = true;
                              instrument_fee = data.instrument_fee[i];
                            }
                          }
                          if(found){
                            $scope.single.fee = vm.parseFloating(instrument_fee.instrument_cost);
                            //console.log($scope.single.fee);
                          }else{
                            $scope.single.fee = vm.parseFloating(data.fee[0].instrument_cost);
                            //console.log($scope.single.fee);
                          }
                        }else{
                          $scope.single.fee = vm.parseFloating(data.fee[0].instrument_cost);
                          //console.log($scope.single.fee);
                        }
                      }
                    });
                  }
                });
              }
            });
            $scope.addPayee = function(payee){
              var record = {};
              angular.copy(payee, record);
              if( record.currency != null && record.payee_ != null && record.payee_ !== '' && vm.parseFloat(record.amount) > 0 && (record.value_date != null || record.value_date != '')){
                vm.ValidateBusinessDate(record.value_date).then(function(data){
                  data = data[0];
                  if(data.status == 'TRUE'){
                      if(vm.popMandate == true && record.payment_purpose == null){
                          $mdToast.show({
                              template: '<md-toast class="md-toast error">Purpose Of Payment Is Mandatory. Check Additional Details.</md-toast>',
                              hideDelay: 4000,
                              position: 'bottom right'
                          });
                          return;
                      }
                      if(record.payee_id != ''){
                          var payee_object = _.filter($scope.payeesList, {id: record.payee_id});
                          if($scope.single.payee_ != record.payee_){
                              $mdToast.show({
                                  template: '<md-toast class="md-toast error">Payee is not valid.</md-toast>',
                                  hideDelay: 4000,
                                  position: 'bottom right'
                              });
                              return;
                          }
                      }
                      if(payee.payee_notification != '' && payee.payee_notification != null){
                          var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                          if(payee.payee_notification.match(re) == null){
                              $mdToast.show({
                                  template: '<md-toast class="md-toast error">Email is invalid.</md-toast>',
                                  hideDelay: 1000,
                                  position: 'bottom right'
                              });
                              vm.SendPayeesAndDisable = false;
                              return false;
                          }
                      }
                      var payee_object = _.filter($scope.payeesList, {id: record.payee_id});
                      var identifier = vm.payeeEntries.length + 1;
                      record.current_id = identifier;
                      record.amount = vm.parseFloat(payee.amount);
                      record.fee = vm.parseFloat(payee.fee);
                      record.value_date = payee.value_date == '' ? vm.defaultValueDate : payee.value_date;
                      record = vm.mergeObjects(record, payee_object[0]);
                      vm.payeeEntries.push(record);
                      vm.showNext = true;
                      vm.showPaymentHistory = true;
                      $mdDialog.hide();
                  }else{
                      $mdToast.show({
                          template: '<md-toast class="md-toast error">'+data.error+'</md-toast>',
                          hideDelay: 4000,
                          position: 'bottom right'
                      });
                      vm.showPaymentHistory = true;
                      vm.showNext = true;
                  }
                });

              }else{
                $mdToast.show({
                  template: '<md-toast class="md-toast error">Currency, Payee, Amount and Value date are compulsory.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
              }
            }
            $scope.makePayeeAndFeeNull = function(){
              $scope.single.payee_ = null
              $scope.single.payee_id = null
              $scope.single.payee_old_id = null
              $scope.single.fee = null;
              $scope.single.purpose_of_payment = null;
            };
          }
        });
      }else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please fill required fields.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        vm.step1Invalid = true;
      }
    };
    vm.editSinglePayment = function(data){
      var record = {};
      // var balance = parseFloat(data.balance);
      $scope.single.payee_ = addBalance(data);
      angular.copy(data, record);
      $mdDialog.show({
        locals:{dataToPass: record},
        fullscreen: vm.customFullscreen,
        clickOutsideToClose: true,
        scope: $scope,
        preserveScope: true,
        templateUrl: 'app/main/payments/make_a_payment/edit_payment.html',
        parent: angular.element(document.body),
        controller: function dialogController($scope, $mdDialog){
          $scope.info = {
            expanded: false
          };
          $scope.showPayeeTable = true;
          $scope.isDisabled = true;
          $scope.showSinglePayeeTable = true;
          $scope.data = {
            expanded: false
          };
          $scope.payeeTable = {
            routing: null,
            payee_payor: null,
            bank_name: null,
            country: null
          };
          $scope.makePayeeAndFeeNull = function(){
            $scope.single.payee_ = null
            $scope.single.payee_id = null
            $scope.single.payee_old_id = null
            $scope.single.fee = null;
          };
          $scope.dialogLoader = true;
          $scope.singlePayeeLoader = true;
          $scope.disableSubmission = false;
          $scope.routingFilter = function(payee){
            if(vm.payeeTable){
              if(
                (payee.beneficiary_accountcode.toLowerCase().indexOf(vm.payeeTable.routing || '') !== -1) ||
                (payee.beneficiarybank_name.toLowerCase().indexOf(vm.payeeTable.routing || '') !== -1)
              ){
                return true;
              }else{
                return false;
              }
            }else{
              return true;
            }
          };
          $scope.payeeFilter = function (payee) {
            if(vm.payeeTable){
              if(
                (payee.name.toLowerCase().indexOf(vm.payeeTable.payee_payor || '') !== -1)
              ){
                return true;
              }else{
                return false;
              }
            }else{
              return true;
            }
          };
          $scope.bankFilter = function (payee) {
            if(vm.payeeTable){
              if(
                (payee.beneficiarybank_name.toLowerCase().indexOf(vm.payeeTable.bank_name || '') !== -1)
              ){
                return true;
              }else{
                return false;
              }
            }else{
              return true;
            }
          };
          $scope.methodFilter = function (payee) {
            if(vm.payeeTable){
              if(
                (payee.type.toLowerCase().indexOf(vm.payeeTable.method || '') !== -1)
              ){
                return true;
              }else{
                return false;
              }
            }else{
              return true;
            }
          };
          $scope.countryFilter = function (payee) {
            if(vm.payeeTable){
              if(
                (payee.country_name.toLowerCase().indexOf(vm.payeeTable.country || '') !== -1)
              ){
                return true;
              }else{
                return false;
              }
            }else{
              return true;
            }
          };
          $scope.payeeSelectFunc = function(event){
            $scope.singlePayeeLoader = false;
            var element = event.target;
            if(element.nodeName.toLowerCase() == 'td'){
              var tr = angular.element(element).closest('tr');
              var payee_id = tr.attr('id');
              var payee_object = _.filter($scope.payeesList, {id: payee_id});
              var old_payee_id = $scope.single.payee_old_id;
              vm.GetFeeByMethodByID(payee_object[0].paymentmethod).then(function(data){
                $scope.singlePayeeLoader = true;
                if(data.error.length > 0){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">'+data.error+'</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  vm.showCircleLoader = true;
                  $scope.showPayeeTable = true;
                  vm.showPayeeTable = true;
                  $scope.single.payee_ = null;
                  $scope.single.payee_old_id = null;
                  $scope.single.fee = null;
                  $scope.single.method = null;
                  return;
                }else{
                  if(payee_object.currency == vm.step1.currency){
                    //if we have same currencies
                    if(data.same_currency_fee.length > 0){
                      if(data.same_currency_fee[0].currency_id == payee_object.currency){
                        $scope.single.fee = vm.parseFloating(data.same_currency_fee[0].instrument_cost);
                      }else{
                        $scope.single.fee = vm.parseFloating(data.fee[0].non_instrument_cost);
                      }
                    }else{
                      $scope.single.fee = vm.parseFloating(data.fee[0].non_instrument_cost);
                    }
                  }else{
                    //else we have different currencies
                    if(data.instrument_fee.length > 0){
                      var found = false;
                      var instrument_fee = {};
                      for(var j=0; j<data.instrument_fee.length; j++){
                        if(data.instrument_fee[j].currency_id == 2){
                          found = true;
                          instrument_fee = data.instrument_fee[j];
                        }
                      }
                      if(found){
                        $scope.single.fee = vm.parseFloating(instrument_fee.instrument_cost);
                      }else{
                        $scope.single.fee = vm.parseFloating(data.fee[0].instrument_cost);
                      }
                    }else{
                      $scope.single.fee = vm.parseFloating(data.fee[0].instrument_cost);
                    }
                  }
                  $scope.showPayeeTable = true;
                  vm.showPayeeTable = true;
                  // $scope.single.payee_ = payee_object[0].name;
                  // var balance = parseFloat(payee_object[0].balance);
                  if(payee_object[0].paymentmethod == 5){
                    $scope.single.payee_ = addBalance(payee_object[0]);
                  }else{
                    $scope.single.payee_ = payee_object[0].name;
                  }
                  $scope.single.payee_id = payee_object[0].id;
                  $scope.single.payee_old_id = old_payee_id;
                  $scope.single.method = payee_object[0].type;
                  vm.singlePayeeLoader = true;
                }
              });
            }
          };
          $scope.hidePayeeTableFunc = function(){
            $scope.showPayeeTable = true;
            vm.showPayeeTable = true;
            vm.payeeTable = {
              routing: null,
              payee_payor: null,
              bank_name: null,
              country: null
            };
          };
          $scope.showPayeeTableFunc = function($event){
            $scope.singlePayeeLoader = false;
            if(vm.showPayeeTable == true){
              vm.showPayeeTable = false;
              if($scope.single.currency != null){
                vm.GetPaymentsByCurrenyAndMethodsInMakeAPaymentSinglePayee($scope.single.currency, vm.payments_methods_joined).then(function(data){
                  $scope.payeesList = data;
                  $scope.singlePayeeLoader = true;
                  vm.payeeTable = {

                    routing: null,
                    payee_payor: null,
                    bank_name: null,
                    country: null
                  };
                });
              }
            }else{
              $scope.singlePayeeLoader = true;
            }
          };
          $scope.closeDialog = function() {
            $mdDialog.hide();
            vm.payeeTable = {
              routing: null,
              payee_payor: null,
              bank_name: null,
              country: null
            };
          };
          $scope.collapseAll = function(data) {
            for(var i in $scope.accordianData) {
              if($scope.accordianData[i] != data) {
                $scope.accordianData[i].expanded = false;
                $scope.showHelloMe = false
              }
            }
            data.expanded = !data.expanded;
            $scope.showHelloMe= true
          };
          var value_date = new Date(Date.parse(record.value_date));
          $scope.single = {
            current_id: record.current_id || null,
            currency: record.currency || null,
            payee_: record.payee_ || null,
            payee_old_id: record.payee_id || null,
            amount: record.amount || null,
            payment_reference: record.payment_reference || null,
            internal_payment_reference: record.internal_payment_reference || null,
            payee_notification: record.payee_notification || null,
            payment_purpose: record.payment_purpose || null,
            account: record.account || null,
            method: record.method || null,
            fee: vm.parseFloat(record.fee),
            value_date: ('0' + (value_date.getMonth()+1)).slice(-2) + '/' + ('0' + value_date.getDate()).slice(-2) + '/' +value_date.getFullYear() || null
          };
          $scope.single = vm.mergeObjects($scope.single, record);
          $scope.single.payee_ = addBalance(record);
          $scope.updatePayee = function(payee) {
            $scope.disableSubmission = true;
            $scope.dialogLoader = false;
            if (vm.parseFloat(payee.amount) > 0 && payee.payee_ != null && payee.fee != null && payee.currency != null && (payee.value_date != null || payee.value_date != '')) {
              vm.ValidateBusinessDate(payee.value_date).then(function (data) {

                var data = data[0];
                if (data.status === "TRUE") {
                  if (vm.popMandate == true && payee.payment_purpose == null) {
                    $mdToast.show({
                      template: '<md-toast class="md-toast error">Purpose Of Payment Is Mandatory. Check Additional Details.</md-toast>',
                      hideDelay: 4000,
                      position: 'bottom right'
                    });
                    $scope.dialogLoader = true;
                    $scope.disableSubmission = false;
                    return;
                  }
                  if (payee.payee_notification != '' && payee.payee_notification != null) {
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (payee.payee_notification.match(re) == null) {
                      $mdToast.show({
                        template: '<md-toast class="md-toast error">Email is invalid.</md-toast>',
                        hideDelay: 1000,
                        position: 'bottom right'
                      });
                      $scope.dialogLoader = true;
                      $scope.disableSubmission = false;
                      return false;
                    }
                  }
                  var localPromise = [];
                  localPromise.push(vm.GetPaymentsByCurrenyAndMethodsInMakeAPaymentSinglePayee(payee.currency_id, vm.payments_methods_joined));
                  $q.all(localPromise).then(function (data) {
                    var payeesList = data[0];
                    for (var i = 0; i < vm.payeeEntries.length; i++) {
                      if (vm.payeeEntries[i].current_id === payee.current_id) {
                        var payee_object = _.filter(payeesList, {id: payee.payee_id});
                        vm.payeeEntries[i] = payee_object[0];
                        vm.payeeEntries[i].current_id = payee.current_id;
                        vm.payeeEntries[i].currency = payee_object[0].currency_id;
                        vm.payeeEntries[i].payee_ = payee_object[0].name;
                        vm.payeeEntries[i].payee_id = payee_object[0].id;
                        vm.payeeEntries[i].id = payee_object[0].id;
                        vm.payeeEntries[i].name = payee_object[0].name;
                        vm.payeeEntries[i].payee_old_id = payee.payee_old_id;
                        vm.payeeEntries[i].amount = vm.parseFloating(payee.amount);
                        vm.payeeEntries[i].payment_reference = payee.payment_reference;
                        vm.payeeEntries[i].internal_payment_reference = payee.internal_payment_reference;
                        vm.payeeEntries[i].payee_notification = payee.payee_notification;
                        vm.payeeEntries[i].payment_purpose = payee.payment_purpose;
                        vm.payeeEntries[i].payment_reference_email_input = payee.payment_reference_email_input;
                        vm.payeeEntries[i].account = payee.account;
                        vm.payeeEntries[i].method = payee_object[0].type;
                        vm.payeeEntries[i].fee = payee.fee;
                        vm.payeeEntries[i].value_date = payee.value_date == '' ? vm.defaultValueDate : payee.value_date;
                      }
                    }
                    $mdToast.show({
                      template: '<md-toast class="md-toast success">Payee Record Updated.</md-toast>',
                      hideDelay: 4000,
                      position: 'bottom right'
                    });
                    $scope.dialogLoader = true;
                    $scope.disableSubmission = false;
                    $scope.closeDialog();
                  });
                }
                else if (data.status === "FALSE") {
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">' + data.error + '</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                }

                else {
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Currency, Payee, Amount and Value date are compulsory.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });

                }
                $scope.disableSubmission = false;
                $scope.dialogLoader = true;
                return true;

              })

            }
          }
        }
      });
    };
    vm.deleteRow = function(payee, event){
      for(var i=0; i<vm.payeeEntries.length; i++){
        if(vm.payeeEntries[i].current_id == payee.current_id){
          vm.payeeEntries.splice(i,1);
          $mdToast.show({
            template: '<md-toast class="md-toast info">Payee record removed from the list.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.disableFunding = false;
          return true;
        }
      }
    };
    vm.duration = 60;
    vm.percent_duration = 0;
    vm.expires_in = 60;
    vm.timer = function(){
      vm.duration = 60;
      vm.percent_duration = 0;
      vm.interval_func = $interval(function(){
        vm.expires_in -= 1;
        vm.percent_duration = (100/(vm.duration/vm.expires_in)).toFixed(2);
        if(vm.expires_in == 0){
          vm.duration = 60;
          vm.expires_in = 60;
          vm.percent_duration = 0;
          $interval.cancel(vm.interval_func);
        }
      }, 1000);
    };
    vm.disableMe = false;
    vm.AcceptQuote = function(){
      if(expireQuote()){
        vm.disableMe = true;
        vm.showCircleLoader = false;
        $mdToast.show({
          template: '<md-toast class="md-toast info">Saving your quote</md-toast>',
          hideDelay: 5000,
          position: 'bottom right'
        });
        $interval.cancel(vm.interval_func);
        vm.interval_func = null;
        $interval.cancel(vm.paymentsRateInterval);
        vm.paymentsRateInterval = null;
        var circular_timer = angular.element('#circular-timer-box #circular-timer');
        circular_timer.TimeCircles().stop();

        var payments = '';
        var deal_transaction = '';
        for(var i=0; i<vm.finalEntries.length; i++){
          var sell_currency = vm.toUpperCase(vm.finalEntries[i].currency);
          var sell_amount = vm.parseFloat(vm.finalEntries[i].amount).toFixed(2);
          var buy_currency = _.filter(vm.paymentCurrencies, {currency_id: vm.step1.currency});
          var buy_currency_id = buy_currency[0].currency_id;
          var buy_amount = vm.parseFloat(vm.finalEntries[i].cost).toFixed(2);
          if(buy_currency_id == sell_currency){
            deal_transaction += '<DEAL_TRANSACTION>';
            deal_transaction += '<BUY_CURRENCY><ID>'+buy_currency_id+'</ID></BUY_CURRENCY>';
            deal_transaction += '<BUY_AMOUNT>'+buy_amount+'</BUY_AMOUNT>';
            deal_transaction += '<EXCHANGE_RATE>1</EXCHANGE_RATE>';
            deal_transaction += '<ACTUAL_MARKET_RATE>1</ACTUAL_MARKET_RATE>';
            deal_transaction += '<MARKET_RATE>1</MARKET_RATE>';
            deal_transaction += '<IS_EQUIVALENT>0</IS_EQUIVALENT>';
            deal_transaction += '<SELL_CURRENCY><ID>'+sell_currency+'</ID></SELL_CURRENCY>';
            deal_transaction += '<SELL_AMOUNT>'+sell_amount+'</SELL_AMOUNT>';
            deal_transaction += '</DEAL_TRANSACTION>';
          }else{
            deal_transaction += '<DEAL_TRANSACTION>';
            deal_transaction += '<BUY_CURRENCY><ID>'+buy_currency_id+'</ID></BUY_CURRENCY>';
            deal_transaction += '<BUY_AMOUNT>'+buy_amount+'</BUY_AMOUNT>';
            deal_transaction += '<EXCHANGE_RATE>'+vm.parseFloat(vm.finalEntries[i].rate)+'</EXCHANGE_RATE>';
            deal_transaction += '<ACTUAL_MARKET_RATE>'+vm.parseFloat(vm.finalEntries[i].actual_market_rate)+'</ACTUAL_MARKET_RATE>';
            deal_transaction += '<MARKET_RATE>'+vm.parseFloat(vm.finalEntries[i].market_rate)+'</MARKET_RATE>';
            deal_transaction += '<IS_EQUIVALENT>0</IS_EQUIVALENT>';
            deal_transaction += '<SELL_CURRENCY><ID>'+sell_currency+'</ID></SELL_CURRENCY>';
            deal_transaction += '<SELL_AMOUNT>'+sell_amount+'</SELL_AMOUNT>';
            deal_transaction += '</DEAL_TRANSACTION>';
          }
        }
        var deal = vm.deal.deal_number;
        var currency_object = _.filter(vm.paymentCurrencies, {currency_id: vm.step1.currency });
        //sending settlements again
        var value_date = vm.defaultValueDate == '' ? new Date() : new Date(Date.parse(vm.defaultValueDate));
        value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
        var totalCost = vm.parseFloat(vm.totalCost).toFixed(2);
        var settlements = '<DEAL>' +
          '<MODULE_ID>12280</MODULE_ID>' +
          '<VALUE_DATE>'+value_date+'</VALUE_DATE>' +
          '<BUY_CURRENCY><Value>'+vm.toUpperCase(currency_object[0].currency_name)+'</Value></BUY_CURRENCY>' +
          '<BUY_AMOUNT>'+totalCost+'</BUY_AMOUNT>' +
          '</DEAL>';
        for(var i=0; i<vm.payeeEntries.length; i++){
          settlements += '<SETTLEMENTS>';
          settlements += '<ACTION><ID>'+vm.payeeEntries[i].paymentmethod+'</ID></ACTION>';
          settlements += '<SELL_CURRENCY><Value>'+vm.payeeEntries[i].currency_name+'</Value></SELL_CURRENCY>';
          settlements += '<SELL_AMOUNT>'+vm.payeeEntries[i].amount+'</SELL_AMOUNT>';
          for(var j=0; j<vm.finalEntries.length; j++){
            if(vm.finalEntries[j].currency == vm.payeeEntries[i].currency_id){
              settlements += '<RATE>'+vm.finalEntries[j].rate+'</RATE>';
            }
          }
          settlements += '</SETTLEMENTS>';
        }
        vm.GetFeeForSettlements(settlements).then(function(data){
          settlements = '';
          var temp_payment = data.shift();
          data = data;

          for(var i=0; i<data.length; i++) {
            vm.payeeEntries[i].total = data[i].sell_amount;
            if (typeof vm.payeeEntries[i].value_date == 'string' && vm.payeeEntries[i].value_date != null) {
              var value_date = new Date(Date.parse(vm.payeeEntries[i].value_date));
            } else if (typeof vm.payeeEntries[i].value_date == 'object') {
              var value_date = vm.payeeEntries[i].value_date;
            } else if (typeof vm.payeeEntries[i].value_date == 'undefined') {
              var value_date = new Date(Date.parse(vm.defaultValueDate));
            }
            value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth() + 1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
            //console.log(vm.payeeEntries[i]);
            if (vm.payeeEntries[i].payment_purpose != null) {
              var purpose_object = _.filter(vm.purposes_of_payment, {id: vm.payeeEntries[i].payment_purpose});
              //console.log(purpose_object);
              var purpose_of_payment = purpose_object[0].id;
            } else {
              var purpose_of_payment = 1;
            }
            var payment_reference = vm.payeeEntries[i].payment_reference == null ? '' : vm.payeeEntries[i].payment_reference;
            var uid = 'INREF-'+ vm.toUpperCase(Math.random().toString(16).slice(2));
            var internal_payment_reference = vm.payeeEntries[i].internal_payment_reference == null ? uid : vm.payeeEntries[i].internal_payment_reference;
            if(vm.payeeEntries[i].internal_payment_reference == null){
              vm.payeeEntries[i].internal_payment_reference = uid;
            }
            var amount = vm.parseFloat(vm.payeeEntries[i].amount).toFixed(2);
            settlements += '<SETTLEMENTS>';
            settlements += '<ACTION><ID>' + vm.payeeEntries[i].paymentmethod + '</ID></ACTION>';
            settlements += '<CURRENCY><Value>' + vm.payeeEntries[i].currency_name + '</Value></CURRENCY>';
            settlements += '<AMOUNT>' + data[i].sell_amount + '</AMOUNT>';
            settlements += '<VALUE_DATE>' + value_date + '</VALUE_DATE>';
            //for settlements pass payment_type ID as 697 which is outgoing.
            if (vm.payeeEntries[i].paymentmethod == 5) {
              if (vm.payeeEntries[i].id != null)
                settlements += '<ACCOUNT><ID>' + vm.payeeEntries[i].id + '</ID></ACCOUNT>';
              settlements += '<ACT_TYPE>E</ACT_TYPE>';
            } else {
              if (vm.payeeEntries[i].id != null)
                settlements += '<PAYEE><ID>' + vm.payeeEntries[i].id + '</ID></PAYEE>';
            }
            settlements += '<REFERENCE>' + payment_reference + '</REFERENCE>';
            settlements += '<BENEFICIARY_REFERENCE>' + payment_reference + '</BENEFICIARY_REFERENCE>';
            settlements += '<NOTIFY_MY_RECEIPIENT></NOTIFY_MY_RECEIPIENT>';
            settlements += '<BENEFICIARY_REMARKS></BENEFICIARY_REMARKS>';
            settlements += '<REMITTENCE_REMARKS></REMITTENCE_REMARKS>';
            settlements += '<PURPOSE_OF_PAYMENT>' + purpose_of_payment + '</PURPOSE_OF_PAYMENT>';
            settlements += '<IS_EQUIVALENT>0</IS_EQUIVALENT>';
            settlements += '<INTERNAL_REFERENCE>' + internal_payment_reference + '</INTERNAL_REFERENCE>';
            settlements += '<FEE>' + data[i].fee + '</FEE>';
            vm.payeeEntries[i].fee = data[i].fee;
            settlements += '<FEE_TYPE><ID>' + data[i].fee_type + '</ID></FEE_TYPE>';
            settlements += '<FEE_CURRENCY><ID>' + data[i].fee_currency_id + '</ID></FEE_CURRENCY>';
            settlements += '</SETTLEMENTS>';
          }
          var value_date = vm.defaultValueDate == '' ? new Date() : new Date(Date.parse(vm.defaultValueDate));
          value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
          if(vm.step1.account){
            var account_object = _.filter(vm.accounts, {id: accountId});
            var payee = account_object[0].id;
          }else{
            var payee = null;
          }
          var totalCost = vm.parseFloat(vm.totalCost).toFixed(2);
          payments += '<PAYMENTS>';
          payments += '<ACTION><ID>'+vm.step1.method+'</ID></ACTION>';
          payments += '<CURRENCY><ID>'+vm.step1.currency+'</ID></CURRENCY>';
          payments += '<AMOUNT>'+temp_payment.buy_amount+'</AMOUNT>';
          payments += '<VALUE_DATE>'+value_date+'</VALUE_DATE>';
          if(vm.step1.method == 5){
            if(payee != null)
              payments += '<ACCOUNT><ID>'+payee+'</ID></ACCOUNT>';
            payments += '<ACT_TYPE>E</ACT_TYPE>';
          }else{
            if(payee != null)
              payments += '<PAYEE><ID>'+payee+'</ID></PAYEE>';
          }
          payments += '</PAYMENTS>';

          value_date = vm.defaultValueDate == '' ? new Date() : new Date(Date.parse(vm.defaultValueDate));
          value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);

          vm.updateWebDealWithSettlementsPaymentsTransactions(vm.deal.id, value_date, deal_transaction, payments, settlements).then(function(data){
            if(typeof data[0].status !== 'undefined'){
              if(data[0].status == 'FALSE'){
                $mdToast.show({
                  template: '<md-toast class="md-toast error">API error: '+data[0].error+'</md-toast>',
                  hideDelay: 5000,
                  position: 'bottom right'
                });
                vm.showCircleLoader = true;
                vm.disableMe = false;
                //vm.previousStep();
              }
            }else{
              var deal = data[0];
              if(typeof data[0].id == 'undefined'){
                $mdToast.show({
                  template: '<md-toast class="md-toast error">Deal Update Failed.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                vm.showCircleLoader = true;
                vm.disableMe = false;
                vm.previousStep();
                return;
              }
              if(typeof data[0].id == 'string'){
                vm.PostWebDeal(deal).then(function(data){
                  if(data[0].status == 'TRUE'){
                    vm.deal['id'] = deal.id;
                    vm.FinalizeDealPaymentSettlement(deal).then(function(data){
                      if(data[0].status == 'TRUE'){
                        for(var key in data[0]){
                          vm.deal[key] = data[0][key];
                        }
                        // vm.deal = data[0];
                        //changing rates and total for payments
                        for(var i=0; i< vm.payeeEntries.length; i++){
                          for(var j=0; j< vm.finalEntries.length; j++){
                            if(vm.payeeEntries[i].currency == vm.finalEntries[j].currency){
                              vm.payeeEntries[i].rate = vm.parseFloat(vm.finalEntries[j].rate);
                            }
                          }
                        }
                        $mdToast.show({
                          template: '<md-toast class="md-toast success">Deal Posted and Finalized.</md-toast>',
                          hideDelay: 4000,
                          position: 'bottom right'
                        });
                        vm.showCircleLoader = true;
                        //vm.nextStep();
                        vm.GetWebDealPaymentAndSettlement(deal).then(function(settlements){
                          for(var i=0; i<settlements.length; i++){
                            if(settlements[i].internal_reference != ''){
                              if(vm.payeeEntries[i].internal_payment_reference == settlements[i].internal_reference){
                                vm.payeeEntries[i].settlement_id = settlements[i].settlement_id;
                              }
                            }
                          }
                          vm.nextStep();
                        });
                        /*vm.GetWebDealPaymentAndSettlement(deal).then(function(settlements){
                         var reports = [];
                         for(var i=0; i<settlements.length; i++){
                         if(settlements[i].internal_reference != ''){
                         reports.push(vm.GetWireReport(settlements[i].settlement_id));
                         }
                         }
                         $q.all(reports).then(function(responses){
                         for(var j=0; j<responses.length; j++){
                         if(vm.payeeEntries[j].internal_payment_reference == settlements[j].internal_reference){
                         vm.payeeEntries[j].print_link = responses[j][0].value;
                         vm.payeeEntries[j].print_name = responses[j][0].recordid;
                         for(var i=0; i<vm.allElectronicMethods.length; i++){

                         if(vm.payeeEntries[j].paymentmethod == vm.allElectronicMethods[i].id){
                         vm.payeeEntries[j].hide_print = true;
                         }else{
                         vm.payeeEntries[j].hide_print = false;
                         }
                         }
                         }
                         }
                         }).finally(function(){
                         //print file for the deal
                         vm.GetDealReport(deal).then(function(report){
                         vm.dealReport = {
                         link: "data:application/pdf;base64,"+ report[0].value,
                         name: report[0].recordid
                         };
                         });
                         });
                         });*/
                      }else{
                        $mdToast.show({
                          template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                          hideDelay: 4000,
                          position: 'bottom right'
                        });
                        vm.showCircleLoader = true;
                        vm.disableMe = false;
                        vm.previousStep();
                      }
                    });
                  }else{
                    $mdToast.show({
                      template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                      hideDelay: 4000,
                      position: 'bottom right'
                    });
                    vm.showCircleLoader = true;
                    vm.disableMe = false;
                    vm.previousStep();
                  }
                });
              }else{
                $mdToast.show({
                  template: '<md-toast class="md-toast error">There is some problem saving the quote.</md-toast>',
                  hideDelay: 5000,
                  position: 'bottom right'
                });
              }
            }
          });
        });
        return;
      }
      vm.showCircleLoader = true;

    };
    function expireQuote(){
      var bool = true;
      // vm.finalEntries[0] = {
      //   rate : 0
      // }
      vm.finalEntries.some(function(elem){
        if(elem.rate == 0){
          vm.DeclineQuote();
          // $interval.cancel(vm.paymentsRateInterval);
          bool = false;
          return true;
        }
      })
      return bool;
    }
    vm.b64toBlob = function (b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    };
    vm.printSettlement = function(settlement){
      vm.showCircleLoader = false;
      if(settlement.settlement_id != null && settlement.settlement_id != ''){

        vm.GetWireReport(settlement.settlement_id).then(function(data){
          data = data[0];
          vm.showCircleLoader = true;
          if(data.status == 'TRUE'){
            var blob = vm.b64toBlob(data.value, 'application/pdf');
            saveAs(blob, data.recordid);
          }else{
            $mdToast.show({
              template: '<md-toast class="md-toast error">No report found.</md-toast>',
              hideDelay: 5000,
              position: 'bottom right'
            });
          }
        });
      }
    };
    vm.printDeal = function(){
      debugger;
      vm.showCircleLoader = false;
      vm.GetDealReport(vm.deal).then(function(data){
        vm.showCircleLoader = true;
        data = data[0];
        if(data.status == 'TRUE'){
          var blob = vm.b64toBlob(data.value, 'application/pdf');
          saveAs(blob, data.recordid);
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">No report found.</md-toast>',
            hideDelay: 5000,
            position: 'bottom right'
          });
        }
      });
    };
    vm.DeclineQuote = function(){
      $interval.cancel(vm.interval_func);
      vm.interval_func = null;
      $interval.cancel(vm.paymentsRateInterval);
      vm.paymentsRateInterval = null;
      var circular_timer = angular.element('#circular-timer-box #circular-timer');
      circular_timer.TimeCircles().stop();
      messageService.error("Quote Decline.", 5000);
      // $mdToast.show({
      //   template: '<md-toast class="md-toast info">Quote declined.</md-toast>',
      //   hideDelay: 4000,
      //   position: 'bottom right'
      // });

      vm.previousStep();
    };
    vm.searchKeys = {
      payee: null,
      method: null,
      currency: null,
      account: null
    };
    vm.hidePayeeTableOnClick = function(){
      if(vm.showPayeeTable == false)
        vm.showPayeeTable = true;
    };
    //Step 2
    vm.showLoader = function () {

      if (vm.selectedIndex === 1) {
        vm.determinateValue = 100;
        vm.expire_in = 50;
        vm.interval_ = $interval(function () {

          vm.determinateValue -= 2;
          vm.expire_in -= 1;
          if (vm.determinateValue < 1) {
            // vm.selectedIndex = 0;
            vm.determinateValue = 100;
            vm.expire_in = 50;
            $interval.cancel(vm.interval_);
          }

        }, 1000);
      } else {
        vm.determinateValue = 100;
        vm.expire_in = 50;
        $interval.cancel(vm.interval_);
        vm.interval_ = null;
      }
    };
    vm.viewBankDetails = function(event, payeeData){
      //console.log(payeeData);
      var record = {};
      angular.copy(payeeData, record);
      $mdDialog.show({
        locals:{dataToPass: record},
        fullscreen: vm.customFullscreen,
        clickOutsideToClose: true,
        scope: $scope,
        preserveScope: true,
        templateUrl: 'app/main/payments/make_a_payment/view_bank_details.html',
        parent: angular.element(document.body),
        controller: function dialogController($scope, $mdDialog){
          $scope.closeDialog = function() {
            $mdDialog.hide();
            vm.payeeTable = {
              routing: null,
              payee_payor: null,
              bank_name: null,
              country: null
            };
          };
          $scope.single = record;
        }
      });
    };
    vm.viewPaymentHistories = function(){
      $location.url('/payment_history');
    };
    /**
     * Method to move tab to next
     * step
     */
    vm.nextStep = function () {
      var index = (vm.selectedIndex === vm.max) ? 0 : vm.selectedIndex + 1;
      vm.selectedIndex = index;
      vm.showLoader();
      var currentTab = $( '#tab_'+index);
      $('md-tab-item').each(function () {
        //console.log($(this).attr('class').split(/\s+/));
        if( $(this).hasClass('md-active') ){
          $(this).addClass('md-previous');
          $(this).prevAll().addClass('md-previous');
        }
      });
      $mdDialog.hide();
    };
    vm.previousStep = function () {
      vm.showCircleLoader = true;
      var index = (vm.selectedIndex === 0) ? 0 : vm.selectedIndex - 1;
      vm.selectedIndex = index;
      var currentTab = $( '#tab_'+index);
      $('md-tab-item').each(function () {
        //console.log($(this).attr('class').split(/\s+/));
        if( $(this).hasClass('md-active') ){
          $(this).removeClass('md-previous');
          $(this).prev().removeClass('md-previous').addClass('md-active');
        }
      });
    };
    vm.getTotalBeneficiary = function(){
      var total = 0;
      for(var i = 0; i < vm.payeeEntries.length; i++){
        var payee = vm.payeeEntries[i];
        total += parseFloat(payee.total);
      }
      return total;
    };
    vm.getTotalBeneficiaryAmount = function(){
      var total = 0;
      for(var i = 0; i < vm.payeeEntries.length; i++){
        var payee = vm.payeeEntries[i];
        total += parseFloat(payee.amount);
      }
      return total.toFixed(2);
    };
    vm.getTotalBeneficiaryFee = function(){
      var total = 0;
      for(var i = 0; i < vm.payeeEntries.length; i++){
        var payee = vm.payeeEntries[i];
        total += parseFloat(payee.fee);
      }
      return total;
    };
    vm.getSingleBeneficiary = function(payee){
      var local_payee = angular.copy(payee);
      var total = 0;
      total += +local_payee.amount;
      return total;
    };
    vm.getSingleBeneficiaryForApproval = function(payee){
      var local_payee = angular.copy(payee);
      var total = 0;
      total += ((+local_payee.amount) + (+local_payee.fee));

        //$digest or $apply
      return total;
    };
    vm.temporaryBeneficiaries = [];
    vm.estimateBeneficiary = function(){
      var totalRows = [];
      if(vm.payeeEntries.length > 0){
        for(var i=0; i<vm.payeeEntries.length; i++){
          var single = {
            currency: vm.payeeEntries[i].currency,
            amount: 0,
            count: 0,
            rate: vm.payeeEntries[i].fee,
            cost: 0
          };
          for(var j=0; j<vm.payeeEntries.length; j++){
            if(vm.payeeEntries[i].currency == vm.payeeEntries[j].currency){
              single.cost += vm.payeeEntries[j].amount * vm.payeeEntries[j].fee;
              single.amount += vm.parseFloat(vm.payeeEntries[j].amount);
              single.count++;
              single.cost = vm.parseFloat(single.cost);
            }
          }
          totalRows.push(single);
        }
      }
      vm.temporaryBeneficiaries = _.unique(totalRows, function(p){ return p.currency; });
    };
    vm.totalCost = 0;
    vm.getTotalCost = function(){
      var total = 0;
      for(var i = 0; i < vm.finalEntries.length; i++){
        var entry = vm.finalEntries[i];
        total += vm.parseFloat(entry.cost);
      }
      vm.totalCost = total;
    };
    vm.estimatePayees = function(){
      var totalRows = [];
      if(vm.payeeEntries.length > 0){
        for(var i=0; i<vm.payeeEntries.length; i++){
          var single = {
            currency: vm.payeeEntries[i].currency,
            amount: 0,
            count: 0,
            rate: vm.payeeEntries[i].fee,
            cost: 0
          };
          for(var j=0; j<vm.payeeEntries.length; j++){
            if(vm.payeeEntries[i].currency == vm.payeeEntries[j].currency){
              single.cost += vm.payeeEntries[j].amount * vm.payeeEntries[j].fee;
              single.amount += vm.parseFloat(vm.payeeEntries[j].amount);
              single.count++;
              single.cost = vm.parseFloat(single.cost);
            }
          }
          totalRows.push(single);
        }
      }
      vm.temporaryPayeeEntries = _.unique(totalRows, function(p){ return p.currency; });
    };
    vm.getTotalTemporaryPayees = function(){
      var total = 0;
      for(var i = 0; i < vm.finalEntries.length; i++){
        var beneficiary = vm.temporaryPayeeEntries[i];
        total += vm.parseFloat(beneficiary.cost);
      }
      return total;
    };
    vm.getTotalBuys = function(){
      var total = 0;
      for(var i = 0; i < vm.finalEntries.length; i++){
        var entry = vm.finalEntries[i];
        total += vm.parseFloat(entry.cost);
      }
      return total;
    };
    vm.getTotalSells = function(){
      var total = 0;
      for(var i = 0; i < vm.finalEntries.length; i++){
        var entry = vm.finalEntries[i];
        total += vm.parseFloat(entry.amount);
      }
      return total;
    };
    vm.GetRateByIDS = function(buy_currency, buy_amount, sell_currency, sell_amount, value_date, client_name){
      return soap_api.GetRateByIDS(buy_currency, buy_amount, sell_currency, sell_amount, value_date, client_name).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Rate').each(function(i,e) {
          var temp = {
            exchange_rate: jQuery(e).find('EXCHANGE_RATE').text(),
            market_rate: jQuery(e).find('MARKET_RATE').text(),
            spread_rate: jQuery(e).find('SPREAD_RATE').text(),
            actual_market_rate: jQuery(e).find('ACTUAL_MARKET_RATE').text(),
            sell_amount: jQuery(e).find('SELL_AMOUNT').text(),
            buy_amount: jQuery(e).find('BUY_AMOUNT').text()
          };
          row.push(temp);
        });
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.tempPayees = [];
    vm.tempPromises = [];
    // Options
    vm.dtOptions = {
      //dom: '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
      dom: '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"pagination"p>>><"info"i>',
      pagingType: 'simple',
      autoWidth: false,
      responsive: true
    };
    //Functions
    vm.showDialog = function (payment, ev) {
      $mdDialog.show(
        $mdDialog.alert()
          .clickOutsideToClose(true)
          .parent(angular.element(document.body))
          .title(payment.number)
          .textContent('Method: ' + payment.method)
          .ok('OK')
          .targetEvent(ev)
      );
    };
    vm.isOpen = false;
    vm.demo = {
      isOpen: false,
      count: 0,
      selectedDirection: 'left'
    };
    vm.pickBeneficiary = function (ev) {
      $mdDialog.show({
        controller: DialogController,
        templateUrl: 'app/main/payments/make_a_payment/pick_a_beneficiary.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
      })
        .then(function (answer) {
          console.log(answer);
        }, function () {
          console.log("Canceled");
        });



    };
    vm.quick_payment = function (ev) {
      $mdDialog.show({
        controller: DialogController,
        templateUrl: 'app/main/payments/make_a_payment/quick_payment.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
      })
        .then(function (answer) {
          console.log(answer);
        }, function () {
          console.log("Canceled");
        });



    };
    function convertCurrency(){
      Number.prototype.formatMoney = function(c, d, t){
        var n = this,
          c = isNaN(c = Math.abs(c)) ? 2 : c,
          d = d == undefined ? "." : d,
          t = t == undefined ? "," : t,
          s = n < 0 ? "-" : "",
          i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c)));
        var j = i.length > 3 ? i.length % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
      };
    }
    function addBalance(record){
      var singlePayee;
      var balance;
      if (record.length){
        balance = parseFloat(record.balance);
        singlePayee = record[0].balance && (record[0].name + " " + (balance).formatMoney(2, '.', ',') ) || payee_object[0].name;
        return singlePayee;
      }
      balance = parseFloat(record.balance);
      return record.type === "Accounts" && (record.name + " " + (balance).formatMoney(2, '.', ',') ) || record.name;

    }
    function DialogController($scope, $mdDialog) {
      $scope.hide = function() {
        $mdDialog.hide();
      };
      $scope.cancel = function() {
        $mdDialog.cancel();
      };
      $scope.answer = function(answer) {
        $mdDialog.hide(answer);
      };
    }
  }
})();
