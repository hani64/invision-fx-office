(function ()
{
  'use strict';

  angular
    .module('app.payments.make_a_payment', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider)
  {
    // State
    $stateProvider.state('app.make_a_payment', {
      url      : '/make_a_payment',
      views    : {
        'content@app': {
          templateUrl: 'app/main/payments/make_a_payment/make_a_payment.html',
          controller : 'MakeAPaymentController as vm',
          resolve: {
            access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
          },
        }
      }
    });

    // Api

  }

})();
