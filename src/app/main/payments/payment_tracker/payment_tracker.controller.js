(function () {
  'use strict';

  angular
    .module('app.payments.payment_tracker')
    .controller('PaymentTrackerController', PaymentTrackerController);

  /** @ngInject */
  function PaymentTrackerController($rootScope, NgTableParams, dateRangeFactory, $filter, $window, $scope, $mdDialog, api, XmlJson) {
    var vm = this;

    $rootScope.headerTitle = 'Payment Tracker';
    vm.showDataTable = true;
    vm.trackers = [];
    vm.enableFilter = false;
    vm.filter = {};
    vm.toggleFilters = function(){
      vm.enableFilter = !vm.enableFilter;
    };
    vm.filterForTable = function(data, filterValues){
      if(vm.filter){
        return data.filter(function(item){
          return (item.deal_number.toLowerCase().indexOf(filterValues.deal_number || '') !== -1) &&
            (item.sell_amount.toString().toLowerCase().indexOf(filterValues.sell_amount || '') !== -1) &&
            (item.beneficiary_name.toLowerCase().indexOf(filterValues.beneficiary_name || '') !== -1);
        });
      }else{
        return data;
      }
    };
    vm.filterAll = function(){
      vm.tableParams.filter({
        deal_number: vm.filter.deal_no,
        sell_amount: vm.filter.amount,
        sell_currency : vm.filter.currency,
        beneficiary_name: vm.filter.payee,
        is_post: (vm.filter.posted === false ? null: true),
        is_paid: (vm.filter.paid === false ? null: true),
        is_finished: (vm.filter.finished === false ? null: true),
        is_settled: (vm.filter.sent === false ? null: true)
      });
    };
    vm.dateRangeFilter = function(){
      var Data = dateRangeFactory.filterData(vm.trackers, vm.filter.date_range);
      vm.tableParams = new NgTableParams({}, {
        counts: [5, 10, 20, 50, 100],
        dataset : Data
      });
      updatePageCount();
    };
    function updatePageCount(){
      return Math.ceil(vm.tableParams.total()/vm.tableParams.count());
    }
    vm.searchAmount = function(text){
      search(text, 'sell_amount');
      if(vm.filter.currency){
        search(vm.filter.currency.toLowerCase(), 'sell_currency');
      }
    };
    vm.searchCurrency = function (text) {
      search(text, 'sell_currency');
      if(vm.filter.amount){
        search(vm.filter.amount, 'sell_amount');
      }
    };
    function search(text, field){
      text = field === 'sell_currency' && text.toLowerCase() || text;
      var trackRecords =  vm.filteredData  || vm.trackers;
      if(text === null || text === ""){
        updateTrackers(trackRecords);
        return;
      }
      var _searchRegex = searchRegex(text);

      var trackers = trackRecords.map(function(elem){
        if(typeof elem[field] === 'number'){
          elem[field] =  elem[field].toString();
        }
        else{
          elem[field] =  elem[field].toLowerCase();
        }
        return elem;
      }).filter(function(elem){

        if(vm.filter.amount && vm.filter.currency){
          var sell_amount = searchRegex(vm.filter.amount);
          var sell_currency = searchRegex(vm.filter.currency);
          return elem.sell_amount.toString().trim().match(sell_amount) && elem.sell_currency.toLowerCase().trim().match(sell_currency);
        }
        return elem[field].trim().match(_searchRegex);
      });

      updateTrackers(trackers);
    }
    vm.filter.posted = false;
    vm.filter.paid = false;
    vm.filter.finished = false;
    vm.filter.sent = false;

    vm.statusFilter = function() {

      if (vm.filter.posted && !vm.filter.paid  && !vm.filter.finished && !vm.filter.sent)
      { //TFFF
        vm.filteredData  = filterTrackersByStatus('is_post');
      }
      else if (!vm.filter.posted && vm.filter.paid  && !vm.filter.finished  && !vm.filter.sent)
      { //FTFF
        vm.filteredData  = filterTrackersByStatus('is_paid');
      }
      else if (!vm.filter.posted && !vm.filter.paid  && vm.filter.finished && !vm.filter.sent)
      { //FFTF
        vm.filteredData  = filterTrackersByStatus('is_finished');
      }
      else if (!vm.filter.posted && !vm.filter.paid  && !vm.filter.finished && vm.filter.sent)
      { //FFFT
        vm.filteredData  = filterTrackersByStatus('is_settled');
      }

      else if (vm.filter.posted && vm.filter.paid  && !vm.filter.finished && !vm.filter.sent)
      { //TTFF
        vm.filteredData  = filterTrackersByStatus('is_post', 'is_paid');
      }
      else if (!vm.filter.posted && vm.filter.paid  && !vm.filter.finished && vm.filter.sent)
      {
        vm.filteredData  = filterTrackersByStatus('is_settled', 'is_paid');
      }

      else if (!vm.filter.posted && !vm.filter.paid  && vm.filter.finished && vm.filter.sent)
      { //TTFF
        vm.filteredData  = filterTrackersByStatus('is_finished', 'is_settled');
      }
      else if (vm.filter.posted && vm.filter.paid  && vm.filter.finished  && !vm.filter.sent)
      { //TTTF
        vm.filteredData  = filterTrackersByStatus('is_post', 'is_paid', 'is_finished');
      }
      else if (!vm.filter.posted  && vm.filter.paid  && vm.filter.finished && vm.filter.sent)
      { //FTTT
        vm.filteredData  = filterTrackersByStatus('is_paid', 'is_finished', 'is_settled');
      }
      else if (vm.filter.posted && !vm.filter.paid && vm.filter.finished  && vm.filter.sent)
      { //TFTT
        vm.filteredData  = filterTrackersByStatus('is_post', 'is_finished', 'is_settled');
      }
      else if (vm.filter.posted && !vm.filter.paid  && !vm.filter.finished && vm.filter.sent)
      { //TFTT
        vm.filteredData  = filterTrackersByStatus('is_post', 'is_settled');
      }
      else if (vm.filter.posted && vm.filter.paid && !vm.filter.finished && vm.filter.sent)
      { //TFTT
        vm.filteredData  = filterTrackersByStatus('is_post', 'is_paid', 'is_settled');
      }
      else if (!vm.filter.posted && vm.filter.paid && vm.filter.finished  && !vm.filter.sent)
      { //TFTT
        vm.filteredData  = filterTrackersByStatus('is_paid', 'is_finished');
      }
      else if (vm.filter.posted === true && vm.filter.paid === false && vm.filter.finished === true && vm.filter.sent === false)
      { //TFTT
        vm.filteredData  = filterTrackersByStatus('is_post', 'is_finished');
        // vm.filteredData = $filter('filter')(vm.trackers, function (value, index, array) {
        //   return value.is_post === true && value.is_finished === true;
        // });
      }
      else if (vm.filter.posted === true && vm.filter.paid === true && vm.filter.finished === true && vm.filter.sent === true)
      { //TTTT
        vm.filteredData  = filterTrackersByStatus('is_post', 'is_paid', 'is_finished', 'is_settled');
        // vm.filteredData = $filter('filter')(vm.trackers, function (value, index, array) {
        //   return value.is_post === true && value.is_paid === true && value.is_finished === true && value.is_settled === true;
        // });
      }
      else
      { //FFFF
        vm.filteredData = vm.trackers;
        debugger;
      }

      updateTrackers(vm.filteredData)
    }
    function filterTrackersByStatus(status1, status2, status3, status4)
    {
     return vm.trackers.filter(filterStatus);
      function filterStatus(elem)
      {
        if(status1 && !status2 && !status3 && !status4)
        {
          return elem[status1] === 'true';
        }
        else if(status1 && status2 && !status3 && !status4)
        {
          return elem[status1] === 'true' && elem[status2] === 'true';
        }
        else if(status1 && status2 && status3 && !status4)
        {
          return elem[status1] === 'true' && elem[status2] === 'true' && elem[status3];
        }
        else if(status1 && status2 && status3 && status4)
        {
          return elem[status1] === 'true' && elem[status2] === 'true' && elem[status3] && elem[status4];
        }

      }
      // vm.filteredData = $filter('filter')(vm.trackers, function (value, index, array) {
      //   return value.is_post === true;
      // });
    }
    function searchRegex(text){
      var search = RegExp("^"+ text +"");
      return search;
    }
    function updateTrackers(trackers){
      vm.tableParams = new NgTableParams({}, {
        counts: [5, 10, 20, 50, 100],
        dataset : trackers,
        filterOptions: {
          filterFn: vm.filterForTable
        }
      });
      updatePageCount();
    }
    api.GetPaymentTracker().then(function(data){
      var payments =  convertDataToJson(data.data, 'payment');
      vm.showDataTable = false;
      vm.trackers = payments;
      vm.tableParams = new NgTableParams({}, {
        counts: [5, 10, 20, 50, 100],
        dataset : vm.trackers,
        filterOptions: {
          filterFn: vm.filterForTable
        }

      });
    });
    function convertDataToJson(xml, name)
    {
      var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
      var actualJson = XmlJson.xmlToJson(xmlDOM);
      return actualJson.output[name];
    }
  }
})();
