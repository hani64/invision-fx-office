(function ()
{
  'use strict';

  angular
    .module('app.payments.payment_tracker', [
      'datatables.columnfilter',
      'datatables.bootstrap',
      'datatables.colreorder',
      'datatables.fixedcolumns',
      'datatables.light-columnfilter'])
    .config(config);

  /** @ngInject */
  function config($stateProvider)
  {
    // State
    $stateProvider.state('app.payment_tracker', {
      url      : '/payment_tracker',
      views    : {
        'content@app': {
          templateUrl: 'app/main/payments/payment_tracker/payment_tracker.html',
          controller : 'PaymentTrackerController as vm',
          resolve: {
            access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
          },
        }
      }
    });
  }

})();
