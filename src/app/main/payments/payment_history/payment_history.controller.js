(function () {
  'use strict';

  angular
    .module('app.payments.payment_history')
    .controller('PaymentHistoryController', PaymentHistoryController);

  /** @ngInject */
  function PaymentHistoryController($window, $mdToast, $rootScope, NgTableParams, $scope, $mdDialog ,DTOptionsBuilder, DTColumnDefBuilder, DTColumnBuilder, dateRangeFactory, soap_api, $q, $filter, $compile) {
    var vm = this;

    $rootScope.headerTitle = 'Payment History';
    vm.circleLoader = false;
    vm.showDataTable = true;
    vm.printLoader = false;
    vm.histories = [];
    vm.enableFilter = false;
    vm.ready = false;
    vm.filter = {
      global_search: null,
      date_range: null,
      payee: null,
      deal_number: null,
      payment_method: null,
      buy_amount: null,
      exchange_rate: null,
      sell_amount: null
    };
    vm.from_date = new Date(new Date().setFullYear( new Date().getFullYear() - 10 ));
    vm.to_date = new Date(new Date().setFullYear( new Date().getFullYear() + 2 ));
    vm.isXML = function(xml){
      try{
        var xmlDoc = jQuery.parseXML(xml);
        return true;
      }catch(error){
        return false;
      }
    };
    vm.replaceWith = function(string, separator){
      string = string || '';
      return string.replace(/ +/g, separator);
    };
    vm.protectAmount = function(event){
      var input = event.key;
      var re = /\d/i;
      if(input != 'Backspace' && input != '.' && input != 'Delete' && input != 'Del' && input != 'Decimal' && input != 'Left' && input != 'Right' && input != 'Tab' && input != 'ArrowLeft' && input != 'ArrowRight'){
        if(input.match(re) == null){
          event.preventDefault();
          return false;
        }
      }else{
        if(input == '.' || input == 'Decimal'){
          re = /\./g;
          if(event.target.value.length > 0){
            if(event.target.value.match(/\./g) != null){
              if(event.target.value.match(/\./g).length > 0){
                event.preventDefault();
                return false;
              }
            }
          }
        }
      }
    };
    vm.floor = function(expression){
      return Math.floor(expression);
    };
    vm.parseBoolean = function(str){
      return str == 'true' ? true: false;
    };
    vm.onlyWords = function(event){
      var input = event.key;
      var re = /^[a-zA-Z\s]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.parseFloat = function(number){
      return parseFloat(number);
    };
    vm.parseDate = function(input){
      if(typeof input !== 'undefined' && input !== null && input !== ''){
        var parts = input.split('-');
        return new Date(parts[2], parts[1]-1, parts[0]);
      }else{
        return input;
      }
    };
    vm.toLowerCase = function(str){
      str = str || '';
      return str.toLowerCase();
    };
    vm.toUpperCase = function(str){
      str = str || '';
      return str.toUpperCase();
    };
    vm.elementInViewport = function elementInViewport(el) {
      var top = el.offsetTop;
      var left = el.offsetLeft;
      var width = el.offsetWidth;
      var height = el.offsetHeight;

      while(el.offsetParent) {
        el = el.offsetParent;
        top += el.offsetTop;
        left += el.offsetLeft;
      }

      return (
        top < (window.pageYOffset + window.innerHeight) &&
        left < (window.pageXOffset + window.innerWidth) &&
        (top + height) > window.pageYOffset &&
        (left + width) > window.pageXOffset
      );
    };
    vm.noSpecialCharactersFunction = function(event){
      var input = event.key;
      var re = /^[a-zA-Z0-9\-\s]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.toggleFilters = function(){
      vm.filter = {
        global_search: null,
        date_range: null,
        payee: null,
        deal_number: null,
        payment_method: null,
        buy_amount: null,
        exchange_rate: null,
        sell_amount: null
      };
      vm.enableFilter = !vm.enableFilter;
    };
    vm.b64toBlob = function (b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    };

    //API methods
    vm.GetAllElectronicMethods = function(){
      return soap_api.GetAllElectronicMethods().then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            is_template: jQuery(e).find('IS_TEMPLATE').text(),
            is_routing_type: jQuery(e).find('IS_ROUTING_TYPE').text(),
            is_bank_code_enabled: jQuery(e).find('IS_BANK_CODE_ENABLED').text(),
            is_electronic: jQuery(e).find('IS_ELECTRONIC').text(),
            is_bank_code_required: jQuery(e).find('IS_BANK_CODE_REQUIRED').text()
          };
          row.push(temp);
        });
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.GetAllElectronicMethods().then(function(data){
      vm.allElectronicMethods = data;
    });
    vm.GetWireReport = function(settlement_id){
      return soap_api.GetWireReport(settlement_id).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function(i,e){
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              recordid: jQuery(e).find('RECORDID').text(),
              error: jQuery(e).find('ERROR').text(),
              value: jQuery(e).find('VALUE').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetPaymentHistory = function(beneficiary_name, from_date, to_date, method){
      return soap_api.GetPaymentHistory(beneficiary_name, from_date, to_date, method).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT PAYMENT').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              settlement_id: jQuery(e).find('SETTLEMENT_ID').text(),
              beneficiary_name: jQuery(e).find('BENEFICIARY_NAME').text(),
              dealnumber: jQuery(e).find('DEALNUMBER').text(),
              value_date: jQuery(e).find('VALUE_DATE').text(),
              method_name: jQuery(e).find('METHOD_NAME').text(),
              method_id: jQuery(e).find('METHOD_ID').text(),
              currencyname: jQuery(e).find('CURRENCYNAME').text(),
              amount: jQuery(e).find('AMOUNT').text(),
              exchange_rate: jQuery(e).find('EXCHANGE_RATE').text(),
              sell_currency: jQuery(e).find('SELL_CURRENCY').text(),
              sell_amount: jQuery(e).find('SELL_AMOUNT').text(),
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetPaymentHistory('', vm.from_date, vm.to_date, '').then(function(data){
      for(var i=0; i<data.length; i++){
        if(data[i].value_date == ''){
          data[i].value_date = null;
        }else{
          data[i].value_date = new Date(Date.parse(data[i].value_date));
        }
        data[i].sell_amount = Math.round(vm.parseFloat(data[i].sell_amount));
        data[i].amount = vm.parseFloat(data[i].amount);
        data[i].exchange_rate = vm.parseFloat(data[i].exchange_rate);
        data[i].remarks = '';
        data[i].hide_print = true;
        for(var j=0; j<vm.allElectronicMethods.length; j++){
          if(vm.toLowerCase(data[i].method_id) == vm.toLowerCase(vm.allElectronicMethods[j].id)){
            data[i].hide_print = false;
          }
        }
        vm.histories.push(data[i]);
      }
      vm.circleLoader = true;
      vm.showDataTable = false;
      vm.ready = true;
      vm.filterForTable = function(data, filterValues){
        if(vm.filter){
          return data.filter(function(item){
            return (item.beneficiary_name.toLowerCase().indexOf(filterValues.beneficiary_name || '') !== -1) &&
              (item.dealnumber.toLowerCase().indexOf(filterValues.dealnumber || '') !== -1) &&
              (item.method_name.toLowerCase().indexOf(filterValues.method_name || '') !== -1) &&
              (item.amount.toString().toLowerCase().indexOf(filterValues.amount || '') !== -1) &&
              (item.sell_amount.toString().toLowerCase().indexOf(filterValues.sell_amount || '') !== -1) &&
              (item.exchange_rate.toString().toLowerCase().indexOf(filterValues.exchange_rate || '') !== -1);
          });
        }else{
          return data;
        }
      };
      vm.tableParams = new NgTableParams({}, {
        counts: [5, 10, 20, 50, 100],

        dataset : vm.histories,
        filterOptions: {
          filterFn: vm.filterForTable
        }
      });
      vm.searchFilterFunction = function(){
        vm.tableParams.filter({ $: vm.filter.global_search })
      };
      vm.dateRangeFilter = function(){
        var filteredData = dateRangeFactory.filterData(vm.histories, vm.filter.date_range);
        vm.tableParams = new NgTableParams({}, {
          counts: [5, 10, 20, 50, 100],
          dataset : filteredData
        });
        updatePageCount();
      };
      // vm.exchaneRateFilter = function(){
      //   var exchangeRate = vm.filter.exchange_rate;
      //   var thisExpressions = [/ + exchangeRate  +/];
      //   var filteredData = vm.histories.filter(function(elem){
      //
      //     return elem.exchange_rate.match(thisExpressions[0]);
      //   });
      //   vm.tableParams = new NgTableParams({}, {
      //     counts: [5, 10, 20, 50, 100],
      //     dataset : filteredData.length === 0 && vm.histories || filteredData
      //   });
      //
      // }
      vm.searchExchange = function(text){
        if(text === null || text === ""){
          updateHistories(vm.histories);
          return;
        }
        var search = RegExp("^"+ text +"");
        var history = vm.histories.map(function(elem){
            elem.exchange_rate =  elem.exchange_rate.toString();
            return elem;
          }).filter(function(elem){
              return elem.exchange_rate.trim().match(search);
        });
        updateHistories(history);
      }
      function updateHistories(histories){
        // vm.histories = exactMatch
        vm.tableParams = new NgTableParams({}, {
          counts: [5, 10, 20, 50, 100],

          dataset : histories,
          filterOptions: {
            filterFn: vm.filterForTable
          }
        });
        updatePageCount();
      };
      vm.moveFirstToLast = function(firstOrLast){
        if(firstOrLast === 'last')
        {
          var lastPage = updatePageCount();
          vm.tableParams.page(lastPage);
          return;
        }
        vm.tableParams.page(1);
      };
      function updatePageCount(){
        return Math.ceil(vm.tableParams.total()/vm.tableParams.count());
      }
      vm.filterAll = function(){
        console.log(vm.tableParams)
        vm.tableParams.filter({
          beneficiary_name: vm.filter.payee,
          dealnumber: vm.filter.deal_number,
          method_name: vm.filter.payment_method,
          amount: vm.filter.buy_amount,
          // exchange_rate: vm.filter.exchange_rate,
          sell_amount: vm.filter.sell_amount
        });
      };
      vm.printRow = function(data){
        vm.printLoader = true;
        vm.GetWireReport(data.settlement_id).then(function(data){
          data = data[0];
          vm.printLoader = false;
          if(data.status == 'TRUE'){
            var blob = vm.b64toBlob(data.value, 'application/pdf');
            saveAs(blob, data.recordid);
          }else{
            $mdToast.show({
              template: '<md-toast class="md-toast error">No report found.</md-toast>',
              hideDelay: 3000,
              position: 'bottom right'
            });
          }
        });
      };
    });
  }
})();
