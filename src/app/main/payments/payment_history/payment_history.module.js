(function ()
{
  'use strict';

  angular
    .module('app.payments.payment_history', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider)
  {
    // State
    $stateProvider.state('app.payment_history', {
      url      : '/payment_history',
      views    : {
        'content@app': {
          templateUrl: 'app/main/payments/payment_history/payment_history.html',
          controller : 'PaymentHistoryController as vm',
          resolve: {
            access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
          },
        }
      }
    });
  }

})();
