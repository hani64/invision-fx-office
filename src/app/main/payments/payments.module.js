(function () {
  'use strict';

  angular
    .module('app.payments', [
      'app.payments.make_a_payment',
      'app.payments.payment_tracker',
      'app.payments.payment_history',
      'app.payments.pay_from_your_account',
    ])
    .config(config);

  /** @ngInject */
  function config(msNavigationServiceProvider) {

    // Navigation
    msNavigationServiceProvider.saveItem('main_menu', {
      title: 'MAIN MENU',
      group: true,
      weight: 1
    });

    msNavigationServiceProvider.saveItem('main_menu.payments', {
      title: 'Payments',
      icon: 'icon-cash-usd',
      weight: 1,
      child_count: 5
    });

    msNavigationServiceProvider.saveItem('main_menu.payments.payment_tracker', {
      title: 'Payment Tracker',
      state: 'app.payment_tracker',
      weight: 1,
      module_id: 13504
    });


    msNavigationServiceProvider.saveItem('main_menu.payments.payment_history', {
      title: 'Payment History',
      state: 'app.payment_history',
      weight: 1,
      module_id: 13508
    });


    msNavigationServiceProvider.saveItem('main_menu.payments.make_payment', {
      title: 'Make a Payment',
      state: 'app.make_a_payment',
      weight: 1,
      module_id: 12280
    });


    msNavigationServiceProvider.saveItem('main_menu.payments.file_transfer_facility', {
      title: 'File Transfer Facility',
      state: 'app.file_transfer_facility',
      weight: 1,
      module_id: 13439
    });

    msNavigationServiceProvider.saveItem('main_menu.payments.pay_from_your_account', {
      title: 'Pay From Your Account',
      state: 'app.pay_from_your_account',
      weight: 1,
      module_id: 364
    });
  }
})();
