// (function () {
//
//
// ()});
(function(){
  'use strict';

  angular
    .module('app.payments.pay_from_your_account')
    .controller('PayFromYourAccountController', PayFromYourAccountController);

  /** @ngInject */
  function PayFromYourAccountController($state, $interval, $rootScope, $mdToast, $scope, $mdDialog, soap_api, api, $filter) {
    var vm = this;

    vm.module_id = 364;
    $rootScope.headerTitle = 'Pay From Your Account';
    var accnt, benefic;
    vm.loader = true;
    vm.selectedIndex = 0;
    vm.max = 3;
    vm.activated = true;
    vm.beneficiaryTable = true;
    vm.entityTable = true;
    vm.getQuoteAgainBool = true;
    vm.temp_account = null;
    vm.temp_beneficiary = null;
    vm.hidePurpose = false;
    vm.currencies = [];
    vm.step1disable = false;
    vm.step1Invalid = false;
    vm.skipQuote = false;
    vm.show_summary = false;
    vm.disableMe = false;
    vm.disableQuoteBtn = false;
    vm.hideQuoteBox = false;
    vm.entityAccounts = [];
    vm.beneficiaryAccounts = [];
    vm.convertName = convertObjects;
    function convertObjects(objectOrStringValue){

      if(typeof objectOrStringValue === 'object')
      {
        return objectOrStringValue['#text'].join(" ");
      }
      return objectOrStringValue
    }
    //API methods
    vm.GetTransferAccountFrom = function (module) {
      return soap_api.GetTransferAccountFrom(module).then(function (success) {
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function (i, e) {
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            type: jQuery(e).find('TYPE').text(),
            is_non_third_party: jQuery(e).find('IS_NON_THIRD_PARTY').text(),
            payment_type: jQuery(e).find('PAYMENT_TYPE').text(),
            act_seg_id: jQuery(e).find('ACT_SEG_ID').text(),
            paymentmethod: jQuery(e).find('PAYMENTMETHOD').text(),
            is_deposit: jQuery(e).find('IS_DEPOSIT').text(),
            is_withdrawal: jQuery(e).find('IS_WITHDRAWAL').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            beneficiary_accountcode: jQuery(e).find('BENEFICIARY_ACCOUNTCODE').text(),
            country_name: jQuery(e).find('COUNTRY_NAME').text(),
            internal_reference: jQuery(e).find('INTERNAL_REFERENCE').text(),
            beneficiarybank_name: jQuery(e).find('BENEFICIARYBANK_NAME').text(),
            beneficiarybank_address1: jQuery(e).find('BENEFICIARYBANK_ADDRESS1').text(),
            beneficiarybank_address2: jQuery(e).find('BENEFICIARYBANK_ADDRESS2').text(),
            beneficiarybank_city: jQuery(e).find('BENEFICIARYBANK_CITY').text(),
            beneficiarybank_province: jQuery(e).find('BENEFICIARYBANK_PROVINCE').text(),
            beneficiarybank_country: jQuery(e).find('BENEFICIARYBANK_COUNTRY').text(),
            balance: jQuery(e).find('BALANCE').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.GetTransferAccountTo = function (module) {
      return soap_api.GetTransferAccountTo(module).then(function (success) {
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function (i, e) {
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            type: jQuery(e).find('TYPE').text(),
            is_non_third_party: jQuery(e).find('IS_NON_THIRD_PARTY').text(),
            payment_type: jQuery(e).find('PAYMENT_TYPE').text(),
            act_seg_id: jQuery(e).find('ACT_SEG_ID').text(),
            paymentmethod: jQuery(e).find('PAYMENTMETHOD').text(),
            is_deposit: jQuery(e).find('IS_DEPOSIT').text(),
            is_withdrawal: jQuery(e).find('IS_WITHDRAWAL').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            beneficiary_accountcode: jQuery(e).find('BENEFICIARY_ACCOUNTCODE').text(),
            country_name: jQuery(e).find('COUNTRY_NAME').text(),
            internal_reference: jQuery(e).find('INTERNAL_REFERENCE').text(),
            beneficiarybank_name: jQuery(e).find('BENEFICIARYBANK_NAME').text(),
            purpose_of_payment_id: jQuery(e).find('PURPOSE_OF_PAYMENT_ID').text(),
            beneficiarybank_address1: jQuery(e).find('BENEFICIARYBANK_ADDRESS1').text(),
            beneficiarybank_address2: jQuery(e).find('BENEFICIARYBANK_ADDRESS2').text(),
            beneficiarybank_city: jQuery(e).find('BENEFICIARYBANK_CITY').text(),
            beneficiarybank_province: jQuery(e).find('BENEFICIARYBANK_PROVINCE').text(),
            beneficiarybank_country: jQuery(e).find('BENEFICIARYBANK_COUNTRY').text(),
            balance: jQuery(e).find('BALANCE').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.GetDefaultDate = function () {
      return soap_api.GetDefaultDate().then(function (success) {
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT RESULT').each(function (i, e) {
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.GetUserApprovalRights = function (deal, module) {
      return soap_api.GetUserApprovalRights(deal, module).then(function (success) {
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT RESULT').each(function (i, e) {
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.GetSavedApprovalsByModule = function (module) {
      return soap_api.GetSavedApprovalsByModule(module).then(function (success) {
        var row = [];
        if (vm.isXML(success.data)) {
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT ADM_WEB_APPROVAL_POLICY').each(function (i, e) {
            var temp = {
              id: jQuery(e).find('ID').text(),
              entity_name: jQuery(e).find('ENTITY_NAME').text(),
              entity_id: jQuery(e).find('ENTITY_ID').text(),
              module_id: jQuery(e).find('MODULE_ID').text(),
              approval_level: jQuery(e).find('APPROVAL_LEVEL').text(),
              created_on: jQuery(e).find('CREATED_ON').text(),
              created_by: jQuery(e).find('CREATED_BY').text(),
              updated_on: jQuery(e).find('UPDATED_ON').text(),
              updated_by: jQuery(e).find('UPDATED_BY').text(),
              is_deleted: jQuery(e).find('IS_DELETED').text(),
              is_approved: jQuery(e).find('IS_APPROVED').text(),
              version_no: jQuery(e).find('VERSION_NO').text()
            };
            row.push(temp);
          });
          jQuery(response).find('OUTPUT ADM_WEB_USER_APPROVAL').each(function (i, e) {
            var temp = {
              id: jQuery(e).find('ID').text(),
              approval_policy_id: jQuery(e).find('APPROVAL_POLICY_ID').text(),
              user_id: jQuery(e).find('USER_ID').text(),
              user_name: jQuery(e).find('USER_NAME').text(),
              approval_type_id: jQuery(e).find('APPROVAL_TYPE_ID').text(),
              approval_type_name: jQuery(e).find('APPROVAL_TYPE_NAME').text(),
              created_on: jQuery(e).find('CREATED_ON').text(),
              created_by: jQuery(e).find('CREATED_BY').text(),
              updated_on: jQuery(e).find('UPDATED_ON').text(),
              updated_by: jQuery(e).find('UPDATED_BY').text(),
              is_deleted: jQuery(e).find('IS_DELETED').text(),
              is_approved: jQuery(e).find('IS_APPROVED').text(),
              version_no: jQuery(e).find('VERSION_NO').text()
            };
            row.push(temp);
          });
        } else {
          console.log(success.data);
        }
        return row;
      });
    };
    vm.GetApprovalHistory = function (deal) {
      return soap_api.GetApprovalHistory(deal).then(function (success) {
        var row = [];
        if (vm.isXML(success.data)) {
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT APPROVAL_HISTORY').each(function (i, e) {
            var temp = {
              trans_approval_id: jQuery(e).find('TRANS_APPROVAL_ID').text(),
              org_approval_id: jQuery(e).find('ORG_APPROVAL_ID').text(),
              transaction_id: jQuery(e).find('TRANSACTION_ID').text(),
              user_id: jQuery(e).find('USER_ID').text(),
              user_name: jQuery(e).find('USER_NAME').text(),
              approval_num: jQuery(e).find('APPROVAL_NUM').text(),
              approval_date: jQuery(e).find('APPROVAL_DATE').text(),
              approval_level: jQuery(e).find('APPROVAL_LEVEL').text(),
              created_on: jQuery(e).find('CREATED_ON').text(),
              created_by: jQuery(e).find('CREATED_BY').text(),
              updated_on: jQuery(e).find('UPDATED_ON').text(),
              updated_by: jQuery(e).find('UPDATED_BY').text(),
              version_no: jQuery(e).find('VERSION_NO').text(),
              is_deleted: jQuery(e).find('IS_DELETED').text()
            };
            row.push(temp);
          });
        } else {
          console.log(success.data);
        }
        return row;
      });
    };
    vm.SetDealApprovalWithID = function (deal) {
      return soap_api.SetDealApprovalWithID(deal).then(function (success) {
        var row = [];
        if (vm.isXML(success.data)) {
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function (i, e) {
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              error: jQuery(e).find('ERROR').text()
            };
            row.push(temp);
          });
        } else {
          console.log(success.data);
        }
        return row;
      });
    };
    vm.ValidateBusinessDate = function (date) {
      return soap_api.ValidateBusinessDate(date).then(function (success) {
        if (vm.isXML(success.data)) {
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function (i, e) {
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              recordid: jQuery(e).find('RECORDID').text(),
              error: jQuery(e).find('ERROR').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetEntity = function () {
      return soap_api.GetEntity().then(function (success) {
        var row = [];
        var temp1 = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT ENTITY').each(function (i, e) {
          var temp = {
            name: jQuery(e).find('NAME').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            entity_no: jQuery(e).find('ENTITY_NO').text(),
            entity_type_id: jQuery(e).find('ENTITY_TYPE_ID').text(),
            type_name: jQuery(e).find('TYPE_NAME').text(),
            status: jQuery(e).find('STATUS').text(),
            value: jQuery(e).find('VALUE').text(),
            entity_class_id: jQuery(e).find('ENTITY_CLASS_ID').text(),
            name1: jQuery(e).find('NAME1').text(),
            entity_org_id: jQuery(e).find('ENTITY_ORG_ID').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            relationship_manager_id: jQuery(e).find('RELATIONSHIP_MANAGER_ID').text(),
            relationship_manager_name: jQuery(e).find('RELATIONSHIP_MANAGER_NAME').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            version_no: jQuery(e).find('VERSION_NO').text(),
            is_deleted1: jQuery(e).find('IS_DELETED1').text()
          };
          temp1['entity'] = temp;
        });
        jQuery(response).find('OUTPUT ADDRESS').each(function (i, e) {
          var temp = {
            id: jQuery(e).find('ID').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            parent_type: jQuery(e).find('PARENT_TYPE').text(),
            address_type: jQuery(e).find('ADDRESS_TYPE').text(),
            address_type_value: jQuery(e).find('ADDRESS_TYPE_VALUE').text(),
            priority: jQuery(e).find('PRIORITY').text(),
            street_address: jQuery(e).find('STREET_ADDRESS').text(),
            post_code: jQuery(e).find('POST_CODE').text(),
            city: jQuery(e).find('CITY').text(),
            province: jQuery(e).find('PROVINCE').text(),
            countryid: jQuery(e).find('COUNTRYID').text(),
            country_value: jQuery(e).find('COUNTRY_VALUE').text(),
            is_default: jQuery(e).find('IS_DEFAULT').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            is_approved: jQuery(e).find('IS_APPROVED').text(),
            is_web_approved: jQuery(e).find('IS_WEB_APPROVED').text(),
            version_no: jQuery(e).find('VERSION_NO').text(),
            country_code: jQuery(e).find('COUNTRY_CODE').text()
          };
          temp1['address'] = temp;
        });
        jQuery(response).find('OUTPUT PHONE').each(function (i, e) {
          var temp = {
            id: jQuery(e).find('ID').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            parent_type: jQuery(e).find('PARENT_TYPE').text(),
            phone_type: jQuery(e).find('PHONE_TYPE').text(),
            phone_type_value: jQuery(e).find('PHONE_TYPE_VALUE').text(),
            phone_number: jQuery(e).find('PHONE_NUMBER').text(),
            priority: jQuery(e).find('PRIORITY').text(),
            is_default: jQuery(e).find('IS_DEFAULT').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            is_approved: jQuery(e).find('IS_APPROVED').text(),
            version_no: jQuery(e).find('VERSION_NO').text(),
            is_web_approved: jQuery(e).find('IS_WEB_APPROVED').text()
          };
          temp1['phone'] = temp;
        });
        var temp2 = [];
        jQuery(response).find('OUTPUT WEB').each(function (i, e) {
          var temp = {
            id: jQuery(e).find('ID').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            PARENT_TYPE_VALUE: jQuery(e).find('PARENT_TYPE_VALUE').text(),
            PARENT_TYPE: jQuery(e).find('PARENT_TYPE').text(),
            WEB_TYPE: jQuery(e).find('WEB_TYPE').text(),
            WEB_TYPE_VALUE: jQuery(e).find('WEB_TYPE_VALUE').text(),
            WEB_ADDRESS: jQuery(e).find('WEB_ADDRESS').text(),
            PRIORITY: jQuery(e).find('PRIORITY').text(),
            IS_DEFAULT: jQuery(e).find('IS_DEFAULT').text(),
            CREATED_ON: jQuery(e).find('CREATED_ON').text(),
            CREATED_BY: jQuery(e).find('CREATED_BY').text(),
            UPDATED_ON: jQuery(e).find('UPDATED_ON').text(),
            UPDATED_BY: jQuery(e).find('UPDATED_BY').text(),
            IS_DELETED: jQuery(e).find('IS_DELETED').text(),
            IS_APPROVED: jQuery(e).find('IS_APPROVED').text(),
            VERSION_NO: jQuery(e).find('VERSION_NO').text(),
            IS_WEB_APPROVED: jQuery(e).find('IS_WEB_APPROVED').text()
          };
          temp2.push(temp);
        });
        temp1['web'] = temp2;
        row.push(temp1);
        return row;
      }, function (error) {
        console.error(error.statusText);
      });
    };
    vm.GetEntityWebDefaults = function () {
      return soap_api.GetEntityWebDefaults().then(function (success) {
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT WEB_DEFAULT').each(function (i, e) {
          var temp = {
            ID: jQuery(e).find('ID').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            allow_forward_deal: jQuery(e).find('ALLOW_FORWARD_DEAL').text(),
            trader_id: jQuery(e).find('TRADER_ID').text(),
            ordertype_id: jQuery(e).find('ORDERTYPE_ID').text(),
            funding_method_id: jQuery(e).find('FUNDING_METHOD_ID').text(),
            is_single_ccy_fee: jQuery(e).find('IS_SINGLE_CCY_FEE').text(),
            fee_cur_id: jQuery(e).find('FEE_CUR_ID').text(),
            fundingpaymentmethod: jQuery(e).find('FundingPaymentMethod').text(),
            payment_method_id: jQuery(e).find('PAYMENT_METHOD_ID').text(),
            paymentmethod: jQuery(e).find('PaymentMethod').text(),
            funding_template_id: jQuery(e).find('FUNDING_TEMPLATE_ID').text(),
            fundingtemplatename: jQuery(e).find('FundingTemplateName').text(),
            payment_template_id: jQuery(e).find('PAYMENT_TEMPLATE_ID').text(),
            paymenttemplatename: jQuery(e).find('PaymentTemplateName').text(),
            funding_cur_id: jQuery(e).find('FUNDING_CUR_ID').text(),
            fundingcurrency: jQuery(e).find('FundingCurrency').text(),
            payment_cur_id: jQuery(e).find('PAYMENT_CUR_ID').text(),
            paymentcurrency: jQuery(e).find('PaymentCurrency').text(),
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.CreateWebDealWithSettlementAndPayments = function (client, trader, notes, internal_remarks, value_date, is_bach, deal_transaction, payments, settlements) {
      return soap_api.CreateWebDealWithSettlementAndPayments(client, trader, notes, internal_remarks, value_date, is_bach, deal_transaction, payments, settlements).then(function (success) {
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function (i, e) {
          var temp = {
            id: jQuery(e).find('ID').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text()
          };
          row.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function (i, e) {
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text(),
            recordnumber: jQuery(e).find('RECORDNUMBER').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.GetFeeByMethodByID = function (ID) {
      return soap_api.GetFeeByMethodByID(ID).then(function (success) {
        var fee = [];
        var instrument_fee = [];
        var same_currency_fee = [];
        var error = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT FEE').each(function (i, e) {
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            free_instruments: jQuery(e).find('FREE_INSTRUMENTS').text(),
            instrument_cost: jQuery(e).find('INSTRUMENT_COST').text(),
            non_instrument_cost: jQuery(e).find('NON_INSTRUMENT_COST').text(),
            inv_act_type_id: jQuery(e).find('INV_ACT_TYPE_ID').text(),
            inv_act_type: jQuery(e).find('INV_ACT_TYPE').text(),
            charge_id: jQuery(e).find('CHARGE_ID').text(),
            charge_type: jQuery(e).find('CHARGE_TYPE').text(),
            is_allowed: jQuery(e).find('IS_ALLOWED').text(),
            source_id: jQuery(e).find('SOURCE_ID').text(),
          };
          fee.push(temp);
        });
        jQuery(response).find('OUTPUT INSTRUMENT_FEE').each(function (i, e) {
          var temp = {
            id: jQuery(e).find('ID').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            currency_id: jQuery(e).find('Currency_ID').text(),
            instrument_cost: jQuery(e).find('Instrument_Cost').text(),
            cost_type: jQuery(e).find('COST_TYPE').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            cost_type_name: jQuery(e).find('COST_TYPE_NAME').text()
          };
          instrument_fee.push(temp);
        });
        jQuery(response).find('OUTPUT SAME_CURRENCY_FEE').each(function (i, e) {
          var temp = {
            id: jQuery(e).find('ID').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            currency_id: jQuery(e).find('Currency_ID').text(),
            instrument_cost: jQuery(e).find('Instrument_Cost').text(),
            cost_type: jQuery(e).find('COST_TYPE').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            cost_type_name: jQuery(e).find('COST_TYPE_NAME').text()
          };
          same_currency_fee.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function (i, e) {
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          error.push(temp);
        });
        var result = {
          fee: fee,
          instrument_fee: instrument_fee,
          same_currency_fee: same_currency_fee,
          error: error
        }
        return result;
      }, function (error) {
        console.error(error.statusText);
      });
    };
    vm.SetDealApproval = function (deal) {
      return soap_api.SetDealApproval(deal).then(function (success) {
        var row = [];
        if (vm.isXML(success.data)) {
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function (i, e) {
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              error: jQuery(e).find('ERROR').text()
            };
            row.push(temp);
          });
        } else {
          console.log(success.data);
        }
        return row;
      });
    };
    vm.GetRateByIDS = function (buy_currency, buy_amount, sell_currency, sell_amount, value_date, client_name) {
      return soap_api.GetRateByIDS(buy_currency, buy_amount, sell_currency, sell_amount, value_date, client_name).then(function (success) {
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Rate').each(function (i, e) {
          var temp = {
            exchange_rate: jQuery(e).find('EXCHANGE_RATE').text(),
            market_rate: jQuery(e).find('MARKET_RATE').text(),
            spread_rate: jQuery(e).find('SPREAD_RATE').text(),
            actual_market_rate: jQuery(e).find('ACTUAL_MARKET_RATE').text(),
            sell_amount: jQuery(e).find('SELL_AMOUNT').text(),
            buy_amount: jQuery(e).find('BUY_AMOUNT').text()
          };
          row.push(temp);
        });
        return row;
      }, function (error) {
        console.error(error.statusText);
      });
    };
    vm.GetFeeForSettlements = function (input_xml) {
      return soap_api.GetFeeForSettlements(input_xml).then(function (success) {
        if (vm.isXML(success.data)) {
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT PAYMENT').each(function (i, e) {
            var temp = {
              buy_amount: jQuery(e).find('BUY_AMOUNT').text(),
              buy_currency_id: jQuery(e).find('BUY_CURRENCY_ID').text(),
              buy_currency: jQuery(e).find('BUY_CURRENCY').text()
            };
            row.push(temp);
          });
          jQuery(response).find('OUTPUT SETTLEMENT').each(function (i, e) {
            var temp = {
              fee: jQuery(e).find('FEE').text(),
              fee_type: jQuery(e).find('FEE_TYPE').text(),
              fee_currency_id: jQuery(e).find('FEE_CURRENCY_ID').text(),
              fee_currency: jQuery(e).find('FEE_CURRENCY').text(),
              action_id: jQuery(e).find('ACTION_ID').text(),
              action_name: jQuery(e).find('ACTION_NAME').text(),
              sell_currency_id: jQuery(e).find('SELL_CURRENCY_ID').text(),
              sell_currency: jQuery(e).find('SELL_CURRENCY').text(),
              sell_amount: jQuery(e).find('SELL_AMOUNT').text(),
              rate: jQuery(e).find('RATE').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.updateWebDealWithSettlementsPaymentsTransactions = function (deal, value_date, deal_transaction, payments, settlements) {
      return soap_api.updateWebDealWithSettlementsPaymentsTransactions(deal, value_date, deal_transaction, payments, settlements).then(function (success) {
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function (i, e) {
          var temp = {
            id: jQuery(e).find('ID').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text()
          };
          row.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function (i, e) {
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text(),
            recordnumber: jQuery(e).find('RECORDNUMBER').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.PostWebDeal = function (deal) {
      return soap_api.PostWebDeal(deal).then(function (success) {
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT RESULT').each(function (i, e) {
          var temp = {
            recordtype: jQuery(e).find('RecordType').text(),
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text(),
            recordnumber: jQuery(e).find('RECORDNUMBER').text()
          };
          row.push(temp);
        });
        return row;
      });

    };
    vm.FinalizeDealPaymentSettlement = function (deal) {
      return soap_api.FinalizeDealPaymentSettlement(deal).then(function (success) {
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT RESULT').each(function (i, e) {
          var temp = {
            recordtype: jQuery(e).find('RecordType').text(),
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text(),
            recordnumber: jQuery(e).find('RECORDNUMBER').text()
          };
          row.push(temp);
        });
        return row;
      });
    }
    vm.GetDealReport = function (deal) {
      return soap_api.GetDealReport(deal).then(function (success) {
        if (vm.isXML(success.data)) {
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function (i, e) {
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              recordid: jQuery(e).find('RECORDID').text(),
              error: jQuery(e).find('ERROR').text(),
              value: jQuery(e).find('VALUE').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetPurposeOfPayment = function () {

      return soap_api.GetPurposeOfPayments().then(function (success) {
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT PURPOSE_OF_PAYMENT').each(function (i, e) {
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            description: jQuery(e).find('DESCRIPTION').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            is_approved: jQuery(e).find('IS_APPROVED').text(),
            version_no: jQuery(e).find('VERSION_NO').text()
          };
          row.push(temp);
        });
        return row;
      }, function (error) {
        console.error(error.statusText);
      });
    };

    //API data
    vm.GetDefaultDate().then(function (data) {
      vm.defaultDate = data[0];
      if (vm.defaultDate.status === 'TRUE') {
        vm.step1.value_date = vm.defaultDate.recordid;
      }
      vm.loader = true;
    });
    vm.GetEntity().then(function (data) {
      vm.entity = data[0];
    });
    vm.GetEntityWebDefaults().then(function (data) {
      vm.entityWebDefaults = data[0];
    });
    vm.GetPurposeOfPayment().then(function (data) {
      vm.purposes = data;
      vm.getPurposeName = function (id) {
        for (var i = 0; i < vm.purposes.length; i++) {
          if (id == vm.purposes[i].id) {
            return vm.toUpperCase(vm.purposes[i].name);
          }
        }
      };
    });

    //utilities
    vm.parseBoolean = function (str) {
      return str == 'true' ? true : false;
    };
    vm.parseFloat = function (number) {
      return parseFloat(number);
    };
    vm.toLowerCase = function (str) {
      str = str || '';
      return str.toLowerCase();
    };
    vm.toUpperCase = function (str) {
      str = str || '';
      return str.toUpperCase();
    };
    vm.isXML = function (xml) {
      try {
        var xmlDoc = jQuery.parseXML(xml);
        return true;
      } catch (error) {
        return false;
      }
    };
    vm.includes = function (string, substring) {
      string = string || '';
      substring = substring || ''
      if (string.toLowerCase().indexOf(substring.toLowerCase()) !== -1) {
        return true;
      } else {
        return false;
      }
    };
    vm.replaceWith = function (string, separator) {
      string = string || '';
      return string.replace(/ +/g, separator);
    };
    vm.stopInput = function (event) {
      event.preventDefault();
    };
    vm.protectAmount = function (event) {
      var input = event.key;
      var re = /\d/i;
      if (input != 'Backspace' && input != '.' && input != 'Delete' && input != 'Del' && input != 'Decimal' && input != 'Left' && input != 'Right' && input != 'Tab' && input != 'ArrowLeft' && input != 'ArrowRight') {
        if (input.match(re) == null) {
          event.preventDefault();
          return false;
        }
      } else {
        if (input == '.' || input == 'Decimal') {
          re = /\./g;
          if (event.target.value.length > 0) {
            if (event.target.value.match(/\./g) != null) {
              if (event.target.value.match(/\./g).length > 0) {
                event.preventDefault();
                return false;
              }
            }
          }
        }
      }
    };
    vm.noSpecialCharactersFunction = function (event) {
      var input = event.key;
      var re = /^[a-zA-Z0-9\-\s]+$/g;
      if (event.key.match(re) == null) {
        event.preventDefault();
        return false;
      }
    };
    vm.limitLength = function (event, limit) {
      if (event.target.value.length == limit) {
        event.preventDefault();
      }
    };
    vm.nextStep = function () {
      var index = (vm.selectedIndex === vm.max) ? 0 : vm.selectedIndex + 1;
      vm.selectedIndex = index;
      var currentTab = $('#tab_' + index);
      $('md-tab-item').each(function () {
        if ($(this).hasClass('md-active')) {
          $(this).addClass('md-previous');
          $(this).prevAll().addClass('md-previous');
        }
      });
    };
    vm.previousStep = function () {
      var index = (vm.selectedIndex === 0) ? 0 : vm.selectedIndex - 1;
      vm.selectedIndex = index;
      var currentTab = $('#tab_' + index);
      angular.element('md-tab-item').each(function () {
        if (angular.element(this).hasClass('md-active')) {
          angular.element(this).removeClass('md-previous');
          angular.element(this).prev().removeClass('md-previous')
        }
      });
    };
    vm.parseFloating = function (value) {
      var the_amount = value;
      if (typeof value == 'string') {
        the_amount = value.split(',').join('');
        the_amount = vm.parseFloat(the_amount).toFixed(2);
      }
      if (typeof value == 'number') {
        the_amount = vm.parseFloat(the_amount).toFixed(2);
      }
      return the_amount;
    };

    //step1
    vm.step1 = {
      value_date: null,
      account: null,
      amount: null,
      currency: null,
      beneficiary: null,
      fee: null
    };
    vm.accountFilter = {
      name: null,
      type: null,
      account_code: null,
      currency: null
    };
    vm.beneficiaryFilter = {
      name: null,
      type: null,
      account_code: null,
      currency: null
    };
    //beneficiary filters
    vm.beneficiaryNameFilter = function (beneficiary) {
      if (vm.beneficiaryFilter) {
        if (beneficiary.name.toLowerCase().indexOf(vm.beneficiaryFilter.name || '') !== -1) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    };
    vm.beneficiaryTypeFilter = function (beneficiary) {
      if (vm.beneficiaryFilter) {
        if (beneficiary.type.toLowerCase().indexOf(vm.beneficiaryFilter.type || '') !== -1) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    };
    vm.beneficiaryAccountFilter = function (beneficiary) {
      if (vm.beneficiaryFilter) {
        if (beneficiary.beneficiary_accountcode.toLowerCase().indexOf(vm.beneficiaryFilter.account_code || '') !== -1) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    };
    vm.beneficiaryCurrencyFilter = function (beneficiary) {
      if (vm.beneficiaryFilter) {
        if (beneficiary.currency_name.toLowerCase().indexOf(vm.beneficiaryFilter.currency || '') !== -1) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    };

    //account filters
    vm.accountNameFilter = function (account) {
      if (vm.accountFilter) {
        if (account.name.toLowerCase().indexOf(vm.accountFilter.name || '') !== -1) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    };
    vm.accountTypeFilter = function (account) {
      if (vm.accountFilter) {
        if (account.type.toLowerCase().indexOf(vm.accountFilter.type || '') !== -1) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    };
    vm.accountAccountFilter = function (account) {
      if (vm.accountFilter) {
        if (account.beneficiary_accountcode.toLowerCase().indexOf(vm.accountFilter.account_code || '') !== -1) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    };
    vm.accountCurrencyFilter = function (account) {
      if (vm.accountFilter) {
        if (account.currency_name.toLowerCase().indexOf(vm.accountFilter.currency || '') !== -1) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    };

    vm.showBeneficiaryTable = function () {
      vm.beneficiaryTable = false;
      vm.beneficiaryFilter = {
        name: null,
        type: null,
        account_code: null,
        currency: null
      };
    };
    vm.hideBeneficiaryTable = function () {
      vm.beneficiaryTable = true;
      vm.beneficiaryFilter = {
        name: null,
        type: null,
        account_code: null,
        currency: null
      };
    };
    vm.filterBeneficiaryName = function (beneficiary) {
      if (vm.beneficiary) {
        if (
          (beneficiary.name.toLowerCase().indexOf(vm.beneficiary.name || '') !== -1)
        ) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    };
    vm.filterBeneficiaryCurrency = function (beneficiary) {
      if (vm.beneficiary) {
        if (
          (beneficiary.currency.toLowerCase().indexOf(vm.beneficiary.currency || '') !== -1)
        ) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    };
    vm.filterBeneficiaryAccount = function (beneficiary) {
      if (vm.beneficiary) {
        if (
          (beneficiary.account.toLowerCase().indexOf(vm.beneficiary.account || '') !== -1)
        ) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    };
    vm.getQuoteAgain = function () {

    };
    vm.reloadModule = function () {
      $state.reload();
    };
    vm.openEntityPopup = function(){
      $mdDialog.show({
        fullscreen: true,
        clickOutsideToClose: true,
        scope: $scope,
        preserveScope: true,
        templateUrl: 'app/main/payments/pay_from_your_account/entity-modern-popup.html',
        parent: angular.element(document.body),
        controller: function dialogController($scope, $mdDialog){
          $scope.dialogLoader = false;
          if(!(vm.entityAccounts.length > 0)){
            api.GetTransferAccountFrom(vm.module_id).then(function (data) {
              vm.entityAccounts = data.output.table1;
              $scope.dialogLoader = true;
            });
          }else{
            $scope.dialogLoader = true;
          }
          $scope.closeDialog = function() {
            $mdDialog.hide();
          };
          $scope.showFilter = true;
          $scope.toggleFilter = function(){
            $scope.showFilter = !$scope.showFilter;
          };
          $scope.selectEntityAccount = function (account) {
            vm.fromAccount = account;
            accnt = account;
            vm.getCurrencyList(vm.accountTo, vm.fromAccount);
            if (account.balance !== 'NA') {
              var balance = $filter('currency')(account.balance, '');
              vm.step1.account =   convertObjects(account.name)+ ' ' + balance;
            } else {
              vm.step1.account = convertObjects(account.name);
            }
            if (vm.step1.account !== null) {
              vm.step1.currency = null;

              vm.step1.account_currency = account.currency_name;
              vm.step1.account_currency_id = account.currency_id;
              vm.temp_account = account;
            }
            $scope.closeDialog();
          };
        }
      });
    };
    vm.openBeneficiaryPopup = function(){
      $mdDialog.show({
        fullscreen: true,
        clickOutsideToClose: true,
        scope: $scope,
        preserveScope: true,
        templateUrl: 'app/main/payments/pay_from_your_account/beneficiary-modern-popup.html',
        parent: angular.element(document.body),
        controller: function dialogController($scope, $mdDialog){
          $scope.dialogLoader = false;
          if(!(vm.beneficiaryAccounts.length > 0)){
            api.GetTransferAccountTo(vm.module_id).then(function (data) {
              vm.beneficiaryAccounts = data.output.table1;
              $scope.dialogLoader = true;
            });
          }else{
            $scope.dialogLoader = true;
          }
          $scope.closeDialog = function() {
            $mdDialog.hide();
          };
          $scope.showFilter = true;
          $scope.toggleFilter = function(){
            $scope.showFilter = !$scope.showFilter;
          };
          vm.defaultSelected = false;
          $scope.selectBeneficiaryAccount = function (beneficiary) {
            debugger
            vm.defaultSelected = true;
            vm.accountTo = beneficiary;
            benefic = beneficiary;
            vm.step1.fee = 0;
            vm.getCurrencyList(vm.accountTo, vm.fromAccount);
            api.GetFeeByMethod(beneficiary.paymentmethod).then(function (data) {
              vm.fee = data.output;
              if(vm.currencies.length === 1){
                //currency is same
                if(angular.isDefined(vm.fee.same_currency_fee)){
                  if(vm.fee.same_currency_fee.currency_id === vm.temp_beneficiary.currency_id){
                    vm.step1.fee = vm.fee.same_currency_fee.instrument_cost;
                  }else{
                    vm.step1.fee = vm.fee.same_currency_fee.non_instrument_cost;
                  }
                }else{
                  vm.step1.fee = vm.fee.fee.non_instrument_cost;
                }
              }else if(vm.currencies.length > 1){
                //we have 2 different currencies
                if(angular.isDefined(vm.fee.instrument_fee) && vm.fee.instrument_fee.length > 0){
                  var found = false;
                  var instrument_fee = {};
                  for(var i=0; i<vm.fee.instrument_fee.length; i++){
                    if(vm.fee.instrument_fee[i].currency_id === '2'){
                      found = true;
                      instrument_fee = vm.fee.instrument_fee[i];
                    }
                  }
                  if(found){
                    vm.step1.fee = instrument_fee.instrument_cost;
                  }else{
                    vm.step1.fee = vm.fee.fee.instrument_cost;
                  }
                }else{
                  vm.step1.fee = vm.fee.fee.instrument_cost;
                }
              }
            });
            vm.step1.payment_purpose = null;
            if (beneficiary.balance !== 'NA') {
              var balance = $filter('currency')(beneficiary.balance, '');
              vm.step1.beneficiary = convertObjects(beneficiary.name) + ' ' + balance;
            } else {
              vm.step1.beneficiary = convertObjects(beneficiary.name);
            }
            if (vm.step1.beneficiary !== null) {
              vm.step1.currency = null;
            }

            vm.step1.beneficiary_currency = beneficiary.currency_name;
            vm.step1.beneficiary_currency_id = beneficiary.currency_id;
            vm.temp_beneficiary = beneficiary;
            vm.step1.payment_purpose = paymentPurpose(beneficiary);
            vm.hidePurpose = beneficiary.paymentmethod === '5';
            $scope.closeDialog();
          };
        }
      });
    };
    function paymentPurpose(beneficiary){
      var paymentPurposeId;
      vm.purposes.some(function(elem){
        if(elem.id === beneficiary.purpose_of_payment_id){
          paymentPurposeId = elem.id;
          return true;
        }
        else if(beneficiary.purpose_of_payment_id === "0"){
          paymentPurposeId = beneficiary.purpose_of_payment_id;
        }
        return false
      })
      return paymentPurposeId;
    }
    vm.getAnotherCurrency = function (currency) {

      if (vm.currencies != null) {
        if (vm.currencies.length == 1) {
          return vm.currencies[0].currency_name;
        } else if (vm.currencies.length == 2) {
          var new_currency = {};
          for (var i = 0; i < vm.currencies.length; i++) {
            if (vm.toLowerCase(vm.currencies[i].currency_name) != vm.toLowerCase(currency)) {
              new_currency = vm.currencies[i];
              break;
            }
          }
          return new_currency.currency_name;
        }
      }
    };
    //for getting list of selected currencies
    vm.getCurrencyList = function (beneficiary, account) {
      if(!vm.step1.beneficiary && beneficiary)
      {
        var currencies = [];
        currencies.push({currency_name: beneficiary.currency_name, currency_id: beneficiary.currency_id});
        vm.currencies = currencies;
      }
      if(!vm.step1.account && account)
      {
        var currencies = [];
        currencies.push({currency_name: account.currency_name, currency_id: account.currency_id});
        vm.currencies = currencies;
      }
      if (vm.step1.account !== null || vm.step1.beneficiary !== null) {
        var currencies = [];
        if(vm.step1.account)
        {
          if(angular.isDefined(vm.step1.account_currency) && angular.isDefined(vm.step1.account_currency_id)){
            // currencies.push({currency_name: vm.step1.account_currency, currency_id: vm.step1.account_currency_id});
            currencies.push({currency_name: account.currency_name, currency_id: account.currency_id});
          }
        }
       if(vm.step1.beneficiary)
       {
         if(angular.isDefined(vm.step1.beneficiary_currency) && angular.isDefined(vm.step1.beneficiary_currency_id)){
           // currencies.push({currency_name: vm.step1.beneficiary_currency, currency_id: vm.step1.beneficiary_currency_id});
           currencies.push({currency_name: beneficiary.currency_name, currency_id: beneficiary.currency_id});
         }
       }

        vm.currencies = _.unique(currencies, function (p) {
          return p.currency_name;
        });
      }
    };
    vm.fetchStep1Currency = function () {

      if (vm.step1.currency == null) {
        vm.getCurrencyList();
      }
    };
    //create deal
    vm.step1Next = function () {
      var benfic = benefic ;
      var amnt   = accnt;
      if (vm.step1.value_date !== null && vm.step1.account !== null && vm.step1.amount !== null && vm.step1.currency !== null && vm.step1.beneficiary !== null) {

        if(vm.deal && vm.deal.id){
          return vm.nextStep();
        }
        if(amnt.type === 'Accounts'){
          var balance = parseInteger(amnt.balance);
          var splitAmount = vm.step1.amount.split(",");
          var stepAmount = parseInteger(splitAmount.join(""));

          if (balance === 0 || balance < stepAmount) {
            errToast("Balance can not be zero or balance cannot be less than deal amount");
            return;
          }

        }
        //also check purpose of payment
        if (vm.hidePurpose !== true) {
          if (vm.step1.payment_purpose == null) {
            $mdToast.show({
              template: '<md-toast class="md-toast error">Please fill required fields.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
            vm.step1Invalid = true;
            return false;
          }
        }
        vm.loader = false;
        vm.step1Invalid = false;
        vm.step1disable = true;
        var value_date = new Date(Date.parse(vm.step1.value_date));
        var amount = vm.parseFloating(vm.step1.amount);
        vm.ValidateBusinessDate(value_date).then(function (data) {
          data = data[0];
          if (data.status === 'TRUE') {
            var client = vm.entity.entity.entity_id;
            var trader = vm.entityWebDefaults.trader_id;
            var notes = '';
            var internal_remarks = '';
            var is_bach = 364;
            var deal_transaction = '';

            if (vm.currencies.length === 1) {
              //if from and to account currencies are same
              deal_transaction += '<DEAL_TRANSACTION>';
              deal_transaction += '<BUY_CURRENCY><ID>' + vm.currencies[0].currency_id + '</ID></BUY_CURRENCY>';
              deal_transaction += '<BUY_AMOUNT>' + amount + '</BUY_AMOUNT>';
              deal_transaction += '<EXCHANGE_RATE>1</EXCHANGE_RATE>';
              deal_transaction += '<ACTUAL_MARKET_RATE>1</ACTUAL_MARKET_RATE>';
              deal_transaction += '<MARKET_RATE>1</MARKET_RATE>';
              deal_transaction += '<IS_EQUIVALENT>1</IS_EQUIVALENT>';
              deal_transaction += '<SELL_CURRENCY><ID>' + vm.currencies[0].currency_id + '</ID></SELL_CURRENCY>';
              deal_transaction += '<SELL_AMOUNT>' + 0 + '</SELL_AMOUNT>';
              deal_transaction += '</DEAL_TRANSACTION>';
            } else if (vm.currencies.length === 2) {
              var buy_currency_id, sell_currency_id;

              if (vm.step1.currency === vm.step1.from_currency) {
                buy_currency_id = vm.step1.account_currency_id;
                sell_currency_id = vm.step1.beneficiary_currency_id;
                // buyBalance =
              }
              else {
                buy_currency_id = vm.step1.beneficiary_currency_id;
                sell_currency_id = vm.step1.account_currency_id;
              }
              //if from and to account currencies are different
              deal_transaction += '<DEAL_TRANSACTION>';
              deal_transaction += '<BUY_CURRENCY><ID>' + buy_currency_id + '</ID></BUY_CURRENCY>';
              deal_transaction += '<BUY_AMOUNT>' + amount + '</BUY_AMOUNT>';
              deal_transaction += '<EXCHANGE_RATE>0</EXCHANGE_RATE>';
              deal_transaction += '<ACTUAL_MARKET_RATE>0</ACTUAL_MARKET_RATE>';
              deal_transaction += '<MARKET_RATE>0</MARKET_RATE>';
              deal_transaction += '<IS_EQUIVALENT>0</IS_EQUIVALENT>';
              deal_transaction += '<SELL_CURRENCY><ID>' + sell_currency_id + '</ID></SELL_CURRENCY>';
              deal_transaction += '<SELL_AMOUNT>0</SELL_AMOUNT>';
              deal_transaction += '</DEAL_TRANSACTION>';
            }

            value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth() + 1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);

            //settlements
            var settlements = '';
            settlements += '<SETTLEMENTS>';
            settlements += '<ACTION><ID>' + vm.temp_beneficiary.paymentmethod + '</ID></ACTION>';
            settlements += '<CURRENCY><ID>' + vm.temp_beneficiary.currency_id + '</ID></CURRENCY>';
            if (vm.step1.currency === vm.step1.account_currency) {
              settlements += '<AMOUNT>0</AMOUNT>';
            } else {
              settlements += '<AMOUNT>' + amount + '</AMOUNT>';
            }
            settlements += '<VALUE_DATE>' + value_date + '</VALUE_DATE>';
            if (vm.temp_beneficiary.paymentmethod === '5') {
              settlements += '<ACCOUNT><ID>' + vm.temp_beneficiary.id + '</ID></ACCOUNT>';
              settlements += '<ACT_TYPE>E</ACT_TYPE>';
            } else {
              settlements += '<PAYEE><ID>' + vm.temp_beneficiary.id + '</ID></PAYEE>';
            }
            settlements += '<REFERENCE>' + vm.step1.reference + '</REFERENCE>';
            settlements += '<BENEFICIARY_REFERENCE></BENEFICIARY_REFERENCE>';
            settlements += '<NOTIFY_MY_RECEIPIENT></NOTIFY_MY_RECEIPIENT>';
            settlements += '<BENEFICIARY_REMARKS></BENEFICIARY_REMARKS>';
            settlements += '<REMITTENCE_REMARKS></REMITTENCE_REMARKS>';
            if (vm.hidePurpose === true) {
              settlements += '<PURPOSE_OF_PAYMENT>0</PURPOSE_OF_PAYMENT>';
            } else {
              settlements += '<PURPOSE_OF_PAYMENT>' + vm.step1.payment_purpose + '</PURPOSE_OF_PAYMENT>';
            }
            settlements += '<IS_EQUIVALENT>0</IS_EQUIVALENT>';
            settlements += '<INTERNAL_REFERENCE></INTERNAL_REFERENCE>';
            settlements += '<FEE>' + vm.step1.fee + '</FEE>';
            settlements += '<FEE_TYPE><ID>' + vm.fee.fee.charge_id + '</ID></FEE_TYPE>';
            if (vm.fee.fee.charge_id === '824') {
              settlements += '<FEE_CURRENCY><ID>' + vm.temp_account.currency_id + '</ID></FEE_CURRENCY>';
            } else {
              settlements += '<FEE_CURRENCY><ID>' + vm.temp_beneficiary.currency_id + '</ID></FEE_CURRENCY>';
            }
            settlements += '</SETTLEMENTS>';

            //payments
            var payments = '';
            payments += '<PAYMENTS>';
            payments += '<ACTION><ID>' + vm.temp_account.paymentmethod + '</ID></ACTION>';
            payments += '<CURRENCY><ID>' + vm.temp_account.currency_id + '</ID></CURRENCY>';
            if (vm.step1.currency === vm.step1.account_currency) {
              payments += '<AMOUNT>' + amount + '</AMOUNT>';
            } else {
              payments += '<AMOUNT>' + 0 + '</AMOUNT>';
            }
            payments += '<VALUE_DATE>' + value_date + '</VALUE_DATE>';
            if (vm.temp_account.paymentmethod === '5') {
              payments += '<ACCOUNT><ID>' + vm.temp_account.id + '</ID></ACCOUNT>';
              payments += '<ACT_TYPE>E</ACT_TYPE>';
            } else {
              payments += '<PAYEE><ID>' + vm.temp_account.id + '</ID></PAYEE>';
            }
            payments += '</PAYMENTS>';

            vm.CreateWebDealWithSettlementAndPayments(client, trader, notes, internal_remarks, value_date, is_bach, deal_transaction, payments, settlements).then(function (data) {
              data = data[0];
              if (data.status == 'FALSE') {
                $mdToast.show({
                  template: '<md-toast class="md-toast error">' + data.error + '</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                vm.loader = true;
                vm.step1disable = false;
              } else {
                vm.deal = data;
                $mdToast.show({
                  template: '<md-toast class="md-toast success">Deal {{dealNumber}} Created.</md-toast>',
                  hideDelay: 3000,
                  controller: function($scope){
                    $scope.dealNumber = vm.deal.deal_number
                  },
                  position: 'bottom right'
                });
                vm.GetSavedApprovalsByModule(vm.module_id).then(function (data) {
                  vm.approvalPolicy = data;
                  if (data[0].approval_level == 0 || data[0].approval_level == '') {
                    vm.skipApprovalStep = true;
                  } else {
                    vm.skipApprovalStep = false;
                  }
                  vm.nextStep();
                  vm.step1disable = false;
                  vm.loader = true;
                });
              }
            });
          } else {
            $mdToast.show({
              template: '<md-toast class="md-toast error">' + data.error + '</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
            vm.loader = true;
            vm.step1disable = false;
          }
        });
      } else {
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please fill required fields.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        vm.step1Invalid = true;
      }
    };
    function parseInteger(val) {
      return parseInt(val)
    }
    function errToast(message) {
      $mdToast.show($mdToast.simple({
        hideDelay: 4000,
        position: 'bottom right',
        content: message,
        toastClass: 'md-toast-error'

      }));
    }
    //step2 + step3
    vm.getQuote = function () {
      vm.disableMe = false; // when decline and getQoute again
      vm.loader = false;
      vm.disableQuoteBtn = true;
      var amount = vm.parseFloating(vm.step1.amount);
      var value_date = new Date(Date.parse(vm.step1.value_date));
      if (vm.currencies.length == 1) {
        vm.skipQuote = true;
        vm.loader = true;
        vm.nextStep();
      } else if (vm.currencies.length == 2) {
        //buy currency is same as selected currency
        if (vm.step1.currency == vm.step1.account_currency) {
          var buy_currency_id = vm.step1.account_currency_id;
          var sell_currency_id = vm.step1.beneficiary_currency_id;
          vm.GetRateByIDS(buy_currency_id, amount, sell_currency_id, 0, value_date, vm.entity.entity.name).then(function (data) {
            vm.temp_rate = data[0];
            vm.rate = vm.temp_rate;
            if (data[0].exchange_rate == 0) {
              $mdToast.show({
                template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
                hideDelay: 2000,
                position: 'bottom right'
              });
              vm.temp_rate = [];
              vm.loader = true;
              vm.disableQuoteBtn = false;
              return false;
            }

            //launch quote timer
            var circular_timer = angular.element('#circular-timer-box');
            circular_timer.empty();
            circular_timer.append('<div id="circular-timer" data-timer="60"></div>');
            var timer = circular_timer.find('#circular-timer');
            timer.TimeCircles({
              "animation": "smooth",
              "bg_width": 1,
              "fg_width": 0.21,
              "circle_bg_color": "#AAAAAA",
              "direction": 'Counter-clockwise',
              "start_angle": 160,
              "time": {
                "Days": {
                  "text": "Days",
                  "color": "#FFCC66",
                  "show": false
                },
                "Hours": {
                  "text": "Hours",
                  "color": "#99CCFF",
                  "show": false
                },
                "Minutes": {
                  "text": "Minutes",
                  "color": "#BBFFBB",
                  "show": false
                },
                "Seconds": {
                  "text": "Expires in",
                  "color": "#FFA834",
                  "show": true
                }
              }
            }).addListener(function (unit, value, total) {
              if (total == -1) {
                timer.TimeCircles().stop();
                $interval.cancel(vm.fetchBuyRates);
                vm.fetchBuyRates = null;
                $mdToast.show({
                  template: '<md-toast class="md-toast error">Quote timeout.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                vm.temp_rate = [];
                vm.disableQuoteBtn = false;
                vm.loader = true;
                vm.previousStep();
              } else {
                if (unit == 'Seconds') {
                  var text_box = timer.find('.textDiv_Seconds');
                  text_box.find('.title').remove();
                  var new_value = value + 'Secs';
                  text_box.append('<span class="title">' + new_value + '</span>');
                }
              }
            });
            //vm.timer();
            vm.fetchBuyRates = $interval(function () {
              var buy_currency_id = vm.step1.account_currency_id;
              var sell_currency_id = vm.step1.beneficiary_currency_id;
              vm.rate = vm.temp_rate;
              vm.GetRateByIDS(buy_currency_id, vm.parseFloating(vm.step1.amount), sell_currency_id, 0, new Date(Date.parse(vm.step1.value_date)), vm.entity.entity.name).then(function (data) {
                vm.temp_rate = data[0];
              });
            }, 2000);
            vm.loader = true;
            vm.nextStep();
          });
        } else {
          //buy currency is different as selected currency
          var buy_currency_id = vm.step1.beneficiary_currency_id;
          var sell_currency_id = vm.step1.account_currency_id;
          vm.GetRateByIDS(buy_currency_id, 0, sell_currency_id, amount, value_date, vm.entity.entity.name).then(function (data) {
            vm.temp_rate = data[0];
            vm.rate = vm.temp_rate;
            if (data[0].exchange_rate == 0) {
              $mdToast.show({
                template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
                hideDelay: 2000,
                position: 'bottom right'
              });
              vm.temp_rate = [];
              vm.disableQuoteBtn = false;
              vm.loader = true;
              return false;
            }
            //launch quote timer
            var circular_timer = angular.element('#circular-timer-box');
            circular_timer.empty();
            circular_timer.append('<div id="circular-timer" data-timer="60"></div>');
            var timer = circular_timer.find('#circular-timer');
            timer.TimeCircles({
              "animation": "smooth",
              "bg_width": 1,
              "fg_width": 0.21,
              "circle_bg_color": "#AAAAAA",
              "direction": 'Counter-clockwise',
              "start_angle": 160,
              "time": {
                "Days": {
                  "text": "Days",
                  "color": "#FFCC66",
                  "show": false
                },
                "Hours": {
                  "text": "Hours",
                  "color": "#99CCFF",
                  "show": false
                },
                "Minutes": {
                  "text": "Minutes",
                  "color": "#BBFFBB",
                  "show": false
                },
                "Seconds": {
                  "text": "Expires in",
                  "color": "#FFA834",
                  "show": true
                }
              }
            }).addListener(function (unit, value, total) {
              if (total == -1) {
                timer.TimeCircles().stop();
                $interval.cancel(vm.fetchBuyRates);
                vm.fetchBuyRates = null;
                $mdToast.show({
                  template: '<md-toast class="md-toast error">Quote timeout.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                vm.temp_rate = [];
                vm.disableQuoteBtn = false;
                vm.loader = true;
                vm.previousStep();
              } else {
                if (unit == 'Seconds') {
                  var text_box = timer.find('.textDiv_Seconds');
                  text_box.find('.title').remove();
                  var new_value = value + 'Secs';
                  text_box.append('<span class="title">' + new_value + '</span>');
                }
              }
            });
            //vm.timer();
            vm.fetchBuyRates = $interval(function () {
              var buy_currency_id = vm.step1.beneficiary_currency_id;
              var sell_currency_id = vm.step1.account_currency_id;
              vm.rate = vm.temp_rate;
              vm.GetRateByIDS(buy_currency_id, 0, sell_currency_id, vm.parseFloating(vm.step1.amount), new Date(Date.parse(vm.step1.value_date)), vm.entity.entity.name).then(function (data) {
                vm.temp_rate = data[0];
              });
            }, 2000);
            vm.loader = true;
            vm.nextStep();
          });
        }
      }
    };
    vm.timer = function () {
      vm.duration = 60;
      vm.percent_duration = 0;
      vm.interval_func = $interval(function () {
        vm.expires_in -= 1;
        //vm.duration -= 1;
        vm.percent_duration = (100 / (vm.duration / vm.expires_in)).toFixed(2);
        if (vm.expires_in < 1) {
          vm.duration = 60;
          vm.expires_in = 60;
          vm.percent_duration = 0;
          $interval.cancel(vm.interval_func);
        }
      }, 1000);
    };
    vm.acceptQuote = function () {
      vm.loader = false;
      vm.disableMe = true;

      var circular_timer = angular.element('#circular-timer-box');
      var timer = circular_timer.find('#circular-timer');
      timer.TimeCircles().stop();
      $interval.cancel(vm.fetchBuyRates);

      var amount = vm.parseFloating(vm.step1.amount);
      if (vm.step1.currency == vm.step1.account_currency) {
        var buy_currency_id = vm.step1.account_currency_id;
        var sell_currency_id = vm.step1.beneficiary_currency_id;
        var buy_amount = amount;
        var sell_amount = 0;
      } else {
        var buy_currency_id = vm.step1.beneficiary_currency_id;
        var sell_currency_id = vm.step1.account_currency_id;
        var buy_amount = 0;
        var sell_amount = amount;
      }
      var deal_transaction = '';
      if (vm.currencies.length == 1) {
        //if from and to account currencies are same
        deal_transaction += '<DEAL_TRANSACTION>';
        deal_transaction += '<BUY_CURRENCY><ID>' + vm.currencies[0].currency_id + '</ID></BUY_CURRENCY>';
        deal_transaction += '<BUY_AMOUNT>' + amount + '</BUY_AMOUNT>';
        deal_transaction += '<EXCHANGE_RATE>1</EXCHANGE_RATE>';
        deal_transaction += '<ACTUAL_MARKET_RATE>1</ACTUAL_MARKET_RATE>';
        deal_transaction += '<MARKET_RATE>1</MARKET_RATE>';
        deal_transaction += '<IS_EQUIVALENT>1</IS_EQUIVALENT>';
        deal_transaction += '<SELL_CURRENCY><ID>' + vm.currencies[0].currency_id + '</ID></SELL_CURRENCY>';
        deal_transaction += '<SELL_AMOUNT>' + amount + '</SELL_AMOUNT>';
        deal_transaction += '</DEAL_TRANSACTION>';
      } else if (vm.currencies.length == 2) {
        //if from and to account currencies are different
        deal_transaction += '<DEAL_TRANSACTION>';
        deal_transaction += '<BUY_CURRENCY><ID>' + buy_currency_id + '</ID></BUY_CURRENCY>';
        deal_transaction += '<BUY_AMOUNT>' + vm.rate.buy_amount + '</BUY_AMOUNT>';
        deal_transaction += '<EXCHANGE_RATE>' + vm.rate.exchange_rate + '</EXCHANGE_RATE>';
        deal_transaction += '<ACTUAL_MARKET_RATE>' + vm.rate.actual_market_rate + '</ACTUAL_MARKET_RATE>';
        deal_transaction += '<MARKET_RATE>' + vm.rate.market_rate + '</MARKET_RATE>';
        deal_transaction += '<IS_EQUIVALENT>0</IS_EQUIVALENT>';
        deal_transaction += '<SELL_CURRENCY><ID>' + sell_currency_id + '</ID></SELL_CURRENCY>';
        deal_transaction += '<SELL_AMOUNT>' + vm.rate.sell_amount + '</SELL_AMOUNT>';
        deal_transaction += '</DEAL_TRANSACTION>';
      }

      var deal = vm.deal.deal_number;
      var value_date = new Date(Date.parse(vm.step1.value_date));
      value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth() + 1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
      var settlements = '<DEAL>' +
        '<MODULE_ID>' + vm.module_id + '</MODULE_ID>' +
        '<VALUE_DATE>' + value_date + '</VALUE_DATE>' +
        '<BUY_CURRENCY><ID>' + buy_currency_id + '</ID></BUY_CURRENCY>';
      if (vm.skipQuote == true) {
        settlements += '<BUY_AMOUNT>' + amount + '</BUY_AMOUNT>'
      } else {
        settlements += '<BUY_AMOUNT>' + vm.rate.buy_amount + '</BUY_AMOUNT>'
      }
      settlements += '</DEAL>';
      settlements += '<SETTLEMENTS>';
      settlements += '<ACTION><ID>' + vm.temp_beneficiary.paymentmethod + '</ID></ACTION>';
      settlements += '<SELL_CURRENCY><ID>' + sell_currency_id + '</ID></SELL_CURRENCY>';
      if (vm.skipQuote == true) {
        settlements += '<SELL_AMOUNT>' + amount + '</SELL_AMOUNT>'
        settlements += '<RATE>1</RATE>';
      } else {
        settlements += '<SELL_AMOUNT>' + vm.rate.sell_amount + '</SELL_AMOUNT>'
        settlements += '<RATE>' + vm.rate.exchange_rate + '</RATE>';
      }
      settlements += '</SETTLEMENTS>';

      vm.GetFeeForSettlements(settlements).then(function (data) {
        settlements = '';
        var temp_payment = data.shift();
        data = data[0];

        //settlements
        settlements += '<SETTLEMENTS>';
        settlements += '<ACTION><ID>' + vm.temp_beneficiary.paymentmethod + '</ID></ACTION>';
        settlements += '<CURRENCY><ID>' + vm.temp_beneficiary.currency_id + '</ID></CURRENCY>';
        settlements += '<AMOUNT>' + data.sell_amount + '</AMOUNT>';
        settlements += '<VALUE_DATE>' + value_date + '</VALUE_DATE>';
        //for settlements pass payment_type ID as 697 which is outgoing.
        if (vm.temp_beneficiary.paymentmethod == 5) {
          settlements += '<ACCOUNT><ID>' + vm.temp_beneficiary.id + '</ID></ACCOUNT>';
          settlements += '<ACT_TYPE>E</ACT_TYPE>';
        } else {
          settlements += '<PAYEE><ID>' + vm.temp_beneficiary.id + '</ID></PAYEE>';
        }
        settlements += '<REFERENCE></REFERENCE>';
        settlements += '<BENEFICIARY_REFERENCE></BENEFICIARY_REFERENCE>';
        settlements += '<NOTIFY_MY_RECEIPIENT></NOTIFY_MY_RECEIPIENT>';
        settlements += '<BENEFICIARY_REMARKS></BENEFICIARY_REMARKS>';
        settlements += '<REMITTENCE_REMARKS></REMITTENCE_REMARKS>';
        if (vm.hidePurpose == true) {
          settlements += '<PURPOSE_OF_PAYMENT>0</PURPOSE_OF_PAYMENT>';
        } else {
          settlements += '<PURPOSE_OF_PAYMENT>' + vm.step1.payment_purpose + '</PURPOSE_OF_PAYMENT>';
        }
        settlements += '<IS_EQUIVALENT>0</IS_EQUIVALENT>';
        settlements += '<INTERNAL_REFERENCE></INTERNAL_REFERENCE>';
        settlements += '<FEE>' + data.fee + '</FEE>';
        vm.step1.fee = data.fee;
        settlements += '<FEE_TYPE><ID>' + data.fee_type + '</ID></FEE_TYPE>';
        settlements += '<FEE_CURRENCY><ID>' + data.fee_currency_id + '</ID></FEE_CURRENCY>';
        settlements += '</SETTLEMENTS>';

        //payments
        var payments = '';
        payments += '<PAYMENTS>';
        payments += '<ACTION><ID>' + vm.temp_account.paymentmethod + '</ID></ACTION>';
        payments += '<CURRENCY><ID>' + vm.temp_account.currency_id + '</ID></CURRENCY>';
        payments += '<AMOUNT>' + temp_payment.buy_amount + '</AMOUNT>';
        payments += '<VALUE_DATE>' + value_date + '</VALUE_DATE>';
        if (vm.temp_account.paymentmethod == 5) {
          payments += '<ACCOUNT><ID>' + vm.temp_account.id + '</ID></ACCOUNT>';
          payments += '<ACT_TYPE>E</ACT_TYPE>';
        } else {
          payments += '<PAYEE><ID>' + vm.temp_account.id + '</ID></PAYEE>';
        }
        payments += '</PAYMENTS>';

        vm.updateWebDealWithSettlementsPaymentsTransactions(vm.deal.id, value_date, deal_transaction, payments, settlements).then(function (data) {
          var deal = data[0];
          if (deal.status == 'FALSE') {
            $mdToast.show({
              template: '<md-toast class="md-toast error">API error: ' + data.error + '</md-toast>',
              hideDelay: 3000,
              position: 'bottom right'
            });
            vm.loader = true;
          } else {
            if (typeof deal.id == 'undefined') {
              $mdToast.show({
                template: '<md-toast class="md-toast error">Deal Update Failed.</md-toast>',
                hideDelay: 4000,
                position: 'bottom right'
              });
              vm.loader = true;
              vm.previousStep();
              return false;
            }
            if (typeof deal.id == 'string') {
              vm.PostWebDeal(deal).then(function (data) {
                data = data[0];
                if (data.status == 'TRUE') {
                  vm.FinalizeDealPaymentSettlement(deal).then(function (data) {
                    data = data[0];
                    vm.deal = data;
                    if (data.status == 'TRUE') {
                      $mdToast.show({
                        template: '<md-toast class="md-toast success">Deal Posted and Finalized.</md-toast>',
                        hideDelay: 4000,
                        position: 'bottom right'
                      });
                      vm.loader = true;
                      vm.show_summary = true;
                      vm.deal = deal;
                      vm.disableMe = false;
                      vm.hideQuoteBox = true;
                    } else {
                      $mdToast.show({
                        template: '<md-toast class="md-toast error">' + data.error + '</md-toast>',
                        hideDelay: 4000,
                        position: 'bottom right'
                      });
                      vm.loader = true;
                      vm.previousStep();
                    }
                  });
                } else {
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">' + data.error + '</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  vm.loader = true;
                  vm.previousStep();
                }
              });
            } else {
              $mdToast.show({
                template: '<md-toast class="md-toast error">There is some problem saving the quote.</md-toast>',
                hideDelay: 3000,
                position: 'bottom right'
              });
            }
          }
        });
      });
    };
    vm.declineQuote = function () {
      vm.disableMe = true;
      vm.loader = true;
      var circular_timer = angular.element('#circular-timer-box');
      var timer = circular_timer.find('#circular-timer');
      timer.TimeCircles().stop();
      $interval.cancel(vm.fetchBuyRates);
      $mdToast.show({
        template: '<md-toast class="md-toast error">Quote declined.</md-toast>',
        hideDelay: 4000,
        position: 'bottom right'
      });
      vm.skipQuote = false;
      vm.previousStep();
    };
    vm.b64toBlob = function (b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    };
    vm.printDeal = function () {
      vm.loader = false;
      vm.GetDealReport(vm.deal).then(function (report) {
        var reportValue = report[0].value
        vm.loader = true;
        var blob = vm.b64toBlob(reportValue, 'application/pdf');
        saveAs(blob, reportValue.recordid);
        // vm.dealReport = {
        //   link: "data:application/pdf;base64,"+ report[0].value,
        //   name: report[0].recordid
        // };
        // console.log(vm.dealReport);
      });
    };
    //Approvals
    vm.showPopOver = true;
    vm.popover = {
      title: 'List of Approvals',
    };
    vm.Approvals = [];
    vm.ApprovalNext = false;
    vm.approvalCounter = 0;
    vm.showPrevious = false;
    vm.showPopOverFunc = function () {
      vm.showPopOver = false;
    };
    vm.showPopLeaveFunc = function () {
      vm.showPopOver = true;
    };
    vm.approveNow = function () {
      if(vm.approvalCounter === 0 ){
        vm.showPrevious = true;
      }
      if (vm.ApprovalNext == false) {
        vm.loader = false;
        vm.GetUserApprovalRights(vm.deal, vm.module_id).then(function (data) {
          if (data[0].status == 'TRUE') {
            vm.SetDealApproval(vm.deal.deal_number).then(function (data) {
              if (data[0].status == 'TRUE') {
                $mdToast.show({
                  template: '<md-toast class="md-toast info">Deal Approved.</md-toast>',
                  hideDelay: 7000,
                  position: 'bottom right'
                });
                vm.GetApprovalHistory(vm.deal).then(function (data) {
                  vm.approvalCounter++;
                  vm.Approvals = data;
                  if (vm.Approvals.length == vm.approvalPolicy[0].approval_level) {
                    vm.ApprovalNext = true;
                  }
                  vm.loader = true;
                });
              } else {
                $mdToast.show({
                  template: '<md-toast class="md-toast error">' + data[0].error + '</md-toast>',
                  hideDelay: 2000,
                  position: 'bottom right'
                });
                vm.showCircleLoader = true;
              }
            });
          } else {
            $mdToast.show({
              template: '<md-toast class="md-toast error">You cannot approve this deal.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
          }
          vm.loader = true;
        });
      }
    };
  }
})();
