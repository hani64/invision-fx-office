(function ()
{
  'use strict';

  angular
    .module('app.payments.pay_from_your_account', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider)
  {
    // State
    $stateProvider.state('app.pay_from_your_account', {
      url      : '/pay_from_your_account',
      views    : {
        'content@app': {
          templateUrl: 'app/main/payments/pay_from_your_account/pay_from_your_account.html',
          controller : 'PayFromYourAccountController as vm',
          resolve: {
            access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
          },
        }
      }
    });
  }

})();
