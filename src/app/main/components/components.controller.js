(function () {
  'use strict';

  angular
    .module('app.components')
    .controller('ComponentController', ComponentController);

  /** @ngInject */
  function ComponentController($mdDialog, Employees) {
    var vm = this;

    // Data
    vm.horizontalStepper = {
      step1: {},
      step2: {},
      step3: {},
      step4: {},
      step5: {}
    };

    // Data
    vm.employees = Employees.data;

    vm.dtOptions = {
      dom: '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
      pagingType: 'simple',
      autoWidth: false,
      responsive: true
    };

    vm.verticalStepper = {
      step1: {},
      step2: {},
      step3: {},
      step4: {},
      step5: {}
    };

    vm.basicForm = {};
    vm.formWizard = {};
    vm.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
    'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
    'WY').split(' ').map(function (state) {
      return {abbrev: state};
    });

    // Methods
    vm.sendForm = sendForm;
    vm.showDataDialog = showDataDialog;
    vm.submitHorizontalStepper = submitHorizontalStepper;
    vm.submitVerticalStepper = submitVerticalStepper;

    //////////

    /**
     * Submit horizontal stepper data
     * @param event
     */
    function submitHorizontalStepper(event) {
      // Show the model data in a dialog
      vm.showDataDialog(event, vm.horizontalStepper);

      // Reset the form model
      vm.horizontalStepper = {
        step1: {},
        step2: {},
        step3: {},
        step4: {},
        step5: {}
      };
    }

    /**
     * Submit vertical stepper data
     *
     * @param event
     */
    function submitVerticalStepper(event) {
      // Show the model data in a dialog
      vm.showDataDialog(event, vm.verticalStepper);

      // Reset the form model
      vm.verticalStepper = {
        step1: {},
        step2: {},
        step3: {},
        step4: {},
        step5: {}
      };
    }

    /**
     * Submit stepper form
     *
     * @param ev
     */
    function showDataDialog(ev, data) {
      // You can do an API call here to send the form to your server

      // Show the sent data.. you can delete this safely.
      $mdDialog.show({
        controller: function ($scope, $mdDialog, formWizardData) {
          $scope.formWizardData = formWizardData;
          $scope.closeDialog = function () {
            $mdDialog.hide();
          };
        },
        template: '<md-dialog>' +
        '  <md-dialog-content><h1>You have sent the form with the following data</h1><div><pre>{{formWizardData | json}}</pre></div></md-dialog-content>' +
        '  <md-dialog-actions>' +
        '    <md-button ng-click="closeDialog()" class="md-primary">' +
        '      Close' +
        '    </md-button>' +
        '  </md-dialog-actions>' +
        '</md-dialog>',
        parent: angular.element('body'),
        targetEvent: ev,
        locals: {
          formWizardData: data
        },
        clickOutsideToClose: true
      });
    }

    /**
     * Send form
     */
    function sendForm(ev) {
      // You can do an API call here to send the form to your server

      // Show the sent data.. you can delete this safely.
      $mdDialog.show({
        controller: function ($scope, $mdDialog, formWizardData) {
          $scope.formWizardData = formWizardData;
          $scope.closeDialog = function () {
            $mdDialog.hide();
          };
        },
        template: '<md-dialog>' +
        '  <md-dialog-content><h1>You have sent the form with the following data</h1><div><pre>{{formWizardData | json}}</pre></div></md-dialog-content>' +
        '  <md-dialog-actions>' +
        '    <md-button ng-click="closeDialog()" class="md-primary">' +
        '      Close' +
        '    </md-button>' +
        '  </md-dialog-actions>' +
        '</md-dialog>',
        parent: angular.element('body'),
        targetEvent: ev,
        locals: {
          formWizardData: vm.formWizard
        },
        clickOutsideToClose: true
      });

      // Clear the form data
      vm.formWizard = {};
    }


    vm.status = '  ';
    vm.customFullscreen = false;

    vm.showAlert = function (ev) {
      // Appending dialog to document.body to cover sidenav in docs app
      // Modal dialogs should fully cover application
      // to prevent interaction outside of dialog
      $mdDialog.show(
        $mdDialog.alert()
          .parent(angular.element(document.querySelector('#popupContainer')))
          .clickOutsideToClose(true)
          .parent(angular.element(document.body))
          .title('This is an alert title')
          .textContent('You can specify some description text in here.')
          .ariaLabel('Alert Dialog Demo')
          .ok('Got it!')
          .targetEvent(ev)
      );
    };

    vm.showConfirm = function (ev) {
      // Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.confirm()
        .title('Would you like to delete your debt?')
        .textContent('All of the banks have agreed to forgive you your debts.')
        .ariaLabel('Lucky day')
        .targetEvent(ev)
        .clickOutsideToClose(true)
        .parent(angular.element(document.body))
        .ok('Please do it!')
        .cancel('Sounds like a scam');

      $mdDialog.show(confirm).then(function () {
        vm.status = 'You decided to get rid of your debt.';
      }, function () {
        vm.status = 'You decided to keep your debt.';
      });
    };

    vm.showPrompt = function (ev) {
      // Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.prompt()
        .title('What would you name your dog?')
        .textContent('Bowser is a common name.')
        .placeholder('Dog name')
        .ariaLabel('Dog name')
        .initialValue('Buddy')
        .targetEvent(ev)
        .parent(angular.element(document.body))
        .ok('Okay!')
        .cancel('I\'m a cat person');

      $mdDialog.show(confirm).then(function (result) {
        vm.status = 'You decided to name your dog ' + result + '.';
      }, function () {
        vm.status = 'You didn\'t name your dog.';
      });
    };

    vm.showAdvanced = function (ev) {
      $mdDialog.show({
        controller: DialogController,
        templateUrl: 'app/main/components/dialogs/1.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        fullscreen: vm.customFullscreen // Only for -xs, -sm breakpoints.
      })
        .then(function (answer) {
          vm.status = 'You said the information was "' + answer + '".';
        }, function () {
          vm.status = 'You cancelled the dialog.';
        });
    };

    vm.showTabDialog = function (ev) {
      $mdDialog.show({
        controller: DialogController,
        templateUrl: 'app/main/components/dialogs/2.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true
      })
        .then(function (answer) {
          vm.status = 'You said the information was "' + answer + '".';
        }, function () {
          vm.status = 'You cancelled the dialog.';
        });
    };

    vm.showPrerenderedDialog = function (ev) {
      $mdDialog.show({
        controller: DialogController,
        contentElement: '#myDialog',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true
      });
    };

    function DialogController($scope, $mdDialog) {
      $scope.hide = function () {
        $mdDialog.hide();
      };

      $scope.cancel = function () {
        $mdDialog.cancel();
      };

      $scope.answer = function (answer) {
        $mdDialog.hide(answer);
      };
    }
  }
})();
