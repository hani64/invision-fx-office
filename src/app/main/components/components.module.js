(function () {
  'use strict';

  angular
    .module('app.components', [
      'datatables'
    ])
    .config(config);

  /** @ngInject */
  function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) {
    // State
    $stateProvider
      .state('app.components', {
        url: '/components',
        views: {
          'content@app': {
            templateUrl: 'app/main/components/components.html',
            controller: 'ComponentController as vm'
          }
        },

        resolve: {
          Employees: function (msApi) {
            return msApi.resolve('tables.employees100@get');
          }
        }
      });

    // Translation
    $translatePartialLoaderProvider.addPart('app/main/components');

    // Api
    msApiProvider.register('tables.employees100', ['app/data/tables/employees100.json']);


    msNavigationServiceProvider.saveItem('main_menu.components', {
      title: 'Components',
      icon: 'icon-arrange-send-backward',
      state: 'app.components',
      weight: 1
    });


  }
})();
