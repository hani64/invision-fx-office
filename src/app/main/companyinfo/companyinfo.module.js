(function () {
  'use strict';

  angular
    .module('app.companyinfo', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) {
    // State
    $stateProvider
      .state('app.companyinfo', {
        url: '/companyinfo',
        views: {
          'content@app': {
            templateUrl: 'app/main/companyinfo/companyinfo.html',
            controller: 'CompanyController as vm'
          }
        }
      });

    // Translation
    $translatePartialLoaderProvider.addPart('app/main/companyinfo');

    // Api
    //msApiProvider.register('sample', ['app/data/payments/payments.json']);


    msNavigationServiceProvider.saveItem('main_menu.companyinfo', {
      title: 'Company Info',
      icon: 'icon-information-outline',
      state: 'app.companyinfo',
      weight: 1,
      child_count: 2
    });

    msNavigationServiceProvider.saveItem('main_menu.companyinfo.general_info', {
      title: 'General Info',
      state: 'app.companyinfo.general_info',
      weight: 1,
      module_id: 11512
    });


    msNavigationServiceProvider.saveItem('main_menu.companyinfo.my_contacts', {
      title: 'My Contacts',
      state: 'app.companyinfo.my_contacts',
      weight: 1,
      module_id: 21002
    });



  }
})();
