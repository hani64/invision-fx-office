(function () {
  'use strict';

  angular
    .module('app.dashboard', [
      // 3rd Party Dependencies
      'nvd3',
    ])
    .config(config);

  /** @ngInject */
  function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) {
    // State
    $stateProvider
      .state('app.dashboard', {
        url: '/dashboard',
        views: {
          'content@app': {
            templateUrl: 'app/main/dashboard/dashboard.html',
            controller: 'DashboardController as vm'
          }
        },
        resolve: {
          DashboardData: function (msApi) {
            return msApi.resolve('dashboard.project@get');
          },
          access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],

        },
        bodyClass: 'dashboard-project'
      });

    // Translation
    $translatePartialLoaderProvider.addPart('app/main/dashboard');

    // Api
    msApiProvider.register('dashboard.project', ['app/data/dashboard/project/data.json']);


    /*msNavigationServiceProvider.saveItem('main_menu.dashboard', {
      title: 'Dashboard',
      icon: 'icon-tile-four',
      state: 'app.dashboard',
      /!*stateParams: {
       'param1': 'page'
       },*!/
      weight: 1
    });*/
  }
})();
