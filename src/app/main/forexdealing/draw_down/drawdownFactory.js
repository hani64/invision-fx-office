/**
 * Created by Tech N Choice on 5/27/2017.
 */
(function(){
  angular
    .module('fuse')
    .factory('drawdownFactory' , drawdownFactory);
})();
drawdownFactory.$inject = ['api', 'QueryFactory', 'XmlJson', '$q'];
function drawdownFactory(api, QueryFactory, XmlJson, $q){

  var service = {
    defaultDate : defaultDate,
    drawDownInventories : drawDownInventories,
    getTransactionsDrawDown : getTransactionsDrawDown,
    userApprovals : userApprovals
  }

  function defaultDate(callback)
  {
    var successOrError
    var input = '<INPUT><IS_FORWARD>false</IS_FORWARD></INPUT>';
    QueryFactory.query('GetDefaultDate', input).then(success, err);

    function success(response){
      var data = convertDataToJson(response.data, 'result');
        callback(data);

    };
    function err(error){
       console.log('Default date error!');
    }
  }


  function drawDownInventories(callback)
  {
    QueryFactory.query('GetDrawDownInvOrgForWeb').then(success, err)
    function success(response){
      var data = convertDataToJson(response.data, 'draw_down_invorg');
      callback(data);
    }
    function err(error){
      console.log('getInventoriesError');
    }
  }

  function getTransactionsDrawDown(deal, date, callback){

    var valueDate = convertDate(date);
    var input = '<INPUT>' +
      '<VALUE_DATE>' + valueDate + '</VALUE_DATE>' +
      '<DEALBASED_IN_CURRENCY>' +
      '<ID>' + deal.id + '</ID>' +
      '<Value>' + deal.name + '</Value>' +
      '</DEALBASED_IN_CURRENCY>' +
      '</INPUT>';
    QueryFactory.query("GetTransactionsForDrawdown", input).then(success, err);

    function success(response) {
      var transactions = convertDataToJson(response.data, 'transaction');
      callback(transactions);
    }

    function err(err) {
       console.log("GetTransactionsForDrawdown");
    }

  }

  function userApprovals(deal){
    api.GetUserApprovalRights(deal, 12277).then(success);
    function success(response){
      console.log(response)
    }
  }




  function convertDate(dt){
    var date = new Date(dt);
    return date.getFullYear() + '-' + ('0' + (date.getMonth()+1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
  }



  function convertDataToJson(xml, name){
    var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
    var actualJson = XmlJson.xmlToJson(xmlDOM);
    return actualJson.output[name];
  }

  return service;
}
