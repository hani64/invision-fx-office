(function () {
  'use strict';

  var app = angular
    .module('app.forexdealing.draw_down', ['datatables'])
    .config(config);

  /** @ngInject */
  function config($stateProvider, msApiProvider) {
    // State
    $stateProvider.state('app.draw_down', {
      url: '/draw_down',
      views: {
        'content@app': {
          templateUrl: 'app/main/forexdealing/draw_down/draw_down.html',
          controller: 'drawDownController as vm'
        }
      },
      resolve: {
        Invoice: function (msApi) {
          return msApi.resolve('invoice@get');
        },
        access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
      },

    });

    // Api
    // Api
    //msApiProvider.register('invoice', ['app/data/invoice/invoice.json']);
  }
})();
