(function () {
  'use strict';

  angular
    .module('app.forexdealing.draw_down')
    .controller('drawDownController', drawDownController);

  /** @ngInject */
  function drawDownController(soap_api, api, $scope, $rootScope, $mdToast, $mdDialog, QueryFactory, XmlJson, $q, messageService, drawdownFactory, userApprovalFactory){


    var vm = this;
    vm.step1 = {};
    vm.module_id = 12277;
    vm.verifyRecord = verifyRecord;
    vm.dealNumber = convertObjects;
    vm.date = convertObjects;
    vm.convertInternalRef = convertObjects;
    function convertObjects(objectOrStringValue){
      console.log(objectOrStringValue)

      if(_.isEmpty(objectOrStringValue))
      {
        return
      }
      if(typeof objectOrStringValue === 'object')
      {
        return objectOrStringValue['#text'].join(" ");
      }
      return objectOrStringValue
    }
    function verifyRecord(stepAmount, price) {
      return price && +parseFloating(stepAmount) > 0 && +parseFloating(stepAmount) < +parseFloating(price) || "";
    };
    function parseFloating(value, s){
      var the_amount = +value;
      if(typeof value === 'string'){
        the_amount = value.split(',').join('');
        the_amount = parseFloat(the_amount).toFixed(2);
      }

      if(typeof value === 'number'){
        the_amount = parseFloat(the_amount).toFixed(2);
      }

      return the_amount;
    };
    //Step 1
    getDefaultDate();
    GetDrawdownInvOrgForWeb()
    userApprovalFactory.GetSavedApprovalsByModule(vm.module_id, saveApprovalCallback);
    function saveApprovalCallback(saveApprovals){
      vm.approvalPolicy = saveApprovals;
    }


    //its compulsory
    api.GetWebPaymentMethod(vm.module_id).then(function(data){
      vm.PaymentMethods = data.output.payment_method;
      var m = [];
      for(var i=0; i<vm.PaymentMethods.length; i++){
        m.push(vm.PaymentMethods[i].id);
      }
      vm.PaymentMethodsJoined = m.join('|');
    });
    api.GetFundingMethod(vm.module_id).then(function(data){
      vm.methods = data.output.funding_method;
    });
    // vm.GetEntityWebDefaults().then(function(data){
    //   vm.entityWebDefaults = data[0];
    // });
    $rootScope.headerTitle = 'Deal Draw Down';

    GetPurposeOfPayments();


    vm.expandableClass = "";
    vm.beneficiary, vm.step4 = {},  vm.balance = 0, vm.funding_balance = 0, vm.payeesList = [] ,
      vm.rate = {}, vm.fundings = [], vm.Approvals = [], vm.ApprovalNext = false;
    vm.approvalCounter = 0, vm.finalDeal = {} ;
    vm.selectedIndex = 0;
    // vm.buy, vm.sell;
    vm.loader = true;
    var confirmXml,dealBase, records = [], oneRequestInProcess = true;
    // vm.addFundingRecord = addFundingRecord;
    vm.addPayeeTotal   = addPayeeTotal;
    vm.approveNow = approveNow;
    vm.countCurrencies = countCurrencies;
    vm.deletePayee = deletePayee;
    vm.deleteFunding = deleteFunding;
    vm.editPayee = editPayee;
    vm.editFunding = editFunding;
    vm.nextStep = nextStep;
    vm.openApprovals = openApprovals;
    vm.openBeneficiaryDetail = openBeneficiaryDetail;
    vm.openConfirmation = openConfirmation;
    vm.openFirstlevelRecord = openFirstlevelRecord;
    vm.openFundingDetails = openFundingDetails;
    vm.openSecondlevelRecord = openSecondlevelRecord;
    vm.openSummaryPage = openSummaryPage;
    vm.parseFloating = parseFloating;
    vm.payeeAndFundingTotal = payeeAndFundingTotal;
    vm.previousStep = previousStep;
    vm.protectAmount = protectAmount;
    vm.removeSummaryRecordDrawdown = removeSummaryRecordDrawdown;
    vm.removeSummaryRecordPredelievery = removeSummaryRecordPredelievery;
    vm.selectChildRow = selectChildRow;
    vm.selectDeal      = selectDeal;
    vm.selectValueDate = selectValueDate;
    vm.showPayeeTableFunc = showPayeeTable;
    vm.step1Next = step1Next;
    vm.step2Back = step2Back;
    vm.step2Next = step2Next;
    vm.toggleChildRows = toggleChildRows;
    vm.toggleExtraRows = toggleExtraRows;
    vm.UpdateBalance = UpdateBalance;
    vm.UpdateDeal = updateDeal;
    vm.UpdateFundingBalance = UpdateFundingBalance;
    // vm.verifyAddPayee = verifyAddPayee;
    // vm.verifyAddFunding = verifyAddFunding;
    vm.addPayee = addPayee;
    vm.selectPayee = selectPayee;

    vm.GetSettlementsByMethodsAndCurrencyInBookASpot = function(methods, currency_id){
      return soap_api.GetSettlementsByMethodsAndCurrencyInBookASpot(methods, currency_id).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT Table1').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              name: jQuery(e).find('NAME').text(),
              parent_id: jQuery(e).find('PARENT_ID').text(),
              entity: jQuery(e).find('ENTITY').text(),
              type: jQuery(e).find('TYPE').text(),
              is_non_third_party: jQuery(e).find('IS_NON_THIRD_PARTY').text(),
              payment_type_name: jQuery(e).find('PAYMENT_TYPE_NAME').text(),
              payment_type: jQuery(e).find('PAYMENT_TYPE').text(),
              act_seg_id: jQuery(e).find('ACT_SEG_ID').text(),
              paymentmethod_name: jQuery(e).find('PAYMENTMETHOD_NAME').text(),
              paymentmethod: jQuery(e).find('PAYMENTMETHOD').text(),
              currency_id: jQuery(e).find('CURRENCY_ID').text(),
              currency_name: jQuery(e).find('CURRENCY_NAME').text(),
              beneficiary_accountcode: jQuery(e).find('BENEFICIARY_ACCOUNTCODE').text(),
              country_name: jQuery(e).find('COUNTRY_NAME').text(),
              beneficiary_email: jQuery(e).find('BENEFICIARY_EMAIL').text(),
              beneficiary_instructions: jQuery(e).find('BENEFICIARY_INSTRUCTIONS').text(),
              beneficiarybank_name: jQuery(e).find('BENEFICIARYBANK_NAME').text(),
              purpose: jQuery(e).find('PURPOSE').text(),
              internal_reference: jQuery(e).find('INTERNAL_REFERENCE').text(),
              purpose_of_payment: jQuery(e).find('PURPOSE_OF_PAYMENT').text(),
              currency_item_id: jQuery(e).find('CURRENCY_ITEM_ID').text(),
            };
            row.push(temp);
          });
          return row;
        }else{
          console.log(success.data);
        }
      });
    };
    vm.GetPaymentsByMethodAndCurrencyInBookASpot = function(method, currency_id){
      return soap_api.GetPaymentsByMethodAndCurrencyInBookASpot(method, currency_id).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT Table1').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              name: jQuery(e).find('NAME').text(),
              parent_id: jQuery(e).find('PARENT_ID').text(),
              entity: jQuery(e).find('ENTITY').text(),
              type: jQuery(e).find('TYPE').text(),
              is_non_third_party: jQuery(e).find('IS_NON_THIRD_PARTY').text(),
              payment_type_name: jQuery(e).find('PAYMENT_TYPE_NAME').text(),
              payment_type: jQuery(e).find('PAYMENT_TYPE').text(),
              act_seg_id: jQuery(e).find('ACT_SEG_ID').text(),
              paymentmethod_name: jQuery(e).find('PAYMENTMETHOD_NAME').text(),
              paymentmethod: jQuery(e).find('PAYMENTMETHOD').text(),
              currency_id: jQuery(e).find('CURRENCY_ID').text(),
              currency_name: jQuery(e).find('CURRENCY_NAME').text(),
              beneficiary_accountcode: jQuery(e).find('BENEFICIARY_ACCOUNTCODE').text(),
              country_name: jQuery(e).find('COUNTRY_NAME').text(),
              beneficiary_email: jQuery(e).find('BENEFICIARY_EMAIL').text(),
              beneficiary_instructions: jQuery(e).find('BENEFICIARY_INSTRUCTIONS').text(),
              beneficiarybank_name: jQuery(e).find('BENEFICIARYBANK_NAME').text(),
              purpose: jQuery(e).find('PURPOSE').text(),
              purpose_of_payment: jQuery(e).find('PURPOSE_OF_PAYMENT').text(),
              currency_item_id: jQuery(e).find('CURRENCY_ITEM_ID').text(),
            };
            row.push(temp);
          });
          return row;
        }else{
          console.log(success.data);
        }
      });
    };

    function editPayee(data)
    {
      //var currency_object = _.filter(vm.paymentCurrencies, {currency_name: vm.toUpperCase(vm.step1.currency_to_buy)});
      //var currency_id = currency_object[0].currency_id;
      var currency = vm.beneficiary.buyCurrencyID;
      vm.GetSettlementsByMethodsAndCurrencyInBookASpot(vm.PaymentMethodsJoined, currency).then(function(data){
        vm.payeesList = data;
        vm.loader = true;
      });
      var record = {};
      angular.copy(data, record);
      $mdDialog.show({
        locals:{dataToPass: record},
        fullscreen: vm.customFullscreen,
        clickOutsideToClose: true,
        scope: $scope,
        preserveScope: true,
        templateUrl: 'app/main/forexdealing/draw_down/edit_payee.html',
        parent: angular.element(document.body),
        controller: function dialogController($scope, $mdDialog){
          $scope.closeDialog = function() {
            $mdDialog.hide();
            vm.payeeTable = {
              routing: null,
              payee_payor: null,
              bank_name: null,
              country: null
            };
          };
          $scope.info = {
            expanded: false
          };
          $scope.payeeTable = {
            routing: null,
            payee_payor: null,
            bank_name: null,
            country: null
          };
          $scope.showPayeeTable = true;
          $scope.isDisabled = true;
          $scope.showSinglePayeeTable = true;
          $scope.data = {
            expanded: false
          };
          $scope.routingFilter = function(payee){
            if(vm.payeeTable){
              if(
                (payee.internal_reference.toLowerCase().indexOf(vm.payeeTable.routing || '') !== -1)
              ){
                return true;
              }else{
                return false;
              }
            }else{
              return true;
            }
          };
          $scope.payeeFilter = function (payee) {
            if(vm.payeeTable){
              if(
                (payee.name.toLowerCase().indexOf(vm.payeeTable.payee_payor || '') !== -1)
              ){
                return true;
              }else{
                return false;
              }
            }else{
              return true;
            }
          };
          $scope.bankFilter = function (payee) {
            if(vm.payeeTable){
              if(
                (payee.beneficiarybank_name.toLowerCase().indexOf(vm.payeeTable.bank_name || '') !== -1)
              ){
                return true;
              }else{
                return false;
              }
            }else{
              return true;
            }
          };
          $scope.countryFilter = function (payee) {
            if(vm.payeeTable){
              if(
                (payee.country_name.toLowerCase().indexOf(vm.payeeTable.country || '') !== -1)
              ){
                return true;
              }else{
                return false;
              }
            }else{
              return true;
            }
          };
          $scope.payeeSelectFunc = function(event){
            $scope.showSinglePayeeTable = false;
            var element = event.target;
            if(element.nodeName.toLowerCase() == 'td'){
              var tr = angular.element(element).closest('tr');
              var payee_id = tr.attr('id');
              var payee_object = _.filter(vm.payeesList, {id: payee_id});
              vm.GetFeeByMethodByID(payee_object[0].paymentmethod).then(function(data){
                $scope.showSinglePayeeTable = true;
                if(data.status === 'FALSE'){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">'+data.error+'</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  vm.showCircleLoader = true;
                  $scope.showPayeeTable = true;
                  $scope.single.payee_ = null;
                  $scope.single.fee = null;
                  return;
                }else{

                  if(payee_object.currency_id === currency){
                    $scope.single.fee = vm.parseFloat(data.non_instrument_cost);
                  }else{
                    $scope.single.fee = vm.parseFloat(data.instrument_cost);
                  }
                  $scope.showPayeeTable = true;
                  $scope.single.payee_ = payee_object[0].name;
                  vm.singlePayeeLoader = true;
                }
              });
            }
          };
          $scope.hidePayeeTableFunc = function(){
            $scope.showPayeeTable = true;
            $scope.payeeTable = {
              routing: null,
              payee_payor: null,
              bank_name: null,
              country: null
            };
          };
          $scope.showPayeeTableFunc = function($event){
            $scope.showPayeeTable = false;
          };
          $scope.first_amount = record.amount;
          var value_date = record.value_date == '' ? new Date() : new Date(Date.parse(record.value_date));
          value_date = ('0' + (value_date.getMonth()+1)).slice(-2) + '/' + ('0' + value_date.getDate()).slice(-2) + '/' + value_date.getFullYear();

          $scope.single = {
            id: record.id || null,
            current_id: record.current_id || null,
            payee_: record.payee_ || null,
            amount: record.amount || null,
            payment_reference: record.payment_reference || null,
            payment_purpose: record.payment_purpose || null,
            internal_payment_reference: record.internal_payment_reference || null,
            payee_notification: record.payment_notification_email || null,
            fee: record.fee || null,
            value_date: value_date || null
          };
          $scope.dialogLoader = true;
          $scope.updatePayee = function(payee){
            if(vm.parseFloat(payee.amount) > 0 && payee.payee_ != null && (payee.value_date != null || payee.value_date != '')){
              $scope.dialogLoader = false;
              var payee_object = _.filter(vm.payeesList, {id: payee.id});
              if(typeof payee.amount != 'number')
                payee.amount = vm.parseFloat(payee.amount.split(',').join(''));
              else
                payee.amount = vm.parseFloat(payee.amount);
              if(vm.step3.amount == 0){
                var total = 0;
                for(var i=0; i<vm.payees.length; i++){
                  total += vm.payees[i].amount;
                }
                var supposed_total = (total - $scope.first_amount) + payee.amount;
                if(payee.amount > total){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Payee amount cannot be greater than total available amount.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  $scope.dialogLoader = true;
                  return;
                }
                if(supposed_total > total){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Payee amount cannot be greater than total available amount.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  $scope.dialogLoader = true;
                  return;
                }
              }
              vm.GetFeeByMethodByID(payee_object[0].paymentmethod).then(function(data){
                if(payee_object[0].currency_id === vm.step1.currency_to_buy){
                  payee.fee = vm.parseFloat(data.non_instrument_cost);
                }else{
                  payee.fee = vm.parseFloat(data.instrument_cost);
                }
                //var identifier = vm.payees.length + 1;
                payee = vm.mergeObjects(payee, payee_object[0]);
                //payee.current_id = identifier;
                vm.payees = _.without(vm.payees, _.findWhere(vm.payees, {id: payee.id}));
                vm.payees.push(payee);
                $scope.payeeTable.method = null;
                var total = 0;
                for(var i=0; i<vm.payees.length; i++){
                  total += vm.payees[i].amount;
                }
                var remaining_amount = vm.fixedBuy - total;

                vm.step3.amount = remaining_amount;
                vm.beneficiary.buy = remaining_amount;
                vm.balance = vm.step3.amount;
                $mdToast.show({
                  template: '<md-toast class="md-toast success">Payee Record Updated.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                vm.SendPayeesAndDisable = false;
                vm.showCircleLoader = true;
                $scope.dialogLoader = true;
                $scope.closeDialog();
              });
            }else{
              $mdToast.show({
                template: '<md-toast class="md-toast error">Fill required fields.</md-toast>',
                hideDelay: 4000,
                position: 'bottom right'
              });
              return;
            }
          };
        }
      });
    };
    function editFunding(data)
    {
      var currency = vm.beneficiary.sellCurrencyID;
      var record = {};
      angular.copy(data, record);
      vm.GetPaymentsByMethodAndCurrencyInBookASpot(data.method, data.currency_id).then(function(data){
        $scope.accounts = data;
      });
      $mdDialog.show({
        locals:{dataToPass: record},
        fullscreen: vm.customFullscreen,
        clickOutsideToClose: true,
        scope: $scope,
        preserveScope: true,
        templateUrl: 'app/main/forexdealing/draw_down/edit_funding.html',
        parent: angular.element(document.body),
        controller: function dialogController($scope, $mdDialog){
          $scope.closeDialog = function() {
            $mdDialog.hide();
            vm.payeeTable = {
              routing: null,
              payee_payor: null,
              bank_name: null,
              country: null
            };
          };
          $scope.isDisabled = true;
          $scope.PickAccount = function(data){
            if(data.method != null && currency != null){
              vm.accounts = [];
              var currency_object = _.filter(vm.fundingCurrencies, {currency_id: currency});
              var currency_id = currency_object[0].currency_id;
              vm.GetPaymentsByMethodAndCurrencyInBookASpot(data.method, currency_id).then(function(data){
                $scope.accounts = data;
                console.log(data);
              });
            }
          };
          $scope.first_amount = vm.parseFloat(record.amount);
          $scope.single = {
            id: record.id || null,
            current_id: record.current_id || null,
            currency_id: record.currency_id || null,
            currency_name: record.currency_name || null,
            payee_: record.payee_ || null,
            amount: $scope.first_amount || null,
            fee: record.fee,
            method: record.method || null,
            account_picker: record.account_picker || null,
            value_date: record.value_date || null
          };

          $scope.dialogLoader = true;
          $scope.updateFunding = function(funding){
            if(funding.method != null && vm.parseFloat(funding.amount) > 0){
              $scope.dialogLoader = false;
              var amount = vm.parseFloating(funding.amount);
              var total = 0;
              for(var i=0; i<vm.fundings.length; i++){
                total += vm.parseFloat(vm.fundings[i].amount);
              }
              console.log('Total ' + total);
              if(vm.step4.amount == null){
                var supposed_total = (total - $scope.first_amount) + amount;
                supposed_total = vm.parseFloat(supposed_total);
                if(amount > total){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Funding amount cannot be greater than total available amount.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  $scope.dialogLoader = true;
                  return;
                }
                if(supposed_total > total){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Funding amount cannot be greater than total available amount.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  $scope.dialogLoader = true;
                  return;
                }
              }else{
                var amount = vm.parseFloating(funding.amount);
                if(vm.step4.amount != 0 && amount > vm.step4.amount){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Funding amount cannot be greater than total available amount.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  $scope.dialogLoader = true;
                  return;
                }
              }
              //update funding record
              if(funding.account_picker != null){
                //account is present.
                console.log(funding);
                for(var i=0; i<vm.fundings.length; i++){
                  if(vm.fundings[i].current_id == funding.current_id){
                    vm.fundings[i].amount = vm.parseFloating(funding.amount);
                    vm.fundings[i].currency_id = funding.currency_id;
                    vm.fundings[i].currency_name = funding.currency_name;
                    vm.fundings[i].value_date = funding.value_date;
                    //var account = _.filter($scope.accounts, {id:funding.account_picker});
                    //console.log(account);
                    //vm.fundings[i].payee_ = account[0].name;
                    vm.fundings[i].method = funding.method;
                    vm.fundings[i].account_picker = funding.account_picker;
                  }
                }
                var total = 0;
                for(var i=0; i<vm.fundings.length; i++){
                  total += vm.fundings[i].amount;
                }
                fundingAmBalUpdate(total);
                $mdToast.show({
                  template: '<md-toast class="md-toast success">Funding record updated.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                if(vm.step4.amount == 0){
                  vm.customShow = true;
                }
                vm.SendFundingAndDisable = false;
                $scope.dialogLoader = true;
                $scope.closeDialog();
              }else{
                //no account attached.
                for(var i=0; i<vm.fundings.length; i++){
                  if(vm.fundings[i].current_id == funding.current_id){
                    vm.fundings[i].amount = vm.parseFloating(funding.amount);
                    vm.fundings[i].currency_id = funding.currency_id;
                    vm.fundings[i].currency_name = funding.currency_name;
                    vm.fundings[i].method = funding.method;
                    vm.fundings[i].value_date = funding.value_date;
                    vm.fundings[i].payee_ = funding.payee_;
                  }
                }
                var total = 0;
                for(var i=0; i<vm.fundings.length; i++){
                  total += vm.fundings[i].amount;
                }


                fundingAmBalUpdate(total);
                $mdToast.show({
                  template: '<md-toast class="md-toast success">Funding record updated.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                if(vm.step4.amount == 0){
                  vm.customShow = true;
                }
                vm.SendFundingAndDisable = false;
                $scope.dialogLoader = true;
                $scope.closeDialog();
              }
            }else{
              $mdToast.show({
                template: '<md-toast class="md-toast error">Fill required fields.</md-toast>',
                hideDelay: 4000,
                position: 'bottom right'
              });
              vm.customShow = false;
              return;
            }

          };
        }
      });
    };
    function fundingAmBalUpdate(total)
    {
      var remaining_amount = vm.fixedSell - total;
      vm.step4.amount = remaining_amount;
      vm.beneficiary.sell = remaining_amount;
      vm.funding_balance = vm.step4.amount;
    }




    //Step 1

    function payeeAndFundingTotal(payeFund, value){
      var payeTotal, fundTotal;
      if(payeFund === 'payee'){
        payeTotal = vm.payees.reduce(function(total_, elem){
            total_ += elem[value];
            return total_;
          },0)


       return payeTotal;
      }

      fundTotal = vm.fundings.reduce(function(total_, elem){
        total_ += elem[value];
        return total_;
      },0)


      return fundTotal;


    }

    vm.GetUserApprovalRights = function(deal, module){
      return soap_api.GetUserApprovalRights(deal, module).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.ValidateBusinessDate = function(date){
      return soap_api.ValidateBusinessDate(date).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function(i,e){
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              recordid: jQuery(e).find('RECORDID').text(),
              error: jQuery(e).find('ERROR').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    Number.prototype.formatMoney = function(c, d, t){
      var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c)));
      var j = i.length > 3 ? i.length % 3 : 0;
      return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };
    function selectPayee(payee){
      vm.step3.payee_ = addBalance(payee);
      vm.step3.payee_id = payee.id;
      vm.showPayeeTable = true;
      $mdDialog.hide();
    };


    function addBalance(payee){
      var singlePayee;
      var balance;
      balance = parseFloat(payee.balance);
      if(payee.paymentmethod === "5") {
        payee.name = payee.name + " " + (balance).formatMoney(2, '.', ',');
      }
      return payee;
    }
    function deletePayee(payee, index){
      if(payee.amount > 0){
        vm.step3.amount = vm.step3.amount + payee.amount;
        // vm.balance.copy_amount = vm.step3.amount + payee.amount;
        vm.balance = 0;
        vm.payees.splice(index, 1);
        // vm.payees = _.without(vm.payees, _.findWhere(vm.payees, {current_id: payee.current_id}));
        $mdToast.show({
          template: '<md-toast class="md-toast info">Payee record removed from the list.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
      }
    }
    function deleteFunding(funding, index){
      if(funding.amount > 0){
        vm.step4.amount = vm.step4.amount + funding.amount;
        // vm.balance.copy_amount = vm.step3.amount + payee.amount;
        vm.funding_balance = 0;
        vm.fundings.splice(index, 1);
        // vm.payees = _.without(vm.payees, _.findWhere(vm.payees, {current_id: payee.current_id}));
        $mdToast.show({
          template: '<md-toast class="md-toast info">Payee record removed from the list.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
      }
    }
    vm.SendSettlements = function(deal, value_date, settlements){
      return soap_api.SendSettlements(deal, value_date, settlements).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text()
          };
          row.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.PickAccount = function(data){
      if(data.method != null && vm.beneficiary.sellCurrencyID != null){
        vm.accounts = [];
        api.GetPaymentsByMethodAndCurrencyInBookASpot(data.method, vm.beneficiary.sellCurrencyID).then(function(data){
          vm.accounts = data.output.table1;
        });
      }
    };
    vm.toUpperCase = function(str){
      str = str || '';
      return str.toUpperCase();
    };
    vm.GetFeeForSettlements = function(input_xml){
      return soap_api.GetFeeForSettlements(input_xml).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT PAYMENT').each(function(i,e){
            var temp = {
              buy_amount: jQuery(e).find('BUY_AMOUNT').text(),
              buy_currency_id: jQuery(e).find('BUY_CURRENCY_ID').text(),
              buy_currency: jQuery(e).find('BUY_CURRENCY').text()
            };
            row.push(temp);
          });
          jQuery(response).find('OUTPUT SETTLEMENT').each(function(i,e){
            var temp = {
              fee: jQuery(e).find('FEE').text(),
              fee_type: jQuery(e).find('FEE_TYPE').text(),
              fee_currency_id: jQuery(e).find('FEE_CURRENCY_ID').text(),
              fee_currency: jQuery(e).find('FEE_CURRENCY').text(),
              action_id: jQuery(e).find('ACTION_ID').text(),
              action_name: jQuery(e).find('ACTION_NAME').text(),
              sell_currency_id: jQuery(e).find('SELL_CURRENCY_ID').text(),
              sell_currency: jQuery(e).find('SELL_CURRENCY').text(),
              sell_amount: jQuery(e).find('SELL_AMOUNT').text(),
              rate: jQuery(e).find('RATE').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetFeeByMethodByID = function(ID){
      return soap_api.GetFeeByMethodByID(ID).then(function(success){
        //var result = [];
        var fee = [];
        var instrument_fee = [];
        var same_currency_fee = [];
        var error = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT FEE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            free_instruments: jQuery(e).find('FREE_INSTRUMENTS').text(),
            instrument_cost: jQuery(e).find('INSTRUMENT_COST').text(),
            non_instrument_cost: jQuery(e).find('NON_INSTRUMENT_COST').text(),
            inv_act_type_id: jQuery(e).find('INV_ACT_TYPE_ID').text(),
            inv_act_type: jQuery(e).find('INV_ACT_TYPE').text(),
            charge_id: jQuery(e).find('CHARGE_ID').text(),
            charge_type: jQuery(e).find('CHARGE_TYPE').text(),
            is_allowed: jQuery(e).find('IS_ALLOWED').text(),
            source_id: jQuery(e).find('SOURCE_ID').text(),
          };
          fee.push(temp);
        });
        jQuery(response).find('OUTPUT INSTRUMENT_FEE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            currency_id: jQuery(e).find('Currency_ID').text(),
            instrument_cost: jQuery(e).find('Instrument_Cost').text(),
            cost_type: jQuery(e).find('COST_TYPE').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            cost_type_name: jQuery(e).find('COST_TYPE_NAME').text()
          };
          instrument_fee.push(temp);
        });
        jQuery(response).find('OUTPUT SAME_CURRENCY_FEE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            currency_id: jQuery(e).find('Currency_ID').text(),
            instrument_cost: jQuery(e).find('Instrument_Cost').text(),
            cost_type: jQuery(e).find('COST_TYPE').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            cost_type_name: jQuery(e).find('COST_TYPE_NAME').text()
          };
          same_currency_fee.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          error.push(temp);
        });
        var result = {
          fee : fee,
          instrument_fee: instrument_fee,
          same_currency_fee: same_currency_fee,
          error: error
        }
        return result;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.isXML = function(xml){
      try{
        var xmlDoc = jQuery.parseXML(xml);
        return true;
      }catch(error){
        return false;
      }
    };
    vm.payees = [];
    vm.mergeObjects = function(obj1,obj2){
      var obj3 = {};
      for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
      for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
      return obj3;
    };
    function addPayee(){
      vm.loader = false;
      vm.SendPayeesAndDisable = true;
      var payee = {};
      if(vm.step3.value_date === null || vm.step3.value_date === ''){
        $mdToast.show({
          template: '<md-toast class="md-toast error">Value date is mandatory. Please check Payee detail.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        vm.loader = true;
        vm.SendPayeesAndDisable = false;
        return false;
      }else{
        var value_date = new Date(Date.parse(vm.step3.value_date));

        var input = '<INPUT><DATE>'+convertDate(value_date)+'</DATE></INPUT>';
        QueryFactory.query('ValidateBusinessDate', input).then(function(data){
          console.log(data);
          var data = convertDataToJson(data.data, 'result');
          if(data.status == 'TRUE'){
            if(vm.step3.payee_ != null && vm.parseFloat(vm.step3.amount) > 0){
              angular.copy(vm.step3, payee);
              if(vm.popMandate == true && payee.payment_purpose == null){
                $mdToast.show({
                  template: '<md-toast class="md-toast error">Purpose of payment is mandatory. Please check details.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                vm.loader = true;
                vm.SendPayeesAndDisable = false;
                return false;
              }

              if(payee.payment_notification_email != '' && payee.payment_notification_email != null){
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if(payee.payment_notification_email.match(re) == null){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Email is invalid.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  vm.loader = true;
                  vm.SendPayeesAndDisable = false;
                  return false;
                }
              }
              //$scope.showSinglePayeeTable = false;
              var payee_object = _.filter(vm.payeesList, {id: payee.payee_id});

              vm.GetFeeByMethodByID(payee_object[0].paymentmethod).then(function(data){
                if( data.error.length > 0 ){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Payee cannot be selected, because payment method: '+data.error[0].error+'</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  vm.loader = true;
                  vm.SendPayeesAndDisable = false;
                  return false;
                }
                else{
                  var id = vm.payees.length + 1;
                  payee.current_id = id;
                  payee = vm.mergeObjects(payee, payee_object[0]);
                  payee.value_date = vm.step3.value_date;
                  payee.rate = vm.rate.exchange_rate;
                  if(payee_object[0].currency_id ===  vm.beneficiary.buyCurrencyID){
                    //if we have same currencies
                    if(data.same_currency_fee.length > 0){
                      if(data.same_currency_fee[0].currency_id == payee_object[0].currency_id){
                        payee.fee = vm.parseFloating(data.same_currency_fee[0].instrument_cost);
                      }else{
                        payee.fee = vm.parseFloating(data.fee[0].non_instrument_cost);
                      }
                    }else{
                      payee.fee = vm.parseFloating(data.fee[0].non_instrument_cost);
                    }
                  }else{
                    //else we have different currencies
                    if(data.instrument_fee.length > 0){
                      var found = false;
                      var instrument_fee = {};
                      for(var i=0; i<data.instrument_fee.length; i++){
                        if(data.instrument_fee[i].currency_id == 2){
                          found = true;
                          instrument_fee = data.instrument_fee[i];
                        }
                      }
                      if(found){
                        payee.fee = vm.parseFloating(instrument_fee.instrument_cost);
                      }else{
                        payee.fee = vm.parseFloating(data.fee[0].instrument_cost);
                      }
                    }else{
                      payee.fee = vm.parseFloating(data.fee[0].instrument_cost);
                    }
                  }
                  if(typeof payee.amount != 'number')
                    payee.amount = vm.parseFloat(payee.amount.split(',').join(''));
                  else
                    payee.amount = vm.parseFloat(payee.amount);
                  vm.payees.push(payee);
                  console.log(vm.payees);
                  if(vm.balance > 0){
                    vm.step3.amount = vm.balance;
                    vm.beneficiary.buy = vm.balance;
                    vm.balance = 0;
                    vm.loader = true;
                    vm.SendPayeesAndDisable = false;
                  }else{
                    vm.beneficiary.buy = 0;
                    vm.step3.amount = null;
                    vm.step4.amount = vm.fixedSell;
                    vm.beneficiary.sell = vm.fixedSell;
                    //send settlements
                    if(vm.payees.length > 0){
                      $mdToast.show({
                        template: '<md-toast class="md-toast info">Adding Payees..</md-toast>',
                        hideDelay: 1000,
                        position: 'bottom right'
                      });
                      // var step1_value_date = vm.step1.value_date == '' ? new Date() : new Date(Date.parse(vm.step1.value_date));
                      // step1_value_date = step1_value_date.getFullYear() + '-' + ('0' + (step1_value_date.getMonth()+1)).slice(-2) + '-' + ('0' + step1_value_date.getDate()).slice(-2);

                      var settlements = '<DEAL>' +
                        '<MODULE_ID>' + vm.module_id+ '</MODULE_ID>' +
                        '<VALUE_DATE>'+convertDate(vm.step1.value_date)+'</VALUE_DATE>' +
                        '<BUY_CURRENCY><ID>'+vm.beneficiary.sellCurrencyID+'</ID></BUY_CURRENCY>' +
                        '<BUY_AMOUNT>'+vm.fixedSell+'</BUY_AMOUNT>' +
                        '</DEAL>';

                      vm.loader = false;
                      for(var i=0; i<vm.payees.length; i++) {
                        if(typeof vm.payees[i].value_date == 'string' && vm.payees[i].value_date != null){
                          var value_date = new Date(Date.parse(vm.payees[i].value_date));
                        }else if(typeof vm.payees[i].value_date == 'object'){
                          var value_date = vm.payees[i].value_date;
                        }else if(typeof vm.payees[i].value_date == 'undefined'){
                          var value_date = new Date(Date.parse(vm.step1.value_date));
                        }
                        value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                        if(vm.payees[i].payment_purpose != null){
                          var purpose_object = _.filter(vm.paymentPurposes, {id: vm.payees[i].payment_purpose});
                          var purpose_of_payment = purpose_object[0].id;
                        }else{
                          var purpose_of_payment = 1;
                        }
                        var payment_reference = vm.payees[i].payment_reference == null ? '' : vm.payees[i].payment_reference;
                        var internal_payment_reference = vm.payees[i].internal_payment_reference == null ? '' : vm.payees[i].internal_payment_reference;
                        settlements += '<SETTLEMENTS>';
                        settlements += '<ACTION><ID>'+vm.payees[i].paymentmethod+'</ID></ACTION>';
                        settlements += '<SELL_CURRENCY><Value>'+vm.payees[i].currency_name+'</Value></SELL_CURRENCY>';
                        settlements += '<SELL_AMOUNT>'+vm.payees[i].amount+'</SELL_AMOUNT>';
                        settlements += '<RATE>'+vm.rate.exchange_rate+'</RATE>';
                        settlements += '</SETTLEMENTS>';
                      }
                      vm.GetFeeForSettlements(settlements).then(function(data){
                        var payment = data.shift();
                        vm.fixedSell = payment.buy_amount;
                        vm.step4.amount = payment.buy_amount;
                        vm.beneficiary.sell = payment.buy_amount;
                        data = data;
                        vm.loader = false;
                        settlements = '';
                        for(var i=0; i<data.length; i++) {
                          vm.payees[i].total = data[i].sell_amount;
                          if(typeof vm.payees[i].value_date == 'string' && vm.payees[i].value_date != null){
                            var value_date = new Date(Date.parse(vm.payees[i].value_date));
                          }else if(typeof vm.payees[i].value_date == 'object'){
                            var value_date = vm.payees[i].value_date;
                          }else if(typeof vm.payees[i].value_date == 'undefined'){
                            var value_date = new Date(Date.parse(vm.step1.value_date));
                          }
                          value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                          if(vm.payees[i].payment_purpose != null){
                            var purpose_object = _.filter(vm.paymentPurposes, {id: vm.payees[i].payment_purpose});
                            var purpose_of_payment = purpose_object[0].id;
                          }else{
                            var purpose_of_payment = 1;
                          }
                          var payment_reference = vm.payees[i].payment_reference == null ? '' : vm.payees[i].payment_reference;
                          //var internal_payment_reference = payeesList[i].internal_payment_reference == null ? '' : payeesList[i].internal_payment_reference;
                          var uid = 'INREF-'+ vm.toUpperCase(Math.random().toString(16).slice(2));
                          var internal_payment_reference = vm.payees[i].internal_payment_reference == null ? uid : vm.payees[i].internal_payment_reference;
                          if(vm.payees[i].internal_payment_reference == null){
                            vm.payees[i].internal_payment_reference = uid;
                          }
                          settlements += '<SETTLEMENTS>';
                          settlements += '<ACTION><ID>'+vm.payees[i].paymentmethod+'</ID></ACTION>';
                          settlements += '<CURRENCY><Value>'+vm.payees[i].currency_name+'</Value></CURRENCY>';
                          settlements += '<AMOUNT>'+data[i].sell_amount+'</AMOUNT>';
                          settlements += '<VALUE_DATE>'+value_date+'</VALUE_DATE>';
                          if(vm.payees[i].paymentmethod == 5){
                            if(vm.payees[i].id != null)
                              settlements += '<ACCOUNT><ID>'+vm.payees[i].id+'</ID></ACCOUNT>';
                            settlements += '<ACT_TYPE>E</ACT_TYPE>';
                          }else{
                            if(vm.payees[i].id != null)
                              settlements += '<PAYEE><ID>'+vm.payees[i].id+'</ID></PAYEE>';
                          }
                          settlements += '<REFERENCE>'+payment_reference+'</REFERENCE>';
                          settlements += '<BENEFICIARY_REFERENCE>'+payment_reference+'</BENEFICIARY_REFERENCE>';
                          settlements += '<NOTIFY_MY_RECEIPIENT></NOTIFY_MY_RECEIPIENT>';
                          settlements += '<BENEFICIARY_REMARKS></BENEFICIARY_REMARKS>';
                          settlements += '<REMITTENCE_REMARKS></REMITTENCE_REMARKS>';
                          settlements += '<PURPOSE_OF_PAYMENT>'+purpose_of_payment+'</PURPOSE_OF_PAYMENT>';
                          settlements += '<IS_EQUIVALENT>0</IS_EQUIVALENT>';
                          settlements += '<INTERNAL_REFERENCE>'+internal_payment_reference+'</INTERNAL_REFERENCE>';
                          settlements += '<FEE>'+data[i].fee+'</FEE>';
                          vm.payees[i].fee = data[i].fee;
                          settlements += '<FEE_TYPE><ID>'+data[i].fee_type+'</ID></FEE_TYPE>';
                          settlements += '<FEE_CURRENCY><ID>'+data[i].fee_currency_id+'</ID></FEE_CURRENCY>';
                          settlements += '</SETTLEMENTS>';
                        }
                        debugger;
                        vm.SendSettlements(vm.dealNumber(vm.beneficiary.recordnumber).replace(/\s+/g,''), convertDate(vm.step1.value_date), settlements).then(function(data){
                          if(data[0].status == 'FALSE'){
                            $mdToast.show({
                              template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                              hideDelay: 4000,
                              position: 'bottom right'
                            });
                            vm.loader = true;
                            vm.SendPayeesAndDisable = false;
                            vm.step3.payee_ = null;
                            vm.step3.amount = 0;
                          }else{
                            $mdToast.show({
                              template: '<md-toast class="md-toast success">Payees Added. Deal Updated.</md-toast>',
                              hideDelay: 4000,
                              position: 'bottom right'
                            });

                            if(vm.beneficiary.sellCurrencyID == $rootScope.entityWebDefaults.funding_cur_id){
                              var default_method = _.filter(vm.methods, {id: $rootScope.entityWebDefaults.funding_method_id});
                              if(default_method.length > 0){
                                vm.step4.method = default_method[0].id;
                                vm.GetPaymentsByMethodAndCurrencyInBookASpot(default_method[0].id, vm.beneficiary.sellCurrencyID).then(function(data){
                                  if(data.length > 0){
                                    vm.accounts = data;
                                    if($rootScope.entityWebDefaults.funding_template_id != ''){
                                      var default_account = _.filter(vm.accounts, {id: $rootScope.entityWebDefaults.funding_template_id});
                                      vm.step4.account_picker = default_account[0].id;
                                    }
                                    vm.nextStep();
                                    vm.loader = true;
                                    vm.SendPayeesAndDisable = false;
                                  }else{
                                    vm.nextStep();
                                    vm.loader = true;
                                    vm.SendPayeesAndDisable = false;
                                  }
                                });
                              }
                            }else{
                              vm.nextStep();
                              console.log(vm.fixedBuy)
                              vm.loader = true;
                              vm.SendPayeesAndDisable = false;
                            }
                          }
                        });
                      });
                    }
                  }
                  vm.balance = 0;
                  vm.step3.payee_ = null;
                  // vm.step3.value_date = null;
                  vm.step3.internal_payment_reference = null;
                  vm.step3.no_notification = null;
                  vm.step3.payment_notification_email = null;
                  vm.step3.payment_purpose = null;
                  vm.step3.payment_reference = null;

                  vm.loader = true;
                  vm.SendPayeesAndDisable = false;
                  return true;
                }
              });

            }else{
              $mdToast.show({
                template: '<md-toast class="md-toast error">Please fill all required fields.</md-toast>',
                hideDelay: 4000,
                position: 'bottom right'
              });
              return true;
            }
          }else{
            $mdToast.show({
              template: '<md-toast class="md-toast error">'+data.error+'</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
            vm.loader = true;
            vm.SendPayeesAndDisable = false;
            return false;
          }
        });
      }

    };
    vm.SendFundingAndDisable = false;
    vm.addFundingRecord = function(funding){
      vm.deal = {
        deal_number : vm.beneficiary.recordnumber,
        id : vm.beneficiary.recordid
      }
      vm.SendFundingAndDisable = true;
      console.log('here1');
      var record = {};
      angular.copy(funding, record);
      if(record.method != null && vm.parseFloat(record.amount) > 0){
        vm.showCircleLoader = false;
        vm.SendFundingAndDisable = true;
        console.log('here2');
        if( record.account_picker != null && record.account_picker != ''){
          vm.GetFeeByMethodByID(record.method).then(function(data){
            console.log(data);
            var identifier = vm.fundings.length + 1;
            record.current_id = identifier;
            record.value_date = new Date();

            if( vm.beneficiary.buyCurrencyID === vm.beneficiary.sellCurrencyID){
              record.fee = data.non_instrument_cost;
            }else{
              record.fee = data.instrument_cost;
            }
            if(typeof record.amount != 'number')
              record.amount = vm.parseFloat(record.amount.split(',').join(''));
            else
              record.amount = vm.parseFloat(record.amount);
            var account_object = _.filter(vm.accounts, {id: record.account_picker});
            record = vm.mergeObjects(record, account_object[0]);
            record.payee_ = account_object[0].name;
            console.log(record);
            vm.fundings.push(record);
            vm.showCircleLoader = true;
            vm.SendFundingAndDisable = false;
            console.log(10);

            if(vm.funding_balance > 0)
            {
              updateBalanceAndAmountStep4();
            }
            else
            {
              // vm.funding_balance.copy_amount = 0;
              vm.step4.amount = null;
              vm.step4.account_picker = null;
              vm.SendFundingAndDisable = false;
              if(vm.fundings.length > 0){
                //send payments
                vm.showCircleLoader = false;
                vm.SendFundingAndDisable = true;
                console.log('here3');
                $mdToast.show({
                  template: '<md-toast class="md-toast info">Adding Funding..</md-toast>',
                  hideDelay: 2000,
                  position: 'bottom right'
                });
                var payments = '';
                var feesPromises = [];
                var fundingList = [];

                for(var i=0; i<vm.fundings.length; i++) {
                  feesPromises.push(vm.GetFeeByMethodByID(vm.fundings[i].method));
                  fundingList.push(vm.fundings[i]);
                }
                $q.all(feesPromises).then(function(data){
                  for(var i=0; i<data.length; i++) {
                    var value_date = new Date(Date.parse(fundingList[i].value_date));
                    value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                    if(fundingList[i].account_picker != null){
                      var payee = fundingList[i].account_picker;
                    }else{
                      var payee = null;
                    }
                    // var currency = _.filter(vm.fundingCurrencies, {currency_id: vm.step1.currency_to_sell});
                    // var currency_id = currency[0].currency_id;
                    var currency_id = vm.beneficiary.sellCurrencyID;
                    payments += '<PAYMENTS>';
                    payments += '<ACTION><ID>'+fundingList[i].method+'</ID></ACTION>';
                    payments += '<CURRENCY><ID>'+currency_id+'</ID></CURRENCY>';
                    payments += '<AMOUNT>'+vm.parseFloat(fundingList[i].amount)+'</AMOUNT>';
                    payments += '<VALUE_DATE>'+value_date+'</VALUE_DATE>';
                    if(fundingList[i].method == 5){
                      if(payee != null)
                        payments += '<ACCOUNT><ID>'+payee+'</ID></ACCOUNT>';
                      payments += '<ACT_TYPE>E</ACT_TYPE>';
                    }else{
                      if(payee != null)
                        payments += '<PAYEE><ID>'+payee+'</ID></PAYEE>';
                    }
                    payments += '</PAYMENTS>';
                  }
                  var date = new Date(Date.parse(vm.step1.value_date));
                  vm.SendPayments(vm.beneficiary.recordnumber, date, payments).then(function(data){
                    if(data[0].status == 'FALSE'){
                      $mdToast.show({
                        template: '<md-toast class="md-toast error">There is an error while adding funding record.</md-toast>',
                        hideDelay: 4000,
                        position: 'bottom right'
                      });
                      $mdToast.show({
                        template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                        hideDelay: 4000,
                        position: 'bottom right'
                      });
                      vm.showCircleLoader = true;
                      vm.SendFundingAndDisable = false;
                      console.log(8);
                      return;
                    }else{
                      $mdToast.show({
                        template: '<md-toast class="md-toast success">Funding(s) Added. Deal Updated.</md-toast>',
                        hideDelay: 4000,
                        position: 'bottom right'
                      });

                      vm.GetUserApprovalRights(vm.deal, vm.module_id).then(function(data){
                        if(data[0].status == 'TRUE'){
                          vm.skipApprovalStep = false;
                        }else{
                          vm.skipApprovalStep = true;
                        }
                        vm.showCircleLoader = true;
                        vm.SendFundingAndDisable = false;
                        console.log(7);
                        vm.nextStep();
                      });
                    }
                  });
                });
              }
            }
            vm.funding_balance = 0;
            vm.step4.method = null;
            vm.step4.account_picker = null;
            vm.SendFundingAndDisable = false;
            console.log(6);
            return true;
          });
        }else{
          var identifier = vm.fundings.length + 1;
          record.current_id = identifier;
          record.value_date = vm.defaultValueDate || new Date();
          if(typeof record.amount != 'number')
            record.amount = vm.parseFloat(record.amount.split(',').join(''));
          else
            record.amount = vm.parseFloat(record.amount);
          // var currency = _.filter(vm.fundingCurrencies, {currency_id: vm.beneficiary.sellCurrencyID});
          record.currency_id = vm.beneficiary.sellCurrencyID;
          record.currency_name = vm.beneficiary.sellCurrencyName;
          record.payee_ = 'N/A';
          record.fee = 0;
          console.log(record);
          vm.fundings.push(record);
          vm.showCircleLoader = true;
          vm.SendFundingAndDisable = false;
          console.log(5);
          if(vm.funding_balance > 0){
            updateBalanceAndAmountStep4();
          }else{
            // vm.funding_balance.copy_amount = 0;
            vm.step4.amount = null;
            vm.step4.account_picker = null;
            //now send payments
            if(vm.fundings.length > 0){
              //send payments
              vm.showCircleLoader = false;
              console.log('11');
              $mdToast.show({
                template: '<md-toast class="md-toast info">Adding Funding..</md-toast>',
                hideDelay: 2000,
                position: 'bottom right'
              });
              var payments = '';
              var feesPromises = [];
              var fundingList = [];

              for(var i=0; i<vm.fundings.length; i++) {
                feesPromises.push(vm.GetFeeByMethodByID(vm.fundings[i].method));
                fundingList.push(vm.fundings[i]);
              }
              $q.all(feesPromises).then(function(data){
                for(var i=0; i<data.length; i++) {
                  var value_date = new Date(Date.parse(fundingList[i].value_date));
                  value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                  if(fundingList[i].account_picker != null){
                    var payee = fundingList[i].account_picker;
                  }else{
                    var payee = null;
                  }
                  // var currency_id = currency[0].currency_id;
                  // var currency = _.filter(vm.fundingCurrencies, {currency_id: vm.beneficiary.sellCurrencyID});
                 var currency_id = vm.beneficiary.sellCurrencyID;

                  payments += '<PAYMENTS>';
                  payments += '<ACTION><ID>'+fundingList[i].method+'</ID></ACTION>';
                  payments += '<CURRENCY><ID>'+currency_id+'</ID></CURRENCY>';
                  payments += '<AMOUNT>'+vm.parseFloat(fundingList[i].amount)+'</AMOUNT>';
                  payments += '<VALUE_DATE>'+value_date+'</VALUE_DATE>';
                  if(fundingList[i].method == 5){
                    if(payee != null)
                      payments += '<ACCOUNT><ID>'+payee+'</ID></ACCOUNT>';
                    payments += '<ACT_TYPE>E</ACT_TYPE>';
                  }else{
                    if(payee != null)
                      payments += '<PAYEE><ID>'+payee+'</ID></PAYEE>';
                  }
                  payments += '</PAYMENTS>';
                }
                var date = new Date(Date.parse(vm.step1.value_date));
                vm.SendPayments(vm.beneficiary.recordnumber, date, payments).then(function(data){
                  if(data[0].status == 'FALSE'){
                    $mdToast.show({
                      template: '<md-toast class="md-toast error">There is an error while adding funding record.</md-toast>',
                      hideDelay: 4000,
                      position: 'bottom right'
                    });
                    $mdToast.show({
                      template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                      hideDelay: 4000,
                      position: 'bottom right'
                    });
                    vm.showCircleLoader = true;
                    vm.SendFundingAndDisable = false;
                    return;
                  }else{
                    $mdToast.show({
                      template: '<md-toast class="md-toast success">Funding(s) Added. Deal Updated.</md-toast>',
                      hideDelay: 4000,
                      position: 'bottom right'
                    });

                    vm.GetUserApprovalRights(vm.deal, vm.module_id).then(function(data){
                      if(data[0].status == 'TRUE'){
                        vm.skipApprovalStep = false;
                      }else{
                        vm.skipApprovalStep = true;
                      }
                      vm.showCircleLoader = true;
                      vm.SendFundingAndDisable = false;
                      //console.log('2');
                      vm.nextStep();
                    });
                  }
                });
              });
            }
          }
          vm.funding_balance = 0;
          vm.step4.method = null;
          vm.step4.account_picker = null;
          return true;
        }
      }else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">Method cannot be empty.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        vm.showCircleLoader = true;
        vm.SendFundingAndDisable = false;
        console.log(3);
      }
    };
    vm.SendPayments = function(deal, value_date, payments){
      return soap_api.SendPayments(deal, value_date, payments).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text()
          };
          row.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.addTotalFundingRecord = function(funding){
      vm.SendFundingAndDisable = true;
      console.log('here6');
      if(funding.method != null && funding.amount != null){
        if( funding.method.length != 0){
          vm.showCircleLoader = false;
          if(vm.funding_balance === 0){
            vm.showCircleLoader = true;
            vm.SendFundingAndDisable = false;
            console.log(1);
          }
          if(vm.addFundingRecord(funding)){
            vm.showCircleLoader = true;
            vm.SendFundingAndDisable = true;
            console.log('here7');
          }
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">Funding method can not be empty.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.showCircleLoader = true;
          vm.SendFundingAndDisable = false;
          console.log(2);
        }
      }else{
        if(vm.fundings.length > 0 && vm.funding_balance == 0){
          //now send payments
          if(vm.fundings.length > 0){
            //send payments
            //console.log('2');
            vm.showCircleLoader = false;
            vm.SendFundingAndDisable = true;
            console.log('here8');
            $mdToast.show({
              template: '<md-toast class="md-toast info">Adding Funding..</md-toast>',
              hideDelay: 2000,
              position: 'bottom right'
            });
            var payments = '';
            var feesPromises = [];
            var fundingList = [];

            for(var i=0; i<vm.fundings.length; i++) {
              feesPromises.push(vm.GetFeeByMethodByID(vm.fundings[i].method));
              fundingList.push(vm.fundings[i]);
            }
            $q.all(feesPromises).then(function(data){
              for(var i=0; i<data.length; i++) {
                var value_date = new Date(Date.parse(fundingList[i].value_date));
                value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                if(fundingList[i].account_picker != null){
                  var payee = fundingList[i].account_picker;
                }else{
                  var payee = null;
                }
                // var currency = _.filter(vm.fundingCurrencies, {currency_id: vm.step1.currency_to_sell});
                // var currency_id = currency[0].currency_id;
                var currency_id = vm.beneficiary.sellCurrencyID;
                payments += '<PAYMENTS>';
                payments += '<ACTION><ID>'+fundingList[i].method+'</ID></ACTION>';
                payments += '<CURRENCY><ID>'+currency_id+'</ID></CURRENCY>';
                payments += '<AMOUNT>'+vm.parseFloat(fundingList[i].amount)+'</AMOUNT>';
                payments += '<VALUE_DATE>'+value_date+'</VALUE_DATE>';
                if(fundingList[i].method == 5){
                  if(payee != null)
                    payments += '<ACCOUNT><ID>'+payee+'</ID></ACCOUNT>';
                  payments += '<ACT_TYPE>E</ACT_TYPE>';
                }else{
                  if(payee != null)
                    payments += '<PAYEE><ID>'+payee+'</ID></PAYEE>';
                }
                payments += '</PAYMENTS>';
              }
              var date = new Date(Date.parse(vm.step1.value_date));
              vm.SendPayments(vm.beneficiary.recordnumber, date, payments).then(function(data){
                if(data[0].status == 'FALSE'){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">There is an error while adding funding record.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  vm.showCircleLoader = true;
                  vm.SendFundingAndDisable = false;
                  return;
                }else{
                  $mdToast.show({
                    template: '<md-toast class="md-toast success">Funding(s) Added. Deal Updated.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });

                  vm.GetUserApprovalRights(vm.deal, vm.module_id).then(function(data){
                    if(data[0].status == 'TRUE'){
                      vm.skipApprovalStep = false;
                    }else{
                      vm.skipApprovalStep = true;
                    }
                    vm.showCircleLoader = true;
                    vm.nextStep();
                  });
                }
              });
            });
          }
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please Fill in all the required Fields.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.showCircleLoader = true;
          vm.SendFundingAndDisable = false;
        }
      }
    };

    vm.parseFloat = function(number){
      return parseFloat(number);
    };
    function addFundingRecord(step){
      nextStep();
    }

    // function addFundingRecord(step){
    //   nextStep();
    // }

    function addPayeeTotal(){
      if(vm.step3.payee_ != null && vm.step3.amount != null){
        if(vm.step3.payee_){
          vm.loader = false;
          vm.SendPayeesAndDisable = true;
          if(vm.addPayee() === true){
            vm.loader = true;
            vm.SendPayeesAndDisable = false;
          }
        }
      }else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please fill all required fields.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
      }
    };
    function countCurrencies(buy, sell){
      var currencies = vm.transactions.filter(function(celem){
        return celem.buy_currency_name === buy && celem.sell_currency_name ===  sell;
      });
      return currencies.length;
    };
    function nextStep(){

      var index = (vm.selectedIndex === vm.max) ? 0 : vm.selectedIndex + 1;
      vm.selectedIndex = index;
      var currentTab = $( '#tab_'+index);
      $('md-tab-item').each(function () {
        if( $(this).hasClass('md-active') ){
          $(this).addClass('md-previous');
          $(this).prevAll().addClass('md-previous');
        }
      });
    };
    function openApprovals(){
      vm.selectedIndex++;
    };
    function openBeneficiaryDetail(){
      vm.selectedIndex++;
    }
    function openConfirmation(){
      vm.selectedIndex++;
    };
    function openFirstlevelRecord(id){
      id = id + 1;
      //Getting current style of first level dropdown
      var currentFirstLevelStatus = document.getElementById('first-level-dropdown-'+id).style.display;

      //Showing First Level Record If Its Hidden
      if(currentFirstLevelStatus == 'none' || currentFirstLevelStatus == ''){

        document.getElementById('first-level-dropdown-'+id).style.display = "block";
      }
      //Hiding First Level Record If Its Shown
      else{

        document.getElementById('first-level-dropdown-'+id).style.display = "none";
      }

    };
    function openFundingDetails(){
      vm.selectedIndex++;
    };
    function openSecondlevelRecord(id,event){

      var str = event.currentTarget.children[0].className;
      console.log(str);
      console.log();
      if(findWord('fa-expand',str)){
        $(event.currentTarget.children[0]).addClass('fa-arrows-alt');
        $(event.currentTarget).addClass('expanded');
        $(event.currentTarget.children[0]).removeClass('fa-expand');
      }
      if(findWord('fa-arrows-alt',str)){
        $(event.currentTarget.children[0]).removeClass('fa-arrows-alt');
        $(event.currentTarget).removeClass('expanded');
        $(event.currentTarget.children[0]).addClass('fa-expand');
      }

      if(vm.expandableClass == id){
        vm.expandableClass = 0;
      }
      else{
        vm.expandableClass =  id;
      }


      //Getting current style of first level dropdown
      var currentSecondLevelStatus = document.getElementById('third-level-dropdown-'+id).style.display;
      //Showing First Level Record If Its Hidden

      if(currentSecondLevelStatus == 'none' ){
        document.getElementById('third-level-dropdown-'+id).style.display = "table";
      }
      //Hiding First Level Record If Its Shown
      else{
        document.getElementById('third-level-dropdown-'+id).style.display = "none";
      }

    };
    function openSummaryPage(){
      vm.selectedIndex++;
    };
    function parseFloating(value, s){
      var the_amount = +value;
      if(typeof value === 'string'){
        the_amount = value.split(',').join('');
        the_amount = parseFloat(the_amount).toFixed(2);
      }
      if(typeof value === 'number'){
        the_amount = parseFloat(the_amount).toFixed(2);
      }

      return the_amount;
    };
    function previousStep () {
      var index = (vm.selectedIndex === 0) ? 0 : vm.selectedIndex - 1;
      vm.selectedIndex = index;
      var currentTab = $( '#tab_'+index);
      angular.element('md-tab-item').each(function(){
        if( angular.element(this).hasClass('md-active') ){
          angular.element(this).removeClass('md-previous');
          angular.element(this).prev().removeClass('md-previous')
        }
      });
    };
    function protectAmount(event){
      var input = event.key;
      var re = /\d/i;
      if(input != 'Backspace' && input != '.' && input != 'Delete' && input != 'Del' && input != 'Decimal' && input != 'Left' && input != 'Right' && input != 'Tab' && input != 'ArrowLeft' && input != 'ArrowRight'){
        if(input.match(re) == null){
          event.preventDefault();
          return false;
        }
      }else{
        if(input == '.' || input == 'Decimal'){
          re = /\./g;
          if(event.target.value.length > 0){
            if(event.target.value.match(/\./g) != null){
              if(event.target.value.match(/\./g).length > 0){
                event.preventDefault();
                return false;
              }
            }
          }
        }
      }
    };
    function removeSummaryRecordDrawdown(event, elem, index){
      vm.step2Records.drawdown.splice(index, 1);
      // var parent = angular.element(event.target).closest('tr');
      // parent.remove();
    };
    function removeSummaryRecordPredelievery(event, elem, index){
      vm.step2Records.predelievery.splice(index, 1);
      // var parent = angular.element(event.target).closest('tr');
      // parent.remove();
    };
    function selectChildRow(event){
      var parent = angular.element(event.target).closest('tr');
      if(parent.hasClass('selected')){
        parent.removeClass('selected');
        //parent.closest('tbody').find('.extra').removeClass('child-show').addClass('child-hide');
      }else{
        parent.addClass('selected');
      }
    };

    function showPayeeTable(event){
      var currency = vm.beneficiary.buyCurrencyID;
      $mdDialog.show({
        locals:{dataToPass: currency},
        fullscreen: true,
        clickOutsideToClose: true,
        scope: $scope,
        preserveScope: true,
        templateUrl: 'app/main/forexdealing/draw_down/steps/modern-popup.html',
        parent: angular.element(document.body),
        controller: function dialogController($scope, $mdDialog){
          $scope.dialogLoader = false;
          if(!(vm.payeesList.length > 0)){
            api.GetSettlementsByMethodsAndCurrencyInBookASpot(currency, vm.PaymentMethodsJoined).then(function(data){
              vm.payeesList = data.output.table1;
              console.log(vm.payeesList);
              $scope.dialogLoader = true;
            });
          }else{
            $scope.dialogLoader = true;
          }
          $scope.closeDialog = function() {
            $mdDialog.hide();
          };
          $scope.showFilter = true;
          $scope.toggleFilter = function(){
            $scope.showFilter = !$scope.showFilter;
          };
        }
      });
    }
    function step1Next(){
      var xml = '<INPUT>';
      vm.loader = false;
      var records = [];
      var results = [];
      var temp = [];
      var isPredelivery = true;
      angular.element('tr.selected').each(function(i, e){
        var data = angular.element(e).data('obj');
        records.push(data);
      })
      var asd;
      records.forEach(function(elem){
        if(elem.drawdown_type === 'PREDELIVERY')
        {
          isPredelivery = false;

          var input = '<INPUT>'+
            '<TRANS_ID>'+elem.trans_id+'</TRANS_ID>' +
            '<DRAWDOWN_VALUEDATE>'+convertDate(vm.step1.value_date)+'</DRAWDOWN_VALUEDATE>' +
            '</INPUT>';
          temp.push(elem.trans_id);
          var jugaar = [];
          results.push(QueryFactory.query('GetWebPredeliveryRates', input));
          $q.all(results).then(function(response){
            var data = convertDataToJson(response[0].data, 'predelivery_rate');
            if(!data.length){
              jugaar.push(data);
            }
            jugaar.map(function(elem, index){
              elem['trans_id'] = temp[index];
              return elem;
            });

          }).finally(function() {
            asd =  records.map(function(elm){
              var element;
              jugaar.some(function(elem){

                if(elm.trans_id === elem.trans_id){
                  elm['buy_amount'] = elem.buy_amount;
                  elm['buy_currency'] = elem.buy_currency;
                  elm['exchange_rate'] = elem.exchange_rate;
                  elm['exchange_spread'] = elem.exchange_spread;
                  elm['market_rate'] = elem.market_rate;
                  elm['market_spread'] = elem.market_spread;
                  elm['orginal_exchange_rate'] = elem.orginal_exchange_rate;
                  elm['orginal_market_rate'] = elem.orginal_market_rate;
                  elm['sell_amount'] = elem.sell_amount;
                  elm['sell_currency'] = elem.sell_currency;
                  elm['swap_points'] = elem.swap_points;
                  element = elm;
                  return;
                }
              })

              return element;
            })
            asd.forEach(function(element){
              if(element !== undefined){
                xml += '<DETAIL>' +
                  '<TRANS_ID>'+element.trans_id+'</TRANS_ID>' +
                  '<BUY_AMOUNT>'+element.buy_amount+'</BUY_AMOUNT>' +
                  '<EXCHANGE_RATE>'+element.exchange_rate+ '</EXCHANGE_RATE>'+
                  '<MARKET_RATE>'+element.market_rate+'</MARKET_RATE>'+
                  '<SELL_AMOUNT>' +element.sell_amount+'</SELL_AMOUNT>' +
                  '</DETAIL>';
              }

            });

           var isAllPredelivery = records.every(function (elem) {
              return elem.drawdown_type === 'PREDELIVERY'
            })
            if(isAllPredelivery){
              createDrawdown();
            }
            else if(!isAllPredelivery){

              createDrawdown();
            }

          })

        }
        if(elem.drawdown_type === 'DRAWDOWN'){
          xml += '<DETAIL>' +
            '<TRANS_ID>'+elem.trans_id+'</TRANS_ID>' +
            '<BUY_AMOUNT>'+elem.buy_amount+'</BUY_AMOUNT>' +
            '<EXCHANGE_RATE>'+elem.exchange_rate+ '</EXCHANGE_RATE>'+
            '<MARKET_RATE>'+elem.market_rate+'</MARKET_RATE>'+
            '<SELL_AMOUNT>' +elem.sell_amount+'</SELL_AMOUNT>' +
            '</DETAIL>';
        }

      });

      if(isPredelivery){
        if(records.length > 0){
          createDrawdown();
        }
        else {
          vm.loader = true;
        }


      }
      function createDrawdown(){
        if(dealBase){
          xml += '<DEAL>' +
            '<INV_ORG_ID>' + dealBase.id + '</INV_ORG_ID>' +
            '<VALUE_DATE>'+convertDate(vm.step1.value_date)+'</VALUE_DATE>' +
            '</DEAL>' +
            '</INPUT>';
          createOrValidateDrawDown('ValidateDrawDownTransactions', xml, success, err )
        }

      }
      function success(response){
        var data = convertDataToJson(response.data, 'result');

        if(data.status === "TRUE"){
          vm.nextStep();
          console.log(records);
          console.log(asd);
          vm.step2Records = records.reduce(function(returnValue, elem){
            elem.drawdown_type === 'DRAWDOWN' && returnValue['drawdown'].push(elem) || returnValue['predelievery'].push(elem)
            return returnValue;
          },{'drawdown' : [] , 'predelievery' : []})
          vm.loader = true;
          confirmXml = xml;
          return;
        }
        else{
          messageService.error(data.error, 5000);
        }
        vm.loader = true;
      }
      function err(error){
        console.log(error);
      }

      // vm.nextStep();
    };
    function step2Back(){
      vm.previousStep();
    };
    function step2Next(){

      vm.step3.value_date = vm.step1.value_date;
      vm.loader = false;
      var xml = '<INPUT>';
      if(vm.step2Records.drawdown.length >0){

        vm.step2Records.drawdown.forEach(function(elem){
           xml += makeXml(elem, xml);
        })
      }
      if(vm.step2Records.predelievery.length > 0){
        vm.step2Records.predelievery.forEach(function(elem){
         xml  +=  makeXml(elem, xml);
        })
      }

      xml += '<DEAL>' +
        '<INV_ORG_ID>' + dealBase.id + '</INV_ORG_ID>' +
        '<VALUE_DATE>'+convertDate(vm.step1.value_date)+'</VALUE_DATE>' +
        '</DEAL>' +
        '</INPUT>';


      createOrValidateDrawDown('CreateDrawDown', xml, success, err );
      function success(response){
        console.log(response);

        var data = convertDataToJson(response.data, 'result');
        if(data.status === "TRUE")
        {
          vm.step2ConfirmRecord = data;
          var input =   '<INPUT>' +
            '<RECORD>' +
            '<Value>' + vm.dealNumber(data.recordnumber).replace(/\s+/g,'') + '</Value>'+
            '</RECORD>' +
            '</INPUT>';
          QueryFactory.query('PostDrawDown', input ).then(function(response){
            var postDrawDown = convertDataToJson(response.data, 'result');
            if(angular.isDefined(postDrawDown.status) && postDrawDown.status === 'TRUE'){

              vm.beneficiary = postDrawDown;
              var buy = 0, sell =0;
              if(vm.step2Records.drawdown.length > 0)
              {
                vm.beneficiary.drawDownSum = vm.step2Records.drawdown.reduce(drawDownAndPredSum,{});
              }
              if(vm.step2Records.predelievery.length > 0)
              {
                vm.beneficiary.predieSum = vm.step2Records.predelievery.reduce(drawDownAndPredSum,{});
              }
              //Ths needs to fix ehsan
              var buyAmount, sellAmount, buyCurrencyName, sellCurrencyName, buyCurrencyID, sellCurrencyID, rate;
              if(vm.beneficiary.drawDownSum && vm.beneficiary.predieSum){
                buyAmount  =  vm.beneficiary.drawDownSum.buy + vm.beneficiary.predieSum.buy;
                sellAmount = vm.beneficiary.drawDownSum.sell + vm.beneficiary.predieSum.sell;
                buyCurrencyName = vm.step2Records.drawdown[0].buy_currency_name;
                sellCurrencyName = vm.step2Records.drawdown[0].sell_currency_name;
                buyCurrencyID = vm.step2Records.drawdown[0].buy_cur_id;
                sellCurrencyID = vm.step2Records.drawdown[0].sell_cur_id;
                rate = vm.step2Records.drawdown[0].exchange_rate;
              }
              if(vm.beneficiary.drawDownSum && !vm.beneficiary.predieSum){
                buyAmount =  vm.beneficiary.drawDownSum.buy;
                sellAmount = vm.beneficiary.drawDownSum.sell;
                buyCurrencyName = vm.step2Records.drawdown[0].buy_currency_name;
                sellCurrencyName = vm.step2Records.drawdown[0].sell_currency_name;
                buyCurrencyID = vm.step2Records.drawdown[0].buy_cur_id;
                sellCurrencyID = vm.step2Records.drawdown[0].sell_cur_id;
                rate = vm.step2Records.drawdown[0].exchange_rate;
              }
              if(!vm.beneficiary.drawDownSum && vm.beneficiary.predieSum){

                buyAmount  =  vm.beneficiary.predieSum.buy;
                sellAmount = vm.beneficiary.predieSum.sell;
                buyCurrencyName = vm.step2Records.predelievery[0].buy_currency_name;
                sellCurrencyName = vm.step2Records.predelievery[0].sell_currency_name;
                buyCurrencyID = vm.step2Records.predelievery[0].buy_cur_id;
                sellCurrencyID = vm.step2Records.predelievery[0].sell_cur_id;
                rate = vm.step2Records.drawdown[0].exchange_rate;
              }
              vm.rate.exchange_rate = rate;
              vm.fixedBuy = buyAmount;
              vm.fixedSell = sellAmount
              vm.beneficiary.buy = buyAmount;
              vm.beneficiary.sell = sellAmount;
              vm.beneficiary.buyCurrencyID = buyCurrencyID;
              vm.beneficiary.sellCurrencyID = sellCurrencyID;
              vm.beneficiary.buyCurrencyName = buyCurrencyName;
              vm.beneficiary.sellCurrencyName = sellCurrencyName;
              vm.step3.amount = buyAmount;
              vm.step4.amount = sellAmount;
              vm.nextStep();


              function drawDownAndPredSum(returnValue, elem)
              {
                buy += parseFloat(elem.buy_amount);
                returnValue['buy'] = buy;
                sell += parseFloat(elem.sell_amount);
                returnValue['sell'] = sell;
                return returnValue
              }
            }
            else
            {

              messageService.error(postDrawDown.error, 5000);
            }
            vm.loader = true;

          });

        }
        else{
          messageService.error(data.error, 5000);
        }



      }

      function err(error){
        console.log(error);
      }

    };

    function makeXml (element, xml){
      xml = '<DETAIL>' +
        '<TRANS_ID>'+element.trans_id+'</TRANS_ID>' +
        '<BUY_AMOUNT>'+element.buy_amount+'</BUY_AMOUNT>' +
        '<EXCHANGE_RATE>'+element.exchange_rate+ '</EXCHANGE_RATE>'+
        '<MARKET_RATE>'+element.market_rate+'</MARKET_RATE>'+
        '<SELL_AMOUNT>' +element.sell_amount+'</SELL_AMOUNT>' +
        '</DETAIL>';
      return xml;
    }
    function toggleChildRows(event){
      var parent = angular.element(event.target).closest('tr');
      if(parent.find('.child-records').hasClass('hide')){
        parent.find('.child-records').removeClass('hide').addClass('show');
      }else{
        parent.find('.child-records').removeClass('show').addClass('hide');
      }
    };
    function toggleExtraRows(event){
      var parent = angular.element(event.target).closest('tbody');
      if(parent.find('tr.extra').hasClass('child-hide')){
        parent.find('tr.extra').removeClass('child-hide').addClass('child-show');
      }else{
        parent.find('tr.extra').removeClass('child-show').addClass('child-hide');
      }
    };
    // function verifyAddPayee(stepAmount, totalBalance) {
    //   return +vm.parseFloating(stepAmount) > 0 && +vm.parseFloating(stepAmount) < +vm.parseFloating(totalBalance) ;
    // };
    // function verifyAddFunding(stepAmount, totalBalance) {
    //   var step4Amount = stepAmount || vm.step4.amount ;
    //   if(vm.beneficiary){
    //     var step4Balanse = totalBalance || vm.beneficiary.sell;
    //     return +vm.parseFloating(stepAmount) >= 0 && +vm.parseFloating(stepAmount) < +vm.parseFloating(totalBalance) ;
    //   }
    //   var step4Balanse = totalBalance || vm.beneficiary && vm.beneficiary.sell;
    //   return +vm.parseFloating(stepAmount) >= 0 && +vm.parseFloating(stepAmount) < +vm.parseFloating(totalBalance) ;
    // };
    function UpdateBalance(event){

      // updatePayee
      var amount = parseFloat( (vm.step3.amount !== undefined) && vm.step3.amount.split(',').join('') || 0 );
      var balanceAccount = parseFloat(vm.beneficiary.buy);
      if( amount <= balanceAccount && amount > 0 ){
        vm.balance = balanceAccount;
        if( isNaN(amount) == false){
          var temp_amount = balanceAccount - amount;
          vm.balance = temp_amount;

        }
      }
      else if(amount > balanceAccount ){
        vm.balance = 0;
        vm.step3.amount = balanceAccount;
      }
      else{
        // alert("This")
        vm.balance = balanceAccount;
        vm.step3.amount = 0;
      }
    };
    function UpdateFundingBalance(event){

      // updatePayee
      var amount = parseFloat( (vm.step4.amount !== undefined) && vm.step4.amount.split(',').join('') || 0 );
      var balanceAccount = parseFloat(vm.beneficiary.sell);
      if( amount <= balanceAccount && amount > 0 ){
        vm.balance = balanceAccount;
        if( isNaN(amount) == false){
          var temp_amount = balanceAccount - amount;
          vm.funding_balance = temp_amount;

        }
      }
      else if(amount > balanceAccount ){
        // vm.balance = 0;
        vm.step4.amount = balanceAccount;
      }
      else{
        // alert("This")
        vm.funding_balance = balanceAccount;
        vm.step4.amount = 0;
      }
    };
    //  vm.getDrawDownInv = getDrawDownInv;
    // vm.loader = true;
    /**
     * openFirstlevelRecord
     * @desc Opens first level dropdown using DOM options
     * @param id
     */
    function findWord(word, str) {
      return RegExp('\\b'+ word +'\\b').test(str)
    }
    /**
     * openSecondlevelRecord
     * @desc Opens Second level dropdown using DOM options
     * @param id
     */
    /**
     * openSummaryPage
     * @desc This function will lead user to summary page
     */
    /**
     * openBeneficiaryDetail
     * @desc This function will lead user to Beneficiary page
     */
    /**
     * openFundingDetails
     * @desc This function will lead user to Funding page
     */
    /**
     * openApprovals
     * @desc This function will lead user to Approval  page
     */
    /**
     * openConfirmation
     * @desc This function will lead user to Confirmation Screen
     */
    /**
     * InsertClassOnClick
     */
    vm.InsertClassOnClick = function(){

    }
    /*
     * Send data to APIs and validate if we can proceed further.
     * */

    function GetPurposeOfPayments(){
      QueryFactory.query('GetPurposeOfPayments').then(success, err)

      function success(response){

        vm.paymentPurposes = convertDataToJson(response.data, 'purpose_of_payment');

      }
      function err(error){
        console.log(error);
      }
    }
    function GetTransactionForDrawdown(deal, date)
    {
      if(oneRequestInProcess)
      {
        oneRequestInProcess = false;
        vm.transactions = vm.transactions && (vm.uniqueTransactions = []);
        drawdownFactory.getTransactionsDrawDown(deal, date, callback);
      }
      function callback(response)
      {
        vm.loader = true;
        oneRequestInProcess = true;
        vm.transactions = response;
        vm.uniqueTransactions = _.uniq(vm.transactions, function (elem) {
          return elem.buy_currency_name && elem.sell_currency_name;
        });
      }
    }
    //Step1
    function getDefaultDate(){
      drawdownFactory.defaultDate(callback)
      function callback(response)
      {
        if(response.status === 'TRUE')
        {
          vm.step1.value_date = response.recordid;
        }
      }
    }
    function GetDrawdownInvOrgForWeb()
    {

      drawdownFactory.drawDownInventories(callback);
      function callback(response)
      {
        if(response.length > 0)
        {
          vm.drawDownInvs = [];
          vm.drawDownInvs = response;
          return
        }
        vm.drawDownInvs = [];
        vm.drawDownInvs.push(response);
      }
    }
    function updateBalanceAndAmountStep4()
    {
      vm.step4.amount = vm.funding_balance;
      vm.beneficiary.sell = vm.funding_balance;
      vm.funding_balance = 0;
      vm.loader = true;
      vm.SendFundingAndDisable = false;
    }

    function selectDeal(deal){
      dealBase = deal;
      getTransactions();
    };
    function selectValueDate(){
      var valueDate = vm.step1.value_date && vm.step1.value_date;
      getTransactions();
    };
    function getTransactions(){
      var valueDate = vm.step1 && vm.step1.value_date;
      if(dealBase && valueDate){
        vm.loader = false;
        GetTransactionForDrawdown(dealBase, valueDate);
      }
    };


    function convertDate(dt){

      var date = new Date(dt);
      return date.getFullYear() + '-' + ('0' + (date.getMonth()+1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
    }
    function createOrValidateDrawDown(method, xml, success, err){
      QueryFactory.query(method, xml).then(
        success, err
      );


      function err(error){
        console.log(error);
      }
    }
    function convertDataToJson(xml, name){
      var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
      var actualJson = XmlJson.xmlToJson(xmlDOM);
      return actualJson.output[name];
    }
    function approveNow(){
      if(!vm.ApprovalNext){
        vm.loader = false;
        userApprovalFactory.SetDealApprovals(vm.deal, dealApprovals)


        // drawdownFactory.userApprovals(vm.deal, 1227)
        // vm.GetUserApprovalRights(vm.deal, 12277).then(function(data){
        //   if(data[0].status == 'TRUE'){
        //     vm.SetDealApproval(vm.deal.deal_number).then(function(data){
        //       if(data[0].status == 'TRUE'){
        //         $mdToast.show({
        //           template: '<md-toast class="md-toast info">Deal Approved.</md-toast>',
        //           hideDelay: 4000,
        //           position: 'bottom right'
        //         });
        //         vm.GetApprovalHistory(vm.deal).then(function(data){
        //           /*console.log(data);
        //            var user = data[0];
        //            var user_row = {
        //            name: user.user_name,
        //            date: user.approval_date
        //            };*/
        //           vm.approvalCounter++;
        //           vm.Approvals = data;
        //           if(vm.Approvals.length == vm.approvalPolicy[0].approval_level){
        //             vm.ApprovalNext = true;
        //           }
        //           vm.showCircleLoader = true;
        //         });
        //       }else{
        //         $mdToast.show({
        //           template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
        //           hideDelay: 2000,
        //           position: 'bottom right'
        //         });
        //         vm.showCircleLoader = true;
        //       }
        //     });
        //   }else{
        //     $mdToast.show({
        //       template: '<md-toast class="md-toast error">You cannot approve this deal.</md-toast>',
        //       hideDelay: 4000,
        //       position: 'bottom right'
        //     });
        //   }
        // });
      }
    };
    function dealApprovals(dealApproval){
      if(dealApproval.status === 'TRUE'){
        messageService.success('Deal Approved.', 5000)
      }
      userApprovalFactory.GetApprovalHistory(vm.deal, approvalHistory)

      function approvalHistory(approval){
        console.log(approval)
        var user = {
          name: approval.user_name,
          date: approval.approval_date
        };
        vm.approvalCounter++;
        vm.Approvals.push(approval);
        if(vm.Approvals.length === parseInt(vm.approvalPolicy.approval_level)){
          vm.ApprovalNext = true;
        }
        vm.loader = true;
      }
    }
    vm.FinalizeDealPaymentSettlement = function (deal) {
      return soap_api.FinalizeDealPaymentSettlement(deal).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            recordtype: jQuery(e).find('RecordType').text(),
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text(),
            recordnumber: jQuery(e).find('RECORDNUMBER').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.GetWebDealPaymentAndSettlement = function(deal){
      return soap_api.GetWebDealPaymentAndSettlement(deal).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT WEB_DLG_DEAL_SETTLEMENT').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              settlement_id: jQuery(e).find('SETTLEMENT_ID').text(),
              deal_id: jQuery(e).find('DEAL_ID').text(),
              internal_reference: jQuery(e).find('INTERNAL_REFERENCE').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };


    function updateDeal(){
      vm.FinalizeDealPaymentSettlement(vm.deal).then(function(data){
        vm.finalDeal['id'] = vm.deal.id;
        if(data[0].status == 'TRUE'){
          // vm.finalDeal = data[0];
          for(var key in data[0]){
            vm.finalDeal[key] = data[0][key];
          }
          $mdToast.show({
            template: '<md-toast class="md-toast success">Deal finalized.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.showCircleLoader = true;
          vm.GetWebDealPaymentAndSettlement(vm.deal).then(function(settlements){
            for(var i=0; i<settlements.length; i++){
              if(settlements[i].internal_reference != ''){
                if(vm.payees[i].internal_payment_reference == settlements[i].internal_reference){
                  vm.payees[i].settlement_id = settlements[i].settlement_id;
                }
              }
            }
            vm.nextStep();
          });
          /*vm.GetWebDealPaymentAndSettlement(vm.deal).then(function(settlements){
           var reports = [];
           for(var i=0; i<settlements.length; i++){
           if(settlements[i].internal_reference != ''){
           reports.push(vm.GetWireReport(settlements[i].settlement_id));
           }
           }
           $q.all(reports).then(function(responses){
           for(var j=0; j<responses.length; j++){
           if(vm.payees[j].internal_payment_reference == settlements[j].internal_reference){
           vm.payees[j].print_link = responses[j][0].value;
           vm.payees[j].print_name = responses[j][0].recordid;
           for(var i=0; i<vm.allElectronicMethods.length; i++){
           if(vm.payees[j].paymentmethod == vm.allElectronicMethods[i].id){
           vm.payees[j].hide_print = true;
           }else{
           vm.payees[j].hide_print = false;
           }
           }
           }
           }
           }).finally(function(){
           //print file for the deal
           vm.GetDealReport(vm.deal).then(function(report){
           vm.dealReport = {
           link: report[0].value,
           name: report[0].recordid
           };
           });
           });
           });*/
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.showCircleLoader = true;
        }
      });
    };
  }
})();
