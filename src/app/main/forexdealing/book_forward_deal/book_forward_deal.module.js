(function () {
  'use strict';

  var app = angular
    .module('app.forexdealing.book_forward_deal', ['datatables'])
    .config(config);

  /** @ngInject */
  function config($stateProvider, msApiProvider) {
    // State
    $stateProvider.state('app.book_forward_deal', {
      url: '/book_forward_deal',
      views: {
        'content@app': {
          templateUrl: 'app/main/forexdealing/book_forward_deal/book_forward_deal.html',
          controller: 'BookForwardDealController as vm'
        }
      },
      resolve: {
        Invoice: function (msApi) {
          return msApi.resolve('invoice@get');
        },
        access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
      }
    });

    // Api
    msApiProvider.register('invoice', ['app/data/invoice/invoice.json']);
    //msApiProvider.register('invoice', ['app/data/invoice/invoice.json']);
  }
})();
