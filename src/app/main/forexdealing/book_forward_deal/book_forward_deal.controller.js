(function () {
  'use strict';

  angular
    .module('app.forexdealing.book_forward_deal')
    .controller('BookForwardDealController', BookForwardDealController);

  /** @ngInject */
  function BookForwardDealController(api, BlobFactory, $state, $timeout, $rootScope, $mdDialog, $scope, $interval, $mdToast, Invoice, $filter, soap_api, $q, messageService, QueryFactory, XmlJson, userApprovalFactory, drawdownFactory) {
    var vm = this;
    getGetCurrencyForDeal('buy');
    getGetCurrencyForDeal();
    getEntity();
    getApprovals();
    getEntityWebDefaults();
    getDefaultDate();
    vm.webDeal = {};
    vm.circleLoader = true;
    vm.doneFunc = doneFunc;
    vm.DeclineQuoteFunc = DeclineQuoteFunc;
    vm.convertDealNumber = convertObjects;
    function convertObjects(objectOrStringValue){

      if(typeof objectOrStringValue === 'object')
      {
        return objectOrStringValue['#text'].join(" ");
      }
      return objectOrStringValue
    }

    function getEntityWebDefaults(){
      QueryFactory.query("GetEntityWebDefaults").then(success)
      function success(webDefaults){
        vm.entityWebDefaults = convertDataToJson(webDefaults.data, 'web_default')
      }
      // vm.GetEntityWebDefaults().then(function(data){
      //   vm.entityWebDefaults = data[0];
      // });
    }
    function getApprovals(){
      userApprovalFactory.GetSavedApprovalsByModule(233, saveApprovalCallback);
      function saveApprovalCallback(saveApprovals){
        vm.approvalPolicy = saveApprovals || {};
        console.log(vm.approvalPolicy);
      }
    }
    function getDefaultDate(){
      drawdownFactory.defaultDate(callback)
      function callback(response)
      {
        if(response.status === 'TRUE')
        {
          vm.step1.value_date = response.recordid;
        }
      }
    }
    //its compulsory
    //CONSTANTS DECLARATION BEGIN
    vm.getQuote = getQuote;
    vm.fetchCurrencies = fetchCurrencies;
    vm.getCurrencyNames = getCurrencyNames;
    vm.parseFloating = parseFloating;
    vm.nextStep = nextStep;
    vm.previousStep = previousStep;
    vm.printDeal = function(){
      vm.circleLoader = false;
      var input = '<?xml version="1.0" encoding="UTF-8"?><INPUT><Report><BATCH><ID>'+vm.webDeal.id+'</ID></BATCH></Report></INPUT>';
      QueryFactory.query('GetDealReport', input).then(success, error)
      function success(report)
      {
        vm.circleLoader = true;
        var reportData = convertDataToJson(report.data, 'result');
        if(reportData.status === 'TRUE')
        {
              var blob = BlobFactory.b64toBlob(reportData.value, 'application/pdf');
              saveAs(blob, report.id);
        }
        console.log(report)
      }
      function error(err)
      {
        console.log(err);
      }

    };
    function doneFunc(){
      $state.reload();
    };

    function stopLaunchCircular(){
      var circular_timer = angular.element('#circular-timer-box #circular-timer');
      circular_timer.TimeCircles().stop();
      cancelInterval();
    }
    vm.acceptQuoteFunc = acceptQuoteFunc;

    function nextStep() {
      var index = (vm.selectedIndex === vm.max) ? 0 : vm.selectedIndex + 1;
      vm.selectedIndex = index;
      vm.showLoader();
      var currentTab = $('#tab_' + index);
      $('md-tab-item').each(function () {
        if ($(this).hasClass('md-active')) {
          $(this).addClass('md-previous');
          $(this).prevAll().addClass('md-previous');
        }
      });
    };
    vm.showLoader = function () {
      if (vm.selectedIndex === 1) {
        vm.determinateValue = 100;
        vm.expire_in = 60;
        vm.interval_ = $interval(function () {

          vm.determinateValue -= 2;
          vm.expire_in -= 1;
          if (vm.determinateValue < 1) {
            // vm.selectedIndex = 0;
            vm.determinateValue = 100;
            vm.expire_in = 60;
            $interval.cancel(vm.interval_);
          }

        }, 1000);
      } else {
        vm.determinateValue = 100;
        vm.expire_in = 60;
        $interval.cancel(vm.interval_);
        vm.interval_ = null;
      }
    };
    $rootScope.headerTitle = 'Book Forward Deal';
    vm.loader = true;
    vm.defaultSelected = true;
    vm.data = {
      group1: 1
    };
    vm.temp_rate = [];
    vm.hideQuoteScreen = false;
    vm.selectedIndex = 0;
    vm.orderType = new Array();
    vm.buyingCurrency = new Array();
    vm.sellingCurrency = new Array();
    vm.wantTo = new Array();
    vm.orderType = new Array();
    vm.step1 = {};
    //CONSTANTS DECLARATION END
    vm.moveForwardTab = function () {
      vm.selectedIndex++;
    }
    /**
     * getPlaceAnOrderData
     * @desc This function will get all dropdown data for place an order
     * @return void
     */
    vm.getPlaceAnOrderData = function () {
      //ORDER TYPE POLUTING BEGIN
      vm.orderType = [
        {
          id: 1,
          label: 'Market Watch'
        },
        {
          id: 2,
          label: 'Daily Watch'
        },
      ];
      //ORDER TYPE POLUTING BEGIN
      //BUYING CURRENCY BEGIN
      vm.buyingCurrency = [
        {
          id: 1,
          label: 'USD'
        },
        {
          id: 2,
          label: 'CNY'
        },
      ];
      //BUYING CURRENCY END
      //I WANT TO PAY BEGIN
      vm.wantTo = [
        {
          id: 1,
          label: 'Buy'
        },
        {
          id: 2,
          label: 'Sell'
        },
      ];
      //I WANT TO PAY END
      //SELLING CURRENCY END
      vm.sellingCurrency = [
        {
          id: 1,
          label: 'USD'
        },
        {
          id: 2,
          label: 'CNY'
        },
      ];
      //SELLING CURRENCY END
    }

    /**
     * submitPlaceAnOrder
     * @param Object
     * @desc This function is used to submit place an order
     */
    vm.submitPlaceAnOrderStep1 = function (data) {
      //INCREASING INDEX TO NEXT INDEX

      var isValidated = true;
      if (vm.step1.orderType == undefined || vm.step1.orderType == '') {
        $mdToast.show({
          template: '<md-toast class="md-toast error">Enter Order Type.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        isValidated = false;
      }
      if (vm.step1.currencyBuying == undefined || vm.step1.currencyBuying == '') {
        $mdToast.show({
          template: '<md-toast class="md-toast error">Select Buying Currency.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        isValidated = false;
      }
      if (vm.step1.i_want_to == undefined || vm.step1.i_want_to == '') {
        $mdToast.show({
          template: '<md-toast class="md-toast error">Select I want to pay.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        isValidated = false;
      }
      if (vm.step1.value_date == undefined || vm.step1.value_date == '') {
        $mdToast.show({
          template: '<md-toast class="md-toast error">Select Value Date.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        isValidated = false;
      }
      if (vm.step1.currencySelling == undefined || vm.step1.currencySelling == '') {
        $mdToast.show({
          template: '<md-toast class="md-toast error">Select Selling Currency.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        isValidated = false;
      }
      if (vm.step1.targetRate == undefined || vm.step1.targetRate == '') {
        $mdToast.show({
          template: '<md-toast class="md-toast error">Select Target Rate.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        isValidated = false;
      }
      if (vm.step1.expiryDate == undefined || vm.step1.expiryDate == '') {
        $mdToast.show({
          template: '<md-toast class="md-toast error">Select Expiry Date.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        isValidated = false;
      }
      if (isValidated) {
        vm.selectedIndex++;
        vm.step1 = data;
      }


    }

    //Ehsan Work
    function getGetCurrencyForDeal(buy) {
      var input, isBuy = false;
      if (buy === 'buy')
      {
        isBuy = true;
      }
      input =
        '<INPUT>' +
        '<IS_BUY>' + isBuy + '</IS_BUY>'+
        '</INPUT>';
      GetCurrencyForDeals(input, buy);
    }

    function getEntity() {
      debugger

      api.GetEntity(QueryFactory.login.CustID).then(success);
      function success(data) {

        vm.entity = data.output.entity;
      }
    }

    function convertDataToJson(xml, name) {
      var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
      var actualJson = XmlJson.xmlToJson(xmlDOM);
      return actualJson.output[name];
    }

    function fetchCurrencies(stepData, buyOrSell) {
      vm.defaultSelected = true;
      var input;
      if (buyOrSell === 'buy') {
        input = '<INPUT><CURRENCY_ID>' + stepData.currency_to_buy + '</CURRENCY_ID><IS_BUY>true</IS_BUY></INPUT>';
      }
      else {
        input = '<INPUT><CURRENCY_ID>' + stepData.currency_to_sell + '</CURRENCY_ID><IS_BUY>false</IS_BUY></INPUT>';
      }
      GetCurrencyForDeals(input, buyOrSell);

    };
    function getCurrencyNames(currency_id, buyOrSell) {
      if (currency_id) {
        var currency;
        if (buyOrSell === 'buy') {
          currency = vm.paymentCurrencies.filter(function (elem) {
            return elem.currency_id === currency_id;
          })
        }
        else {
          currency = vm.fundingCurrencies.filter(function (elem) {
            return elem.currency_id === currency_id;
          })
        }

        return currency.length > 0 && currency[0].currency_name || '';

      }
    };
    function GetCurrencyForDeals(input, buyOrSell) {
      QueryFactory.query('GetCurrencyForDeal', input).then(success, err);
      function success(currencyForDeal) {
        var buyOrSellCurrencies = convertDataToJson(currencyForDeal.data, 'table1');
        if (buyOrSell === 'buy')
        {

          if(buyOrSellCurrencies.length)
          {
            vm.fundingCurrencies =  buyOrSellCurrencies;
          }
          else {
            vm.fundingCurrencies.push(buyOrSellCurrencies)
          }

        }
        else
          {
            if(buyOrSellCurrencies.length)
            {
              vm.paymentCurrencies =  buyOrSellCurrencies;
            }
            else {
              vm.paymentCurrencies.push(buyOrSellCurrencies)
            }
          }
        // return vm.paymentCurrencies = convertDataToJson(currencyForDeal.data, 'table1');

      }

      function err(err) {
        console.log(err)
      }
    }

    function getQuote() {
      vm.step1_disabled = true;
      vm.circleLoader = false;
      vm.temp_rate = [];
      if
      (
        vm.step1.currency_to_buy != null &&
        vm.step1.currency_to_sell != null &&
        vm.step1.amount != null &&
        vm.step1.value_date != null &&
        vm.step1.value_date != '' &&
        vm.step1.i_want_to !== null
      ) {
        vm.disableQuote = true;
        var input = '<INPUT><DATE>' + convertDate(vm.step1.value_date) + '</DATE></INPUT>';
        QueryFactory.query('ValidateBusinessDate', input).then(validateDate)
        //  vm.ValidateBusinessDate(value_date).then(function(data){
        //   data = data[0];
        //   if(data.status == 'TRUE'){
        //     if(typeof vm.entity === 'undefined'){
        //       $mdToast.show({
        //         template: '<md-toast class="md-toast error">Entity record not found.</md-toast>',
        //         hideDelay: 4000,
        //         position: 'bottom right'
        //       });
        //       vm.temp_rate = [];
        //       vm.step1_disabled = false;
        //       vm.circleLoader = true;
        //       vm.disableQuote = false;
        //       vm.hideQuoteScreen = false;
        //       vm.acceptQuoteDisable = false;
        //       vm.declineQuoteDisable = false;
        //       return false;
        //     }
        //     if(vm.step1.currency_to_buy === vm.step1.currency_to_sell){
        //       //both currencies are same.
        //       if(vm.step1.i_want_to == 'buy')
        //       {
        //           vm.GetRateByIDS(vm.step1.currency_to_buy, amount, vm.step1.currency_to_sell, 0, value_date, vm.entity.entity.name).then(function(data){
        //             vm.temp_rate = data[0];
        //             vm.rate = vm.temp_rate;
        //             if(data[0].exchange_rate == 0){
        //               $mdToast.show({
        //                 template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
        //                 hideDelay: 2000,
        //                 position: 'bottom right'
        //               });
        //               vm.temp_rate = [];
        //               vm.step1_disabled = false;
        //               vm.circleLoader = true;
        //               vm.disableQuote = false;
        //               return false;
        //             }
        //             vm.hideQuoteScreen = true;
        //             vm.nextStep();
        //           });
        //       }
        //       else{
        //         vm.GetRateByIDS(vm.step1.currency_to_buy, 0, vm.step1.currency_to_sell, amount, value_date, vm.entity.entity.name).then(function(data){
        //           vm.temp_rate = data[0];
        //           vm.rate = vm.temp_rate;
        //           if(data[0].exchange_rate == 0){
        //             $mdToast.show({
        //               template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
        //               hideDelay: 2000,
        //               position: 'bottom right'
        //             });
        //             vm.temp_rate = [];
        //             vm.step1_disabled = false;
        //             vm.circleLoader = true;
        //             vm.disableQuote = false;
        //             return false;
        //           }
        //           vm.hideQuoteScreen = true;
        //           vm.nextStep();
        //         });
        //       }
        //     }else{
        //       //both currencies are not same.
        //       if(vm.step1.i_want_to == 'buy'){
        //         vm.GetRateByIDS(vm.step1.currency_to_buy, amount, vm.step1.currency_to_sell, 0, value_date, vm.entity.entity.name).then(function(data){
        //           vm.temp_rate = data[0];
        //           vm.rate = vm.temp_rate;
        //           if(data[0].exchange_rate == 0){
        //             $mdToast.show({
        //               template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
        //               hideDelay: 2000,
        //               position: 'bottom right'
        //             });
        //             vm.temp_rate = [];
        //             vm.step1_disabled = false;
        //             vm.circleLoader = true;
        //             vm.disableQuote = false;
        //             return false;
        //           }
        //
        //           //launch circular and linear timer.
        //           var circular_timer = angular.element('#circular-timer-box');
        //           circular_timer.empty();
        //           circular_timer.append('<div id="circular-timer" data-timer="60"></div>');
        //           var timer = circular_timer.find('#circular-timer');
        //           timer.TimeCircles({
        //             "animation": "smooth",
        //             "bg_width": 1,
        //             "fg_width": 0.21,
        //             "circle_bg_color": "#AAAAAA",
        //             "direction":'Counter-clockwise',
        //             "start_angle": 160,
        //             "time": {
        //               "Days": {
        //                 "text": "Days",
        //                 "color": "#FFCC66",
        //                 "show": false
        //               },
        //               "Hours": {
        //                 "text": "Hours",
        //                 "color": "#99CCFF",
        //                 "show": false
        //               },
        //               "Minutes": {
        //                 "text": "Minutes",
        //                 "color": "#BBFFBB",
        //                 "show": false
        //               },
        //               "Seconds": {
        //                 "text": "Expires in",
        //                 "color": "#FFA834",
        //                 "show": true
        //               }
        //             }
        //           }).addListener(function(unit, value, total){
        //             if(total == -1){
        //               timer.TimeCircles().stop();
        //               $interval.cancel(vm.fetchBuyRates);
        //               vm.fetchBuyRates = null;
        //               if(vm.step1.currency_to_buy != vm.step1.currency_to_sell){
        //                 $mdToast.show({
        //                   template: '<md-toast class="md-toast error">Quote timeout.</md-toast>',
        //                   hideDelay: 4000,
        //                   position: 'bottom right'
        //                 });
        //                 vm.temp_rate = [];
        //                 vm.step1_disabled = false;
        //                 vm.circleLoader = true;
        //                 vm.disableQuote = false;
        //                 vm.hideQuoteScreen = false;
        //                 vm.acceptQuoteDisable = false;
        //                 vm.declineQuoteDisable = false;
        //                 vm.previousStep();
        //               }
        //             }else{
        //               var text_box = timer.find('.textDiv_Seconds');
        //               text_box.find('.title').remove();
        //               var new_value = value + 'Secs';
        //               text_box.append('<span class="title">'+new_value+'</span>');
        //             }
        //           });
        //           vm.fetchBuyRates = $interval(function(){
        //             vm.rate = vm.temp_rate;
        //             vm.GetRateByIDS(vm.step1.currency_to_buy, vm.parseFloating(vm.step1.amount), vm.step1.currency_to_sell, 0, new Date(Date.parse(vm.step1.value_date)), vm.entity.entity.name).then(function(data){
        //               vm.temp_rate = data[0];
        //             });
        //           }, 2000);
        //           vm.nextStep();
        //         });
        //       }else{
        //         vm.GetRateByIDS(vm.step1.currency_to_buy, 0, vm.step1.currency_to_sell, amount, value_date, vm.entity.entity.name).then(function(data){
        //           vm.temp_rate = data[0];
        //           vm.rate = vm.temp_rate;
        //           if(data[0].exchange_rate == 0){
        //             $mdToast.show({
        //               template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
        //               hideDelay: 2000,
        //               position: 'bottom right'
        //             });
        //             vm.temp_rate = [];
        //             vm.step1_disabled = false;
        //             vm.circleLoader = true;
        //             vm.disableQuote = false;
        //             return false;
        //           }
        //
        //           //launch circular and linear timer.
        //           var circular_timer = angular.element('#circular-timer-box');
        //           circular_timer.empty();
        //           circular_timer.append('<div id="circular-timer" data-timer="60"></div>');
        //           var timer = circular_timer.find('#circular-timer');
        //           timer.TimeCircles({
        //             "animation": "smooth",
        //             "bg_width": 1,
        //             "fg_width": 0.21,
        //             "circle_bg_color": "#AAAAAA",
        //             "direction":'Counter-clockwise',
        //             "start_angle": 160,
        //             "time": {
        //               "Days": {
        //                 "text": "Days",
        //                 "color": "#FFCC66",
        //                 "show": false
        //               },
        //               "Hours": {
        //                 "text": "Hours",
        //                 "color": "#99CCFF",
        //                 "show": false
        //               },
        //               "Minutes": {
        //                 "text": "Minutes",
        //                 "color": "#BBFFBB",
        //                 "show": false
        //               },
        //               "Seconds": {
        //                 "text": "Expires in",
        //                 "color": "#FFA834",
        //                 "show": true
        //               }
        //             }
        //           }).addListener(function(unit, value, total){
        //             if(total == -1){
        //               timer.TimeCircles().stop();
        //               $interval.cancel(vm.fetchSellRates);
        //               vm.fetchSellRates = null;
        //               if(vm.step1.currency_to_buy != vm.step1.currency_to_sell){
        //                 $mdToast.show({
        //                   template: '<md-toast class="md-toast error">Quote timeout.</md-toast>',
        //                   hideDelay: 4000,
        //                   position: 'bottom right'
        //                 });
        //                 vm.temp_rate = [];
        //                 vm.step1_disabled = false;
        //                 vm.circleLoader = true;
        //                 vm.disableQuote = false;
        //                 vm.hideQuoteScreen = false;
        //                 vm.acceptQuoteDisable = false;
        //                 vm.declineQuoteDisable = false;
        //                 vm.previousStep();
        //               }
        //             }else{
        //               var text_box = timer.find('.textDiv_Seconds');
        //               text_box.find('.title').remove();
        //               var new_value = value + 'Secs';
        //               text_box.append('<span class="title">'+new_value+'</span>');
        //             }
        //           });
        //           vm.fetchSellRates = $interval(function(){
        //             vm.rate = vm.temp_rate;
        //             vm.GetRateByIDS(vm.step1.currency_to_buy, 0, vm.step1.currency_to_sell, vm.parseFloating(vm.step1.amount), new Date(Date.parse(vm.step1.value_date)), vm.entity.entity.name).then(function(data){
        //               vm.temp_rate = data[0];
        //             });
        //           }, 2000);
        //           vm.nextStep();
        //         });
        //       }
        //     }
        //   }else{
        //     $mdToast.show({
        //       template: '<md-toast class="md-toast error">'+data.error+'</md-toast>',
        //       hideDelay: 4000,
        //       position: 'bottom right'
        //     });
        //     vm.temp_rate = [];
        //     vm.step1_disabled = false;
        //     vm.circleLoader = true;
        //     vm.disableQuote = false;
        //     vm.hideQuoteScreen = false;
        //     vm.acceptQuoteDisable = false;
        //     vm.declineQuoteDisable = false;
        //     return false;
        //   }
        // });
      }
      else {
        vm.step1_disabled = false;
        vm.circleLoader = true;
        emptyData();
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please fill required fields.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
      }

    }

    function validateDate(validateDt) {

      var amount = parseFloating(vm.step1.amount);
      var value_date = new Date(Date.parse(vm.step1.value_date))
      var _validateDate = convertDataToJson(validateDt.data, 'result');
      if (_validateDate.status === 'TRUE') {

        if (!vm.entity || Object.keys(vm.entity).length === 0) {
          messageService.error('Entity record not found.', 5000);
          getEntity();
          emptyData();
          return;
        }
        else if (vm.step1.currency_to_buy === vm.step1.currency_to_sell) {
          //both currencies are same.
          if (vm.step1.i_want_to === 'buy') {
            getRateByIds(vm.step1.currency_to_buy, amount, vm.step1.currency_to_sell, 0, value_date, vm.entity.name)
          }
          else {
            getRateByIds(vm.step1.currency_to_buy, 0, vm.step1.currency_to_sell, amount, value_date, vm.entity.name)
            // });
          }
        }
        else {
          //both currencies are not same.
          if (vm.step1.i_want_to === 'buy') {
            getRateByIds(vm.step1.currency_to_buy, amount, vm.step1.currency_to_sell, 0, value_date, vm.entity.name, vm.step1.i_want_to)
            // launchCrcular();
            // vm.GetRateByIDS(vm.step1.currency_to_buy, amount, vm.step1.currency_to_sell, 0, value_date, vm.entity.entity.name, vm.step1.i_want_to).then(function(data){
            //   vm.temp_rate = data[0];
            //   vm.rate = vm.temp_rate;
            //   if(data[0].exchange_rate == 0){
            //     $mdToast.show({
            //       template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
            //       hideDelay: 2000,
            //       position: 'bottom right'
            //     });
            //     vm.temp_rate = [];
            //     vm.step1_disabled = false;
            //     vm.circleLoader = true;
            //     vm.disableQuote = false;
            //     return false;
            //   }
            //
            //   //launch circular and linear timer.
            //   var circular_timer = angular.element('#circular-timer-box');
            //   circular_timer.empty();
            //   circular_timer.append('<div id="circular-timer" data-timer="60"></div>');
            //   var timer = circular_timer.find('#circular-timer');
            //   timer.TimeCircles({
            //     "animation": "smooth",
            //     "bg_width": 1,
            //     "fg_width": 0.21,
            //     "circle_bg_color": "#AAAAAA",
            //     "direction":'Counter-clockwise',
            //     "start_angle": 160,
            //     "time": {
            //       "Days": {
            //         "text": "Days",
            //         "color": "#FFCC66",
            //         "show": false
            //       },
            //       "Hours": {
            //         "text": "Hours",
            //         "color": "#99CCFF",
            //         "show": false
            //       },
            //       "Minutes": {
            //         "text": "Minutes",
            //         "color": "#BBFFBB",
            //         "show": false
            //       },
            //       "Seconds": {
            //         "text": "Expires in",
            //         "color": "#FFA834",
            //         "show": true
            //       }
            //     }
            //   }).addListener(function(unit, value, total){
            //     if(total == -1){
            //       timer.TimeCircles().stop();
            //       $interval.cancel(vm.fetchBuyRates);
            //       vm.fetchBuyRates = null;
            //       if(vm.step1.currency_to_buy != vm.step1.currency_to_sell){
            //         $mdToast.show({
            //           template: '<md-toast class="md-toast error">Quote timeout.</md-toast>',
            //           hideDelay: 4000,
            //           position: 'bottom right'
            //         });
            //         vm.temp_rate = [];
            //         vm.step1_disabled = false;
            //         vm.circleLoader = true;
            //         vm.disableQuote = false;
            //         vm.hideQuoteScreen = false;
            //         vm.acceptQuoteDisable = false;
            //         vm.declineQuoteDisable = false;
            //         vm.previousStep();
            //       }
            //     }else{
            //       var text_box = timer.find('.textDiv_Seconds');
            //       text_box.find('.title').remove();
            //       var new_value = value + 'Secs';
            //       text_box.append('<span class="title">'+new_value+'</span>');
            //     }
            //   });
            //   vm.fetchBuyRates = $interval(function(){
            //     vm.rate = vm.temp_rate;
            //     vm.GetRateByIDS(vm.step1.currency_to_buy, vm.parseFloating(vm.step1.amount), vm.step1.currency_to_sell, 0, new Date(Date.parse(vm.step1.value_date)), vm.entity.entity.name).then(function(data){
            //       vm.temp_rate = data[0];
            //     });
            //   }, 2000);
            //   vm.nextStep();
            // });
          }
          else {
            getRateByIds(vm.step1.currency_to_buy, 0, vm.step1.currency_to_sell, amount, value_date, vm.entity.name, vm.step1.i_want_to);

          }
        }
      }
    }

    function getRateByIds(buyCurrency, buyAmount, sellCurrency, sellAmount, valueDate, clientName, iWantTo) {
      var input = rateInput(buyCurrency, buyAmount, sellCurrency, sellAmount, valueDate, clientName);
      QueryFactory.query('GetRate', input).then(success);
      function success(data) {

        vm.temp_rate = temporaryRate(data.data);
        vm.rate = vm.temp_rate;
        debugger
        if ( !vm.temp_rate && !vm.temp_rate.exchange_rate || vm.temp_rate.exchange_rate === 0) {
          $mdToast.show({
            template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
            hideDelay: 2000,
            position: 'bottom right'
          });
          vm.temp_rate = [];
          emptyData();
          // vm.step1_disabled = false;
          // vm.circleLoader = true;
          // vm.disableQuote = false;
          return false;
        }
        else if (iWantTo) {

          launchCrcular();
          vm.circleLoader = true;
          if(vm.selectedIndex === 2 )
          {
            vm.fetchBuyRates = $interval(function () {
              vm.rate = vm.temp_rate;
              var input = rateInput(vm.step1.currency_to_buy, vm.parseFloating(vm.step1.amount), vm.step1.currency_to_sell, 0, new Date(Date.parse(vm.step1.value_date)), vm.entity.name);
              QueryFactory.query('GetRate', input).then(success);
              function success(data) {
                vm.temp_rate = temporaryRate(data.data);
              }

            }, 2000);
          }

        }

        else {

          vm.hideQuoteScreen = true;
        }

        vm.nextStep();
      }
    }

    // vm.currencyName = function(currency_id, fundingOrPayment){
    //   if(fundingOrPayment === 'funding'){
    //     vm.fundingCurrencies.some(getCurrency);
    //   }
    //   else {
    //     vm.fundingCurrencies.some(getCurrency);
    //   }
    //
    //   function getCurrency(element){
    //    return element.currency_id === currency_id && element.currency_name;
    //   }
    //   if(typeof vm.fundingCurrencies !== 'undefined'){
    //     for(var i=0; i<vm.fundingCurrencies.length; i++){
    //       if(vm.fundingCurrencies[i].currency_id == currency_id){
    //         return vm.fundingCurrencies[i].currency_name;
    //       }
    //     }
    //   }
    // };
    function rateInput(buyCurrency, buyAmount, sellCurrency, sellAmount, valueDate, clientName) {
      var input =
        '<Data>' +
        '<ENTITY>' +
        '<Value>' + clientName + '</Value>' +
        '</ENTITY>' +
        '<BUY_ITEM>' +
        '<ID>' + buyCurrency + '</ID>' +
        '</BUY_ITEM>' +
        '<SELL_ITEM>' +
        '<ID>' + sellCurrency + '</ID>' +
        '</SELL_ITEM>' +
        '<VALUE_DATE>' + convertDate(valueDate) + '</VALUE_DATE>' +
        '<BUY_AMOUNT>' + buyAmount + '</BUY_AMOUNT>' +
        '<SELL_AMOUNT>' + sellAmount + '</SELL_AMOUNT>' +
        '</Data>';
      return input;
    };
    function temporaryRate(data) {
      return convertDataToJson(data, 'rate');
    }

    function acceptQuoteFunc(){
      vm.showCircleLoader = false;
      vm.acceptQuoteDisable = true;
      vm.declineQuoteDisable = true;
      stopLaunchCircular();
      vm.fetchSellRates = null;
      var client = vm.entity.name;
      var trader = vm.entityWebDefaults && vm.entityWebDefaults.trader_id;
      var notes = 'test';
      var internal_remarks = 'internal remarks';
      var value_date = vm.step1.value_date == '' ? new Date() : new Date(Date.parse(vm.step1.value_date));
      value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
      var is_bach = 233;
      var deal_transaction;

      var buy_currency = _.filter(vm.paymentCurrencies, {currency_id: vm.step1.currency_to_buy});
      var sell_currency = _.filter(vm.fundingCurrencies, {currency_id: vm.step1.currency_to_sell});

      var buy_currency_id = buy_currency[0].currency_id;
      var sell_currency_id = sell_currency[0].currency_id;

      var buy_amount = vm.parseFloating(vm.rate.buy_amount);
      var sell_currency = sell_currency.currency_name;
      var sell_amount = vm.parseFloating(vm.rate.sell_amount);
      if(vm.step1.currency_to_buy === vm.step1.currency_to_sell){
        deal_transaction = dealTransactions(sell_currency_id, sell_amount, 1, 1, 1, 0, buy_currency_id, buy_amount);
      }
      else{
        deal_transaction = dealTransactions(sell_currency_id, sell_amount, vm.temp_rate.exchange_rate, vm.temp_rate.actual_market_rate, vm.temp_rate.market_rate, 0, buy_currency_id, buy_amount);
      }

      var input =
        '<INPUT> ' +
        '<DETAIL> ' +
        '<CLIENT> ' +
        '<Value>'+client+'</Value> ' +
        '</CLIENT> ' +
        '<TRADER> ' +
        '<ID>'+trader+'</ID> ' +
        '</TRADER> ' +
        '<NOTES>'+notes+'</NOTES> ' +
        '<INTERNAL_REMARKS>'+internal_remarks+'</INTERNAL_REMARKS> ' +
        '</DETAIL> ' +
        '<DEAL> ' +
        '<VALUE_DATE>'+value_date+'</VALUE_DATE> ' +
        '<IS_BATCH>'+is_bach+'</IS_BATCH> ' +
        '</DEAL>' +
        deal_transaction +
        '</INPUT>';

      if(buy_amount > 0 && sell_amount > 0 && parseFloat(vm.temp_rate.exchange_rate) >  0)
      {
        QueryFactory.query('CreateWebDeal', input).then(success)
        function success(_webDeal){

          vm.webDeal = convertDataToJson(_webDeal.data, 'table1');
          if(Object.keys(vm.approvalPolicy).length === 0){
            input = '<INPUT><DEAL><ID>'+vm.webDeal.id+'</ID></DEAL></INPUT>';
            QueryFactory.query('PostWebDeal', input).then(success);

            function success(data){

              console.log(data);
              // vm.dealNumber =
                vm.deal = convertDataToJson(data.data, 'result');
                if(vm.deal.status === 'FALSE'){
                  messageService.error(vm.deal.error, 5000);

                  vm.acceptQuoteDisable = false;
                  vm.disableQuote       = false;
                  vm.previousStep();
                  return
                }
                vm.selectedIndex = 2;
              stopLaunchCircular();
              vm.circleLoader = true;
              nextStep();
            }
          }

        }
        return
      }
      messageService.error("Amounts cannot be zero!", 5000);
      emptyData();
      vm.previousStep();




      // vm.CreateWebDeal(client, trader, notes, internal_remarks, value_date, is_bach, deal_transaction).then(function(data){
      //   if(data[0].status == 'FALSE'){
      //     $mdToast.show({
      //       template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
      //       hideDelay: 4000,
      //       position: 'bottom right'
      //     });
      //     var circular_timer = angular.element('#circular-timer-box #circular-timer');
      //     circular_timer.TimeCircles().stop();
      //     $interval.cancel(vm.fetchRates);
      //     vm.fetchRates = null;
      //     vm.acceptQuoteDisable = false;
      //     vm.declineQuoteDisable = false;
      //     vm.step1_disabled = false;
      //     vm.showCircleLoader = true;
      //     vm.previousStep();
      //   }else{
      //     vm.deal = data[0];
      //     vm.PostWebDeal(vm.deal).then(function(data){
      //       vm.post_deal = data[0];
      //       if(data[0].status == 'TRUE'){
      //         vm.postDeal = data[0];
      //         vm.temp_deal = vm.deal;
      //         vm.balance.copy_amount = vm.rate.buy_amount;
      //         vm.step1.amount = vm.rate.buy_amount;
      //         angular.copy(vm.step1, vm.quote);
      //         angular.copy(vm.step1, vm.quote_copy);
      //         vm.step3.amount = vm.rate.buy_amount;
      //         $mdToast.show({
      //           template: '<md-toast class="md-toast success">Deal Saved and Posted</md-toast>',
      //           hideDelay: 4000,
      //           position: 'bottom right'
      //         });
      //         //now get payees
      //         var currency_object = _.filter(vm.paymentCurrencies, {currency_id: vm.step1.currency_to_buy});
      //         var currency_id = currency_object[0].currency_id;
      //         vm.GetSettlementsByMethodsAndCurrencyInBookASpot(vm.payee_methods_joined, currency_id).then(function(data){
      //           vm.payeesList = data;
      //           //set default payees, currency, method
      //           var payment_currency = _.filter(vm.paymentCurrencies, {currency_id: vm.step1.currency_to_buy});
      //           var payment_currency_id = payment_currency[0].currency_id;
      //           if(payment_currency_id == vm.entityWebDefaults.payment_cur_id){
      //             var default_payee = _.filter(vm.payeesList, {id: vm.entityWebDefaults.payment_template_id});
      //             if(default_payee.length > 0){
      //               vm.step3.payee_ = default_payee[0].name;
      //               vm.step3.payee_id = default_payee[0].id;
      //               vm.showCircleLoader = true;
      //               var circular_timer = angular.element('#circular-timer-box #circular-timer');
      //               circular_timer.TimeCircles().stop();
      //               $interval.cancel(vm.fetchRates);
      //               vm.fetchRates = null;
      //             }
      //           }else{
      //             vm.showCircleLoader = true;
      //             var circular_timer = angular.element('#circular-timer-box #circular-timer');
      //             circular_timer.TimeCircles().stop();
      //             $interval.cancel(vm.fetchRates);
      //             vm.fetchRates = null;
      //           }
      //         });
      //         vm.nextStep();
      //         vm.showCircleLoader = true;
      //       }else{
      //         $mdToast.show({
      //           template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
      //           hideDelay: 6000,
      //           position: 'bottom right'
      //         });
      //
      //         // vm.showCircleLoader = true;
      //         // vm.step1_disabled = false;
      //         // vm.acceptQuoteDisable = false;
      //         // vm.declineQuoteDisable = false;
      //
      //         vm.step1_disabled = false;
      //         vm.circleLoader = true;
      //         vm.disableQuote = false;
      //         vm.hideQuoteScreen = false;
      //         vm.acceptQuoteDisable = false;
      //         vm.declineQuoteDisable = false;
      //         vm.previousStep();
      //       }
      //     });
      //   }
      // });

      // if(buy_amount > 0 && sell_amount > 0){
      //   if(!_.isEmpty(vm.deal)){
      //     vm.updateWebDeal(vm.deal.id, value_date, deal_transaction).then(function(data){
      //       if(data[0].status == 'FALSE'){
      //         $mdToast.show({
      //           template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
      //           hideDelay: 4000,
      //           position: 'bottom right'
      //         });
      //         var circular_timer = angular.element('#circular-timer-box #circular-timer');
      //         circular_timer.TimeCircles().stop();
      //         $interval.cancel(vm.fetchRates);
      //         vm.fetchRates = null;
      //         vm.step1_disabled = false;
      //         vm.circleLoader = true;
      //         vm.disableQuote = false;
      //         vm.hideQuoteScreen = false;
      //         vm.acceptQuoteDisable = false;
      //         vm.declineQuoteDisable = false;
      //         vm.previousStep();
      //       }else{
      //         vm.deal = data[0];
      //         vm.PostWebDeal(vm.deal).then(function(data){
      //           vm.post_deal = data[0];
      //           if(data[0].status == 'TRUE'){
      //             vm.postDeal = data[0];
      //             vm.temp_deal = vm.deal;
      //             vm.balance.copy_amount = vm.rate.buy_amount;
      //             vm.step1.amount = vm.rate.buy_amount;
      //             angular.copy(vm.step1, vm.quote);
      //             angular.copy(vm.step1, vm.quote_copy);
      //             vm.step3.amount = vm.rate.buy_amount;
      //             $mdToast.show({
      //               template: '<md-toast class="md-toast success">Deal Saved and Posted</md-toast>',
      //               hideDelay: 4000,
      //               position: 'bottom right'
      //             });
      //             //now get payees
      //             var currency_object = _.filter(vm.paymentCurrencies, {currency_id: vm.step1.currency_to_buy});
      //             var currency_id = currency_object[0].currency_id;
      //             vm.GetSettlementsByMethodsAndCurrencyInBookASpot(vm.payee_methods_joined, currency_id).then(function(data){
      //               vm.payeesList = data;
      //               //set default payees, currency, method
      //               var payment_currency = _.filter(vm.paymentCurrencies, {currency_id: vm.step1.currency_to_buy});
      //               var payment_currency_id = payment_currency[0].currency_id;
      //               if(payment_currency_id == vm.entityWebDefaults.payment_cur_id){
      //                 var default_payee = _.filter(vm.payeesList, {id: vm.entityWebDefaults.payment_template_id});
      //                 if(default_payee.length > 0){
      //                   vm.step3.payee_ = default_payee[0].name;
      //                   vm.step3.payee_id = default_payee[0].id;
      //                   vm.showCircleLoader = true;
      //                   var circular_timer = angular.element('#circular-timer-box #circular-timer');
      //                   circular_timer.TimeCircles().stop();
      //                   $interval.cancel(vm.fetchRates);
      //                   vm.fetchRates = null;
      //                 }
      //               }else{
      //                 vm.showCircleLoader = true;
      //                 var circular_timer = angular.element('#circular-timer-box #circular-timer');
      //                 circular_timer.TimeCircles().stop();
      //                 $interval.cancel(vm.fetchRates);
      //                 vm.fetchRates = null;
      //               }
      //             });
      //             vm.nextStep();
      //             vm.showCircleLoader = true;
      //           }else{
      //             $mdToast.show({
      //               template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
      //               hideDelay: 6000,
      //               position: 'bottom right'
      //             });
      //
      //             // vm.showCircleLoader = true;
      //             // vm.step1_disabled = false;
      //             // vm.acceptQuoteDisable = false;
      //             // vm.declineQuoteDisable = false;
      //             vm.step1_disabled = false;
      //             vm.circleLoader = true;
      //             vm.disableQuote = false;
      //             vm.hideQuoteScreen = false;
      //             vm.acceptQuoteDisable = false;
      //             vm.declineQuoteDisable = false;
      //             vm.previousStep();
      //           }
      //         });
      //       }
      //     });
      //   }else{
      //
      //   }
      // }else{
      //   $mdToast.show({
      //     template: '<md-toast class="md-toast error">Amounts cannot be zero.</md-toast>',
      //     hideDelay: 4000,
      //     position: 'bottom right'
      //   });
      //   vm.temp_rate = [];
      //   vm.step1_disabled = false;
      //   vm.circleLoader = true;
      //   vm.disableQuote = false;
      //   vm.hideQuoteScreen = false;
      //   vm.acceptQuoteDisable = false;
      //   vm.declineQuoteDisable = false;
      //   vm.previousStep();
      // }
    };
    function dealTransactions(sellCurrencyId, sellAmount, exchangeRate, actualMarketRate, marketRate, isEquivalent, buyCurrencyId, buyAmount)
    {
      var  deal_transaction = "";
      deal_transaction += '<DEAL_TRANSACTION>';
      deal_transaction += '<BUY_CURRENCY><ID>'+sellCurrencyId+'</ID></BUY_CURRENCY>';
      deal_transaction += '<BUY_AMOUNT>'+sellAmount+'</BUY_AMOUNT>';
      deal_transaction += '<EXCHANGE_RATE>'+ exchangeRate +'</EXCHANGE_RATE>';
      deal_transaction += '<ACTUAL_MARKET_RATE>'+ actualMarketRate +'</ACTUAL_MARKET_RATE>';
      deal_transaction += '<MARKET_RATE>'+ marketRate +'</MARKET_RATE>';
      deal_transaction += '<IS_EQUIVALENT>'+ isEquivalent  +'</IS_EQUIVALENT>';
      deal_transaction += '<SELL_CURRENCY><ID>'+buyCurrencyId+'</ID></SELL_CURRENCY>';
      deal_transaction += '<SELL_AMOUNT>'+buyAmount+'</SELL_AMOUNT>';
      deal_transaction += '</DEAL_TRANSACTION>';

      return deal_transaction;
    }

    function convertDate(dt) {
      var date = new Date(dt);
      return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
    }

    function parseFloating(value) {
      var the_amount = value;
      if (typeof value == 'string') {
        the_amount = value.split(',').join('');
      }
      // if(typeof value == 'number'){
      //   the_amount = parseFloat(the_amount).toFixed(2);
      // }
      the_amount = parseFloat(the_amount).toFixed(2);
      return the_amount;
    };
    function launchCrcular() {
      var circular_timer = angular.element('#circular-timer-box');
      circular_timer.empty();
      circular_timer.append('<div id="circular-timer" data-timer="60"></div>');
      var timer = circular_timer.find('#circular-timer');
      timer.TimeCircles({
        "animation": "smooth",
        "bg_width": 1,
        "fg_width": 0.21,
        "circle_bg_color": "#AAAAAA",
        "direction": 'Counter-clockwise',
        "start_angle": 160,
        "time": {
          "Days": {
            "text": "Days",
            "color": "#FFCC66",
            "show": false
          },
          "Hours": {
            "text": "Hours",
            "color": "#99CCFF",
            "show": false
          },
          "Minutes": {
            "text": "Minutes",
            "color": "#BBFFBB",
            "show": false
          },
          "Seconds": {
            "text": "Expires in",
            "color": "#FFA834",
            "show": true
          }
        }
      }).addListener(function (unit, value, total) {
        if (total === -1) {
          timer.TimeCircles().stop();
          cancelInterval();


          vm.fetchSellRates = null;
          if (vm.step1.currency_to_buy != vm.step1.currency_to_sell) {
            $mdToast.show({
              template: '<md-toast class="md-toast error">Quote timeout.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
            emptyData();
            vm.previousStep();
          }
        }
        else {
          var text_box = timer.find('.textDiv_Seconds');
          text_box.find('.title').remove();
          var new_value = value + 'Secs';
          text_box.append('<span class="title">' + new_value + '</span>');
        }
      });
    }
    function cancelInterval(){

      $interval.cancel(vm.fetchSellRates);
      vm.fetchSellRates = null;
    }
    function previousStep() {
      var index = (vm.selectedIndex === 0) ? 0 : vm.selectedIndex - 1;
      vm.selectedIndex = index;
      var currentTab = $( '#tab_'+index);
      angular.element('md-tab-item').each(function(){
        if( angular.element(this).hasClass('md-active') ){
          angular.element(this).removeClass('md-previous');
          angular.element(this).prev().removeClass('md-previous')
        }
      });
    };
    function emptyData() {
      vm.temp_rate = [];
      vm.step1_disabled = false;
      vm.circleLoader = true;
      vm.disableQuote = false;
      vm.hideQuoteScreen = false;
      vm.acceptQuoteDisable = false;
      vm.declineQuoteDisable = false;
    }

    //INVOKING METHOD BEGIN
    vm.getPlaceAnOrderData();
    function DeclineQuoteFunc()
    {
      vm.previousStep();
      emptyData();
    }
    // INVOKING METHOD END
  }
})();
