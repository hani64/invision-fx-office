(function () {
  'use strict';

  var app = angular
    .module('app.forexdealing.dealing_history', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider, msApiProvider) {
    // State
    $stateProvider.state('app.dealing_history', {
      url: '/deal_history',
      views: {
        'content@app': {
          templateUrl: 'app/main/forexdealing/deal_history/deal_history.html',
          controller: 'DealHistoryController as vm'
        }
      },
      resolve: {
        Invoice: function (msApi) {
          return msApi.resolve('invoice@get');
        },
        access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
      }
    });

    // Api
    msApiProvider.register('invoice', ['app/data/invoice/invoice.json']);
    //msApiProvider.register('invoice', ['app/data/invoice/invoice.json']);
  }
})();
