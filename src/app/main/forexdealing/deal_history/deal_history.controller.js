(function () {
  'use strict';

  angular
    .module('app.forexdealing.dealing_history')
    .controller('DealHistoryController', DealHistoryController);

  /** @ngInject */
  function DealHistoryController($state, api, appUtility, $timeout, XmlJson, NgTableParams, $rootScope, $mdDialog, $scope, $interval, $mdToast, Invoice, $filter, soap_api, $q, QueryFactory, drawdownFactory, messageService){
    var vm = this;
    vm.additionalFields = null;
    vm.addPayeeTotal   = addPayeeTotal;
    vm.edit_deal_hide = true;
    vm.module_id;
    vm.showDataTable = true;
    vm.payees = [];
    vm.fundings = [];
    vm.step2 = {};
    getDefaultDate();
    vm.addPayee = addPayee;
    vm.addFundingRecord = addFundingRecord;
    vm.toggleFilters = toggleFilters;
    vm.historyTypes  = historyTypes;
    vm.dealNo  = convertObjects;
    vm.entryDate  = convertObjects;
    vm.valueDate  = convertObjects;
    vm.funding_balance = 0;
    vm.addTotalFundingRecord = function(funding)
    {
      vm.SendFundingAndDisable = true;
      console.log('here6');
      if(funding.method != null && funding.amount != null)
      {
        if( funding.method.length != 0){
          vm.showCircleLoader = false;
          if(vm.deal_information.sell_amounnt === 0){
            vm.showCircleLoader = true;
            vm.SendFundingAndDisable = false;
            console.log(1);
          }
          if(addFundingRecord(funding))
          {
            vm.showCircleLoader = true;
            vm.SendFundingAndDisable = true;
          }
        }
        else
          {
            messageService.success('Funding method can not be empty.', 5000)
            vm.showCircleLoader = true;
            vm.SendFundingAndDisable = false;

          }
      }
      else
        {
        if(vm.fundings.length > 0 && vm.funding_balance.copy_amount == 0)
        {
          //now send payments
          if(vm.fundings.length > 0)
          {
            vm.showCircleLoader = false;
            vm.SendFundingAndDisable = true;
            messageService.success('Adding Funding..', 5000);
            var payments = '';
            var feesPromises = [];
            var fundingList = [];
            for(var i=0; i<vm.fundings.length; i++)
            {
              feesPromises.push(vm.GetFeeByMethodByID(vm.fundings[i].method));
              fundingList.push(vm.fundings[i]);
            }
            $q.all(feesPromises).then(function(data){
              for(var i=0; i<data.length; i++) {
                var value_date = new Date(Date.parse(fundingList[i].value_date));
                value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                if(fundingList[i].account_picker != null){
                  var payee = fundingList[i].account_picker;
                }else{
                  var payee = null;
                }
                // var currency = _.filter(vm.fundingCurrencies, {currency_id: vm.step1.currency_to_sell});
                // var currency_id = currency[0].currency_id;
                payments += '<PAYMENTS>';
                payments += '<ACTION><ID>'+fundingList[i].method+'</ID></ACTION>';
                payments += '<CURRENCY><Value>'+vm.deal_information.sell_currency+'</Value></CURRENCY>';
                payments += '<AMOUNT>'+vm.parseFloat(fundingList[i].amount)+'</AMOUNT>';
                payments += '<VALUE_DATE>'+value_date+'</VALUE_DATE>';
                if(fundingList[i].method == 5){
                  if(payee != null)
                    payments += '<ACCOUNT><ID>'+payee+'</ID></ACCOUNT>';
                  payments += '<ACT_TYPE>E</ACT_TYPE>';
                }else{
                  if(payee != null)
                    payments += '<PAYEE><ID>'+payee+'</ID></PAYEE>';
                }
                payments += '</PAYMENTS>';
              }
              var date = new Date(Date.parse(vm.step1.value_date));
              vm.SendPayments(vm.deal_information.deal, date, payments).then(function(data){
                if(data[0].status == 'FALSE'){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">There is an error while adding funding record.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  vm.showCircleLoader = true;
                  vm.SendFundingAndDisable = false;
                  return;
                }else{
                  $mdToast.show({
                    template: '<md-toast class="md-toast success">Funding(s) Added. Deal Updated.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  // vm.GetUserApprovalRights(vm.deal, 12277).then(function(data){
                  //   if(data[0].status == 'TRUE'){
                  //     vm.skipApprovalStep = false;
                  //   }else{
                  //     vm.skipApprovalStep = true;
                  //   }
                  //   vm.showCircleLoader = true;
                  //   vm.nextStep();
                  // });
                }
              });
            });
          }
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please Fill in all the required Fields.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.showCircleLoader = true;
          vm.SendFundingAndDisable = false;
        }
      }
    };
    function convertObjects(objectOrStringValue, row){
      debugger
      if(typeof objectOrStringValue === 'object')
      {
        return objectOrStringValue['#text'].join(" ");
      }
      return objectOrStringValue
    }
    function getDefaultDate()
    {
      drawdownFactory.defaultDate(callback)
      function callback(response)
      {
        if(response.status === 'TRUE')
        {
          vm.step1.value_date = response.recordid;
        }
      }
    }
    //Payee work start from here
    function addPayeeTotal()
    {
      if(vm.step1.payee_ !== null && vm.step1.amount !== null){
        if(vm.step1.payee_){
          hideLoader();
          if(vm.addPayee() == true){
            vm.loader = true;
            vm.SendPayeesAndDisable = false;
          }
        }
      }else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please fill all required fields.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
      }
    }
    function addPayee()
    {
      vm.loader = false;
      vm.SendPayeesAndDisable = true;

      addPayees_();
    }
    function addPayees_()
    {
      if(!vm.step1.value_date || vm.step1.value_date === ''){
        $mdToast.show({
          template: '<md-toast class="md-toast error">Value date is mandatory. Please check Payee detail.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        vm.loader = true;
        vm.SendPayeesAndDisable = false;
        return false;
      }else{
        checkBusinessDate();
      }
      function checkBusinessDate(){
        var value_date = new Date(Date.parse(vm.step1.value_date));
        var input = '<INPUT><DATE>'+convertDate(value_date)+'</DATE></INPUT>';
        callBuisnessDateApi(input);
      }
      function callBuisnessDateApi(input){
        QueryFactory.query('ValidateBusinessDate', input).then(success);
        function success (data){
          console.log(data);
          var data = convertDataToJson(data.data, 'result');
          validate(data);
        }
      }
      function validate(data)
      {
        var payee = {};
        if(data.status === 'TRUE'){
          if(vm.step1.payee_ != null && parseFloat(vm.step1.amount) > 0)
          {
            angular.copy(vm.step1, payee);
            if(vm.popMandate === true && payee.payment_purpose === null)
            {

              messageService.error('Purpose of payment is mandatory. Please check details', 5000);
              hideLoader();
              return false;
            }
            if(payee.payment_notification_email != '' && payee.payment_notification_email != null)
            {
              var emailRegex = _emailRegex();
              if(payee.payment_notification_email.match(emailRegex) === null)
              {
                messageService.error('Email is invalid.', 5000);
                hideLoader();
              }
            }
            //$scope.showSinglePayeeTable = false;
            var payee_object = _.filter(vm.payeesList, {id: payee.payee_id});
            getFeeMethodById(payee_object, payee);
          }else{
            $mdToast.show({
              template: '<md-toast class="md-toast error">Please fill all required fields.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
            return true;
          }
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">'+data.error+'</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.loader = true;
          vm.SendPayeesAndDisable = false;
          return false;
        }
      }
    }
    function getFeeMethodById(payeeObj, payee)
    {
      var payees= payeeObj[0];
      var paymentMethod = payees.paymentmethod;
      var input = '<INPUT><METHOD><ID>'+paymentMethod+'</ID></METHOD></INPUT>';
      QueryFactory.query('GetFeeByMethod', input).then(feeByMethod);

      function feeByMethod(data){
        var data = convertDataToJson(data.data, 'fee');
        if(data.status)
        {
          messageService.error("Payee cannot be selected, because payment method: " + data.error , 5000);
          hideLoader();
          return false;
        }
        else
        {
          var id = vm.payees.length + 1;
          payee.current_id = id;
          payee = mergeObjects(payee, payees);
          payee.value_date = vm.step1.value_date;
          payee.rate = vm.deal_information.rate;

          if(payee.currency_name === vm.deal_information.buy_currency)
          {
            //if we have same currencies

            var payeeFee;
            if(data.same_currency_fee &&  data.same_currency_fee.length > 0)
            {
              if(data.same_currency_fee[0].currency_id === payees[0].currency_id)
              {
                payeeFee =  parseCurrencies(data.same_currency_fee[0].instrument_cost);
              }
              else
              {
                payeeFee = parseCurrencies(data.fee[0].non_instrument_cost);
              }
            }
            else
            {
              payeeFee = parseCurrencies(data.non_instrument_cost);
            }
            payee.fee = payeeFee
          }
          else
            {
            //else we have different currencies
            if(data.instrument_fee.length  !== undefined)
            {
              var found = false;
              var instrument_fee = {};
              data.instrument_fee.some(instrumentFee);

              function instrumentFee(element)
              {
                return element.currency_id === 2 && (instrument_fee = element, found = true);
              }
              // for(var i=0; i<data.instrument_fee.length; i++){
              //   if(data.instrument_fee[i].currency_id == 2){
              //     found = true;
              //     instrument_fee = data.instrument_fee[i];
              //   }
              // }
              if(found)
              {
                payee.fee = parseCurrencies(instrument_fee.instrument_cost);
              }
              else
              {
                payee.fee = parseCurrencies(data.instrument_cost);
              }
            }
            else
            {
              payee.fee = parseCurrencies(data.instrument_cost);
            }
          }
          if(typeof payee.amount != 'number')
            payee.amount = parseFloat(payee.amount.split(',').join(''));
          else
            payee.amount = parseFloat(payee.amount);
          vm.payees.push(payee);
          console.log(vm.payees);
          if(vm.balance > 0)
          {
            vm.step1.amount = vm.balance;
            vm.deal_information.buy_amount = vm.balance
            vm.balance = 0;
            vm.loader = true;
            vm.SendPayeesAndDisable = false;
          }
          else
            {
              // vm.deal_information.buy_amount = 0;
            vm.step1.amount = null;
            vm.step2.amount = vm.deal_information.sell_amount;
            // vm.beneficiary.sell = vm.fixedSell;
            //send settlements
            if(vm.payees.length > 0)
            {
              messageService.success('Adding Payees..', 5000)
              // var step1_value_date = vm.step1.value_date == '' ? new Date() : new Date(Date.parse(vm.step1.value_date));
              // step1_value_date = step1_value_date.getFullYear() + '-' + ('0' + (step1_value_date.getMonth()+1)).slice(-2) + '-' + ('0' + step1_value_date.getDate()).slice(-2);

              var settlements = '<DEAL>' +
                '<MODULE_ID>' + vm.module_id+ '</MODULE_ID>' +
                '<VALUE_DATE>'+ convertDate(vm.step1.value_date)+'</VALUE_DATE>' +
                '<BUY_CURRENCY><Value>'+ vm.deal_information.sell_currency +'</Value></BUY_CURRENCY>' +
                '<BUY_AMOUNT>'+vm.deal_information.sell_amount+'</BUY_AMOUNT>' +
                '</DEAL>';

              vm.loader = false;
              for(var i=0; i<vm.payees.length; i++)
              {
                if(typeof vm.payees[i].value_date == 'string' && vm.payees[i].value_date != null)
                {
                  var value_date = new Date(Date.parse(vm.payees[i].value_date));
                }
                else if(typeof vm.payees[i].value_date == 'object')
                {
                  var value_date = vm.payees[i].value_date;
                }
                else if(typeof vm.payees[i].value_date == 'undefined')
                {
                  var value_date = new Date(Date.parse(vm.step1.value_date));
                }
                value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                if(vm.payees[i].payment_purpose != null)
                {
                  var purpose_object = _.filter(vm.paymentPurposes, {id: vm.payees[i].payment_purpose});
                  var purpose_of_payment = purpose_object[0].id;
                }else{
                  var purpose_of_payment = 1;
                }
                var payment_reference = vm.payees[i].payment_reference == null ? '' : vm.payees[i].payment_reference;
                var internal_payment_reference = vm.payees[i].internal_payment_reference == null ? '' : vm.payees[i].internal_payment_reference;
                var settlementData =
                  {
                    method : vm.payees[i].paymentmethod,
                    currencyName : vm.payees[i].currency_name,
                    amount : vm.payees[i].amount,
                    rate : vm.deal_information.rate
                  }
                settlements = settlementsXML(settlementData, settlements);
              }
              var input = '<INPUT>'+settlements+'</INPUT>';
              QueryFactory.query('GetFeeForSettlements', input).then(successFeeSettlements);
              function successFeeSettlements(successFeeSettlementsResponse)
              {

                var payments = convertDataToJson(successFeeSettlementsResponse.data, 'payment');
                var settlements_ = convertDataToJson(successFeeSettlementsResponse.data, 'settlement');
                // var payment    = successFee.shift;
                vm.deal_information.sell_amount = payments.buy_amount;
                vm.step2.amount = payments.buy_amount;
                vm.loader = false;
                settlements = '';
                for(var i=0; i<settlements_.length; i++)
                {
                  vm.payees[i].total = settlements_[i].sell_amount;
                  if(typeof vm.payees[i].value_date == 'string' && vm.payees[i].value_date != null)
                  {
                    var value_date = new Date(Date.parse(vm.payees[i].value_date));
                  }
                  else if(typeof vm.payees[i].value_date == 'object')
                  {
                    var value_date = vm.payees[i].value_date;
                  }
                  else if(typeof vm.payees[i].value_date == 'undefined')
                  {
                    var value_date = new Date(Date.parse(vm.step1.value_date));
                  }
                  value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                  if(vm.payees[i].payment_purpose != null){
                    var purpose_object = _.filter(vm.paymentPurposes, {id: vm.payees[i].payment_purpose});
                    var purpose_of_payment = purpose_object[0].id;
                  }
                  else
                  {
                    var purpose_of_payment = 1;
                  }
                  var payment_reference = vm.payees[i].payment_reference == null ? '' : vm.payees[i].payment_reference;
                  //var internal_payment_reference = payeesList[i].internal_payment_reference == null ? '' : payeesList[i].internal_payment_reference;

                  var uid = 'INREF-'+ toUpperCase(Math.random().toString(16).slice(2));
                  var internal_payment_reference = vm.payees[i].internal_payment_reference == null ? uid : vm.payees[i].internal_payment_reference;
                  if(vm.payees[i].internal_payment_reference == null){
                    vm.payees[i].internal_payment_reference = uid;
                  }
                  settlements += '<SETTLEMENTS>';
                  settlements += '<ACTION><ID>'+vm.payees[i].paymentmethod+'</ID></ACTION>';
                  settlements += '<CURRENCY><Value>'+vm.payees[i].currency_name+'</Value></CURRENCY>';
                  settlements += '<AMOUNT>'+settlements_[i].sell_amount+'</AMOUNT>';
                  settlements += '<VALUE_DATE>'+value_date+'</VALUE_DATE>';
                  if(vm.payees[i].paymentmethod == 5){
                    if(vm.payees[i].id != null)
                      settlements += '<ACCOUNT><ID>'+vm.payees[i].id+'</ID></ACCOUNT>';
                      settlements += '<ACT_TYPE>E</ACT_TYPE>';
                  }else{
                    if(vm.payees[i].id != null)
                      settlements += '<PAYEE><ID>'+vm.payees[i].id+'</ID></PAYEE>';
                  }
                  settlements += '<REFERENCE>'+payment_reference+'</REFERENCE>';
                  settlements += '<BENEFICIARY_REFERENCE>'+payment_reference+'</BENEFICIARY_REFERENCE>';
                  settlements += '<NOTIFY_MY_RECEIPIENT></NOTIFY_MY_RECEIPIENT>';
                  settlements += '<BENEFICIARY_REMARKS></BENEFICIARY_REMARKS>';
                  settlements += '<REMITTENCE_REMARKS></REMITTENCE_REMARKS>';
                  settlements += '<PURPOSE_OF_PAYMENT>'+purpose_of_payment+'</PURPOSE_OF_PAYMENT>';
                  settlements += '<IS_EQUIVALENT>0</IS_EQUIVALENT>';
                  settlements += '<INTERNAL_REFERENCE>'+internal_payment_reference+'</INTERNAL_REFERENCE>';
                  settlements += '<FEE>'+settlements_[i].fee+'</FEE>';
                  vm.payees[i].fee = settlements_[i].fee;
                  settlements += '<FEE_TYPE><ID>'+settlements_[i].fee_type+'</ID></FEE_TYPE>';
                  settlements += '<FEE_CURRENCY><ID>'+settlements_[i].fee_currency_id+'</ID></FEE_CURRENCY>';
                  settlements += '</SETTLEMENTS>';
                }
                var settlementRecord =
                  {
                    recordNumber : vm.deal_information.deal,
                    date : convertDate(vm.step1.value_date),
                    settlements : settlements
                  }
                sendSettlements(settlementRecord)
              }
              // vm.GetFeeForSettlements(settlements).then(function(data){
              //   var payment = data.shift();
              //   vm.fixedSell = payment.buy_amount;
              //   vm.step4.amount = payment.buy_amount;
              //   vm.beneficiary.sell = payment.buy_amount;
              //   data = data;
              //   vm.loader = false;
              //
              //
              //
              // });
            }
          }
          vm.balance = 0;
          vm.step1.payee_ = null;
          // vm.step3.value_date = null;
          vm.step1.internal_payment_reference = null;
          vm.step1.no_notification = null;
          vm.step1.payment_notification_email = null;
          vm.step1.payment_purpose = null;
          vm.step1.payment_reference = null;

          hideLoader();
          return true;
          // vm.loader = true;
          // vm.SendPayeesAndDisable = false;
          // return true;
        }
      }
    }

    function sendSettlements(settlmentsRecord)
    {
     var input = '<INPUT>' +
        '<DEAL>' +
        '<RECORD><Value>'+settlmentsRecord.recordNumber+'</Value></RECORD>' +
        '<VALUE_DATE>'+settlmentsRecord.date+'</VALUE_DATE>' +
        '</DEAL>' +
        settlmentsRecord.settlements +
       '</INPUT>'
      QueryFactory.query('UpdateWebDeal', input).then(successSettlements);

      function successSettlements(settlements)
      {
        var settlements_ = convertDataToJson(settlements.data, 'table1');
        if(settlements_.status === 'FALSE')
        {
          messageService.error(settlements_.error, 5000);
          hideLoader();
          vm.step1.payee_ = null;
          vm.step1.amount = 0;
        }
        else
        {
          messageService.success('Payees Added. Deal Updated.', 5000)
        }

        if($rootScope.entityWebDefaults.funding_cur_id &&  vm.deal_information.sell_currency === $rootScope.entityWebDefaults.funding_cur_id)
        {

          var default_method = _.filter(vm.methods, {id: $rootScope.entityWebDefaults.funding_method_id});
          if(default_method.length > 0)
          {
            vm.step2.method = default_method[0].id;
            var input = '<INPUT>' +
              '<PAYEE>' +
              '<PAYMENT_METHOD><ID>'+default_method[0].id+'</ID></PAYMENT_METHOD>' +
              '<CURRENCY><Value>'+vm.deal_information.sell_currency+'</Value></CURRENCY>' +
              '</PAYEE>' +
              '</INPUT>';

            QueryFactory.query('GetIncomingBeneficiaries', input).then(successIncomingBeneficiaries);
            function successIncomingBeneficiaries(successIncoming)
            {

            }

            // vm.GetPaymentsByMethodAndCurrencyInBookASpot(default_method[0].id, vm.beneficiary.sellCurrencyID).then(function(data){
            //   if(data.length > 0){
            //     vm.accounts = data;
            //     if($rootScope.entityWebDefaults.funding_template_id != ''){
            //       var default_account = _.filter(vm.accounts, {id: $rootScope.entityWebDefaults.funding_template_id});
            //       vm.step4.account_picker = default_account[0].id;
            //     }
            //     vm.nextStep();
            //     vm.loader = true;
            //     vm.SendPayeesAndDisable = false;
            //   }else{
            //     vm.nextStep();
            //     vm.loader = true;
            //     vm.SendPayeesAndDisable = false;
            //   }
            // });
          }
        }else{
          vm.nextStep();
         hideLoader();
        }

      }
      // vm.SendSettlements(vm.beneficiary.recordnumber, convertDate(vm.step1.value_date), settlements).then(function(data){
      //   if(data[0].status == 'FALSE'){
      //     $mdToast.show({
      //       template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
      //       hideDelay: 4000,
      //       position: 'bottom right'
      //     });
      //     vm.loader = true;
      //     vm.SendPayeesAndDisable = false;
      //     vm.step3.payee_ = null;
      //     vm.step3.amount = 0;
      //   }else{
      //     $mdToast.show({
      //       template: '<md-toast class="md-toast success">Payees Added. Deal Updated.</md-toast>',
      //       hideDelay: 4000,
      //       position: 'bottom right'
      //     });
      //
      //     if(vm.beneficiary.sellCurrencyID == $rootScope.entityWebDefaults.funding_cur_id){
      //       var default_method = _.filter(vm.methods, {id: $rootScope.entityWebDefaults.funding_method_id});
      //       if(default_method.length > 0){
      //         vm.step4.method = default_method[0].id;
      //         vm.GetPaymentsByMethodAndCurrencyInBookASpot(default_method[0].id, vm.beneficiary.sellCurrencyID).then(function(data){
      //           if(data.length > 0){
      //             vm.accounts = data;
      //             if($rootScope.entityWebDefaults.funding_template_id != ''){
      //               var default_account = _.filter(vm.accounts, {id: $rootScope.entityWebDefaults.funding_template_id});
      //               vm.step4.account_picker = default_account[0].id;
      //             }
      //             vm.nextStep();
      //             vm.loader = true;
      //             vm.SendPayeesAndDisable = false;
      //           }else{
      //             vm.nextStep();
      //             vm.loader = true;
      //             vm.SendPayeesAndDisable = false;
      //           }
      //         });
      //       }
      //     }else{
      //       vm.nextStep();
      //       console.log(vm.fixedBuy)
      //       vm.loader = true;
      //       vm.SendPayeesAndDisable = false;
      //     }
      //   }
      // });
    }
    //Payee work ends here
   function toUpperCase(str){
      str = str || '';
      return str.toUpperCase();
    }

    vm.SendFundingAndDisable = false;
    function addFundingRecord(funding)
    {
      vm.SendFundingAndDisable = true;
      var record = {};
      angular.copy(funding, record);
      if(  record.method != null && parseFloat(record.amount) > 0 )
      {
        vm.showCircleLoader = false;
        vm.SendFundingAndDisable = true;
        if( record.account_picker != null && record.account_picker != ''  )
        {

          vm.GetFeeByMethodByID(record.method).then(function(data){
            var identifier = vm.fundings.length + 1;
            record.current_id = identifier;
            record.value_date = new Date();
            if( vm.step1.currency_to_buy == vm.step1.currency_to_sell )
            {
              record.fee = data.non_instrument_cost;
            }
            else{
              record.fee = data.instrument_cost;
            }
            if(typeof record.amount != 'number')
              record.amount = parseFloat(record.amount.split(',').join(''));
            else
              record.amount = parseFloat(record.amount);
            var account_object = _.filter(vm.accounts, {id: record.account_picker});
            record = vm.mergeObjects(record, account_object[0]);
            record.payee_ = account_object[0].name;
            console.log(record);
            vm.fundings.push(record);
            vm.showCircleLoader = true;
            vm.SendFundingAndDisable = false;
            console.log(10);
            if(vm.balance > 0){
              vm.step2.amount = vm.balance;
              // vm.funding_balance.copy_amount = vm.funding_balance.amount;
              vm.SendFundingAndDisable = false;
              console.log(9);
            }else{
              // vm.funding_balance.copy_amount = 0;
              vm.step2.amount = null;
              vm.step2.account_picker = null;
              vm.SendFundingAndDisable = false;
              if(vm.fundings.length > 0){
                //send payments
                vm.showCircleLoader = false;
                vm.SendFundingAndDisable = true;
                console.log('here3');
                $mdToast.show({
                  template: '<md-toast class="md-toast info">Adding Funding..</md-toast>',
                  hideDelay: 2000,
                  position: 'bottom right'
                });
                var payments = '';
                var feesPromises = [];
                var fundingList = [];

                for(var i=0; i<vm.fundings.length; i++) {
                  feesPromises.push(vm.GetFeeByMethodByID(vm.fundings[i].method));
                  fundingList.push(vm.fundings[i]);
                }
                $q.all(feesPromises).then(function(data){
                  for(var i=0; i<data.length; i++) {
                    var value_date = new Date(Date.parse(fundingList[i].value_date));
                    value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                    if(fundingList[i].account_picker != null){
                      var payee = fundingList[i].account_picker;
                    }else{
                      var payee = null;
                    }
                    // var currency = _.filter(vm.fundingCurrencies, {currency_id: vm.step1.currency_to_sell});
                    // var currency_id = currency[0].currency_id;
                    payments += '<PAYMENTS>';
                    payments += '<ACTION><ID>'+fundingList[i].method+'</ID></ACTION>';
                    payments += '<CURRENCY><Value>'+vm.deal_information.sell_currency+'</Value></CURRENCY>';
                    payments += '<AMOUNT>'+ parseFloat(fundingList[i].amount)+'</AMOUNT>';
                    payments += '<VALUE_DATE>'+value_date+'</VALUE_DATE>';
                    if(fundingList[i].method == 5){
                      if(payee != null)
                        payments += '<ACCOUNT><ID>'+payee+'</ID></ACCOUNT>';
                      payments += '<ACT_TYPE>E</ACT_TYPE>';
                    }else{
                      if(payee != null)
                        payments += '<PAYEE><ID>'+payee+'</ID></PAYEE>';
                    }
                    payments += '</PAYMENTS>';
                  }
                  var paymentsObject =
                    {
                      deal : vm.deal_information.deal,
                      payments : payments

                    }
                  sendPayments(paymentsObject);
                 });
              }
            }
            vm.balance = 0;
            vm.step2.method = null;
            vm.step2.account_picker = null;
            vm.SendFundingAndDisable = false;
            console.log(6);
            return true;
          });
        }
        else
          {
          var identifier = vm.fundings.length + 1;
          record.current_id = identifier;
          record.value_date = vm.defaultValueDate || new Date();
          if(typeof record.amount != 'number')
          {
            record.amount = parseFloat(record.amount.split(',').join(''));
          }

          else
          {
            record.amount = parseFloat(record.amount);
          }
          // var currency = _.filter(vm.fundingCurrencies, {currency_id: vm.step1.currency_to_sell});
          // record.currency_id = currency[0].currency_id;
          record.currency_name = vm.deal_information.sell_currency;
          record.payee_ = 'N/A';
          record.fee = 0;
          vm.fundings.push(record);
          vm.showCircleLoader = true;
          vm.SendFundingAndDisable = false;
          if(vm.balance > 0)
          {
            vm.step2.amount = vm.balance;
            vm.deal_information.sell_amount = vm.balance;
            vm.balance = 0;
            vm.loader = true;
            vm.SendPayeesAndDisable = false;
            console.log(4);
          }
          else
          {
            // vm.funding_balance.copy_amount = 0;
            vm.step2.amount = null;
            vm.step2.account_picker = null;
            //now send payments
            if(vm.fundings.length > 0)
            {
              //send payments
              vm.showCircleLoader = false;
              messageService.success('Adding Funding...', 5000);
              var payments = '';
              var feesPromises = [];
              var fundingList = [];
              for(var i=0; i<vm.fundings.length; i++)
              {
                feesPromises.push(vm.GetFeeByMethodByID(vm.fundings[i].method));
                fundingList.push(vm.fundings[i]);
              }
              $q.all(feesPromises).then(function(data)
              {
                for(var i=0; i<data.length; i++)
                {
                  var value_date = new Date(Date.parse(fundingList[i].value_date));
                  value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                  if(fundingList[i].account_picker != null)
                  {
                    var payee = fundingList[i].account_picker;
                  }
                  else
                  {
                    var payee = null;
                  }
                  // var currency = _.filter(vm.fundingCurrencies, {currency_id: vm.step1.currency_to_sell});
                  // var currency_id = currency[0].currency_id;
                  payments += '<PAYMENTS>';
                  payments += '<ACTION><ID>'+fundingList[i].method+'</ID></ACTION>';
                  payments += '<CURRENCY><Value>'+vm.deal_information.sell_currency+'</Value></CURRENCY>';
                  payments += '<AMOUNT>'+parseFloat(fundingList[i].amount)+'</AMOUNT>';
                  payments += '<VALUE_DATE>'+value_date+'</VALUE_DATE>';
                  if(fundingList[i].method == 5)
                  {
                    if(payee != null)
                      payments += '<ACCOUNT><ID>'+payee+'</ID></ACCOUNT>';
                      payments += '<ACT_TYPE>E</ACT_TYPE>';
                  }
                  else
                  {
                    if(payee != null)
                      payments += '<PAYEE><ID>'+payee+'</ID></PAYEE>';
                  }
                  payments += '</PAYMENTS>';
                }
                var paymentsObject =
                  {
                    deal :  vm.deal_information.deal,
                    payments : payments

                  }

                sendPayments(paymentsObject);
                // var date = new Date(Date.parse(vm.step1.value_date));
                // vm.SendPayments(vm.deal.deal_number, date, payments).then(function(data)
                // {
                //   if(data[0].status == 'FALSE')
                //   {
                //     messageService.success('There is an error while adding funding record.', 5000);
                //     messageService.success(data[0].error, 5000);
                //     vm.showCircleLoader = true;
                //     vm.SendFundingAndDisable = false;
                //     return;
                //   }
                //   else
                //     {
                //       messageService.success('Funding(s) Added. Deal Updated.', 5000);
                //     //
                //     //   vm.GetUserApprovalRights(vm.deal, 12277).then(function(data){
                //     //   if(data[0].status == 'TRUE'){
                //     //     vm.skipApprovalStep = false;
                //     //   }else{
                //     //     vm.skipApprovalStep = true;
                //     //   }
                //     //   vm.showCircleLoader = true;
                //     //   vm.SendFundingAndDisable = false;
                //     //   //console.log('2');
                //     //   vm.nextStep();
                //     // });
                //   }
                // });
              });
            }
          }
          vm.balance = 0;
          vm.step2.method = null;
          vm.step2.account_picker = null;
          return true;
        }
      }
      else
        {
          messageService.success('Method cannot be empty.', 5000);
          vm.showCircleLoader = true;
          vm.SendFundingAndDisable = false;
        }
    }
    vm.GetFeeByMethodByID = function(ID)
    {
      return soap_api.GetFeeByMethodByID(ID).then(function(success){
        //var result = [];
        var fee = [];
        var instrument_fee = [];
        var same_currency_fee = [];
        var error = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT FEE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            free_instruments: jQuery(e).find('FREE_INSTRUMENTS').text(),
            instrument_cost: jQuery(e).find('INSTRUMENT_COST').text(),
            non_instrument_cost: jQuery(e).find('NON_INSTRUMENT_COST').text(),
            inv_act_type_id: jQuery(e).find('INV_ACT_TYPE_ID').text(),
            inv_act_type: jQuery(e).find('INV_ACT_TYPE').text(),
            charge_id: jQuery(e).find('CHARGE_ID').text(),
            charge_type: jQuery(e).find('CHARGE_TYPE').text(),
            is_allowed: jQuery(e).find('IS_ALLOWED').text(),
            source_id: jQuery(e).find('SOURCE_ID').text(),
          };
          fee.push(temp);
        });
        jQuery(response).find('OUTPUT INSTRUMENT_FEE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            currency_id: jQuery(e).find('Currency_ID').text(),
            instrument_cost: jQuery(e).find('Instrument_Cost').text(),
            cost_type: jQuery(e).find('COST_TYPE').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            cost_type_name: jQuery(e).find('COST_TYPE_NAME').text()
          };
          instrument_fee.push(temp);
        });
        jQuery(response).find('OUTPUT SAME_CURRENCY_FEE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            currency_id: jQuery(e).find('Currency_ID').text(),
            instrument_cost: jQuery(e).find('Instrument_Cost').text(),
            cost_type: jQuery(e).find('COST_TYPE').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            cost_type_name: jQuery(e).find('COST_TYPE_NAME').text()
          };
          same_currency_fee.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          error.push(temp);
        });
        var result = {
          fee : fee,
          instrument_fee: instrument_fee,
          same_currency_fee: same_currency_fee,
          error: error
        }
        return result;
      }, function(error){
        console.error(error.statusText);
      });
    };
    function sendPayments(payments)
    {
      var date = new Date(Date.parse(vm.step1.value_date));
      vm.finalizeDeal = {};
      vm.SendPayments(payments.deal, date, payments.payments).then(function(data){
        vm.finalizeDeal = data[0]
        if(data[0].status == 'FALSE'){
          $mdToast.show({
            template: '<md-toast class="md-toast error">There is an error while adding funding record.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          $mdToast.show({
            template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.showCircleLoader = true;
          vm.SendFundingAndDisable = false;
          console.log(8);
          return;
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast success">Funding(s) Added. Deal Updated.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
         var input = '<INPUT><DEAL><ID>'+vm.finalizeDeal.id+'</ID></DEAL></INPUT>';
         QueryFactory.query('PostWebDeal', input ).then(successPostWebDeal);
        }
      });
      function successPostWebDeal(successPost)
      {
          var successPostResponse = convertDataToJson(successPost.data, 'result');
          if(successPostResponse.status)
          {
            var  input = '<INPUT><DEAL><ID>'+vm.finalizeDeal.id+'</ID></DEAL></INPUT>';
            QueryFactory.query('FinalizeDealPaymentSettlement', input).then(successFinalizeDeal)

          }

          function successFinalizeDeal(successFinalData){
            var successFinalDataResponse = convertDataToJson(successFinalData.data, 'result');
            if(successFinalDataResponse === 'TRUE')
            {
              messageService.success('Deal Finalized.', 5000);
            }
          }

      }
    }
    vm.SendPayments = function(deal, value_date, payments)
    {
      return soap_api.SendPayments(deal, value_date, payments).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text()
          };
          row.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.isXML = function(xml){
      try{
        var xmlDoc = jQuery.parseXML(xml);
        return true;
      }catch(error){
        return false;
      }
    };











    vm.selectPayee = selectPayee;
    vm.UpdateBalance = UpdateBalance;
    vm.verifyRecord = verifyRecord;
    function UpdateBalance(event, amounts, price, stepAmount, step){
      amounts = amounts || 0
      // updatePayee
      var amount = parseFloat( (amounts !== undefined) && stepAmount.split(',').join('') || 0 );
      var balanceAccount = parseFloat(price);
     if( amount <= balanceAccount && amount > 0 ){
      vm.balance = balanceAccount;
      if(!isNaN(amount))
      {
        var temp_amount = balanceAccount - amount;
        vm.balance = temp_amount;

      }
    }
    else if(amount > balanceAccount )
    {
      vm.balance = 0;
      buyOrSellExist(price, step)
    }
    else{
      // alert("This")
      vm.balance = balanceAccount;
      price  = 0;
    }
  };
    function buyOrSellExist(price, step )
    {
      if(step === 'step1')
      {
        vm.step1.amount = price;
      }
      else
      {
        vm.step2.amount = price;
      }



    }
    function selectPayee(payee){
      vm.step1.payee_ = payee.name;
      vm.step1.payee_id = payee.id;
      vm.showPayeeTable = true;
      $mdDialog.hide();
    };
    function verifyRecord(stepAmount, price) {
      return price && +parseFloating(stepAmount) > 0 && +parseFloating(stepAmount) < +parseFloating(price) || "";
    };
    function parseFloating(value, s){
      var the_amount = +value;
      if(typeof value === 'string'){
        the_amount = value.split(',').join('');
        the_amount = parseFloat(the_amount).toFixed(2);
      }

      if(typeof value === 'number'){
        the_amount = parseFloat(the_amount).toFixed(2);
      }

      return the_amount;
    };

    $rootScope.headerTitle = 'Dealing History';
    vm.selectedIndex = 0;
    vm.deal_information = {
      deal: null,
      type: null,
      entry_date: null,
      value_date: null,
      buy_amount: null,
      sell_amount: null,
      rate: null,
      buy_currency: null,
      buy_currency_id: null,
      sell_currency: null,
      sell_currency_id: null,
      module_id : null
    };
    vm.payeesList = [];
    vm.deal_histories = [];

    //API
    api.GetWebDeals().then(function(data){

      vm.deal_histories = data.output.web_deal;
      vm.showDataTable = false;
      vm.tableParams = new NgTableParams({}, {
        counts: [5, 10, 20, 50, 100],
        dataset : vm.deal_histories,
        filterOptions: {
          filterFn: filterColumns
        }
      });
    });
    api.GetPurposeOfPayments().then(function(data){
      vm.paymentPurposes = data.output.purpose_of_payment;
    });



    //methods
    vm.nextStep = function(){
      var index = (vm.selectedIndex === vm.max) ? 0 : vm.selectedIndex + 1;
      vm.selectedIndex = index;
      var currentTab = $( '#tab_'+index);
      $('md-tab-item').each(function () {
        if( $(this).hasClass('md-active') ){
          $(this).addClass('md-previous');
          $(this).prevAll().addClass('md-previous');
        }
      });
    };
    vm.previousStep = function () {
      var index = (vm.selectedIndex === 0) ? 0 : vm.selectedIndex - 1;
      vm.selectedIndex = index;
      var currentTab = $( '#tab_'+index);
      angular.element('md-tab-item').each(function(){
        if( angular.element(this).hasClass('md-active') ){
          angular.element(this).removeClass('md-previous');
          angular.element(this).prev().removeClass('md-previous')
        }
      });
    };
    vm.backBtn = function(){
      vm.tableSection = true;
      vm.editSection = false;
    };
    vm.editDeal = function(deal){
      var module;
      if(deal.module_id === '233'){
        return
      }
      else if(deal.module_id === "13360"){
        module = 12277

      }
      else
      {
        module = deal.module_id;
      }
      vm.module_id = module;
      var abc = [api.GetFundingMethod(vm.module_id), api.GetWebPaymentMethod(vm.module_id) ]
      $q.all(abc).then(success);

      function success(data){
        var methods = [];
        if(data[0] && data[0].output.funding_method.length)
        {
          vm.methods = data[0].output.funding_method;
        }
        else
        {
          methods.push(data[0].output.funding_method)
          vm.methods = methods;
        }


        vm.PaymentMethods = data[1].output.payment_method;
        var m = [];
        for(var i=0; i<vm.PaymentMethods.length; i++){
          m.push(vm.PaymentMethods[i].id);
        }
        vm.PaymentMethodsJoined = m.join('|');
        vm.edit_deal_hide = false;

        vm.deal_information.deal = deal.deal_number;
        vm.deal_information.type = deal.type;
        vm.deal_information.entry_date = deal.entry_date;
        vm.deal_information.value_date = deal.value_date;
        vm.deal_information.buy_amount = deal.buy_amount;
        vm.deal_information.buy_amounnt = deal.buy_amount;
        vm.deal_information.sell_amount = deal.sell_amount;
        vm.deal_information.sell_amounnt = deal.buy_amount;
        vm.deal_information.buy_currency = deal.buy_cur_name;
        vm.deal_information.sell_currency = deal.sell_cur_name;
        vm.deal_information.rate = deal.exchange_rate;
        vm.deal_information.module_id = deal.module_id
        vm.step1.amount = deal.buy_amount;
      }
      // api.GetFundingMethod(vm.module_id).then(function(data){
      //   vm.methods = data.output.funding_method;
      // });





      // vm.step2.amount = deal.sell_amount;
    };
    vm.backToHistory = function(){
      vm.edit_deal_hide = true;
    };
    vm.viewDeal = function(){};
    vm.showPayeeTableFunc = function(event){
      event.preventDefault();
      var currency = vm.deal_information.buy_currency;
      $mdDialog.show({
        locals:{dataToPass: currency},
        fullscreen: true,
        clickOutsideToClose: true,
        scope: $scope,
        preserveScope: true,
        templateUrl: 'app/main/forexdealing/deal_history/steps/modern-popup.html',
        parent: angular.element(document.body),
        controller: function dialogController($scope, $mdDialog){
          $scope.dialogLoader = false;
          if(vm.payeesList.length === 0){
            api.GetSettlementsByMethodsAndCurrencyInDealHistory(currency, vm.PaymentMethodsJoined).then(function(data){
              vm.payeesList = data.output.table1;
              $scope.dialogLoader = true;
            });
          }else{
            $scope.dialogLoader = true;
          }
          $scope.closeDialog = function() {
            $mdDialog.hide();
          };
          $scope.showFilter = true;
          $scope.toggleFilter = function(){
            $scope.showFilter = !$scope.showFilter;
          };
        }
      });
    };
    function filterColumns(data, filterValues) {
      if(vm.filter){
        return data.filter(function(item){
          return (item.deal_number.toLowerCase().indexOf(filterValues.deal_number || '') !== -1) &&
            (item.sell_amount.toString().toLowerCase().indexOf(filterValues.sell_amount || '') !== -1) &&
            (item.beneficiary_name.toLowerCase().indexOf(filterValues.beneficiary_name || '') !== -1);
        });
      }else{
        return data;
      }
    }
    function hideLoader()
    {
      vm.loader = true;
      vm.SendPayeesAndDisable = false;

    }
    function convertDate(dt){
      var date = new Date(dt);
      return date.getFullYear() + '-' + ('0' + (date.getMonth()+1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
    }
    function convertDataToJson(xml, name){
      var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
      var actualJson = XmlJson.xmlToJson(xmlDOM);
      return actualJson.output[name];
    }
    function _emailRegex()
    {
      return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    }
    function mergeObjects(obj1,obj2)
    {
      return jQuery.extend(obj1, obj2);
    }
    function parseCurrencies(currency)
    {
      return  parseFloating(currency);
    }
    function settlementsXML(settlementData, settlements )
    {

      settlements += '<SETTLEMENTS>';
      settlements += '<ACTION><ID>'+settlementData.method+'</ID></ACTION>';
      settlements += '<SELL_CURRENCY><Value>'+settlementData.currencyName+'</Value></SELL_CURRENCY>';
      settlements += '<SELL_AMOUNT>'+settlementData.amount+'</SELL_AMOUNT>';
      settlements += '<RATE>'+settlementData.rate+'</RATE>';
      settlements += '</SETTLEMENTS>';
      return settlements;

    }
    function toggleFilters()
    {
      vm.enableFilter =  !vm.enableFilter
    }
    vm.PickAccount = function(data){
      if(data.method != null && vm.deal_information.sell_currency != null){
        vm.accounts = [];
        var currency_id = vm.deal_information.sell_currency;
        api.GetPaymentsByMethodAndCurrencyInBookASpot(data.method, currency_id).then(function(data){
          vm.accounts = data.output.table1;
        });
      }
    };

    var htype;
    function historyTypes(type)
    {
      console.log(type);

      switch(type) {
        case "FIXED DATED FORWARD":
          htype = "F D F"
          break;
        case "Same Day":
          htype = "S D"
          break;
        case "OPTION DATED FORWARD":
           htype = "O D F";
           break;
        default:
          htype = type;
      }
      return htype;
    }

  }
})();
