(function () {
  'use strict';

  angular
    .module('app.forexdealing.place_order')
    .controller('placeAnOrderController', placeAnOrderController);

  /** @ngInject */
  function placeAnOrderController($state, messageService, appUtility, api, $timeout, $rootScope, $mdDialog, $scope, $interval, $mdToast, Invoice, $filter, soap_api, $q, QueryFactory, XmlJson, userApprovalFactory){
    var vm = this;
    vm.approvalCounter = 0;
    vm.Approvals = [];
    vm.data = null;
    vm.ApprovalNext = false;
    vm.hideExpiryDate = false;
    vm.loader = true;
    vm.hideStartDateField = true;



    //API
    api.GetEntity().then(function(data){
      vm.entity = data.output;
    });
    userApprovalFactory.GetSavedApprovalsByModule(924, saveApprovalCallback);
    function saveApprovalCallback(saveApprovals){
      vm.approvalPolicy = saveApprovals;
    }

    vm.selectType = selectType;
    vm.selectOrderBasis = selectOrderBasis;
    vm.selectCurrencies = selectCurrencies;
    vm.selectExpiryType = selectExpiryType;
    vm.approveNow       = approveNow;
    vm.finalizeDeal     = finalizeDeal;
    vm.hideStartDate = hideStartDate;
    vm.orderNumber = convertObjects;

    // vm.printDeal =  printDeal;
    // function printDeal(){
    //   vm.loader = false;
    //   vm.GetDealReport(vm.finalDeal).then(function(data){
    //     data = data[0];
    //     vm.showCircleLoader = true;
    //     if(data.status == 'TRUE'){
    //       var blob = vm.b64toBlob(data.value, 'application/pdf');
    //       saveAs(blob, data.recordid);
    //     }else{
    //       $mdToast.show({
    //         template: '<md-toast class="md-toast error">No report found.</md-toast>',
    //         hideDelay: 5000,
    //         position: 'bottom right'
    //       });
    //     }
    //   });
    // };
    vm.doneFunc = doneFunc;
    function doneFunc(){
      $state.reload();
    };
    function hideStartDate() {
      console.log(vm.step1.deal_type);
    }

    function selectType(dealType){
      // vm.step1.dealTypeId = dealType.id;
      var dealId = dealType.id;
      if(dealId === '4' || dealId === '2')
      {
        vm.step1.start_date = null;
        vm.hideStartDateField = true;
        return
      }
      delete vm.step1.start_date;
      vm.hideStartDateField = false;
    }
    function selectOrderBasis(orderBasis){
      vm.step1.orderBasisId = orderBasis.id
    }
    function selectCurrencies(currencyName, currency){
      vm.step1.sellCurrencyId = currencyName === 'CAD' && currency.id || vm.step1.sellCurrencyId;
      vm.step1.buyCurrencyId =  currencyName === 'BUY' && currency.id || vm.step1.buyCurrencyId;
    }
    function selectExpiryType(type){
      vm.disabledExpiryDate = (type.id === '406');
      vm.step1.expiryTypeId = type.id;
      //vm.hideExpiryDate = type.id === '406';
    }

    // Changes XML to JSON
    // function xmlToJson(xml) {
    //
    //   // Create the return object
    //   var obj = {};
    //
    //   if (xml.nodeType == 1) { // element
    //     // do attributes
    //     if (xml.attributes.length > 0) {
    //       obj["@attributes"] = {};
    //       for (var j = 0; j < xml.attributes.length; j++) {
    //         var attribute = xml.attributes.item(j);
    //         obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
    //       }
    //     }
    //   } else if (xml.nodeType == 3) { // text
    //     obj = xml.nodeValue;
    //   }
    //
    //   // do children
    //   if (xml.hasChildNodes()) {
    //     for(var i = 0; i < xml.childNodes.length; i++) {
    //       var item = xml.childNodes.item(i);
    //       var nodeName = item.nodeName;
    //       if (typeof(obj[nodeName]) == "undefined") {
    //         obj[nodeName] = xmlToJson(item);
    //       } else {
    //         if (typeof(obj[nodeName].push) == "undefined") {
    //           var old = obj[nodeName];
    //           obj[nodeName] = [];
    //           obj[nodeName].push(old);
    //         }
    //         obj[nodeName].push(xmlToJson(item));
    //       }
    //     }
    //   }
    //   return obj;
    // };

    orderBasis()
    function orderBasis(){
      QueryFactory.query("GetOrderBasis").then(success, err);

      function success(orders){
        // debugger;
        // console.log(orders);
        var xml = orders.data;
        var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
        var parsedOrderBasis = xmlToJson(xmlDOM);
        // var parsedOrderBasis = xmlToJson(orders.data);
        // console.log("Parsed Order Basis XML");
        // console.log(parsedOrderBasis.OUTPUT.ORDER_BASIS[0].VALUE);

        vm.orderBasis = parsedOrderBasis.OUTPUT.ORDER_BASIS.filter(function(order_basis){
            return order_basis.VALUE;
        }).reduce(function(returnType, elem){
          var types = {};
          types['name'] = elem['VALUE'];
          types['value'] = elem['VALUE'];
          types['id'] = elem['ID'];
          returnType.push(types);
          return returnType
        },[])
        // console.log("Temp Orders");
        // console.log(tempOrderBasis);

        // for(var i=0;i<tempOrderBasis.length;i++){
        //    // console.log(tempOrderBasis[i].VALUE);
        //    vm.orderBasis.push({name:tempOrderBasis[i].VALUE,
        //                        value:tempOrderBasis[i].VALUE})
        // }


        // console.log(parsedOrderBasis.find('ID'));
      };
      function err(err){
        console.log(err)
      }

    }

    dealTypes();
    function dealTypes(){
      QueryFactory.query('GetDealTypes').then(success,err)

      function success(deals){

        var xml = deals.data;
        var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
        var parsedDealTypes = xmlToJson(xmlDOM);

        // console.log("Parsed Deal Types");
        // console.log(parsedDealTypes);

        vm.dealTypes = parsedDealTypes.OUTPUT.DEAL_TYPE.filter(function(deal_types){
          return deal_types.TYPE;
        }).reduce(function(returnType, elem){
          var types = {};
          types['name'] = elem['TYPE'];
          types['value'] = elem['TYPE'];
          types['id'] = elem['ID'];
          returnType.push(types);
          return returnType
        },[])

        // for(var i=0;i<tempDealTypes.length;i++){
        //   // console.log(tempDealTypes[i].TYPE);
        //   vm.dealTypes.push({name:tempDealTypes[i].TYPE,
        //     value:tempDealTypes[i].TYPE})
        // }
        //
        // vm.dealTypes =

      }

      function err(err){
        console.log("Error");
        console.log(err);
      }
    }

    buying_currency();
    function buying_currency(){

      var input = '<?xml version="1.0" encoding="UTF-8"?><INPUT> <CURRENCY_ID>2</CURRENCY_ID><IS_BUY>true</IS_BUY></INPUT>'
      QueryFactory.query('GetCurrencyForDeal',input).then(success,err)

      function success(currency){
        // console.log(currency);

        var xml = currency.data;
        var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
        var parsedCurrencies = xmlToJson(xmlDOM);
        vm.buying_currency = parsedCurrencies.OUTPUT.Table1.filter(function(currency){
          return currency.CURRENCY_NAME;
        }).reduce(function(returnType, elem){
          var types = {};
          types['name'] = elem['CURRENCY_NAME'] +" "+"-"+" "+elem['CURRENCY_DESCRIPTION'];
          types['value'] = elem['CURRENCY_NAME'];
          types['id'] = elem['CURRENCY_ID'];
          returnType.push(types);
          return returnType
        },[])
      }
      function err(err){
        console.log(err);
      }

    }

    sellingCurrency();
    function sellingCurrency(){

      var input = '<?xml version="1.0" encoding="UTF-8"?><INPUT> <CURRENCY_ID>2</CURRENCY_ID><IS_BUY>false</IS_BUY></INPUT>'
      QueryFactory.query('GetCurrencyForDeal',input).then(success,err)

      function success(currency){
        var xml = currency.data;
        var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
        var parsedCurrencies = xmlToJson(xmlDOM);
        vm.selling_currency = parsedCurrencies.OUTPUT.Table1.filter(function(currency){

          return currency.CURRENCY_NAME;
        }).reduce(function(returnType, elem){
          var types = {};
          types['name'] = elem['CURRENCY_NAME'] +" "+"-"+" "+elem['CURRENCY_DESCRIPTION'];
          types['value'] = elem['CURRENCY_NAME'];
          types['id'] = elem['CURRENCY_ID'];
          returnType.push(types);
          return returnType
        },[])


      }

      function err(err){
        console.log(err);
      }

    }

    expiryTypes();
    function expiryTypes(){

      QueryFactory.query('GetOrderExpiryType').then(success,err)

      function success(types){
        // console.log("Types");
        // console.log(types);

        var xml = types.data;
        var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
        var parsedExpiry_types = xmlToJson(xmlDOM);

        // console.log(parsedExpiry_types);

        vm.types = parsedExpiry_types.OUTPUT.Table1.filter(function(type){

          return type.VALUE;
        }).reduce(function(returnType, elem){
          var types = {};
          types['name'] = elem['VALUE'];
          types['value'] = elem['VALUE'];
          types['id'] = elem['ID'];
          returnType.push(types);
          return returnType
        },[])

        // for(var i=0;i<tempTypes.length;i++){
        //   // console.log(tempTypes[i].VALUE);
        //   vm.types.push({name:tempTypes[i].VALUE,
        //     value:tempTypes[i].VALUE})
        // }

      }

      function err(err){
        console.log(err);
      }

    }

    defaultDate();
    function defaultDate(){

      var input = '<?xml version="1.0" encoding="utf-8"?> <INPUT> <IS_FORWARD>true</IS_FORWARD> </INPUT>'

      QueryFactory.query('GetDefaultDate',input).then(success,error)

      function success(defaultDate){
        // console.log("Default Date");
        // console.log(defaultDate);
        var xml = defaultDate.data;
        var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
        var parsedDefault_date = xmlToJson(xmlDOM);
        var defaultDate_fetched = parsedDefault_date.OUTPUT.RESULT.RECORDID;
        vm.step1.value_date = defaultDate_fetched;

        vm.step1.entry_date = entryDate();
      }

      function error(err){
        console.log(err);
      }

    }

    function xmlToJson(xml) {

      // Create the return object
      var obj = {};

      if (xml.nodeType === 1) { // element
        // do attributes
        if (xml.attributes.length > 0) {
          obj["@attributes"] = {};
          for (var j = 0; j < xml.attributes.length; j++) {
            var attribute = xml.attributes.item(j);
            if(reBlank.test(attribute.nodeValue))
            {
              obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
            }

          }
        }
      } else if (xml.nodeType === 3) { // text
        obj = xml.nodeValue;
      }

      // do children
      // If just one text node inside
      if (xml.hasChildNodes() && xml.childNodes.length === 1 && xml.childNodes[0].nodeType === 3) {
        obj = xml.childNodes[0].nodeValue;
      }
      else if (xml.hasChildNodes()) {
        for(var i = 0; i < xml.childNodes.length; i++) {
          var item = xml.childNodes.item(i);
          var nodeName = item.nodeName;
          if (typeof(obj[nodeName]) === "undefined") {
            obj[nodeName] = xmlToJson(item);
          } else {
            if (typeof(obj[nodeName].push) === "undefined") {
              var old = obj[nodeName];
              obj[nodeName] = [];
              obj[nodeName].push(old);
            }
            obj[nodeName].push(xmlToJson(item));
          }
        }
      }
      return obj;
    }
    //its compulsory
    //CONSTANTS DECLARATION BEGIN
    $rootScope.headerTitle = 'Place an order';
    vm.circleLoader = true;
    vm.step1_disabled = false;
    vm.step1Invalid = false;
    vm.selectedIndex = 0;

    //use appUtility functions
    vm.stopInput = function(event){
      appUtility.stopInput(event);
    };

    //step1
    vm.step1 = {
      order_basis: null,
      deal_type: null,
      buy_currency: null,
      sell_currency: null,
      i_want_to: null,
      amount: null,
      target_rate: null,
      entry_date: null,
      start_date: null,
      value_date: null,
      expiry_type: null,
      expiry_date: null
    };
    // vm.orderBasis = [
    //   {
    //     name: 'Limit',
    //     value: 'limit'
    //   }
    // ];
    vm.orderBasis = [];
    // vm.dealTypes = [
    //   {
    //     name: 'Spot',
    //     value: 'spot'
    //   }
    // ];
    vm.dealTypes = [];
    // vm.currencies = [
    //   {
    //     name: 'CAD',
    //     value: 'cad'
    //   },
    //   {
    //     name: 'USD',
    //     value: 'usd'
    //   },
    //   {
    //     name: 'EUR',
    //     value: 'eur'
    //   }
    // ];
    vm.currencies = [];
    vm.buying_currency = [];
    vm.selling_currency = [];
    vm.types = [
      // {
      //   name: 'Type',
      //   value: 'type'
      // }
    ];

    vm.checkExpiry_type = function(){
      var hideExpiryDate = true;
      if(vm.expiry_type == 'Good till Cancel'){
        console.log("Good till Cancel");
      }
    }

    vm.step1Submit = function(){
      vm.loader = false;
      var valueDate  = new Date(vm.step1.value_date);
      if(!vm.disabledExpiryDate)
        var expiryDate = new Date(vm.step1.expiry_date);
      var entityId   = vm.entity.entity.entity_id;
      if(!entityId){
        messageService.error('No Internet Connection.', 5000);
        return 1;
      }
      if(
        vm.step1.order_basis === null ||
        vm.step1.deal_type === null ||
        vm.step1.buy_currency === null ||
        vm.step1.sell_currency === null ||
        vm.step1.i_want_to === null ||
        vm.step1.amount === null ||
        vm.step1.target_rate === null ||
        vm.step1.entry_date === null ||
        vm.step1.start_date === null ||
        vm.step1.value_date === null ||
        vm.step1.expiry_type === null ){
        messageService.error('Fill required fields', 5000);
        vm.step1Invalid = true;
        vm.step1_disabled = false;
        vm.loader = true
      }
      else if(!vm.hideExpiryDate && (expiryDate && valueDate > expiryDate) ){
      messageService.error('Value date must be less then expiry date', 5000);
      return 0;
      }
      else{
          //console.log("Expiry Type");
          //console.log(vm.step1.expiry_type);

          if(vm.step1.expiry_type == 'Good till Cancel'){
             //console.log("Good till Cg expiry date");
            // vm.hideExpiryDate ancel is selected");
             // console.log("Hidin= true;
          }

          vm.step1Invalid = false;
          var input = "<INPUT>" +
            "<ORDER>" +
            "<ENTITY>" +
            "<ID>" + entityId + "</ID>" +
            "</ENTITY>" +
            "<DEAL_TYPE>" +
            // "<ID>" + vm.step1.dealTypeId + "</ID>" +
            "<Value>" + vm.step1.deal_type + "</Value>" +
            "</DEAL_TYPE>" +
            "<EMAIL_ADDRESS> </EMAIL_ADDRESS>" +
            "<ORDER_BASIS>" +
              "<ID>"+vm.step1.orderBasisId+"</ID>"+
            "<Value>" + vm.step1.order_basis + "</Value>"+
            "</ORDER_BASIS>" ;

            if(vm.step1.start_date)
            {
             input += "<START_DATE>" + convertDate(vm.step1.start_date) + "</START_DATE>";
            }

            input += "<VALUE_DATE>" + convertDate(vm.step1.value_date) + "</VALUE_DATE>" +
            "<STATUS>" +
            "<Value>Open</Value>" +
            "</STATUS>" +
            "<ENTRY_DATE>" + convertDate(vm.step1.entry_date) + "</ENTRY_DATE>" +
            "<EXPIRY_TYPE>" +
            "<ID>" + vm.step1.expiryTypeId + "</ID>" +
            "</EXPIRY_TYPE>";
           // vm.step1.expiryTypeId === 405
          if(vm.step1.expiry_date){
              input += "<EXPIRY_DATE>" + convertDate(vm.step1.expiry_date) + "</EXPIRY_DATE>";
            }
            else{
              input += "<EXPIRY_DATE>" + convertDate(vm.step1.expiry_date) + "</EXPIRY_DATE>";
            }
            input += "<EXPIRY_TIME></EXPIRY_TIME>" +
            "<REMARKS></REMARKS>" +
            "</ORDER>" +
            "<ORDERLIST_TRANSACTION>" +
            "<BUY_CURRENCY>" +
            "<ID>" + vm.step1.buyCurrencyId + "</ID>" +
            "</BUY_CURRENCY>" +
            "<SELL_CURRENCY>" +
            "<ID>" + vm.step1.sellCurrencyId + "</ID>" +
            "</SELL_CURRENCY>" +

            "<TYPE>" + capitalizeFirstLetter(vm.step1.i_want_to)+ "</TYPE>" +

            "<AMOUNT>" + vm.step1.amount + "</AMOUNT>"+

            "<RATE>"+ vm.step1.target_rate + "</RATE>" +

            "</ORDERLIST_TRANSACTION>"+

            "</INPUT>";

          QueryFactory.query("CreateOrder", input).then(successOrder, errOrder);
      }
    };
    function capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
    function successOrder(response){

      var data = convertDataToJson(response.data, 'result');
      if(data.status === 'TRUE' && data.error === 'N/A')
      {
        vm.dealRecord = data;
        messageService.success('Deal Created!', 5000);
        vm.loader = true;
       getUserApproval(data);
      }
      else{
        messageService.success(data.error, 5000)
        vm.loader = true;
      }


    }
    function errOrder(error){
      console.log(error)
    }
    function approveNow(){
        if(!vm.ApprovalNext){
          userApprovalRights()
        }
    }

    function getUserApproval(data){
      vm.deal = {
        id : data.recordid,
        module : 924
      }
      userApprovalRights('firstStep')
      // var input = '<INPUT><DETAIL><RECORD_ID>'+data.id+'</RECORD_ID><MODULE_ID>'+925+'</MODULE_ID></DETAIL></INPUT>';
      // QueryFactory.query('GetUserApprovalRights', input ).then(approvalSuccess, approvalError);
      //
      // function approvalSuccess(approvals){
      //   var data = convertDataToJson(approvals.data, 'result');
      //  if(data.status === 'TRUE')
      //  {
      //    return true;
      //  }
      //  return false;
      // }
      //
      // function approvalError(error){
      //   console.log(error)
      // }
    }
    function userApprovalRights(step){
      userApprovalFactory.GetUserApprovalRights(vm.deal, userRights);
      function userRights(userRights){
        if(step === 'firstStep'){
          if(userRights.status === 'TRUE'){
            vm.skipApprovalStep = false;
          }
          else{
            vm.skipApprovalStep = true;
          }
          vm.nextStep();

        }

        else
          {
            if(userRights.status == 'TRUE'){
              userApprovalFactory.SetDealApprovals(vm.deal, dealApprovals)
              // vm.SetDealApproval(vm.deal.deal_number).then(function(data){
              //   if(data[0].status == 'TRUE'){
              //     $mdToast.show({
              //       template: '<md-toast class="md-toast info">Deal Approved.</md-toast>',
              //       hideDelay: 4000,
              //       position: 'bottom right'
              //     });
              //     vm.GetApprovalHistory(vm.deal).then(function(data){
              //       /*console.log(data);
              //        var user = data[0];
              //        var user_row = {
              //        name: user.user_name,
              //        date: user.approval_date
              //        };*/
              //       vm.approvalCounter++;
              //       vm.Approvals = data;
              //       if(vm.Approvals.length == vm.approvalPolicy[0].approval_level){
              //         vm.ApprovalNext = true;
              //       }
              //       vm.loader = true;
              //     });
              //   }else{
              //     $mdToast.show({
              //       template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
              //       hideDelay: 2000,
              //       position: 'bottom right'
              //     });
              //     vm.loader = true;
              //   }
              // });
            }else{
              $mdToast.show({
                template: '<md-toast class="md-toast error">You cannot approve this deal.</md-toast>',
                hideDelay: 4000,
                position: 'bottom right'
              });
            }

           }





      }

    }

    function dealApprovals(dealApproval){
      if(dealApproval.status === 'TRUE'){
        messageService.success('Deal Approved.', 5000)
      }
      userApprovalFactory.GetApprovalHistory(vm.deal, approvalHistory)

      function approvalHistory(approval){
        console.log(approval)
        var user = {
                 name: approval.user_name,
                 date: approval.approval_date
                 };
        vm.approvalCounter++;
        vm.Approvals.push(approval);
              if(vm.Approvals.length === parseInt(vm.approvalPolicy.approval_level)){
                vm.ApprovalNext = true;
              }
              vm.loader = true;
      }
    }

    function finalizeDeal() {
      var input = '<INPUT><DEAL><ID>' + vm.deal.id + '</ID></DEAL></INPUT>';
      QueryFactory.query("FinalizeDealPaymentSettlement", input).then(success)
      function success(finalizeRecord) {
        if (finalizeRecord.status === 200) {
          messageService.success('Deal finalized.', 5000)
          vm.loader = true;
          vm.nextStep();
        }
      }
    }


    //for every step
    vm.nextStep = function(){
      var index = (vm.selectedIndex === vm.max) ? 0 : vm.selectedIndex + 1;
      vm.selectedIndex = index;
      //var currentTab = angular.element( '#tab_'+index);
      angular.element('md-tab-item').each(function () {
        if( angular.element(this).hasClass('md-active') ){
          angular.element(this).addClass('md-previous');
          angular.element(this).prevAll().addClass('md-previous');
        }
      });
    };
    vm.previousStep = function () {
      var index = (vm.selectedIndex === 0) ? 0 : vm.selectedIndex - 1;
      vm.selectedIndex = index;
      //var currentTab = angular.element( '#tab_'+index);
      angular.element('md-tab-item').each(function(){
        if( angular.element(this).hasClass('md-active') ){
          angular.element(this).removeClass('md-previous');
          angular.element(this).prev().removeClass('md-previous')
        }
      });
    };

    //--------------------------------------- Added Code dated:18-5-2017---------------------------------------//
    vm.parseFloating = function(value){
      var the_amount = value;
      if(typeof value == 'string'){
        the_amount = value.split(',').join('');
        the_amount = vm.parseFloat(the_amount).toFixed(2);
      }
      if(typeof value == 'number'){
        the_amount = vm.parseFloat(the_amount).toFixed(2);
      }
      return the_amount;
    };

    vm.parseBoolean = function(str){
      return str == 'true' ? true: false;
    };
    vm.parseFloat = function(number){
      return parseFloat(number);
    };

    vm.fetchBuyFullCurrency = function(currency){
      if(currency !== null){
        var full_currency = _.filter(vm.buying_currency, {value: currency});
        return full_currency[0].name;
      }
    };

    vm.fetchSellFullCurrency = function(currency){
      if(currency !== null){
        var full_currency = _.filter(vm.selling_currency, {value: currency});
        return full_currency[0].name;
      }
    };

    vm.getQuote = function(){
      vm.loader = false;
      vm.disableQuoteBtn = true;
      var amount = vm.parseFloating(vm.step1.amount);
      var value_date = new Date(Date.parse(vm.step1.date));
      if(vm.currencies.length == 1){
        vm.skipQuote = true;
        vm.loader = true;
        vm.nextStep();
      }else if(vm.currencies.length == 2){
        //buy currency is same as selected currency
        if(vm.step1.currency == vm.step1.from_currency){
          var buy_currency_id = vm.step1.from_currency_id;
          var sell_currency_id = vm.step1.to_currency_id;
          vm.GetRateByIDS(buy_currency_id, amount, sell_currency_id, 0, value_date, vm.entity.entity.name).then(function(data){
            vm.temp_rate = data[0];
            vm.rate = vm.temp_rate;
            if(data[0].exchange_rate == 0){
              $mdToast.show({
                template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
                hideDelay: 2000,
                position: 'bottom right'
              });
              vm.temp_rate = [];
              vm.disableQuoteBtn = false;
              vm.loader = true;
              return false;
            }

            //launch quote timer
            var circular_timer = angular.element('#circular-timer-box');
            circular_timer.empty();
            circular_timer.append('<div id="circular-timer" data-timer="60"></div>');
            var timer = circular_timer.find('#circular-timer');
            timer.TimeCircles({
              "animation": "smooth",
              "bg_width": 1,
              "fg_width": 0.21,
              "circle_bg_color": "#AAAAAA",
              "direction":'Counter-clockwise',
              "start_angle": 160,
              "time": {
                "Days": {
                  "text": "Days",
                  "color": "#FFCC66",
                  "show": false
                },
                "Hours": {
                  "text": "Hours",
                  "color": "#99CCFF",
                  "show": false
                },
                "Minutes": {
                  "text": "Minutes",
                  "color": "#BBFFBB",
                  "show": false
                },
                "Seconds": {
                  "text": "Expires in",
                  "color": "#FFA834",
                  "show": true
                }
              }
            }).addListener(function(unit, value, total){
              if( total == -1){
                timer.TimeCircles().stop();
                $interval.cancel(vm.fetchBuyRates);
                vm.fetchBuyRates = null;
                $mdToast.show({
                  template: '<md-toast class="md-toast error">Quote timeout.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                vm.temp_rate = [];
                vm.disableQuoteBtn = false;
                vm.loader = true;
                vm.previousStep();
              }else{
                if(unit == 'Seconds'){
                  var text_box = timer.find('.textDiv_Seconds');
                  text_box.find('.title').remove();
                  var new_value = value + 'Secs';
                  text_box.append('<span class="title">'+new_value+'</span>');
                }
              }
            });
            //vm.timer();
            vm.fetchBuyRates = $interval(function(){
              var buy_currency_id = vm.step1.from_currency_id;
              var sell_currency_id = vm.step1.to_currency_id;
              vm.rate = vm.temp_rate;
              vm.GetRateByIDS(buy_currency_id, vm.parseFloating(vm.step1.amount), sell_currency_id, 0, new Date(Date.parse(vm.step1.date)), vm.entity.entity.name).then(function(data){
                vm.temp_rate = data[0];
              });
            }, 2000);
            vm.loader = true;
            vm.nextStep();
          });
        }else{
          //buy currency is different as selected currency
          var buy_currency_id = vm.step1.to_currency_id;
          var sell_currency_id = vm.step1.from_currency_id;


          vm.GetRateByIDS(sell_currency_id, vm.parseFloating(vm.step1.amount), buy_currency_id, 0, new Date(Date.parse(vm.step1.date)), vm.entity.entity.name).then(function(data){
            vm.temp_rate = data[0];
            vm.rate = vm.temp_rate;
            if(data[0].exchange_rate == 0){
              $mdToast.show({
                template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
                hideDelay: 2000,
                position: 'bottom right'
              });
              vm.temp_rate = [];
              vm.disableQuoteBtn = false;
              vm.loader = true;
              return false;
            }
            //launch quote timer
            var circular_timer = angular.element('#circular-timer-box');
            circular_timer.empty();
            circular_timer.append('<div id="circular-timer" data-timer="60"></div>');
            var timer = circular_timer.find('#circular-timer');
            timer.TimeCircles({
              "animation": "smooth",
              "bg_width": 1,
              "fg_width": 0.21,
              "circle_bg_color": "#AAAAAA",
              "direction":'Counter-clockwise',
              "start_angle": 160,
              "time": {
                "Days": {
                  "text": "Days",
                  "color": "#FFCC66",
                  "show": false
                },
                "Hours": {
                  "text": "Hours",
                  "color": "#99CCFF",
                  "show": false
                },
                "Minutes": {
                  "text": "Minutes",
                  "color": "#BBFFBB",
                  "show": false
                },
                "Seconds": {
                  "text": "Expires in",
                  "color": "#FFA834",
                  "show": true
                }
              }
            }).addListener(function(unit, value, total){
              if( total == -1){
                timer.TimeCircles().stop();
                $interval.cancel(vm.fetchBuyRates);
                vm.fetchBuyRates = null;
                $mdToast.show({
                  template: '<md-toast class="md-toast error">Quote timeout.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                vm.temp_rate = [];
                vm.disableQuoteBtn = false;
                vm.loader = true;
                vm.previousStep();
              }else{
                if(unit == 'Seconds'){
                  var text_box = timer.find('.textDiv_Seconds');
                  text_box.find('.title').remove();
                  var new_value = value + 'Secs';
                  text_box.append('<span class="title">'+new_value+'</span>');
                }
              }
            });
            //vm.timer();
            vm.fetchBuyRates = $interval(function(){
              var buy_currency_id = vm.step1.to_currency_id;
              var sell_currency_id = vm.step1.from_currency_id;
              vm.rate = vm.temp_rate;
              // vm.GetRateByIDS(buy_currency_id, 0, sell_currency_id, vm.parseFloating(vm.step1.amount), new Date(Date.parse(vm.step1.date)), vm.entity.entity.name).then(function(data){
              //   vm.temp_rate = data[0];
              // });
              vm.GetRateByIDS(sell_currency_id, vm.parseFloating(vm.step1.amount), buy_currency_id, 0, new Date(Date.parse(vm.step1.date)), vm.entity.entity.name).then(function(data){
                vm.temp_rate = data[0];
              });
            }, 2000);

            vm.loader = true;
            vm.nextStep();
          });
        }
      }
    };

    function convertDate(dt){
      var date = new Date(dt);
      return date.getFullYear() + '-' + ('0' + (date.getMonth()+1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
    }
    function entryDate(){
      var date = new Date();
      return ('0' + (date.getMonth()+1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear();
    }
    function convertDataToJson(xml, name){
      var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
      var actualJson = XmlJson.xmlToJson(xmlDOM);
      return actualJson.output[name];
    }
    function convertObjects(objectOrStringValue){

      if(typeof objectOrStringValue === 'object')
      {
        return objectOrStringValue['#text'].join(" ");
      }
      return objectOrStringValue
    }
    // console.log("Expiry Type");
    // console.log(vm.step1.expiry_type);

  }
})();
