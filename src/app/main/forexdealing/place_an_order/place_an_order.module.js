(function () {
  'use strict';

  var app = angular
    .module('app.forexdealing.place_order', ['datatables'])
    .config(config);

  /** @ngInject */
  function config($stateProvider, msApiProvider) {
    // State
    $stateProvider.state('app.place_order', {
      url: '/place_an_order',
      views: {
        'content@app': {
          templateUrl: 'app/main/forexdealing/place_an_order/place_an_order.html',
          controller: 'placeAnOrderController as vm'
        }
      },
      resolve: {
        Invoice: function (msApi) {
          return msApi.resolve('invoice@get');
        },
        access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
      },


    });

    // Api
    // Api
    //msApiProvider.register('invoice', ['app/data/invoice/invoice.json']);
  }
})();
