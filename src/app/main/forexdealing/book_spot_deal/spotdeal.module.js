(function () {
  'use strict';

  var app = angular
    .module('app.forexdealing.book_spot_deal', ['datatables'])
    .config(config);

  /** @ngInject */
  function config($stateProvider, msApiProvider) {
    // State
    $stateProvider.state('app.book_spot_deal', {
      url: '/book_spot_deal',
      views: {
        'content@app': {
          templateUrl: 'app/main/forexdealing/book_spot_deal/spotdeal.html',
          controller: 'SpotDealController as vm'
        }
      },
      resolve: {
        Invoice: function (msApi) {
          return msApi.resolve('invoice@get');
        },
        access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
      },

    });

    // Api
    // Api
    msApiProvider.register('invoice', ['app/data/invoice/invoice.json']);
  }
})();
