(function () {
  'use strict';

  angular
    .module('app.forexdealing.book_spot_deal')
    .controller('SpotDealController', SpotDealController);

  /** @ngInject */
  function SpotDealController($state, api, $timeout,$compile, $rootScope, $mdDialog, $scope, $interval, $mdToast, Invoice, $filter, soap_api, $q){
  // function SpotDealController($state, $timeout,$compile, $rootScope, $mdDialog, $scope, $interval, $mdToast, Invoice, $filter, soap_api, $q){
    var vm = this;


    vm.circleLoader = true;
    vm.showCircleLoader = true;
    vm.step1_disabled = false;
    vm.defaultSelected = true;


    vm.GetFeeByMethodByID = function(ID){
      return soap_api.GetFeeByMethodByID(ID).then(function(success){
        //var result = [];
        var fee = [];
        var instrument_fee = [];
        var same_currency_fee = [];
        var error = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT FEE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            free_instruments: jQuery(e).find('FREE_INSTRUMENTS').text(),
            instrument_cost: jQuery(e).find('INSTRUMENT_COST').text(),
            non_instrument_cost: jQuery(e).find('NON_INSTRUMENT_COST').text(),
            inv_act_type_id: jQuery(e).find('INV_ACT_TYPE_ID').text(),
            inv_act_type: jQuery(e).find('INV_ACT_TYPE').text(),
            charge_id: jQuery(e).find('CHARGE_ID').text(),
            charge_type: jQuery(e).find('CHARGE_TYPE').text(),
            is_allowed: jQuery(e).find('IS_ALLOWED').text(),
            source_id: jQuery(e).find('SOURCE_ID').text(),
          };
          fee.push(temp);
        });
        jQuery(response).find('OUTPUT INSTRUMENT_FEE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            currency_id: jQuery(e).find('Currency_ID').text(),
            instrument_cost: jQuery(e).find('Instrument_Cost').text(),
            cost_type: jQuery(e).find('COST_TYPE').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            cost_type_name: jQuery(e).find('COST_TYPE_NAME').text()
          };
          instrument_fee.push(temp);
        });
        jQuery(response).find('OUTPUT SAME_CURRENCY_FEE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            currency_id: jQuery(e).find('Currency_ID').text(),
            instrument_cost: jQuery(e).find('Instrument_Cost').text(),
            cost_type: jQuery(e).find('COST_TYPE').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            cost_type_name: jQuery(e).find('COST_TYPE_NAME').text()
          };
          same_currency_fee.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          error.push(temp);
        });
        var result = {
          fee : fee,
          instrument_fee: instrument_fee,
          same_currency_fee: same_currency_fee,
          error: error
        }
        return result;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.isXML = function(xml){
      try{
        var xmlDoc = jQuery.parseXML(xml);
        return true;
      }catch(error){
        return false;
      }
    };
    vm.parseDate = function parseDate(input) {
      console.log(typeof input);
      var parts = input.split('-');
      // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
      return new Date(parts[0], parts[1]-1, parts[2]); // Note: months are 0-based
    };
    vm.pasteLimit = function(event, limit){
      var length = event.originalEvent.clipboardData.getData('text/plain').length + event.originalEvent.target.value.length;
      if(length > limit){
        event.preventDefault();
      }
    };
    vm.rate = [];
    vm.temp_rate = [];
    vm.step2 = {
      rate: vm.rate
    };
    vm.post_deal = {};
    vm.deal = {};
    vm.temp_deal = {};
    vm.mergeObjects = function(obj1,obj2){
      var obj3 = {};
      for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
      for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
      return obj3;
    };
    vm.toFixed = function(floating, to){
      floating = floating || 0.0;
      return parseFloat(floating).toFixed(to);
    };
    vm.GetDefaultDate = function(){
      return soap_api.GetDefaultDate().then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.GetDefaultDate().then(function(data){
      if(data[0].status == 'TRUE'){
        vm.getDefaultDate = data[0];
        vm.defaultValueDate = new Date(Date.parse(data[0].recordid));
        vm.defaultValueDate = ('0' + (vm.defaultValueDate.getMonth()+1)).slice(-2) + '/' + ('0' + vm.defaultValueDate.getDate()).slice(-2) + '/' + vm.defaultValueDate.getFullYear();
        vm.step1.value_date = vm.defaultValueDate;
        vm.step3.value_date = vm.step1.value_date;
      }
    });
    vm.GetUserApprovalRights = function(deal, module){
      return soap_api.GetUserApprovalRights(deal, module).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.GetAllCurrencies = function(){
      var row = [];
      soap_api.GetAllCurrencies().then(function(success){
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT CURRENCY').each(function(i,e){
          var temp = {
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text(),
          };
          row.push(temp);
        });
      }, function(error){
        console.error(error.statusText);
      });
      return row;
    };
    vm.GetAllElectronicMethods = function(){
      return soap_api.GetAllElectronicMethods().then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            is_template: jQuery(e).find('IS_TEMPLATE').text(),
            is_routing_type: jQuery(e).find('IS_ROUTING_TYPE').text(),
            is_bank_code_enabled: jQuery(e).find('IS_BANK_CODE_ENABLED').text(),
            is_electronic: jQuery(e).find('IS_ELECTRONIC').text(),
            is_bank_code_required: jQuery(e).find('IS_BANK_CODE_REQUIRED').text()
          };
          row.push(temp);
        });
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.GetAllElectronicMethods().then(function(data){
      vm.allElectronicMethods = data;
    });
    api.GetEntity().then(function (data) {
      vm.entity = data.output;
      if(angular.isDefined(vm.entity) && vm.entity !== null){
        api.GetEntitySpreadTemplate(vm.entity.entity.entity_id).then(function(data){
          vm.entitySpreadTemplate = data.output.spread_template;
        });
      }
      /*vm.entity = data[0];
      if(typeof vm.entity !== 'undefined'){
        vm.GetEntitySpreadTemplate(vm.entity.entity.entity_id).then(function(data){
          vm.entitySpreadTemplate = data[0];
        });
      }*/
    });
    vm.GetEntityWebDefaults = function(){
      return soap_api.GetEntityWebDefaults().then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT WEB_DEFAULT').each(function(i,e){
          var temp = {
            ID: jQuery(e).find('ID').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            allow_forward_deal: jQuery(e).find('ALLOW_FORWARD_DEAL').text(),
            trader_id: jQuery(e).find('TRADER_ID').text(),
            ordertype_id: jQuery(e).find('ORDERTYPE_ID').text(),
            funding_method_id: jQuery(e).find('FUNDING_METHOD_ID').text(),
            is_single_ccy_fee: jQuery(e).find('IS_SINGLE_CCY_FEE').text(),
            fee_cur_id: jQuery(e).find('FEE_CUR_ID').text(),
            fundingpaymentmethod: jQuery(e).find('FundingPaymentMethod').text(),
            payment_method_id: jQuery(e).find('PAYMENT_METHOD_ID').text(),
            paymentmethod: jQuery(e).find('PaymentMethod').text(),
            funding_template_id: jQuery(e).find('FUNDING_TEMPLATE_ID').text(),
            fundingtemplatename: jQuery(e).find('FundingTemplateName').text(),
            payment_template_id: jQuery(e).find('PAYMENT_TEMPLATE_ID').text(),
            paymenttemplatename: jQuery(e).find('PaymentTemplateName').text(),
            funding_cur_id: jQuery(e).find('FUNDING_CUR_ID').text(),
            fundingcurrency: jQuery(e).find('FundingCurrency').text(),
            payment_cur_id: jQuery(e).find('PAYMENT_CUR_ID').text(),
            paymentcurrency: jQuery(e).find('PaymentCurrency').text(),
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.GetEntityWebDefaults().then(function(data){
      vm.entityWebDefaults = data[0];
    });
    vm.GetRate = function(buy_currency, buy_amount, sell_currency, sell_amount, value_date, client_name){
      return soap_api.GetRate(buy_currency, buy_amount, sell_currency, sell_amount, value_date, client_name).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Rate').each(function(i,e) {
          var temp = {
            exchange_rate: jQuery(e).find('EXCHANGE_RATE').text(),
            market_rate: jQuery(e).find('MARKET_RATE').text(),
            spread_rate: jQuery(e).find('SPREAD_RATE').text(),
            actual_market_rate: jQuery(e).find('ACTUAL_MARKET_RATE').text(),
            sell_amount: jQuery(e).find('SELL_AMOUNT').text(),
            buy_amount: jQuery(e).find('BUY_AMOUNT').text()
          };
          row.push(temp);
        });
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.GetRateByIDS = function(buy_currency, buy_amount, sell_currency, sell_amount, value_date, client_name){
      return soap_api.GetRateByIDS(buy_currency, buy_amount, sell_currency, sell_amount, value_date, client_name).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Rate').each(function(i,e) {
          var temp = {
            exchange_rate: jQuery(e).find('EXCHANGE_RATE').text(),
            market_rate: jQuery(e).find('MARKET_RATE').text(),
            spread_rate: jQuery(e).find('SPREAD_RATE').text(),
            actual_market_rate: jQuery(e).find('ACTUAL_MARKET_RATE').text(),
            sell_amount: jQuery(e).find('SELL_AMOUNT').text(),
            buy_amount: jQuery(e).find('BUY_AMOUNT').text()
          };
          row.push(temp);
        });
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.step2.rate = vm.rate;
    vm.GetPurposeOfPayments = function(){
      var row = [];
      soap_api.GetPurposeOfPayments().then(function(success){
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT PURPOSE_OF_PAYMENT').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            description: jQuery(e).find('DESCRIPTION').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            is_approved: jQuery(e).find('IS_APPROVED').text(),
            version_no: jQuery(e).find('VERSION_NO').text()
          };
          row.push(temp);
        });
      }, function(error){
        console.error(error.statusText);
      });
      return row;
    };
    vm.paymentPurposes = vm.GetPurposeOfPayments();
    vm.GetAllPayeeTemplates = function(){
      //vm.showCircleLoader = false;
      return soap_api.GetAllPayeeTemplates().then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT Table1').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              name: jQuery(e).find('NAME').text(),
              parent_id: jQuery(e).find('PARENT_ID').text(),
              entity: jQuery(e).find('ENTITY').text(),
              type: jQuery(e).find('TYPE').text(),
              is_non_third_party: jQuery(e).find('IS_NON_THIRD_PARTY').text(),
              payment_type_name: jQuery(e).find('PAYMENT_TYPE_NAME').text(),
              payment_type: jQuery(e).find('PAYMENT_TYPE').text(),
              act_seg_id: jQuery(e).find('ACT_SEG_ID').text(),
              paymentmethod_name: jQuery(e).find('PAYMENTMETHOD_NAME').text(),
              paymentmethod: jQuery(e).find('PAYMENTMETHOD').text(),
              currency_id: jQuery(e).find('CURRENCY_ID').text(),
              currency_name: jQuery(e).find('CURRENCY_NAME').text(),
              beneficiary_accountcode: jQuery(e).find('BENEFICIARY_ACCOUNTCODE').text(),
              country_name: jQuery(e).find('COUNTRY_NAME').text(),
              beneficiary_email: jQuery(e).find('BENEFICIARY_EMAIL').text(),
              beneficiary_instructions: jQuery(e).find('BENEFICIARY_INSTRUCTIONS').text(),
              beneficiarybank_name: jQuery(e).find('BENEFICIARYBANK_NAME').text(),
              purpose: jQuery(e).find('PURPOSE').text(),
              purpose_of_payment: jQuery(e).find('PURPOSE_OF_PAYMENT').text(),
              currency_item_id: jQuery(e).find('CURRENCY_ITEM_ID').text(),
            };
            row.push(temp);
          });
          return row;
        }else{
          console.log(success.data);
        }
      });
    };
    vm.GetFeeByMethod = function(method){
      var row = [];
      soap_api.GetFeeByMethod(method).then(function(success){
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT FEE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            free_instruments: jQuery(e).find('FREE_INSTRUMENTS').text(),
            instrument_cost: jQuery(e).find('INSTRUMENT_COST').text(),
            non_instrument_cost: jQuery(e).find('NON_INSTRUMENT_COST').text(),
            inv_act_type_id: jQuery(e).find('INV_ACT_TYPE_ID').text(),
            inv_act_type: jQuery(e).find('INV_ACT_TYPE').text(),
            charge_id: jQuery(e).find('CHARGE_ID').text(),
            charge_type: jQuery(e).find('CHARGE_TYPE').text(),
            is_allowed: jQuery(e).find('IS_ALLOWED').text(),
            source_id: jQuery(e).find('SOURCE_ID').text(),
          };
          row.push(temp);
        });
      }, function(error){
        console.error(error.statusText);
      });
      return row;
    };
    vm.GetCountries = function(){
      return soap_api.GetCountries().then(function(success){
        //vm.showCircleLoader = false;
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT COUNTRY').each(function(i,e){
          var temp = {
            country_id: jQuery(e).find('COUNTRY_ID').text(),
            country_name: jQuery(e).find('COUNTRY_NAME').text(),
            continent_id: jQuery(e).find('CONTINENT_ID').text(),
            country_code: jQuery(e).find('COUNTRY_CODE').text(),
            is_watch_list: jQuery(e).find('IS_WATCH_LIST').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            is_approved: jQuery(e).find('IS_APPROVED').text(),
            version_no: jQuery(e).find('VERSION_NO').text()
          };
          row.push(temp);
        });
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.uniqueCountries = function(array){
      var flags = [], output = [], l = array.length, i;
      for( i=0; i<l; i++) {
        if( flags[array[i].country_name]) continue;
        flags[array[i].country_name] = true;
        output.push(array[i]);
      }
      return output;
    };
    vm.CreateWebDeal = function(client, trader, notes, internal_remarks, value_date, is_bach, deal_transaction){
      return soap_api.CreateWebDeal(client, trader, notes, internal_remarks, value_date, is_bach, deal_transaction).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text()
          };
          row.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text(),
            recordnumber: jQuery(e).find('RECORDNUMBER').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.GetFeeByMethod = function(method){
      return soap_api.GetFeeByMethod(method).then(function(success){
        var result = {};
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT FEE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            free_instruments: jQuery(e).find('FREE_INSTRUMENTS').text(),
            instrument_cost: jQuery(e).find('INSTRUMENT_COST').text(),
            non_instrument_cost: jQuery(e).find('NON_INSTRUMENT_COST').text(),
            inv_act_type_id: jQuery(e).find('INV_ACT_TYPE_ID').text(),
            inv_act_type: jQuery(e).find('INV_ACT_TYPE').text(),
            charge_id: jQuery(e).find('CHARGE_ID').text(),
            charge_type: jQuery(e).find('CHARGE_TYPE').text(),
            is_allowed: jQuery(e).find('IS_ALLOWED').text(),
            source_id: jQuery(e).find('SOURCE_ID').text(),
          };
          angular.copy(temp, result);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          angular.copy(temp, result);
        });
        return result;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.GetFundingCurrencies = function(){
      return soap_api.GetFundingCurrencies().then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            buy_item_id: jQuery(e).find('BUY_ITEM_ID').text(),
            item_name: jQuery(e).find('ITEM_NAME').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            curr_rate_conversion_type: jQuery(e).find('CURR_RATE_CONVERSION_TYPE').text(),
            currency_price_rounding: jQuery(e).find('CURRENCY_PRICE_ROUNDING').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetFundingCurrenciesFilterByCurrency = function(currency_id){
      return soap_api.GetFundingCurrenciesFilterByCurrency(currency_id).then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            buy_item_id: jQuery(e).find('BUY_ITEM_ID').text(),
            item_name: jQuery(e).find('ITEM_NAME').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            curr_rate_conversion_type: jQuery(e).find('CURR_RATE_CONVERSION_TYPE').text(),
            currency_price_rounding: jQuery(e).find('CURRENCY_PRICE_ROUNDING').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetFundingCurrenciesWithoutFilter = function(){
      return soap_api.GetFundingCurrenciesWithoutFilter().then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            buy_item_id: jQuery(e).find('BUY_ITEM_ID').text(),
            item_name: jQuery(e).find('ITEM_NAME').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            curr_rate_conversion_type: jQuery(e).find('CURR_RATE_CONVERSION_TYPE').text(),
            currency_price_rounding: jQuery(e).find('CURRENCY_PRICE_ROUNDING').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetPaymentCurrencies = function(){
      return soap_api.GetPaymentCurrencies().then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            buy_item_id: jQuery(e).find('BUY_ITEM_ID').text(),
            item_name: jQuery(e).find('ITEM_NAME').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            curr_rate_conversion_type: jQuery(e).find('CURR_RATE_CONVERSION_TYPE').text(),
            currency_price_rounding: jQuery(e).find('CURRENCY_PRICE_ROUNDING').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetPaymentCurrenciesFilterByCurrency = function(currency_id){
      return soap_api.GetPaymentCurrenciesFilterByCurrency(currency_id).then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            buy_item_id: jQuery(e).find('BUY_ITEM_ID').text(),
            item_name: jQuery(e).find('ITEM_NAME').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            curr_rate_conversion_type: jQuery(e).find('CURR_RATE_CONVERSION_TYPE').text(),
            currency_price_rounding: jQuery(e).find('CURRENCY_PRICE_ROUNDING').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetPaymentCurrenciesWithoutFilter = function(){
      return soap_api.GetPaymentCurrenciesWithoutFilter().then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            buy_item_id: jQuery(e).find('BUY_ITEM_ID').text(),
            item_name: jQuery(e).find('ITEM_NAME').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            curr_rate_conversion_type: jQuery(e).find('CURR_RATE_CONVERSION_TYPE').text(),
            currency_price_rounding: jQuery(e).find('CURRENCY_PRICE_ROUNDING').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetSettlementsPayeesByCurrency = function(currency_id){
      return soap_api.GetSettlementsPayeesByCurrency(currency_id).then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        console.log(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            buy_item_id: jQuery(e).find('BUY_ITEM_ID').text(),
            item_name: jQuery(e).find('ITEM_NAME').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            curr_rate_conversion_type: jQuery(e).find('CURR_RATE_CONVERSION_TYPE').text(),
            currency_price_rounding: jQuery(e).find('CURRENCY_PRICE_ROUNDING').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetSettlementsByMethodsAndCurrencyInBookASpot = function(methods, currency_id){
      return soap_api.GetSettlementsByMethodsAndCurrencyInBookASpot(methods, currency_id).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT Table1').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              name: jQuery(e).find('NAME').text(),
              parent_id: jQuery(e).find('PARENT_ID').text(),
              entity: jQuery(e).find('ENTITY').text(),
              type: jQuery(e).find('TYPE').text(),
              is_non_third_party: jQuery(e).find('IS_NON_THIRD_PARTY').text(),
              payment_type_name: jQuery(e).find('PAYMENT_TYPE_NAME').text(),
              payment_type: jQuery(e).find('PAYMENT_TYPE').text(),
              act_seg_id: jQuery(e).find('ACT_SEG_ID').text(),
              paymentmethod_name: jQuery(e).find('PAYMENTMETHOD_NAME').text(),
              paymentmethod: jQuery(e).find('PAYMENTMETHOD').text(),
              currency_id: jQuery(e).find('CURRENCY_ID').text(),
              currency_name: jQuery(e).find('CURRENCY_NAME').text(),
              beneficiary_accountcode: jQuery(e).find('BENEFICIARY_ACCOUNTCODE').text(),
              country_name: jQuery(e).find('COUNTRY_NAME').text(),
              beneficiary_email: jQuery(e).find('BENEFICIARY_EMAIL').text(),
              beneficiary_instructions: jQuery(e).find('BENEFICIARY_INSTRUCTIONS').text(),
              beneficiarybank_name: jQuery(e).find('BENEFICIARYBANK_NAME').text(),
              purpose: jQuery(e).find('PURPOSE').text(),
              internal_reference: jQuery(e).find('INTERNAL_REFERENCE').text(),
              purpose_of_payment: jQuery(e).find('PURPOSE_OF_PAYMENT').text(),
              currency_item_id: jQuery(e).find('CURRENCY_ITEM_ID').text(),
            };
            row.push(temp);
          });
          return row;
        }else{
          console.log(success.data);
        }
      });
    };
    vm.GetPaymentsByMethodAndCurrencyInBookASpot = function(method, currency_id){
      return soap_api.GetPaymentsByMethodAndCurrencyInBookASpot(method, currency_id).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT Table1').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              name: jQuery(e).find('NAME').text(),
              parent_id: jQuery(e).find('PARENT_ID').text(),
              entity: jQuery(e).find('ENTITY').text(),
              type: jQuery(e).find('TYPE').text(),
              is_non_third_party: jQuery(e).find('IS_NON_THIRD_PARTY').text(),
              payment_type_name: jQuery(e).find('PAYMENT_TYPE_NAME').text(),
              payment_type: jQuery(e).find('PAYMENT_TYPE').text(),
              act_seg_id: jQuery(e).find('ACT_SEG_ID').text(),
              paymentmethod_name: jQuery(e).find('PAYMENTMETHOD_NAME').text(),
              paymentmethod: jQuery(e).find('PAYMENTMETHOD').text(),
              currency_id: jQuery(e).find('CURRENCY_ID').text(),
              currency_name: jQuery(e).find('CURRENCY_NAME').text(),
              beneficiary_accountcode: jQuery(e).find('BENEFICIARY_ACCOUNTCODE').text(),
              country_name: jQuery(e).find('COUNTRY_NAME').text(),
              beneficiary_email: jQuery(e).find('BENEFICIARY_EMAIL').text(),
              beneficiary_instructions: jQuery(e).find('BENEFICIARY_INSTRUCTIONS').text(),
              beneficiarybank_name: jQuery(e).find('BENEFICIARYBANK_NAME').text(),
              purpose: jQuery(e).find('PURPOSE').text(),
              purpose_of_payment: jQuery(e).find('PURPOSE_OF_PAYMENT').text(),
              currency_item_id: jQuery(e).find('CURRENCY_ITEM_ID').text(),
            };
            row.push(temp);
          });
          return row;
        }else{
          console.log(success.data);
        }
      });
    };
    vm.GetSavedApprovalsByModule = function(module){
      return soap_api.GetSavedApprovalsByModule(module).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT ADM_WEB_APPROVAL_POLICY').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              entity_name: jQuery(e).find('ENTITY_NAME').text(),
              entity_id: jQuery(e).find('ENTITY_ID').text(),
              module_id: jQuery(e).find('MODULE_ID').text(),
              approval_level: jQuery(e).find('APPROVAL_LEVEL').text(),
              created_on: jQuery(e).find('CREATED_ON').text(),
              created_by: jQuery(e).find('CREATED_BY').text(),
              updated_on: jQuery(e).find('UPDATED_ON').text(),
              updated_by: jQuery(e).find('UPDATED_BY').text(),
              is_deleted: jQuery(e).find('IS_DELETED').text(),
              is_approved: jQuery(e).find('IS_APPROVED').text(),
              version_no: jQuery(e).find('VERSION_NO').text()
            };
            row.push(temp);
          });
          jQuery(response).find('OUTPUT ADM_WEB_USER_APPROVAL').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              approval_policy_id: jQuery(e).find('APPROVAL_POLICY_ID').text(),
              user_id: jQuery(e).find('USER_ID').text(),
              user_name: jQuery(e).find('USER_NAME').text(),
              approval_type_id: jQuery(e).find('APPROVAL_TYPE_ID').text(),
              approval_type_name: jQuery(e).find('APPROVAL_TYPE_NAME').text(),
              created_on: jQuery(e).find('CREATED_ON').text(),
              created_by: jQuery(e).find('CREATED_BY').text(),
              updated_on: jQuery(e).find('UPDATED_ON').text(),
              updated_by: jQuery(e).find('UPDATED_BY').text(),
              is_deleted: jQuery(e).find('IS_DELETED').text(),
              is_approved: jQuery(e).find('IS_APPROVED').text(),
              version_no: jQuery(e).find('VERSION_NO').text()
            };
            row.push(temp);
          });
        }else{
          console.log(success.data);
        }
        return row;
      });
    };
    vm.GetSavedApprovalsByModule(12277).then(function(data){

      vm.approvalPolicy = data;
      if(data[0].approval_level == 0 || data[0].approval_level == ''){
        vm.skipApprovalStep = true;
      }else{
        vm.skipApprovalStep = false;
      }
    });
    vm.GetFeeForSettlements = function(input_xml){
      return soap_api.GetFeeForSettlements(input_xml).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT PAYMENT').each(function(i,e){
            var temp = {
              buy_amount: jQuery(e).find('BUY_AMOUNT').text(),
              buy_currency_id: jQuery(e).find('BUY_CURRENCY_ID').text(),
              buy_currency: jQuery(e).find('BUY_CURRENCY').text()
            };
            row.push(temp);
          });
          jQuery(response).find('OUTPUT SETTLEMENT').each(function(i,e){
            var temp = {
              fee: jQuery(e).find('FEE').text(),
              fee_type: jQuery(e).find('FEE_TYPE').text(),
              fee_currency_id: jQuery(e).find('FEE_CURRENCY_ID').text(),
              fee_currency: jQuery(e).find('FEE_CURRENCY').text(),
              action_id: jQuery(e).find('ACTION_ID').text(),
              action_name: jQuery(e).find('ACTION_NAME').text(),
              sell_currency_id: jQuery(e).find('SELL_CURRENCY_ID').text(),
              sell_currency: jQuery(e).find('SELL_CURRENCY').text(),
              sell_amount: jQuery(e).find('SELL_AMOUNT').text(),
              rate: jQuery(e).find('RATE').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetSystemConfiguration = function(){
      return soap_api.GetSystemConfiguration().then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT SYSTEM_SETUP').each(function(i,e){
            var temp = {
              fx_fwd_days: jQuery(e).find('FX_FWD_DAYS').text(),
              web_quotation_expiry: jQuery(e).find('WEB_QUOTATION_EXPIRY').text(),
              rfq_expiry: jQuery(e).find('RFQ_EXPIRY').text(),
              trader_quotation_expiry: jQuery(e).find('TRADER_QUOTATION_EXPIRY').text(),
              allow_online_trading: jQuery(e).find('ALLOW_ONLINE_TRADING').text(),
              default_web_page_id: jQuery(e).find('DEFAULT_WEB_PAGE_ID').text(),
              rfq_quotation_mode: jQuery(e).find('RFQ_QUOTATION_MODE').text(),
              show_all_web_methods: jQuery(e).find('SHOW_ALL_WEB_METHODS').text(),
              max_online_forward_days: jQuery(e).find('MAX_ONLINE_FORWARD_DAYS').text(),
              allow_secure_ques_on_login: jQuery(e).find('ALLOW_SECURE_QUES_ON_LOGIN').text(),
              enforce_term_condition: jQuery(e).find('ENFORCE_TERM_CONDITION').text(),
              password_reset_type: jQuery(e).find('PASSWORD_RESET_TYPE').text(),
              allow_email_approval: jQuery(e).find('ALLOW_EMAIL_APPROVAL').text(),
              allow_1to1_deal: jQuery(e).find('ALLOW_1TO1_DEAL').text(),
              is_fee_fx_based: jQuery(e).find('IS_FEE_FX_BASED').text(),
              is_pop_mandatory: jQuery(e).find('IS_POP_MANDATORY').text()
            };
            row.push(temp);
          });
          jQuery(response).find('OUTPUT ALLOWED_TIMES').each(function(i,e){
            var temp = {
              day: jQuery(e).find('DAY').text(),
              is_allowed: jQuery(e).find('IS_ALLOWED').text(),
              from_time: jQuery(e).find('FROM_TIME').text(),
              to_time: jQuery(e).find('TO_TIME').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetSystemConfiguration().then(function(data){
      vm.systemConfig = data;
      if(vm.systemConfig[0].is_pop_mandatory == 'false'){
        vm.popMandate = false;
      }else{
        vm.popMandate = true;
      }
    });
    vm.GetWebDealPaymentAndSettlement = function(deal){
      return soap_api.GetWebDealPaymentAndSettlement(deal).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT WEB_DLG_DEAL_SETTLEMENT').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              settlement_id: jQuery(e).find('SETTLEMENT_ID').text(),
              deal_id: jQuery(e).find('DEAL_ID').text(),
              internal_reference: jQuery(e).find('INTERNAL_REFERENCE').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetWireReport = function(settlement_id){
      return soap_api.GetWireReport(settlement_id).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function(i,e){
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              recordid: jQuery(e).find('RECORDID').text(),
              error: jQuery(e).find('ERROR').text(),
              value: jQuery(e).find('VALUE').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetDealReport = function(deal){
      return soap_api.GetDealReport(deal).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function(i,e){
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              recordid: jQuery(e).find('RECORDID').text(),
              error: jQuery(e).find('ERROR').text(),
              value: jQuery(e).find('VALUE').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.ValidateBusinessDate = function(date){
      return soap_api.ValidateBusinessDate(date).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function(i,e){
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              recordid: jQuery(e).find('RECORDID').text(),
              error: jQuery(e).find('ERROR').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetAllElectronicMethods = function(){
      return soap_api.GetAllElectronicMethods().then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            is_template: jQuery(e).find('IS_TEMPLATE').text(),
            is_routing_type: jQuery(e).find('IS_ROUTING_TYPE').text(),
            is_bank_code_enabled: jQuery(e).find('IS_BANK_CODE_ENABLED').text(),
            is_electronic: jQuery(e).find('IS_ELECTRONIC').text(),
            is_bank_code_required: jQuery(e).find('IS_BANK_CODE_REQUIRED').text()
          };
          row.push(temp);
        });
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };


    $rootScope.headerTitle = 'Book a Spot Deal';
    //global functions
    //vm.entity = vm.GetEntity();
    vm.parseFloat = parseFloat;
    vm.parseInt = parseInt;
    vm.isNaN = isNaN;
    vm.typeof = function(value){
      return typeof value;
    };
    vm.toLowerCase = function(str){
      return str.toLowerCase();
    };
    vm.replaceWith = function(string, separator){
      string = string || '';
      return string.replace(/ +/g, separator);
    };
    vm.parseFloat = function(number){
      return parseFloat(number);
    };
    vm.parseInt = function(number){
      return parseInt(number);
    };
    vm.toLowerCase = function(str){
      str = str || '';
      return str.toLowerCase();
    };
    vm.toUpperCase = function(str){
      str = str || '';
      return str.toUpperCase();
    };
    vm.includes = function(string, substring){
      string = string || '';
      substring = substring || '';
      if( string.toLowerCase().indexOf(substring.toLowerCase()) !== -1){
        return true;
      }else{
        return false;
      }
    };
    vm.replaceWith = function(string, separator){
      string = string || '';
      return string.replace(/ +/g, separator);
    };
    vm.protectAmount = function(event){
      var regExp = new RegExp('[a-zA-Z]'),
        inputVal = '';
      var input = event.key;
      var re = /\d/i;
      if(input != 'Backspace' && input != '.' && input != 'Delete' && input != 'Del' && input != 'Decimal' && input != 'Left' && input != 'Right' && input != 'Tab' && input != 'ArrowLeft' && input != 'ArrowRight'){
        if(input.match(re) == null){
          event.preventDefault();
          return false;
        }
      }else{
        if(input == '.' || input == 'Decimal'){
          re = /\./g;
          if(event.target.value.length > 0){
            if(event.target.value.match(/\./g) != null){
              if(event.target.value.match(/\./g).length > 0){
                event.preventDefault();
                return false;
              }
            }
          }
        }
      }
    };
    vm.hideQuoteScreen = false;
    vm.noSpecialCharactersFunction = function(event){
      var input = event.key;
      var re = /^[a-zA-Z0-9\-\s]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.protectDate = function(event){
      console.log(event.target.value);
    };
    vm.stopInputing = function(event){
      event.preventDefault();
    };
    vm.limitLength = function(event, limit){
      if(event.target.value.length == limit){
        event.preventDefault();
      }
    };
    //Properties
    vm.book_a_spot = {};
    vm.spot_deal_form = {};
    vm.interval_ = null;
    $scope.isDisabled = true;
    // Data
    vm.invoice = Invoice.data;
    vm.dtOptions = {
      // dom: '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
      dom: '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"pagination"p>>><"info"i>',
      pagingType: 'simple',
      autoWidth: false,
      responsive: true,
      search: false
    };
    vm.spots = [
      {
        "name": "SPOT",
        "value": "SPOT"
      }
    ];
    vm.selectedIndex = 0;
    vm.max = 5;
    vm.activated = true;
    vm.determinateValue = 100;
    vm.showPayeeTable = true;

    //Currencies
    vm.GetFundingCurrenciesWithoutFilter().then(function(data){
      vm.fundingCurrencies = data;
    });
    vm.GetPaymentCurrenciesWithoutFilter().then(function(data){
      vm.paymentCurrencies = data;
    });


    vm.fetchFundingCurrencies = function(stepData){
      vm.defaultSelected = true;
      vm.GetFundingCurrenciesFilterByCurrency(stepData.currency_to_buy).then(function(data){
        vm.fundingCurrencies = data;
        //console.log(data);
        /*if(vm.step1.currency_to_sell != null){
          var change = false;
          for(var i=0; i<data.length; i++){
            if(data[i].currency_id == vm.step1.currency_to_sell){
              change = true;
              break;
            }
          }
          if(change){
            vm.step1.currency_to_buy = null;
          }
          vm.fundingCurrencies = data;
        }else{
          vm.fundingCurrencies = data;
        }*/
      });
    };

    vm.fetchPaymentCurrencies = function(stepData){

      vm.GetPaymentCurrenciesFilterByCurrency(stepData.currency_to_sell).then(function(data){

        vm.paymentCurrencies = data;
        //console.log(vm.step1.currency_to_buy);
        /*if(vm.step1.currency_to_buy != null){
          var change = false;
          for(var i=0; i<data.length; i++){
            if(data[i].currency_id == vm.step1.currency_to_buy){
              change = true;
              break;
            }
          }
          if(change){
            vm.step1.currency_to_sell = null;
          }
        }else{
          vm.paymentCurrencies = data;
        }*/
      });
    };
    vm.getFundingCurrencyName = function(currency_id){
      if(currency_id != null){
        var currency = _.filter(vm.paymentCurrencies, {currency_id: currency_id});
        if(currency.length > 0)
          return currency[0].currency_name;
        else
          return '';
      }
    };
    vm.getPaymentCurrencyName = function(currency_id){
      if(currency_id != null){
        var currency = _.filter(vm.fundingCurrencies, {currency_id: currency_id});
        if(currency.length > 0)
          return currency[0].currency_name;
        else
          return '';
      }
    };

    vm.GetWebPaymentMethods = function(module){
      return soap_api.GetWebPaymentMethods(module).then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT PAYMENT_METHOD').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            allow_funding_picker: jQuery(e).find('ALLOW_FUNDING_PICKER').text(),
            allow_payment_picker: jQuery(e).find('ALLOW_PAYMENT_PICKER').text(),
            allow_payment_edit: jQuery(e).find('ALLOW_PAYMENT_EDIT').text(),
            allow_funding_edit: jQuery(e).find('ALLOW_FUNDING_EDIT').text(),
            free_instruments: jQuery(e).find('FREE_INSTRUMENTS').text(),
            instrument_cost: jQuery(e).find('INSTRUMENT_COST').text(),
            revenue_type_id: jQuery(e).find('REVENUE_TYPE_ID').text(),
            is_funding: jQuery(e).find('IS_FUNDING').text(),
            source_id: jQuery(e).find('SOURCE_ID').text(),
            charge_id: jQuery(e).find('CHARGE_ID').text(),
            charge_type: jQuery(e).find('CHARGE_TYPE').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetFundingMethod = function(module){
      return soap_api.GetFundingMethod(module).then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT FUNDING_METHOD').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            allow_funding_picker: jQuery(e).find('ALLOW_FUNDING_PICKER').text(),
            allow_payment_picker: jQuery(e).find('ALLOW_PAYMENT_PICKER').text(),
            allow_payment_edit: jQuery(e).find('ALLOW_PAYMENT_EDIT').text(),
            allow_funding_edit: jQuery(e).find('ALLOW_FUNDING_EDIT').text(),
            free_instruments: jQuery(e).find('FREE_INSTRUMENTS').text(),
            instrument_cost: jQuery(e).find('INSTRUMENT_COST').text(),
            revenue_type_id: jQuery(e).find('REVENUE_TYPE_ID').text(),
            is_funding: jQuery(e).find('IS_FUNDING').text(),
            source_id: jQuery(e).find('SOURCE_ID').text(),
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetSettlementsPayees = function(methods){
      return soap_api.GetSettlementsPayees(methods).then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('DATASET_OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            entity: jQuery(e).find('ENTITY').text(),
            type: jQuery(e).find('TYPE').text(),
            is_non_third_party: jQuery(e).find('IS_NON_THIRD_PARTY').text(),
            payment_type_name: jQuery(e).find('PAYMENT_TYPE_NAME').text(),
            payment_type: jQuery(e).find('PAYMENT_TYPE').text(),
            act_seg_id: jQuery(e).find('ACT_SEG_ID').text(),
            paymentmethod_name: jQuery(e).find('PAYMENTMETHOD_NAME').text(),
            paymentmethod: jQuery(e).find('PAYMENTMETHOD').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            beneficiary_accountcode: jQuery(e).find('BENEFICIARY_ACCOUNTCODE').text(),
            country_name: jQuery(e).find('COUNTRY_NAME').text(),
            beneficiary_email: jQuery(e).find('BENEFICIARY_EMAIL').text(),
            beneficiary_instructions: jQuery(e).find('BENEFICIARY_INSTRUCTIONS').text(),
            internal_reference: jQuery(e).find('INTERNAL_REFERENCE').text(),
            beneficiarybank_name: jQuery(e).find('BENEFICIARYBANK_NAME').text(),
            purpose: jQuery(e).find('PURPOSE').text(),
            purpose_of_payment: jQuery(e).find('PURPOSE_OF_PAYMENT').text(),
            currency_item_id: jQuery(e).find('CURRENCY_ITEM_ID').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.updateWebDeal = function(deal, value_date, deal_transaction){
      return soap_api.updateWebDeal(deal, value_date, deal_transaction).then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        console.log(response);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text(),
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetFundingMethod(12277).then(function(data){
      vm.methods = data;
    });
    vm.GetWebPaymentMethods(12277).then(function(data){
      vm.settlementMethods = data;
      //console.log(vm.settlementMethods);
      var m = [];
      for(var i=0; i<data.length; i++){
        m.push(data[i].id);
      }
      vm.payee_methods_joined = m.join('|');
    });
    //countries
    vm.GetCountries().then(function(data){
      vm.countries = vm.uniqueCountries(data);
    });
    vm.GetAllElectronicMethods().then(function(data){
      vm.allElectronicMethods = data;
    });
    //form data containing arrays
    vm.payees = [];
    vm.fundings = [];
    vm.quote = [];
    //temporary data holding
    vm.quote_copy = [];

    vm.step1 = {
      currency_to_buy: null,
      currency_to_sell: null,
      i_want_to: null,
      amount: null,
      value_date: null
    };

    //step 3
    vm.step3 = {
      payee_: null,
      amount: null,
      payment_reference: null,
      value_date: '',
      payment_purpose: null,
      internal_payment_reference: null,
      payment_notification_email: null,
      no_notification: false
    };
    vm.payeeTable = {
      routing : '',
      payee_payor : '',
      bank_name : '',
      country : ''
    };
    vm.balance = {
      amount: null,
      copy_amount: null
    };
    //step4
    vm.step4 = {
      method: null,
      amount: null,
      account_picker: null,
      value_date: ''
    };
    vm.funding_balance = {
      amount: null,
      copy_amount: null
    };
    //step 5
    vm.GetApprovalHistory = function(deal){
      return soap_api.GetApprovalHistory(deal).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT APPROVAL_HISTORY').each(function(i,e){
            var temp = {
              trans_approval_id: jQuery(e).find('TRANS_APPROVAL_ID').text(),
              org_approval_id: jQuery(e).find('ORG_APPROVAL_ID').text(),
              transaction_id: jQuery(e).find('TRANSACTION_ID').text(),
              user_id: jQuery(e).find('USER_ID').text(),
              user_name: jQuery(e).find('USER_NAME').text(),
              approval_num: jQuery(e).find('APPROVAL_NUM').text(),
              approval_date: jQuery(e).find('APPROVAL_DATE').text(),
              approval_level: jQuery(e).find('APPROVAL_LEVEL').text(),
              created_on: jQuery(e).find('CREATED_ON').text(),
              created_by: jQuery(e).find('CREATED_BY').text(),
              updated_on: jQuery(e).find('UPDATED_ON').text(),
              updated_by: jQuery(e).find('UPDATED_BY').text(),
              version_no: jQuery(e).find('VERSION_NO').text(),
              is_deleted: jQuery(e).find('IS_DELETED').text()
            };
            row.push(temp);
          });
        }else{
          console.log(success.data);
        }
        return row;
      });
    };
    vm.showPopOver = true;
    vm.popover = {
      title: 'List of Approvals',
    };
    vm.Approvals = [];
    vm.ApprovalNext = false;
    vm.approvalCounter = 0;
    //Methods
    vm.parseFloating = function(value, s){

      var the_amount = +value;
      if(typeof value === 'string'){
        the_amount = value.split(',').join('');
        the_amount = vm.parseFloat(the_amount).toFixed(2);
      }
      if(typeof value === 'number'){
        the_amount = vm.parseFloat(the_amount).toFixed(2);
      }

      return the_amount;
    };
    vm.doneFunc = function(){
      $state.reload();
    };
    //step 1
    vm.getQuote = function(){
      vm.step1_disabled = true;
      vm.circleLoader = false;
      vm.temp_rate = [];
      if(vm.step1.currency_to_buy != null && vm.step1.currency_to_sell != null && vm.step1.amount != null && vm.step1.value_date != null && vm.step1.value_date != '' && vm.step1.i_want_to !== null){
          vm.disableQuote = true;
          var value_date = new Date(Date.parse(vm.step1.value_date));
          var amount = vm.parseFloating(vm.step1.amount);
          vm.ValidateBusinessDate(value_date).then(function(data){
              data = data[0];
              if(data.status == 'TRUE'){
                  if(typeof vm.entity === 'undefined'){
                      $mdToast.show({
                          template: '<md-toast class="md-toast error">Entity record not found.</md-toast>',
                          hideDelay: 4000,
                          position: 'bottom right'
                      });
                      vm.temp_rate = [];
                      vm.step1_disabled = false;
                      vm.circleLoader = true;
                      vm.disableQuote = false;
                      vm.hideQuoteScreen = false;
                      vm.acceptQuoteDisable = false;
                      vm.declineQuoteDisable = false;
                      return false;
                  }
                  if(vm.step1.currency_to_buy == vm.step1.currency_to_sell){
                      //both currencies are same.
                      if(vm.step1.i_want_to == 'buy'){
                          vm.GetRateByIDS(vm.step1.currency_to_buy, amount, vm.step1.currency_to_sell, 0, value_date, vm.entity.entity.name).then(function(data){
                              vm.temp_rate = data[0];
                              vm.rate = vm.temp_rate;
                              if(data[0].exchange_rate == 0){
                                  $mdToast.show({
                                      template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
                                      hideDelay: 2000,
                                      position: 'bottom right'
                                  });
                                  vm.temp_rate = [];
                                  vm.step1_disabled = false;
                                  vm.circleLoader = true;
                                  vm.disableQuote = false;
                                  return false;
                              }
                              vm.hideQuoteScreen = true;
                              vm.nextStep();
                          });
                      }else{
                          vm.GetRateByIDS(vm.step1.currency_to_buy, 0, vm.step1.currency_to_sell, amount, value_date, vm.entity.entity.name).then(function(data){
                              vm.temp_rate = data[0];
                              vm.rate = vm.temp_rate;
                              if(data[0].exchange_rate == 0){
                                  $mdToast.show({
                                      template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
                                      hideDelay: 2000,
                                      position: 'bottom right'
                                  });
                                  vm.temp_rate = [];
                                  vm.step1_disabled = false;
                                  vm.circleLoader = true;
                                  vm.disableQuote = false;
                                  return false;
                              }
                              vm.hideQuoteScreen = true;
                              vm.nextStep();
                          });
                      }
                  }else{
                      //both currencies are not same.
                      if(vm.step1.i_want_to == 'buy'){
                          vm.GetRateByIDS(vm.step1.currency_to_buy, amount, vm.step1.currency_to_sell, 0, value_date, vm.entity.entity.name).then(function(data){
                              vm.temp_rate = data[0];
                              vm.rate = vm.temp_rate;
                              if(!data[0] || (data[0] && data[0].exchange_rate == 0) ){
                                  $mdToast.show({
                                      template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
                                      hideDelay: 2000,
                                      position: 'bottom right'
                                  });
                                  vm.temp_rate = [];
                                  vm.step1_disabled = false;
                                  vm.circleLoader = true;
                                  vm.disableQuote = false;
                                  return false;
                              }

                              //launch circular and linear timer.
                              var circular_timer = angular.element('#circular-timer-box');
                              circular_timer.empty();
                              circular_timer.append('<div id="circular-timer" data-timer="60"></div>');
                              var timer = circular_timer.find('#circular-timer');
                              timer.TimeCircles({
                                  "animation": "smooth",
                                  "bg_width": 1,
                                  "fg_width": 0.21,
                                  "circle_bg_color": "#AAAAAA",
                                  "direction":'Counter-clockwise',
                                  "start_angle": 160,
                                  "time": {
                                      "Days": {
                                          "text": "Days",
                                          "color": "#FFCC66",
                                          "show": false
                                      },
                                      "Hours": {
                                          "text": "Hours",
                                          "color": "#99CCFF",
                                          "show": false
                                      },
                                      "Minutes": {
                                          "text": "Minutes",
                                          "color": "#BBFFBB",
                                          "show": false
                                      },
                                      "Seconds": {
                                          "text": "Expires in",
                                          "color": "#FFA834",
                                          "show": true
                                      }
                                  }
                              }).addListener(function(unit, value, total){
                                  if(total == -1){
                                      timer.TimeCircles().stop();
                                      $interval.cancel(vm.fetchBuyRates);
                                      vm.fetchBuyRates = null;
                                      if(vm.step1.currency_to_buy != vm.step1.currency_to_sell){
                                          $mdToast.show({
                                              template: '<md-toast class="md-toast error">Quote timeout.</md-toast>',
                                              hideDelay: 4000,
                                              position: 'bottom right'
                                          });
                                          vm.temp_rate = [];
                                          vm.step1_disabled = false;
                                          vm.circleLoader = true;
                                          vm.disableQuote = false;
                                          vm.hideQuoteScreen = false;
                                          vm.acceptQuoteDisable = false;
                                          vm.declineQuoteDisable = false;
                                          vm.previousStep();
                                      }
                                  }else{
                                      var text_box = timer.find('.textDiv_Seconds');
                                      text_box.find('.title').remove();
                                      var new_value = value + 'Secs';
                                      text_box.append('<span class="title">'+new_value+'</span>');
                                  }
                              });
                              vm.fetchBuyRates = $interval(function(){
                                  vm.rate = vm.temp_rate;
                                  vm.GetRateByIDS(vm.step1.currency_to_buy, vm.parseFloating(vm.step1.amount), vm.step1.currency_to_sell, 0, new Date(Date.parse(vm.step1.value_date)), vm.entity.entity.name).then(function(data){
                                      vm.temp_rate = data[0];
                                  });
                              }, 2000);
                              vm.nextStep();
                          });
                      }else{
                          vm.GetRateByIDS(vm.step1.currency_to_buy, 0, vm.step1.currency_to_sell, amount, value_date, vm.entity.entity.name).then(function(data){
                              vm.temp_rate = data[0];
                              vm.rate = vm.temp_rate;
                              if(data[0].exchange_rate == 0){
                                  $mdToast.show({
                                      template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
                                      hideDelay: 2000,
                                      position: 'bottom right'
                                  });
                                  vm.temp_rate = [];
                                  vm.step1_disabled = false;
                                  vm.circleLoader = true;
                                  vm.disableQuote = false;
                                  return false;
                              }

                              //launch circular and linear timer.
                              var circular_timer = angular.element('#circular-timer-box');
                              circular_timer.empty();
                              circular_timer.append('<div id="circular-timer" data-timer="60"></div>');
                              var timer = circular_timer.find('#circular-timer');
                              timer.TimeCircles({
                                  "animation": "smooth",
                                  "bg_width": 1,
                                  "fg_width": 0.21,
                                  "circle_bg_color": "#AAAAAA",
                                  "direction":'Counter-clockwise',
                                  "start_angle": 160,
                                  "time": {
                                      "Days": {
                                          "text": "Days",
                                          "color": "#FFCC66",
                                          "show": false
                                      },
                                      "Hours": {
                                          "text": "Hours",
                                          "color": "#99CCFF",
                                          "show": false
                                      },
                                      "Minutes": {
                                          "text": "Minutes",
                                          "color": "#BBFFBB",
                                          "show": false
                                      },
                                      "Seconds": {
                                          "text": "Expires in",
                                          "color": "#FFA834",
                                          "show": true
                                      }
                                  }
                              }).addListener(function(unit, value, total){
                                  if(total == -1){
                                      timer.TimeCircles().stop();
                                      $interval.cancel(vm.fetchSellRates);
                                      vm.fetchSellRates = null;
                                      if(vm.step1.currency_to_buy != vm.step1.currency_to_sell){
                                          $mdToast.show({
                                              template: '<md-toast class="md-toast error">Quote timeout.</md-toast>',
                                              hideDelay: 4000,
                                              position: 'bottom right'
                                          });
                                          vm.temp_rate = [];
                                          vm.step1_disabled = false;
                                          vm.circleLoader = true;
                                          vm.disableQuote = false;
                                          vm.hideQuoteScreen = false;
                                          vm.acceptQuoteDisable = false;
                                          vm.declineQuoteDisable = false;
                                          vm.previousStep();
                                      }
                                  }else{
                                      var text_box = timer.find('.textDiv_Seconds');
                                      text_box.find('.title').remove();
                                      var new_value = value + 'Secs';
                                      text_box.append('<span class="title">'+new_value+'</span>');
                                  }
                              });
                              vm.fetchSellRates = $interval(function(){
                                  vm.rate = vm.temp_rate;
                                  vm.GetRateByIDS(vm.step1.currency_to_buy, 0, vm.step1.currency_to_sell, vm.parseFloating(vm.step1.amount), new Date(Date.parse(vm.step1.value_date)), vm.entity.entity.name).then(function(data){
                                      vm.temp_rate = data[0];
                                  });
                              }, 2000);
                              vm.nextStep();
                          });
                      }
                  }
              }else{
                  $mdToast.show({
                      template: '<md-toast class="md-toast error">'+data.error+'</md-toast>',
                      hideDelay: 4000,
                      position: 'bottom right'
                  });
                  vm.temp_rate = [];
                  vm.step1_disabled = false;
                  vm.circleLoader = true;
                  vm.disableQuote = false;
                  vm.hideQuoteScreen = false;
                  vm.acceptQuoteDisable = false;
                  vm.declineQuoteDisable = false;
                  return false;
              }
          });
      }else{
        vm.step1_disabled = false;
        vm.circleLoader = true;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please fill required fields.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
      }
    };
    vm.add_quote = function(){
      vm.step1_disabled = true;
      if(vm.step1.currency_to_buy != null && vm.step1.currency_to_sell != null && vm.step1.amount != null && vm.step1.value_date != null && vm.step1.value_date != ''){
        vm.showCircleLoader = false;
        vm.disableQuote = true;
        if(vm.step1.currency_to_buy == vm.step1.currency_to_sell){
          if(vm.step1.i_want_to == 'buy'){
              vm.GetRate(vm.getFundingCurrencyName(vm.step1.currency_to_buy), vm.step1.amount, vm.getPaymentCurrencyName(vm.step1.currency_to_sell), 0, new Date(Date.parse(vm.step1.value_date)), vm.entity.entity.name).then(function(data){
                vm.temp_rate = data[0];
                if(vm.parseFloat(vm.rate.exchange_rate) == 0){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
                    hideDelay: 2000,
                    position: 'bottom right'
                  });
                  vm.step1_disabled = false;
                  vm.showCircleLoader = true;
                  vm.disableQuote = false;
                  return;
                }
                vm.showCircleLoader = true;
                vm.disableQuote = false;
                vm.hideQuoteScreen = true;
                vm.nextStep();
              });
            }else{
              vm.GetRate( vm.getFundingCurrencyName(vm.step1.currency_to_buy), 0,  vm.getPaymentCurrencyName(vm.step1.currency_to_sell), vm.parseFloating(vm.step1.amount), new Date(Date.parse(vm.step1.value_date)), vm.entity.entity.name).then(function(data){
                vm.temp_rate = data[0];
                if(vm.parseFloat(vm.rate.exchange_rate) == 0){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
                    hideDelay: 2000,
                    position: 'bottom right'
                  });
                  vm.step1_disabled = false;
                  vm.showCircleLoader = true;
                  vm.disableQuote = false;
                  return;
                }
                vm.showCircleLoader = true;
                vm.disableQuote = false;
                vm.hideQuoteScreen = true;
                vm.nextStep();
              });
            }
        }else{
          if(vm.step1.i_want_to == 'buy'){
            //send buying amount and set selling 0
            vm.GetRate(vm.getFundingCurrencyName(vm.step1.currency_to_buy), vm.step1.amount, vm.getPaymentCurrencyName(vm.step1.currency_to_sell), 0, new Date(Date.parse(vm.step1.value_date)), vm.entity.entity.name).then(function(data){
              vm.rate = data[0];
              if(vm.parseFloat(vm.rate.exchange_rate) == 0){
                $mdToast.show({
                  template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
                  hideDelay: 2000,
                  position: 'bottom right'
                });
                vm.step1_disabled = false;
                vm.showCircleLoader = true;
                vm.disableQuote = false;
                return;
              }
              vm.temp_rate = vm.rate;
              vm.showCircleLoader = true;

              //for step2
              var circular_timer = angular.element('#circular-timer-box');
              circular_timer.empty();
              circular_timer.append('<div id="circular-timer" data-timer="60"></div>');
              var timer = circular_timer.find('#circular-timer');
              timer.TimeCircles({
                "animation": "smooth",
                "bg_width": 1,
                "fg_width": 0.21,
                "circle_bg_color": "#AAAAAA",
                "direction":'Counter-clockwise',
                "start_angle": 160,
                "time": {
                  "Days": {
                    "text": "Days",
                    "color": "#FFCC66",
                    "show": false
                  },
                  "Hours": {
                    "text": "Hours",
                    "color": "#99CCFF",
                    "show": false
                  },
                  "Minutes": {
                    "text": "Minutes",
                    "color": "#BBFFBB",
                    "show": false
                  },
                  "Seconds": {
                    "text": "Expires in",
                    "color": "#FFA834",
                    "show": true
                  }
                }
              }).addListener(function(unit, value, total){
                if( total == 0){
                  vm.quote = [];
                  vm.quote_copy = [];
                  timer.TimeCircles().stop();
                  $interval.cancel(vm.interval_func);
                  vm.interval_func = null;
                  if(vm.step1.currency_to_buy != vm.step1.currency_to_sell){
                    $mdToast.show({
                      template: '<md-toast class="md-toast error">Quote timeout.</md-toast>',
                      hideDelay: 4000,
                      position: 'bottom right'
                    });
                    vm.step1_disabled = false;
                    vm.previousStep();
                  }
                }else{
                  if(unit == 'Seconds'){
                    var text_box = timer.find('.textDiv_Seconds');
                    text_box.find('.title').remove();
                    var new_value = value + 'Secs';
                    text_box.append('<span class="title">'+new_value+'</span>');
                  }
                }
              });
              vm.timer();
              vm.disableQuote = false;
              vm.nextStep();

              vm.rateInternval = $interval(function(){
                vm.GetRate(vm.getFundingCurrencyName(vm.step1.currency_to_buy), vm.step1.amount, vm.getPaymentCurrencyName(vm.step1.currency_to_sell), 0, new Date(Date.parse(vm.step1.value_date)), vm.entity.entity.name).then(function(data){
                  vm.temp_rate = data[0];
                });
              }, 2000);
            });
          }else{
            //send selling amount and set buying 0
            vm.GetRate( vm.getFundingCurrencyName(vm.step1.currency_to_buy), 0,  vm.getPaymentCurrencyName(vm.step1.currency_to_sell), vm.parseFloating(vm.step1.amount), new Date(Date.parse(vm.step1.value_date)), vm.entity.entity.name).then(function(data){
              vm.rate = data[0];
              if(vm.parseFloat(vm.rate.exchange_rate) == 0){
                $mdToast.show({
                  template: '<md-toast class="md-toast error">Rate not found.</md-toast>',
                  hideDelay: 2000,
                  position: 'bottom right'
                });
                vm.step1_disabled = false;
                vm.showCircleLoader = true;
                vm.disableQuote = false;
                return;
              }
              vm.temp_rate = vm.rate;
              vm.showCircleLoader = true;
              //for step2
              var circular_timer = angular.element('#circular-timer-box');
              circular_timer.empty();
              circular_timer.append('<div id="circular-timer" data-timer="60"></div>');
              var timer = circular_timer.find('#circular-timer');
              timer.TimeCircles({
                "animation": "smooth",
                "bg_width": 1,
                "fg_width": 0.21,
                "circle_bg_color": "#AAAAAA",
                "direction":'Counter-clockwise',
                "start_angle": 160,
                "time": {
                  "Days": {
                    "text": "Days",
                    "color": "#FFCC66",
                    "show": false
                  },
                  "Hours": {
                    "text": "Hours",
                    "color": "#99CCFF",
                    "show": false
                  },
                  "Minutes": {
                    "text": "Minutes",
                    "color": "#BBFFBB",
                    "show": false
                  },
                  "Seconds": {
                    "text": "Expires in",
                    "color": "#FFA834",
                    "show": true
                  }
                }
              }).addListener(function(unit, value, total){
                if( total == 0){
                  vm.quote = [];
                  vm.quote_copy = [];
                  timer.TimeCircles().stop();
                  $interval.cancel(vm.interval_func);
                  vm.interval_func = null;
                  if(vm.step1.currency_to_buy != vm.step1.currency_to_sell){
                    $mdToast.show({
                      template: '<md-toast class="md-toast error">Quote timeout.</md-toast>',
                      hideDelay: 4000,
                      position: 'bottom right'
                    });
                    vm.step1_disabled = false;
                    vm.previousStep();
                  }
                }else{
                  if(unit == 'Seconds'){
                    var text_box = timer.find('.textDiv_Seconds');
                    text_box.find('.title').remove();
                    var new_value = value + 'Secs';
                    text_box.append('<span class="title">'+new_value+'</span>');
                  }
                }
              });
              vm.timer();
              vm.disableQuote = false;
              vm.nextStep();

              vm.rateInternval = $interval(function(){
                vm.GetRate(vm.getFundingCurrencyName(vm.step1.currency_to_buy), 0, vm.getPaymentCurrencyName(vm.step1.currency_to_sell), vm.step1.amount, new Date(Date.parse(vm.step1.value_date)), vm.entity.entity.name).then(function(data){
                  vm.temp_rate = data[0];
                });
              }, 2000);
            });
          }
        }
      }else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please fill required fields.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
      }
    };
    vm.acceptQuoteDisable = false;
    //step 2
    vm.AcceptQuoteFunc = function(){
      vm.showCircleLoader = false;
      vm.acceptQuoteDisable = true;
      vm.declineQuoteDisable = true;

      var circular_timer = angular.element('#circular-timer-box #circular-timer');
      circular_timer.TimeCircles().stop();
      $interval.cancel(vm.fetchSellRates);
      vm.fetchSellRates = null;

      $interval.cancel(vm.fetchBuyRates);
      vm.fetchBuyRates = null;

      var client = vm.entity.entity.name;
      var trader = vm.entityWebDefaults.trader_id;
      var notes = 'test';
      var internal_remarks = 'internal remarks';
      var value_date = vm.step1.value_date == '' ? new Date() : new Date(Date.parse(vm.step1.value_date));
      value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
      var is_bach = 12277;
      var deal_transaction = '';

      var buy_currency = _.filter(vm.paymentCurrencies, {currency_id: vm.step1.currency_to_buy});
      var sell_currency = _.filter(vm.fundingCurrencies, {currency_id: vm.step1.currency_to_sell});

      var buy_currency_id = buy_currency[0].currency_id;
      var sell_currency_id = sell_currency[0].currency_id;

      var buy_amount = vm.parseFloating(vm.rate.buy_amount);
      var sell_currency = sell_currency.currency_name;
      var sell_amount = vm.parseFloating(vm.rate.sell_amount);
      if(vm.step1.currency_to_buy == vm.step1.currency_to_sell){
        deal_transaction += '<DEAL_TRANSACTION>';
        deal_transaction += '<BUY_CURRENCY><ID>'+sell_currency_id+'</ID></BUY_CURRENCY>';
        deal_transaction += '<BUY_AMOUNT>'+sell_amount+'</BUY_AMOUNT>';
        deal_transaction += '<EXCHANGE_RATE>1</EXCHANGE_RATE>';
        deal_transaction += '<ACTUAL_MARKET_RATE>1</ACTUAL_MARKET_RATE>';
        deal_transaction += '<MARKET_RATE>1</MARKET_RATE>';
        deal_transaction += '<IS_EQUIVALENT>0</IS_EQUIVALENT>';
        deal_transaction += '<SELL_CURRENCY><ID>'+buy_currency_id+'</ID></SELL_CURRENCY>';
        deal_transaction += '<SELL_AMOUNT>'+buy_amount+'</SELL_AMOUNT>';
        deal_transaction += '</DEAL_TRANSACTION>';
      }else{
        deal_transaction += '<DEAL_TRANSACTION>';
        deal_transaction += '<BUY_CURRENCY><ID>'+sell_currency_id+'</ID></BUY_CURRENCY>';
        deal_transaction += '<BUY_AMOUNT>'+sell_amount+'</BUY_AMOUNT>';
        deal_transaction += '<EXCHANGE_RATE>'+vm.temp_rate.exchange_rate+'</EXCHANGE_RATE>';
        deal_transaction += '<ACTUAL_MARKET_RATE>'+vm.temp_rate.actual_market_rate+'</ACTUAL_MARKET_RATE>';
        deal_transaction += '<MARKET_RATE>'+vm.temp_rate.market_rate+'</MARKET_RATE>';
        deal_transaction += '<IS_EQUIVALENT>0</IS_EQUIVALENT>';
        deal_transaction += '<SELL_CURRENCY><ID>'+buy_currency_id+'</ID></SELL_CURRENCY>';
        deal_transaction += '<SELL_AMOUNT>'+buy_amount+'</SELL_AMOUNT>';
        deal_transaction += '</DEAL_TRANSACTION>';
      }
      if(buy_amount > 0 && sell_amount > 0){
        if(!_.isEmpty(vm.deal)){
          vm.updateWebDeal(vm.deal.id, value_date, deal_transaction).then(function(data){
            if(data[0].status == 'FALSE'){
              $mdToast.show({
                template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                hideDelay: 4000,
                position: 'bottom right'
              });
              var circular_timer = angular.element('#circular-timer-box #circular-timer');
              circular_timer.TimeCircles().stop();
              $interval.cancel(vm.fetchRates);
              vm.fetchRates = null;
              vm.step1_disabled = false;
              vm.circleLoader = true;
              vm.disableQuote = false;
              vm.hideQuoteScreen = false;
              vm.acceptQuoteDisable = false;
              vm.declineQuoteDisable = false;
              vm.previousStep();
            }else{
              vm.deal = data[0];
              vm.PostWebDeal(vm.deal).then(function(data){
                vm.post_deal = data[0];
                if(data[0].status == 'TRUE'){
                  vm.postDeal = data[0];
                  vm.temp_deal = vm.deal;
                  vm.balance.copy_amount = vm.rate.buy_amount;
                  vm.step1.amount = vm.rate.buy_amount;
                  angular.copy(vm.step1, vm.quote);
                  angular.copy(vm.step1, vm.quote_copy);
                  vm.step3.amount = vm.rate.buy_amount;
                  $mdToast.show({
                    template: '<md-toast class="md-toast success">Deal Saved and Posted</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  //now get payees
                  var currency_object = _.filter(vm.paymentCurrencies, {currency_id: vm.step1.currency_to_buy});
                  var currency_id = currency_object[0].currency_id;
                  vm.GetSettlementsByMethodsAndCurrencyInBookASpot(vm.payee_methods_joined, currency_id).then(function(data){
                    debugger
                    vm.payeesList = data;
                    //set default payees, currency, method
                    var payment_currency = _.filter(vm.paymentCurrencies, {currency_id: vm.step1.currency_to_buy});
                    var payment_currency_id = payment_currency[0].currency_id;
                    if(payment_currency_id == vm.entityWebDefaults.payment_cur_id){
                      var default_payee = _.filter(vm.payeesList, {id: vm.entityWebDefaults.payment_template_id});
                      if(default_payee.length > 0){
                        vm.step3.payee_ = default_payee[0].name;
                        vm.step3.payee_id = default_payee[0].id;
                        vm.showCircleLoader = true;
                        var circular_timer = angular.element('#circular-timer-box #circular-timer');
                        circular_timer.TimeCircles().stop();
                        $interval.cancel(vm.fetchRates);
                        vm.fetchRates = null;
                      }
                    }else{
                      vm.showCircleLoader = true;
                      var circular_timer = angular.element('#circular-timer-box #circular-timer');
                      circular_timer.TimeCircles().stop();
                      $interval.cancel(vm.fetchRates);
                      vm.fetchRates = null;
                    }
                  });
                  vm.nextStep();
                  vm.showCircleLoader = true;
                }else{
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                    hideDelay: 6000,
                    position: 'bottom right'
                  });

                  // vm.showCircleLoader = true;
                  // vm.step1_disabled = false;
                  // vm.acceptQuoteDisable = false;
                  // vm.declineQuoteDisable = false;
                  vm.step1_disabled = false;
                  vm.circleLoader = true;
                  vm.disableQuote = false;
                  vm.hideQuoteScreen = false;
                  vm.acceptQuoteDisable = false;
                  vm.declineQuoteDisable = false;
                  vm.previousStep();
                }
              });
            }
          });
        }else{
          vm.CreateWebDeal(client, trader, notes, internal_remarks, value_date, is_bach, deal_transaction).then(function(data){
            if(data[0].status == 'FALSE'){
              $mdToast.show({
                template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                hideDelay: 4000,
                position: 'bottom right'
              });
              var circular_timer = angular.element('#circular-timer-box #circular-timer');
              circular_timer.TimeCircles().stop();
              $interval.cancel(vm.fetchRates);
              vm.fetchRates = null;
              vm.acceptQuoteDisable = false;
              vm.declineQuoteDisable = false;
              vm.step1_disabled = false;
              vm.showCircleLoader = true;
              vm.previousStep();
            }else{
              vm.deal = data[0];
              vm.PostWebDeal(vm.deal).then(function(data){
                vm.post_deal = data[0];
                if(data[0].status == 'TRUE'){
                  vm.postDeal = data[0];
                  vm.temp_deal = vm.deal;
                  vm.balance.copy_amount = vm.rate.buy_amount;
                  vm.step1.amount = vm.rate.buy_amount;
                  angular.copy(vm.step1, vm.quote);
                  angular.copy(vm.step1, vm.quote_copy);
                  vm.step3.amount = vm.rate.buy_amount;
                  $mdToast.show({
                    template: '<md-toast class="md-toast success">Deal Saved and Posted</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  //now get payees
                  var currency_object = _.filter(vm.paymentCurrencies, {currency_id: vm.step1.currency_to_buy});
                  var currency_id = currency_object[0].currency_id;
                  vm.GetSettlementsByMethodsAndCurrencyInBookASpot(vm.payee_methods_joined, currency_id).then(function(data){
                    debugger
                    vm.payeesList = data;
                    //set default payees, currency, method
                    var payment_currency = _.filter(vm.paymentCurrencies, {currency_id: vm.step1.currency_to_buy});
                    var payment_currency_id = payment_currency[0].currency_id;
                    if(payment_currency_id == vm.entityWebDefaults.payment_cur_id){
                      var default_payee = _.filter(vm.payeesList, {id: vm.entityWebDefaults.payment_template_id});
                      if(default_payee.length > 0){
                        vm.step3.payee_ = default_payee[0].name;
                        vm.step3.payee_id = default_payee[0].id;
                        vm.showCircleLoader = true;
                        var circular_timer = angular.element('#circular-timer-box #circular-timer');
                        circular_timer.TimeCircles().stop();
                        $interval.cancel(vm.fetchRates);
                        vm.fetchRates = null;
                      }
                    }else{
                      vm.showCircleLoader = true;
                      var circular_timer = angular.element('#circular-timer-box #circular-timer');
                      circular_timer.TimeCircles().stop();
                      $interval.cancel(vm.fetchRates);
                      vm.fetchRates = null;
                    }
                  });
                  vm.nextStep();
                  vm.showCircleLoader = true;
                }else{
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                    hideDelay: 6000,
                    position: 'bottom right'
                  });

                  // vm.showCircleLoader = true;
                  // vm.step1_disabled = false;
                  // vm.acceptQuoteDisable = false;
                  // vm.declineQuoteDisable = false;

                  vm.step1_disabled = false;
                  vm.circleLoader = true;
                  vm.disableQuote = false;
                  vm.hideQuoteScreen = false;
                  vm.acceptQuoteDisable = false;
                  vm.declineQuoteDisable = false;
                  vm.previousStep();
                }
              });
            }
          });
        }
      }else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">Amounts cannot be zero.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        vm.temp_rate = [];
        vm.step1_disabled = false;
        vm.circleLoader = true;
        vm.disableQuote = false;
        vm.hideQuoteScreen = false;
        vm.acceptQuoteDisable = false;
        vm.declineQuoteDisable = false;
        vm.previousStep();
      }
    };
    vm.declineQuoteDisable = false;
    vm.DeclineQuoteFunc = function(){
      var circular_timer = angular.element('#circular-timer-box');
      var timer = circular_timer.find('#circular-timer');
      timer.TimeCircles().stop();

      $interval.cancel(vm.fetchSellRates);
      vm.fetchSellRates = null;

      $interval.cancel(vm.fetchBuyRates);
      vm.fetchBuyRates = null;

      vm.temp_rate = [];
      vm.step1_disabled = false;
      vm.circleLoader = true;
      vm.disableQuote = false;
      vm.hideQuoteScreen = false;
      vm.acceptQuoteDisable = false;
      vm.declineQuoteDisable = false;
      $mdToast.show({
        template: '<md-toast class="md-toast error">Quote Declined.</md-toast>',
        hideDelay: 4000,
        position: 'bottom right'
      });
      vm.previousStep();
    };
    //step 3
    vm.preventUserInput = function(event){
      console.log(event.key);
    };
    vm.UpdatePayeeBalance = function(event){
      var amount = parseFloat( (vm.step3.amount !== undefined) && vm.step3.amount.split(',').join('') || 0 );
      var actual_amount = parseFloat( vm.quote_copy.amount.split(',').join('') );
      var balanceAccount = +vm.balance.copy_amount;
      if( amount <= balanceAccount && amount > 0 ){
        vm.balance.amount = balanceAccount;
        if( isNaN(amount) == false){
          var temp_amount = balanceAccount - amount;
          vm.balance.amount = temp_amount;
        }
      }
      else if(amount > balanceAccount ){
        vm.balance.amount = 0;

      }
      else{

        vm.balance.amount = balanceAccount;
      }
      /*var input_amount = parseFloat(vm.step3.amount.replace(/,/g, ''));
      var actual_amount = parseFloat(vm.quote_copy.amount.replace(/,/g, ''));
      if(vm.balance.copy_amount != null)
      var balance_copy_amount = parseFloat(vm.balance.copy_amount);
      if( !isNaN(input_amount) ){
        if(vm.balance.copy_amount == null){
          var amount = actual_amount - input_amount;
          //console.log(typeof input_amount);
          if(vm.step3.amount == '')
            vm.balance.amount = vm.quote_copy.amount;
          else
            vm.balance.amount = amount;
        }else{
          var amount = balance_copy_amount - input_amount;
          //console.log(typeof input_amount);
          if(vm.step3.amount == '')
            vm.balance.amount = vm.quote_copy.amount;
          else
            vm.balance.amount = amount;
        }
      }*/
    };
    vm.LimitPayeeAmountTotal = function(event){
      var amount_input = event.target.value.split(',').join('');
      var amount = vm.balance.copy_amount.split(',').join('');
      if(amount_input.length == parseFloat(amount).toFixed(2).length || amount_input.length > parseFloat(amount).toFixed(2).length){
        event.preventDefault();
        return false;
      }
    };
    vm.LimitPayeeAmountInt = function(event){
      var amount_input = event.target.value.split(',').join('');
//      console.log(typeof vm.balance.copy_amount);
      var amount = typeof vm.balance.copy_amount == 'string' ? vm.balance.copy_amount.split(',').join('') : vm.balance.copy_amount;
      if(amount_input == '-'){
        event.preventDefault();
        return false;
      }
      if( !isNaN(parseFloat(amount_input)) ){
        if(parseFloat(amount_input) < 0){
          event.target.value = $filter('currency')(0, '');
        }
      }
      if(parseInt(amount_input) > parseInt(amount)){
        event.target.value = $filter('currency')(amount, '');
      }
    };
    vm.SendPayeesAndDisable = false;
    vm.addPayee = function(){
      vm.showCircleLoader = false;
      vm.SendPayeesAndDisable = true;
      var payee = {};
      if(vm.step3.value_date == null || vm.step3.value_date == ''){
          $mdToast.show({
              template: '<md-toast class="md-toast error">Value date is mandatory. Please check Payee detail.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
          });
          vm.showCircleLoader = true;
          vm.SendPayeesAndDisable = false;
          return false;
        }else{
        var value_date = new Date(Date.parse(vm.step3.value_date));
        vm.ValidateBusinessDate(value_date).then(function(data){
          data = data[0];
          if(data.status == 'TRUE'){
            if(vm.step3.payee_ != null && vm.parseFloat(vm.step3.amount) > 0){
              angular.copy(vm.step3, payee);
              if(vm.popMandate == true && payee.payment_purpose == null){
                $mdToast.show({
                  template: '<md-toast class="md-toast error">Purpose of payment is mandatory. Please check details.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                vm.showCircleLoader = true;
                vm.SendPayeesAndDisable = false;
                return false;
              }

              if(payee.payment_notification_email != '' && payee.payment_notification_email != null){
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if(payee.payment_notification_email.match(re) == null){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Email is invalid.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  vm.showCircleLoader = true;
                  vm.SendPayeesAndDisable = false;
                  return false;
                }
              }
              //$scope.showSinglePayeeTable = false;
              var payee_object = _.filter(vm.payeesList, {id: payee.payee_id});
              vm.GetFeeByMethodByID(payee_object[0].paymentmethod).then(function(data){
                if( data.error.length > 0 ){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Payee cannot be selected, because payment method: '+data.error[0].error+'</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  vm.showCircleLoader = true;
                  vm.SendPayeesAndDisable = false;
                  return false;
                }else{
                  var id = vm.payees.length + 1;
                  payee.current_id = id;
                  payee = vm.mergeObjects(payee, payee_object[0]);
                  payee.value_date = vm.step3.value_date;
                  if(payee_object[0].currency_id == vm.step1.currency_to_buy){
                    //if we have same currencies
                    if(data.same_currency_fee.length > 0){
                      if(data.same_currency_fee[0].currency_id == payee_object[0].currency_id){
                        payee.fee = vm.parseFloating(data.same_currency_fee[0].instrument_cost);
                      }else{
                        payee.fee = vm.parseFloating(data.fee[0].non_instrument_cost);
                      }
                    }else{
                      payee.fee = vm.parseFloating(data.fee[0].non_instrument_cost);
                    }
                  }else{
                    //else we have different currencies
                    if(data.instrument_fee.length > 0){
                      var found = false;
                      var instrument_fee = {};
                      for(var i=0; i<data.instrument_fee.length; i++){
                        if(data.instrument_fee[i].currency_id == 2){
                          found = true;
                          instrument_fee = data.instrument_fee[i];
                        }
                      }
                      if(found){
                        payee.fee = vm.parseFloating(instrument_fee.instrument_cost);
                      }else{
                        payee.fee = vm.parseFloating(data.fee[0].instrument_cost);
                      }
                    }else{
                      payee.fee = vm.parseFloating(data.fee[0].instrument_cost);
                    }
                  }
                  if(typeof payee.amount !== 'number')
                    payee.amount = vm.parseFloat(payee.amount.split(',').join(''));
                  else
                    payee.amount = vm.parseFloat(payee.amount);
                  vm.payees.push(payee);
                  if(vm.balance.amount > 0){
                    vm.step3.amount = vm.balance.amount;
                    vm.balance.copy_amount = vm.balance.amount;
                    vm.balance.amount = 0;
                    vm.showCircleLoader = true;
                    vm.SendPayeesAndDisable = false;
                  }else{
                    vm.balance.copy_amount = 0;
                    vm.step3.amount = null;
                    vm.step4.amount = vm.rate.sell_amount;
                    vm.funding_balance.copy_amount = vm.rate.sell_amount;
                    //send settlements
                    if(vm.payees.length > 0){
                      $mdToast.show({
                        template: '<md-toast class="md-toast info">Adding Payees..</md-toast>',
                        hideDelay: 1000,
                        position: 'bottom right'
                      });
                      var step1_value_date = vm.step1.value_date == '' ? new Date() : new Date(Date.parse(vm.step1.value_date));
                      step1_value_date = step1_value_date.getFullYear() + '-' + ('0' + (step1_value_date.getMonth()+1)).slice(-2) + '-' + ('0' + step1_value_date.getDate()).slice(-2);
                       //Buy amount may sell amount pass kio kir rhay hou
                      var settlements = '<DEAL>' +
                          '<MODULE_ID>12277</MODULE_ID>' +
                          '<VALUE_DATE>'+step1_value_date+'</VALUE_DATE>' +
                          '<BUY_CURRENCY><ID>'+vm.step1.currency_to_sell+'</ID></BUY_CURRENCY>' +
                          '<BUY_AMOUNT>'+vm.rate.sell_amount+'</BUY_AMOUNT>' +
                          '</DEAL>';

                      vm.showCircleLoader = false;
                      for(var i=0; i<vm.payees.length; i++) {
                        if(typeof vm.payees[i].value_date == 'string' && vm.payees[i].value_date != null){
                          var value_date = new Date(Date.parse(vm.payees[i].value_date));
                        }else if(typeof vm.payees[i].value_date == 'object'){
                          var value_date = vm.payees[i].value_date;
                        }else if(typeof vm.payees[i].value_date == 'undefined'){
                          var value_date = new Date(Date.parse(vm.step1.value_date));
                        }
                        value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                        if(vm.payees[i].payment_purpose != null){
                          var purpose_object = _.filter(vm.paymentPurposes, {id: vm.payees[i].payment_purpose});
                          var purpose_of_payment = purpose_object[0].id;
                        }else{
                          var purpose_of_payment = 1;
                        }
                        var payment_reference = vm.payees[i].payment_reference == null ? '' : vm.payees[i].payment_reference;
                        var internal_payment_reference = vm.payees[i].internal_payment_reference == null ? '' : vm.payees[i].internal_payment_reference;
                        settlements += '<SETTLEMENTS>';
                        settlements += '<ACTION><ID>'+vm.payees[i].paymentmethod+'</ID></ACTION>';
                        settlements += '<SELL_CURRENCY><Value>'+vm.payees[i].currency_name+'</Value></SELL_CURRENCY>';
                        settlements += '<SELL_AMOUNT>'+vm.payees[i].amount+'</SELL_AMOUNT>';
                        settlements += '<RATE>'+vm.rate.exchange_rate+'</RATE>';
                        settlements += '</SETTLEMENTS>';
                      }
                      vm.GetFeeForSettlements(settlements).then(function(data){
                        var payment = data.shift();
                        vm.rate.sell_amount = payment.buy_amount;
                        vm.step4.amount = payment.buy_amount;
                        vm.funding_balance.copy_amount = payment.buy_amount;
                        data = data;
                        vm.showCircleLoader = false;
                        settlements = '';
                        for(var i=0; i<data.length; i++) {
                          vm.payees[i].total = data[i].sell_amount;
                          if(typeof vm.payees[i].value_date == 'string' && vm.payees[i].value_date != null){
                            var value_date = new Date(Date.parse(vm.payees[i].value_date));
                          }else if(typeof vm.payees[i].value_date == 'object'){
                            var value_date = vm.payees[i].value_date;
                          }else if(typeof vm.payees[i].value_date == 'undefined'){
                            var value_date = new Date(Date.parse(vm.step1.value_date));
                          }
                          value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                          if(vm.payees[i].payment_purpose != null){
                            var purpose_object = _.filter(vm.paymentPurposes, {id: vm.payees[i].payment_purpose});
                            var purpose_of_payment = purpose_object[0].id;
                          }else{
                            var purpose_of_payment = 1;
                          }
                          var payment_reference = vm.payees[i].payment_reference == null ? '' : vm.payees[i].payment_reference;
                          //var internal_payment_reference = payeesList[i].internal_payment_reference == null ? '' : payeesList[i].internal_payment_reference;
                          var uid = 'INREF-'+ vm.toUpperCase(Math.random().toString(16).slice(2));
                          var internal_payment_reference = vm.payees[i].internal_payment_reference == null ? uid : vm.payees[i].internal_payment_reference;
                          if(vm.payees[i].internal_payment_reference == null){
                            vm.payees[i].internal_payment_reference = uid;
                          }
                          settlements += '<SETTLEMENTS>';
                          settlements += '<ACTION><ID>'+vm.payees[i].paymentmethod+'</ID></ACTION>';
                          settlements += '<CURRENCY><Value>'+vm.payees[i].currency_name+'</Value></CURRENCY>';
                          settlements += '<AMOUNT>'+data[i].sell_amount+'</AMOUNT>';
                          settlements += '<VALUE_DATE>'+value_date+'</VALUE_DATE>';
                          if(vm.payees[i].paymentmethod == 5){
                            if(vm.payees[i].id != null)
                              settlements += '<ACCOUNT><ID>'+vm.payees[i].id+'</ID></ACCOUNT>';
                            settlements += '<ACT_TYPE>E</ACT_TYPE>';
                          }else{
                            if(vm.payees[i].id != null)
                              settlements += '<PAYEE><ID>'+vm.payees[i].id+'</ID></PAYEE>';
                          }
                          settlements += '<REFERENCE>'+payment_reference+'</REFERENCE>';
                          settlements += '<BENEFICIARY_REFERENCE>'+payment_reference+'</BENEFICIARY_REFERENCE>';
                          settlements += '<NOTIFY_MY_RECEIPIENT></NOTIFY_MY_RECEIPIENT>';
                          var length = vm.allElectronicMethods.length, isWire=false;
                          for(var j=0; j<length; j++){
                            if(vm.allElectronicMethods[j].id === vm.payees[i].paymentmethod){
                              isWire = true;
                              break;
                            }
                          }
                          if(isWire){
                            settlements += '<BENEFICIARY_REMARKS>'+internal_payment_reference+'</BENEFICIARY_REMARKS>';
                          }else{
                            settlements += '<BENEFICIARY_REMARKS></BENEFICIARY_REMARKS>';
                          }
                          settlements += '<REMITTENCE_REMARKS></REMITTENCE_REMARKS>';
                          settlements += '<PURPOSE_OF_PAYMENT>'+purpose_of_payment+'</PURPOSE_OF_PAYMENT>';
                          settlements += '<IS_EQUIVALENT>0</IS_EQUIVALENT>';
                          settlements += '<INTERNAL_REFERENCE>'+internal_payment_reference+'</INTERNAL_REFERENCE>';
                          settlements += '<FEE>'+data[i].fee+'</FEE>';
                          vm.payees[i].fee = data[i].fee;
                          settlements += '<FEE_TYPE><ID>'+data[i].fee_type+'</ID></FEE_TYPE>';
                          settlements += '<FEE_CURRENCY><ID>'+data[i].fee_currency_id+'</ID></FEE_CURRENCY>';
                          settlements += '</SETTLEMENTS>';
                        }
                        vm.SendSettlements(vm.deal.deal_number, step1_value_date, settlements).then(function(data){
                          if(data[0].status === 'FALSE'){
                            $mdToast.show({
                              template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                              hideDelay: 4000,
                              position: 'bottom right'
                            });
                            vm.showCircleLoader = true;
                            vm.SendPayeesAndDisable = false;
                            vm.step3.payee_ = null;
                            vm.step3.amount = 0;
                          }else{
                            $mdToast.show({
                              template: '<md-toast class="md-toast success">Payees Added. Deal Updated.</md-toast>',
                              hideDelay: 4000,
                              position: 'bottom right'
                            });

                            if(vm.step1.currency_to_sell == vm.entityWebDefaults.funding_cur_id){
                              var default_method = _.filter(vm.methods, {id: vm.entityWebDefaults.funding_method_id});
                              if(default_method.length > 0){
                                vm.step4.method = default_method[0].id;
                                vm.GetPaymentsByMethodAndCurrencyInBookASpot(default_method[0].id, vm.step1.currency_to_sell).then(function(data){
                                  if(data.length > 0){
                                    vm.accounts = data;
                                    if(vm.entityWebDefaults.funding_template_id != ''){
                                      var default_account = _.filter(vm.accounts, {id: vm.entityWebDefaults.funding_template_id});
                                      vm.step4.account_picker = default_account[0].id;
                                    }
                                    vm.nextStep();
                                    vm.showCircleLoader = true;
                                    vm.SendPayeesAndDisable = false;
                                  }else{
                                    vm.nextStep();
                                    vm.showCircleLoader = true;
                                    vm.SendPayeesAndDisable = false;
                                  }
                                });
                              }
                            }else{
                              vm.nextStep();
                              vm.showCircleLoader = true;
                              vm.SendPayeesAndDisable = false;
                            }
                          }
                        });
                      });
                    }
                  }
                  vm.balance.amount = 0;
                  vm.step3.payee_ = null;
                  // vm.step3.value_date = null;
                  vm.step3.internal_payment_reference = null;
                  vm.step3.no_notification = null;
                  vm.step3.payment_notification_email = null;
                  vm.step3.payment_purpose = null;
                  vm.step3.payment_reference = null;

                  vm.showCircleLoader = true;
                  vm.SendPayeesAndDisable = false;
                  return true;
                }
              });
            }else{
              $mdToast.show({
                template: '<md-toast class="md-toast error">Please fill all required fields.</md-toast>',
                hideDelay: 4000,
                position: 'bottom right'
              });
              return true;
            }
          }else{
            $mdToast.show({
              template: '<md-toast class="md-toast error">'+data.error+'</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
            vm.showCircleLoader = true;
            vm.SendPayeesAndDisable = false;
            return false;
          }
        });
      }

    };
    vm.addPayeeTotal = function(){
      if(vm.step3.payee_ != null && vm.step3.amount != null){
        if(vm.step3.payee_.length > 0){
          vm.showCircleLoader = false;
          vm.SendPayeesAndDisable = true;
          if(vm.addPayee() == true){
            vm.showCircleLoader = true;
            vm.SendPayeesAndDisable = false;
          }
        }
      }else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please fill all required fields.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
      }
    };

    vm.dontAllowMeInPayee = function(event){
      console.log(event.target.value);
    };
    vm.showPayeeTableFunc = function($event){
      vm.showPayeeTable = false;
    };
    vm.hidePayeeTableFunc = function(){
      vm.showPayeeTable = true;
      vm.payeeTable = {
        routing : null,
        payee_payor : null,
        bank_name : null,
        country : null
      };
    };
    vm.payeeSelectFunc = function(event){
      var element = event.target;
      if(element.nodeName.toLowerCase() == 'td'){
        var tr = angular.element(element).closest('tr');
        var payee = tr.find('td:nth-child(2)').text();
        vm.step3.payee_ = payee;
        vm.step3.payee_id = tr.attr('id');
        vm.showPayeeTable = true;
      }
    };
    vm.payeeSkip = function(){
      vm.balance.copy_amount = 0;
      vm.step3.amount = null;
      vm.step4.amount = vm.rate.sell_amount;
      vm.funding_balance.copy_amount = vm.rate.sell_amount;
      vm.nextStep();
    };
    vm.customFullscreen = true;
    vm.editPayee = function(data){
      //var currency_object = _.filter(vm.paymentCurrencies, {currency_name: vm.toUpperCase(vm.step1.currency_to_buy)});
      //var currency_id = currency_object[0].currency_id;
      vm.GetSettlementsByMethodsAndCurrencyInBookASpot(vm.payee_methods_joined, vm.step1.currency_to_buy).then(function(data){
        debugger
        vm.payeesList = data;
        vm.showCircleLoader = true;
      });
      var record = {};
      angular.copy(data, record);
      $mdDialog.show({
        locals:{dataToPass: record},
        fullscreen: vm.customFullscreen,
        clickOutsideToClose: true,
        scope: $scope,
        preserveScope: true,
        templateUrl: 'app/main/forexdealing/book_spot_deal/edit_payee.html',
        parent: angular.element(document.body),
        controller: function dialogController($scope, $mdDialog){
          $scope.closeDialog = function() {
            $mdDialog.hide();
            vm.payeeTable = {
              routing: null,
              payee_payor: null,
              bank_name: null,
              country: null
            };
          };
          $scope.info = {
            expanded: false
          };
          $scope.payeeTable = {
            routing: null,
            payee_payor: null,
            bank_name: null,
            country: null
          };
          $scope.showPayeeTable = true;
          $scope.isDisabled = true;
          $scope.showSinglePayeeTable = true;
          $scope.data = {
            expanded: false
          };
          $scope.routingFilter = function(payee){
            if(vm.payeeTable){
              if(
                (payee.internal_reference.toLowerCase().indexOf(vm.payeeTable.routing || '') !== -1)
              ){
                return true;
              }else{
                return false;
              }
            }else{
              return true;
            }
          };
          $scope.payeeFilter = function (payee) {
            if(vm.payeeTable){
              if(
                (payee.name.toLowerCase().indexOf(vm.payeeTable.payee_payor || '') !== -1)
              ){
                return true;
              }else{
                return false;
              }
            }else{
              return true;
            }
          };
          $scope.bankFilter = function (payee) {
            if(vm.payeeTable){
              if(
                (payee.beneficiarybank_name.toLowerCase().indexOf(vm.payeeTable.bank_name || '') !== -1)
              ){
                return true;
              }else{
                return false;
              }
            }else{
              return true;
            }
          };
          $scope.countryFilter = function (payee) {
            if(vm.payeeTable){
              if(
                (payee.country_name.toLowerCase().indexOf(vm.payeeTable.country || '') !== -1)
              ){
                return true;
              }else{
                return false;
              }
            }else{
              return true;
            }
          };
          $scope.payeeSelectFunc = function(event){
            debugger
            $scope.showSinglePayeeTable = false;
            var element = event.target;
            if(element.nodeName.toLowerCase() == 'td'){
              var tr = angular.element(element).closest('tr');
              var payee_id = tr.attr('id');
              var payee_object = _.filter(vm.payeesList, {id: payee_id});
              vm.GetFeeByMethodByID(payee_object[0].paymentmethod).then(function(data){
                $scope.showSinglePayeeTable = true;
                if(data.status === 'FALSE'){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">'+data.error+'</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  vm.showCircleLoader = true;
                  $scope.showPayeeTable = true;
                  $scope.single.payee_ = null;
                  $scope.single.fee = null;
                  return;
                }else{
                  if(payee_object.currency_id === vm.step1.currency_to_buy){
                    $scope.single.fee = vm.parseFloat(data.non_instrument_cost);
                  }else{
                    $scope.single.fee = vm.parseFloat(data.instrument_cost);
                  }
                  $scope.showPayeeTable = true;
                  $scope.single.payee_ = payee_object[0].name;
                  vm.singlePayeeLoader = true;
                }
              });
            }
          };
          $scope.hidePayeeTableFunc = function(){
            $scope.showPayeeTable = true;
            $scope.payeeTable = {
              routing: null,
              payee_payor: null,
              bank_name: null,
              country: null
            };
          };
          $scope.showPayeeTableFunc = function($event){
            $scope.showPayeeTable = false;
          };
          $scope.first_amount = record.amount;
          var value_date = record.value_date == '' ? new Date() : new Date(Date.parse(record.value_date));
          value_date = ('0' + (value_date.getMonth()+1)).slice(-2) + '/' + ('0' + value_date.getDate()).slice(-2) + '/' + value_date.getFullYear();

          $scope.single = {
            id: record.id || null,
            current_id: record.current_id || null,
            payee_: record.payee_ || null,
            amount: record.amount || null,
            payment_reference: record.payment_reference || null,
            payment_purpose: record.payment_purpose || null,
            internal_payment_reference: record.internal_payment_reference || null,
            payee_notification: record.payment_notification_email || null,
            fee: record.fee || null,
            value_date: value_date || null
          };
          $scope.dialogLoader = true;
          $scope.updatePayee = function(payee){
            if(vm.parseFloat(payee.amount) > 0 && payee.payee_ != null && (payee.value_date != null || payee.value_date != '')){
              $scope.dialogLoader = false;
              var payee_object = _.filter(vm.payeesList, {id: payee.id});
              if(typeof payee.amount != 'number')
                payee.amount = vm.parseFloat(payee.amount.split(',').join(''));
              else
                payee.amount = vm.parseFloat(payee.amount);
              if(vm.step3.amount == 0){
                var total = 0;
                for(var i=0; i<vm.payees.length; i++){
                  total += vm.payees[i].amount;
                }
                var supposed_total = (total - $scope.first_amount) + payee.amount;
                if(payee.amount > total){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Payee amount cannot be greater than total available amount.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  $scope.dialogLoader = true;
                  return;
                }
                if(supposed_total > total){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Payee amount cannot be greater than total available amount.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  $scope.dialogLoader = true;
                  return;
                }
              }
              vm.GetFeeByMethodByID(payee_object[0].paymentmethod).then(function(data){
                if(payee_object[0].currency_id === vm.step1.currency_to_buy){
                  payee.fee = vm.parseFloat(data.non_instrument_cost);
                }else{
                  payee.fee = vm.parseFloat(data.instrument_cost);
                }
                //var identifier = vm.payees.length + 1;
                payee = vm.mergeObjects(payee, payee_object[0]);
                //payee.current_id = identifier;
                vm.payees = _.without(vm.payees, _.findWhere(vm.payees, {id: payee.id}));
                vm.payees.push(payee);
                $scope.payeeTable.method = null;
                var total = 0;
                for(var i=0; i<vm.payees.length; i++){
                  total += vm.payees[i].amount;
                }
                var remaining_amount = vm.rate.buy_amount - total;
                vm.step3.amount = remaining_amount;
                vm.balance.copy_amount = vm.step3.amount;
                $mdToast.show({
                  template: '<md-toast class="md-toast success">Payee Record Updated.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                vm.SendPayeesAndDisable = false;
                vm.showCircleLoader = true;
                $scope.dialogLoader = true;
                $scope.closeDialog();
              });
            }else{
              $mdToast.show({
                template: '<md-toast class="md-toast error">Fill required fields.</md-toast>',
                hideDelay: 4000,
                position: 'bottom right'
              });
              return;
            }
          };
        }
      });
    };
    vm.deletePayee = function(payee){
      if(payee.amount > 0){
        vm.step3.amount = vm.step3.amount + payee.amount;
        vm.balance.copy_amount = vm.step3.amount + payee.amount;
        vm.balance.amount = 0;
        vm.payees = _.without(vm.payees, _.findWhere(vm.payees, {current_id: payee.current_id}));
        $mdToast.show({
          template: '<md-toast class="md-toast info">Payee record removed from the list.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
      }
    };

    //step4
    vm.UpdateFundingBalance = function(){
      // if(typeof vm.step4.amount)if

        var amount = parseFloat( (vm.step4.amount !== undefined) && vm.step4.amount.split(',').join('') || 0 );
        var actual_amount = parseFloat( vm.quote_copy.amount.split(',').join('') );
        var balanceAccount = +vm.funding_balance.copy_amount;
        if( amount <= balanceAccount && amount > 0 ){
          vm.funding_balance.amount = balanceAccount;
          if( isNaN(amount) == false){
            var temp_amount = balanceAccount - amount;
            vm.funding_balance.amount = temp_amount;
          }
        }
        else if(amount > balanceAccount  ){
          vm.funding_balance.amount = 0;
          vm.step4.amount = balanceAccount;
        }
        else{
          vm.funding_balance.amount = balanceAccount;
        }
      }
      // vm.step4.amount = ""




    // vm.UpdateFundingBalance = function(){
    //   var amount = parseFloat( vm.step4.amount.split(',').join('') );
    //   var actual_amount = parseFloat( vm.quote_copy.amount.split(',').join('') );
    //   if( amount <= actual_amount ){
    //     vm.funding_balance.amount = vm.funding_balance.copy_amount;
    //     if( isNaN(amount) == false){
    //       var temp_amount = vm.funding_balance.copy_amount - amount;
    //       vm.funding_balance.amount = temp_amount;
    //     }
    //   }else{
    //     vm.funding_balance.amount = 0;
    //   }
    // };
    vm.LimitAmountTotal = function(e){
      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message

        return false;
      }
      // var amount_input = event.target.value.split(',').join('');
      // if(typeof vm.funding_balance.copy_amount == 'string')
      //   var amount = vm.funding_balance.copy_amount.split(',').join('');
      // else
      //   var amount = vm.funding_balance.copy_amount;
      // if(amount_input.length == parseFloat(amount).toFixed(2).length || amount_input.length > parseFloat(amount).toFixed(2).length){
      //   event.preventDefault();
      //   return false;
      // }
    };
    vm.LimitFundingAmountInt = function(event){
      var amount_input = event.target.value.split(',').join('');
      if(typeof vm.funding_balance.copy_amount == 'string')
        var amount = vm.funding_balance.copy_amount.split(',').join('');
      else
        var amount = vm.funding_balance.copy_amount;
      if(amount_input == '-'){
        event.preventDefault();
        return false;
      }
      if( !isNaN(parseFloat(amount_input)) ){
        if(parseFloat(amount_input) < 0){
          event.target.value = $filter('currency')(0, '');
        }
      }
      if(parseInt(amount_input) > parseInt(amount)){
        event.target.value = $filter('currency')(amount, '');
      }
    };
    vm.SendFundingAndDisable = false;
    vm.addFundingRecord = function(funding){
      vm.SendFundingAndDisable = true;
      console.log('here1');
      var record = {};
      angular.copy(funding, record);
      if(record.method != null && vm.parseFloat(record.amount) > 0){
        vm.showCircleLoader = false;
        vm.SendFundingAndDisable = true;
        console.log('here2');
        if( record.account_picker != null && record.account_picker != ''){
          vm.GetFeeByMethodByID(record.method).then(function(data){
            console.log(data);
            var identifier = vm.fundings.length + 1;
            record.current_id = identifier;
            record.value_date = new Date();
            if(vm.step1.currency_to_buy == vm.step1.currency_to_sell){
              record.fee = data.non_instrument_cost;
            }else{
              record.fee = data.instrument_cost;
            }
            if(typeof record.amount != 'number')
              record.amount = vm.parseFloat(record.amount.split(',').join(''));
            else
              record.amount = vm.parseFloat(record.amount);
            var account_object = _.filter(vm.accounts, {id: record.account_picker});
            record = vm.mergeObjects(record, account_object[0]);
            record.payee_ = account_object[0].name;
            console.log(record);
            vm.fundings.push(record);
            vm.showCircleLoader = true;
            vm.SendFundingAndDisable = false;
            console.log(10);
            if(vm.funding_balance.amount > 0){
              vm.step4.amount = vm.funding_balance.amount;
              vm.funding_balance.copy_amount = vm.funding_balance.amount;
              vm.SendFundingAndDisable = false;
              console.log(9);
            }else{
              vm.funding_balance.copy_amount = 0;
              vm.step4.amount = null;
              vm.step4.account_picker = null;
              vm.SendFundingAndDisable = false;
              if(vm.fundings.length > 0){
                //send payments
                vm.showCircleLoader = false;
                vm.SendFundingAndDisable = true;
                console.log('here3');
                $mdToast.show({
                  template: '<md-toast class="md-toast info">Adding Funding..</md-toast>',
                  hideDelay: 2000,
                  position: 'bottom right'
                });
                var payments = '';
                var feesPromises = [];
                var fundingList = [];

                for(var i=0; i<vm.fundings.length; i++) {
                  feesPromises.push(vm.GetFeeByMethodByID(vm.fundings[i].method));
                  fundingList.push(vm.fundings[i]);
                }
                $q.all(feesPromises).then(function(data){
                  for(var i=0; i<data.length; i++) {
                    var value_date = new Date(Date.parse(fundingList[i].value_date));
                    value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                    if(fundingList[i].account_picker != null){
                      var payee = fundingList[i].account_picker;
                    }else{
                      var payee = null;
                    }
                    var currency = _.filter(vm.fundingCurrencies, {currency_id: vm.step1.currency_to_sell});
                    var currency_id = currency[0].currency_id;
                    payments += '<PAYMENTS>';
                    payments += '<ACTION><ID>'+fundingList[i].method+'</ID></ACTION>';
                    payments += '<CURRENCY><ID>'+currency_id+'</ID></CURRENCY>';
                    payments += '<AMOUNT>'+vm.parseFloat(fundingList[i].amount)+'</AMOUNT>';
                    payments += '<VALUE_DATE>'+value_date+'</VALUE_DATE>';
                    if(fundingList[i].method == 5){
                      if(payee != null)
                        payments += '<ACCOUNT><ID>'+payee+'</ID></ACCOUNT>';
                      payments += '<ACT_TYPE>E</ACT_TYPE>';
                    }else{
                      if(payee != null)
                        payments += '<PAYEE><ID>'+payee+'</ID></PAYEE>';
                    }
                    payments += '</PAYMENTS>';
                  }
                  var date = new Date(Date.parse(vm.step1.value_date));
                  vm.SendPayments(vm.deal.deal_number, date, payments).then(function(data){
                    if(data[0].status == 'FALSE'){
                      $mdToast.show({
                        template: '<md-toast class="md-toast error">There is an error while adding funding record.</md-toast>',
                        hideDelay: 4000,
                        position: 'bottom right'
                      });
                      $mdToast.show({
                        template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                        hideDelay: 4000,
                        position: 'bottom right'
                      });
                      vm.showCircleLoader = true;
                      vm.SendFundingAndDisable = false;
                      console.log(8);
                      return;
                    }else{
                      $mdToast.show({
                        template: '<md-toast class="md-toast success">Funding(s) Added. Deal Updated.</md-toast>',
                        hideDelay: 4000,
                        position: 'bottom right'
                      });
                      vm.GetUserApprovalRights(vm.deal, 12277).then(function(data){

                        if(data[0].status == 'TRUE'){
                          vm.skipApprovalStep = false;
                        }else{
                          vm.skipApprovalStep = true;
                        }
                        vm.showCircleLoader = true;
                        vm.SendFundingAndDisable = false;
                        console.log(7);
                        vm.nextStep();
                      });
                    }
                  });
                });
              }
            }
            vm.funding_balance.amount = 0;
            vm.step4.method = null;
            vm.step4.account_picker = null;
            vm.SendFundingAndDisable = false;
            console.log(6);
            return true;
          });
        }
        else{
          var identifier = vm.fundings.length + 1;
          record.current_id = identifier;
          record.value_date = vm.defaultValueDate || new Date();
          if(typeof record.amount != 'number')
            record.amount = vm.parseFloat(record.amount.split(',').join(''));
          else
            record.amount = vm.parseFloat(record.amount);
          var currency = _.filter(vm.fundingCurrencies, {currency_id: vm.step1.currency_to_sell});
          record.currency_id = currency[0].currency_id;
          record.currency_name = currency[0].currency_name;
          record.payee_ = 'N/A';
          record.fee = 0;
          console.log(record);
          vm.fundings.push(record);
          vm.showCircleLoader = true;
          vm.SendFundingAndDisable = false;
          console.log(5);
          if(vm.funding_balance.amount > 0){
            vm.step4.amount = vm.funding_balance.amount;
            vm.funding_balance.copy_amount = vm.funding_balance.amount;
            vm.SendFundingAndDisable = false;
            console.log(4);
          }else{
            vm.funding_balance.copy_amount = 0;
            vm.step4.amount = null;
            vm.step4.account_picker = null;
            //now send payments
            if(vm.fundings.length > 0){
              //send payments
              vm.showCircleLoader = false;
             console.log('11');
              $mdToast.show({
                template: '<md-toast class="md-toast info">Adding Funding..</md-toast>',
                hideDelay: 2000,
                position: 'bottom right'
              });
              var payments = '';
              var feesPromises = [];
              var fundingList = [];

              for(var i=0; i<vm.fundings.length; i++) {
                feesPromises.push(vm.GetFeeByMethodByID(vm.fundings[i].method));
                fundingList.push(vm.fundings[i]);
              }
              $q.all(feesPromises).then(function(data){
                for(var i=0; i<data.length; i++) {
                  var value_date = new Date(Date.parse(fundingList[i].value_date));
                  value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                  if(fundingList[i].account_picker != null){
                    var payee = fundingList[i].account_picker;
                  }else{
                    var payee = null;
                  }
                  var currency = _.filter(vm.fundingCurrencies, {currency_id: vm.step1.currency_to_sell});
                  var currency_id = currency[0].currency_id;
                  payments += '<PAYMENTS>';
                  payments += '<ACTION><ID>'+fundingList[i].method+'</ID></ACTION>';
                  payments += '<CURRENCY><ID>'+currency_id+'</ID></CURRENCY>';
                  payments += '<AMOUNT>'+vm.parseFloat(fundingList[i].amount)+'</AMOUNT>';
                  payments += '<VALUE_DATE>'+value_date+'</VALUE_DATE>';
                  if(fundingList[i].method == 5){
                    if(payee != null)
                      payments += '<ACCOUNT><ID>'+payee+'</ID></ACCOUNT>';
                    payments += '<ACT_TYPE>E</ACT_TYPE>';
                  }else{
                    if(payee != null)
                      payments += '<PAYEE><ID>'+payee+'</ID></PAYEE>';
                  }
                  payments += '</PAYMENTS>';
                }
                var date = new Date(Date.parse(vm.step1.value_date));
                vm.SendPayments(vm.deal.deal_number, date, payments).then(function(data){
                  if(data[0].status == 'FALSE'){
                    $mdToast.show({
                      template: '<md-toast class="md-toast error">There is an error while adding funding record.</md-toast>',
                      hideDelay: 4000,
                      position: 'bottom right'
                    });
                    $mdToast.show({
                      template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                      hideDelay: 4000,
                      position: 'bottom right'
                    });
                    vm.showCircleLoader = true;
                    vm.SendFundingAndDisable = false;
                    return;
                  }else{
                    $mdToast.show({
                      template: '<md-toast class="md-toast success">Funding(s) Added. Deal Updated.</md-toast>',
                      hideDelay: 4000,
                      position: 'bottom right'
                    });
                    vm.GetUserApprovalRights(vm.deal, 12277).then(function(data){
                      if(data[0].status == 'TRUE'){
                        vm.skipApprovalStep = false;
                      }else{
                        vm.skipApprovalStep = true;
                      }
                      vm.showCircleLoader = true;
                      vm.SendFundingAndDisable = false;
                      //console.log('2');
                      vm.nextStep();
                    });
                  }
                });
              });
            }
          }
          vm.funding_balance.amount = 0;
          vm.step4.method = null;
          vm.step4.account_picker = null;
          return true;
        }
      }else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">Method cannot be empty.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        vm.showCircleLoader = true;
        vm.SendFundingAndDisable = false;
        console.log(3);
      }
    };
    vm.addTotalFundingRecord = function(funding){
      vm.SendFundingAndDisable = true;
      console.log('here6');
      if(funding.method != null && funding.amount != null){
        if( funding.method.length != 0){
          vm.showCircleLoader = false;
          if(vm.funding_balance.copy_amount == 0){
            vm.showCircleLoader = true;
            vm.SendFundingAndDisable = false;
            console.log(1);
          }
          if(vm.addFundingRecord(funding)){
            vm.showCircleLoader = true;
            vm.SendFundingAndDisable = true;
            console.log('here7');
          }
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">Funding method can not be empty.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.showCircleLoader = true;
          vm.SendFundingAndDisable = false;
          console.log(2);
        }
      }else{
        if(vm.fundings.length > 0 && vm.funding_balance.copy_amount == 0){
          //now send payments
            if(vm.fundings.length > 0){
              //send payments
              //console.log('2');
              vm.showCircleLoader = false;
              vm.SendFundingAndDisable = true;
              console.log('here8');
              $mdToast.show({
                template: '<md-toast class="md-toast info">Adding Funding..</md-toast>',
                hideDelay: 2000,
                position: 'bottom right'
              });
              var payments = '';
              var feesPromises = [];
              var fundingList = [];

              for(var i=0; i<vm.fundings.length; i++) {
                feesPromises.push(vm.GetFeeByMethodByID(vm.fundings[i].method));
                fundingList.push(vm.fundings[i]);
              }
              $q.all(feesPromises).then(function(data){
                for(var i=0; i<data.length; i++) {
                  var value_date = new Date(Date.parse(fundingList[i].value_date));
                  value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
                  if(fundingList[i].account_picker != null){
                    var payee = fundingList[i].account_picker;
                  }else{
                    var payee = null;
                  }
                  var currency = _.filter(vm.fundingCurrencies, {currency_id: vm.step1.currency_to_sell});
                  var currency_id = currency[0].currency_id;
                  payments += '<PAYMENTS>';
                  payments += '<ACTION><ID>'+fundingList[i].method+'</ID></ACTION>';
                  payments += '<CURRENCY><ID>'+currency_id+'</ID></CURRENCY>';
                  payments += '<AMOUNT>'+vm.parseFloat(fundingList[i].amount)+'</AMOUNT>';
                  payments += '<VALUE_DATE>'+value_date+'</VALUE_DATE>';
                  if(fundingList[i].method == 5){
                    if(payee != null)
                      payments += '<ACCOUNT><ID>'+payee+'</ID></ACCOUNT>';
                    payments += '<ACT_TYPE>E</ACT_TYPE>';
                  }else{
                    if(payee != null)
                      payments += '<PAYEE><ID>'+payee+'</ID></PAYEE>';
                  }
                  payments += '</PAYMENTS>';
                }
                var date = new Date(Date.parse(vm.step1.value_date));
                vm.SendPayments(vm.deal.deal_number, date, payments).then(function(data){
                  if(data[0].status == 'FALSE'){
                    $mdToast.show({
                      template: '<md-toast class="md-toast error">There is an error while adding funding record.</md-toast>',
                      hideDelay: 4000,
                      position: 'bottom right'
                    });
                    $mdToast.show({
                      template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                      hideDelay: 4000,
                      position: 'bottom right'
                    });
                    vm.showCircleLoader = true;
                    vm.SendFundingAndDisable = false;
                    return;
                  }else{
                    $mdToast.show({
                      template: '<md-toast class="md-toast success">Funding(s) Added. Deal Updated.</md-toast>',
                      hideDelay: 4000,
                      position: 'bottom right'
                    });
                    vm.GetUserApprovalRights(vm.deal, 12277).then(function(data){
                      if(data[0].status == 'TRUE'){
                        vm.skipApprovalStep = false;
                      }else{
                        vm.skipApprovalStep = true;
                      }
                      vm.showCircleLoader = true;
                      vm.nextStep();
                    });
                  }
                });
              });
            }
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please Fill in all the required Fields.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.showCircleLoader = true;
          vm.SendFundingAndDisable = false;
        }
      }
    };
    vm.PickAccount = function(data){
      if(data.method != null && vm.step1.currency_to_sell != null){
        vm.accounts = [];
        var currency_object = _.filter(vm.fundingCurrencies, {currency_id: vm.step1.currency_to_sell});
        var currency_id = currency_object[0].currency_id;
        vm.GetPaymentsByMethodAndCurrencyInBookASpot(data.method, currency_id).then(function(data){
          vm.accounts = data;
        });
      }
    };
    vm.customShow = false;
    vm.finalDeal = {};
    vm.postDeal = null;
    vm.editFunding = function(data){
      var record = {};
      angular.copy(data, record);
      vm.GetPaymentsByMethodAndCurrencyInBookASpot(data.method, data.currency_id).then(function(data){
        $scope.accounts = data;
      });
      $mdDialog.show({
        locals:{dataToPass: record},
        fullscreen: vm.customFullscreen,
        clickOutsideToClose: true,
        scope: $scope,
        preserveScope: true,
        templateUrl: 'app/main/forexdealing/book_spot_deal/edit_funding.html',
        parent: angular.element(document.body),
        controller: function dialogController($scope, $mdDialog){
          $scope.closeDialog = function() {
            $mdDialog.hide();
            vm.payeeTable = {
              routing: null,
              payee_payor: null,
              bank_name: null,
              country: null
            };
          };
          $scope.isDisabled = true;
          $scope.PickAccount = function(data){
            if(data.method != null && vm.step1.currency_to_sell != null){
              vm.accounts = [];
              var currency_object = _.filter(vm.fundingCurrencies, {currency_id: vm.step1.currency_to_sell});
              var currency_id = currency_object[0].currency_id;
              vm.GetPaymentsByMethodAndCurrencyInBookASpot(data.method, currency_id).then(function(data){
                $scope.accounts = data;
                console.log(data);
              });
            }
          };
          $scope.first_amount = vm.parseFloat(record.amount);
          $scope.single = {
            id: record.id || null,
            current_id: record.current_id || null,
            currency_id: record.currency_id || null,
            currency_name: record.currency_name || null,
            payee_: record.payee_ || null,
            amount: $scope.first_amount || null,
            fee: record.fee,
            method: record.method || null,
            account_picker: record.account_picker || null,
            value_date: record.value_date || null
          };

          $scope.dialogLoader = true;
          $scope.updateFunding = function(funding){
            if(funding.method != null && vm.parseFloat(funding.amount) > 0){
              $scope.dialogLoader = false;
              var amount = vm.parseFloating(funding.amount);
              var total = 0;
              for(var i=0; i<vm.fundings.length; i++){
                total += vm.parseFloat(vm.fundings[i].amount);
              }
              console.log('Total ' + total);
              if(vm.step4.amount == null){
                var supposed_total = (total - $scope.first_amount) + amount;
                supposed_total = vm.parseFloat(supposed_total);
                if(amount > total){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Funding amount cannot be greater than total available amount.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  $scope.dialogLoader = true;
                  return;
                }
                if(supposed_total > total){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Funding amount cannot be greater than total available amount.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  $scope.dialogLoader = true;
                  return;
                }
              }else{
                var amount = vm.parseFloating(funding.amount);
                if(vm.step4.amount != 0 && amount > vm.step4.amount){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Funding amount cannot be greater than total available amount.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  $scope.dialogLoader = true;
                  return;
                }
              }
              //update funding record
              if(funding.account_picker != null){
                //account is present.
                console.log(funding);
                for(var i=0; i<vm.fundings.length; i++){
                  if(vm.fundings[i].current_id == funding.current_id){
                    vm.fundings[i].amount = vm.parseFloating(funding.amount);
                    vm.fundings[i].currency_id = funding.currency_id;
                    vm.fundings[i].currency_name = funding.currency_name;
                    vm.fundings[i].value_date = funding.value_date;
                    //var account = _.filter($scope.accounts, {id:funding.account_picker});
                    //console.log(account);
                    //vm.fundings[i].payee_ = account[0].name;
                    vm.fundings[i].method = funding.method;
                    vm.fundings[i].account_picker = funding.account_picker;
                  }
                }
                var total = 0;
                for(var i=0; i<vm.fundings.length; i++){
                  total += vm.fundings[i].amount;
                }
                var remaining_amount = vm.rate.sell_amount - total;
                vm.step4.amount = remaining_amount;
                $mdToast.show({
                  template: '<md-toast class="md-toast success">Funding record updated.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                if(vm.step4.amount == 0){
                  vm.customShow = true;
                }
                vm.SendFundingAndDisable = false;
                $scope.dialogLoader = true;
                $scope.closeDialog();
              }else{
                //no account attached.
                for(var i=0; i<vm.fundings.length; i++){
                  if(vm.fundings[i].current_id == funding.current_id){
                    vm.fundings[i].amount = vm.parseFloating(funding.amount);
                    vm.fundings[i].currency_id = funding.currency_id;
                    vm.fundings[i].currency_name = funding.currency_name;
                    vm.fundings[i].method = funding.method;
                    vm.fundings[i].value_date = funding.value_date;
                    vm.fundings[i].payee_ = funding.payee_;
                  }
                }
                var total = 0;
                for(var i=0; i<vm.fundings.length; i++){
                  total += vm.fundings[i].amount;
                }
                var remaining_amount = vm.rate.sell_amount - total;
                vm.step4.amount = remaining_amount;
                $mdToast.show({
                  template: '<md-toast class="md-toast success">Funding record updated.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                if(vm.step4.amount == 0){
                  vm.customShow = true;
                }
                vm.SendFundingAndDisable = false;
                $scope.dialogLoader = true;
                $scope.closeDialog();
              }
            }else{
              $mdToast.show({
                template: '<md-toast class="md-toast error">Fill required fields.</md-toast>',
                hideDelay: 4000,
                position: 'bottom right'
              });
              vm.customShow = false;
              return;
            }
            /*if(funding.method != null && vm.parseFloat(funding.amount) > 0){
              $scope.dialogLoader = false;
              var temp_record = {};
              angular.copy(funding, temp_record);
              if(vm.step4.amount == 0){
                var total = 0;
                for(var i=0; i<vm.fundings.length; i++){
                  total += vm.fundings[i].amount;
                }
                var supposed_total = (total - $scope.first_amount) + funding.amount;
                if(funding.amount > total){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Funding amount cannot be greater than total available amount.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  $scope.dialogLoader = true;
                  return;
                }
                if(supposed_total > total){
                  $mdToast.show({
                    template: '<md-toast class="md-toast error">Funding amount cannot be greater than total available amount.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  $scope.dialogLoader = true;
                  return;
                }
              }
              /!*if(funding.amount > vm.step4.amount){
                $mdToast.show({
                  template: '<md-toast class="md-toast error">Funding amount cannot be greater than total available amount.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                $scope.dialogLoader = true;
                return;
              }*!/
              if( temp_record.account_picker != null){
                vm.GetFeeByMethodByID(temp_record.method).then(function(data){
                  //var identifier = vm.fundings.length + 1;
                 // temp_record.current_id = identifier;
                  temp_record.value_date = new Date();
                  if(vm.toUpperCase(vm.step1.currency_to_buy) == vm.toLowerCase(vm.step1.currency_to_sell)){
                    record.fee = data.non_instrument_cost;
                  }else{
                    record.fee = data.instrument_cost;
                  }
                  if(typeof temp_record.amount != 'number')
                    temp_record.amount = vm.parseFloat(temp_record.amount.split(',').join(''));
                  else
                    temp_record.amount = vm.parseFloat(temp_record.amount);
                  //var account_object = _.filter(vm.accounts, {id: temp_record.account_picker});
                  //temp_record = vm.mergeObjects(temp_record, account_object[0]);
                  //temp_record.payee_ = account_object[0].name;
                  vm.fundings = _.without(vm.fundings, _.findWhere(vm.fundings, {current_id: funding.current_id}));
                  vm.fundings.push(temp_record);
                  var total = 0;
                  for(var i=0; i<vm.fundings.length; i++){
                    total += vm.fundings[i].amount;
                  }
                  var remaining_amount = vm.rate.sell_amount - total;
                  vm.step4.amount = remaining_amount;
                  $mdToast.show({
                    template: '<md-toast class="md-toast success">Funding record updated.</md-toast>',
                    hideDelay: 4000,
                    position: 'bottom right'
                  });
                  if(vm.step4.amount == 0){
                    vm.customShow = true;
                  }
                  vm.SendFundingAndDisable = false;
                  $scope.dialogLoader = true;
                  $scope.closeDialog();
                });
              }else{
                //var identifier = vm.fundings.length + 1;
                //temp_record.current_id = identifier;
                temp_record.value_date = new Date();
                if(typeof temp_record.amount != 'number')
                  temp_record.amount = vm.parseFloat(temp_record.amount.split(',').join(''));
                else
                  temp_record.amount = vm.parseFloat(temp_record.amount);
                var currency = _.filter(vm.fundingCurrencies, {currency_name: vm.toUpperCase(vm.step1.currency_to_sell)});
                temp_record.currency_id = currency[0].currency_id;
                temp_record.currency_name = currency[0].currency_name;
                temp_record.payee_ = 'N/A';
                temp_record.fee = 0;
                vm.fundings = _.without(vm.fundings, _.findWhere(vm.fundings, {current_id: funding.current_id}));
                vm.fundings.push(temp_record);
                var total = 0;
                for(var i=0; i<vm.fundings.length; i++){
                  total += vm.fundings[i].amount;
                }
                var remaining_amount = vm.rate.sell_amount - total;
                vm.step4.amount = remaining_amount;
                $mdToast.show({
                  template: '<md-toast class="md-toast success">Funding record updated.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                if(vm.step4.amount == 0){
                  vm.customShow = true;
                }
                vm.SendFundingAndDisable = false;
                $scope.dialogLoader = true;
                $scope.closeDialog();
              }
            }else{
              $mdToast.show({
                template: '<md-toast class="md-toast error">Fill required fields.</md-toast>',
                hideDelay: 4000,
                position: 'bottom right'
              });
              return;
            }*/
          };
        }
      });
    };
    vm.deleteFunding = function(funding){
      console.log(funding);
      for(var i=0; i<vm.fundings.length; i++){
        if(vm.fundings[i].current_id == funding.current_id){
          vm.step4.amount = vm.step4.amount + funding.amount;
          vm.fundings.splice(i,1);
          $mdToast.show({
            template: '<md-toast class="md-toast info">Funding record removed from the list.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          return;
        }
      }
    };
    //step 5
    vm.showPopOverFunc = function(){
      vm.showPopOver = false;
    };
    vm.showPopLeaveFunc = function(){
      vm.showPopOver = true;
    };
    vm.SetDealApproval = function(deal){
      return soap_api.SetDealApproval(deal).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function(i,e){
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              error: jQuery(e).find('ERROR').text()
            };
            row.push(temp);
          });
        }else{
          console.log(success.data);
        }
        return row;
      });
    };
    vm.approveNow = function(){
      if(vm.ApprovalNext == false){
        vm.showCircleLoader = false;
        vm.GetUserApprovalRights(vm.deal, 12277).then(function(data){
          if(data[0].status == 'TRUE'){
            vm.SetDealApproval(vm.deal.deal_number).then(function(data){
              if(data[0].status == 'TRUE'){
                $mdToast.show({
                  template: '<md-toast class="md-toast info">Deal Approved.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                vm.GetApprovalHistory(vm.deal).then(function(data){
                  /*console.log(data);
                  var user = data[0];
                  var user_row = {
                    name: user.user_name,
                    date: user.approval_date
                  };*/
                  vm.approvalCounter++;
                  vm.Approvals = data;
                  if(vm.Approvals.length == vm.approvalPolicy[0].approval_level){
                    vm.ApprovalNext = true;
                  }
                  vm.showCircleLoader = true;
                });
              }else{
                $mdToast.show({
                  template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                  hideDelay: 2000,
                  position: 'bottom right'
                });
                vm.showCircleLoader = true;
              }
            });
          }else{
            $mdToast.show({
              template: '<md-toast class="md-toast error">You cannot approve this deal.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
          }
        });
      }
    };
    vm.ApprovalPrevious = function(){
      if(vm.fundings.length > 0){
        vm.step4.amount = null;
        vm.funding_balance.copy_amount = 0;
        vm.step4.account_picker = null;
        vm.step4.method = null;
        vm.SendFundingAndDisable = false;
      }
      vm.previousStep();
    };
    vm.PostWebDeal = function(deal){
      return soap_api.PostWebDeal(deal).then(function (success) {
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT RESULT').each(function(i,e) {
          var temp = {
            recordtype: jQuery(e).find('RecordType').text(),
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text(),
            recordnumber: jQuery(e).find('RECORDNUMBER').text()
          };
          row.push(temp);
        });
        return row;
      });

    };
    vm.FinalizeDealPaymentSettlement = function (deal) {
      return soap_api.FinalizeDealPaymentSettlement(deal).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            recordtype: jQuery(e).find('RecordType').text(),
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text(),
            recordnumber: jQuery(e).find('RECORDNUMBER').text()
          };
          row.push(temp);
        });
        return row;
      });
    }
    vm.SendPayments = function(deal, value_date, payments){
      return soap_api.SendPayments(deal, value_date, payments).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text()
          };
          row.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.SendSettlements = function(deal, value_date, settlements){
      return soap_api.SendSettlements(deal, value_date, settlements).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text()
          };
          row.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.UpdateDeal = function(){
      vm.FinalizeDealPaymentSettlement(vm.deal).then(function(data){
        vm.finalDeal['id'] = vm.deal.id;
        if(data[0].status == 'TRUE'){
          // vm.finalDeal = data[0];
          for(var key in data[0]){
            vm.finalDeal[key] = data[0][key];
          }
          $mdToast.show({
            template: '<md-toast class="md-toast success">Deal finalized.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.showCircleLoader = true;
          vm.GetWebDealPaymentAndSettlement(vm.deal).then(function(settlements){
            for(var i=0; i<settlements.length; i++){
              if(settlements[i].internal_reference != ''){
                if(vm.payees[i].internal_payment_reference == settlements[i].internal_reference){
                  vm.payees[i].settlement_id = settlements[i].settlement_id;
                }
              }
            }
            vm.nextStep();
          });
          /*vm.GetWebDealPaymentAndSettlement(vm.deal).then(function(settlements){
            var reports = [];
            for(var i=0; i<settlements.length; i++){
              if(settlements[i].internal_reference != ''){
                reports.push(vm.GetWireReport(settlements[i].settlement_id));
              }
            }
            $q.all(reports).then(function(responses){
              for(var j=0; j<responses.length; j++){
                if(vm.payees[j].internal_payment_reference == settlements[j].internal_reference){
                  vm.payees[j].print_link = responses[j][0].value;
                  vm.payees[j].print_name = responses[j][0].recordid;
                  for(var i=0; i<vm.allElectronicMethods.length; i++){
                    if(vm.payees[j].paymentmethod == vm.allElectronicMethods[i].id){
                      vm.payees[j].hide_print = true;
                    }else{
                      vm.payees[j].hide_print = false;
                    }
                  }
                }
              }
            }).finally(function(){
              //print file for the deal
              vm.GetDealReport(vm.deal).then(function(report){
                vm.dealReport = {
                  link: report[0].value,
                  name: report[0].recordid
                };
              });
            });
          });*/
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.showCircleLoader = true;
        }
      });
    };
    //for all steps
    vm.getTotalBeneficiary = function(){
      var total = 0;
      for(var i = 0; i < vm.payees.length; i++){
        var payee = vm.payees[i];
        total += parseFloat(payee.total);
      }
      return total;
    };
    vm.getTotalBeneficiaryAmount = function(){
      var total = 0;
      for(var i = 0; i < vm.payees.length; i++){
        var payee = vm.payees[i];
        total += vm.parseFloat(payee.amount);
      }
      return total;
    };
    vm.getTotalBeneficiaryFee = function(){
      var total = 0;
      for(var i = 0; i < vm.payees.length; i++){
        var payee = vm.payees[i];
        total += vm.parseFloat(payee.fee);
      }
      return total;
    };
    vm.getTotalFundingAmount = function(){
      var total = 0;
      for(var i = 0; i < vm.fundings.length; i++){
        var funding = vm.fundings[i];
        total += vm.parseFloat(funding.amount);
      }
      return total;
    };
    vm.getSingleBeneficiary = function(payee){
      var local_payee = angular.copy(payee);
      var total = 0;
      total += parseFloat(local_payee.total);
      return total;
    };
    //iterate to next step in tabs
    vm.nextStep = function(){
      var index = (vm.selectedIndex === vm.max) ? 0 : vm.selectedIndex + 1;
      vm.selectedIndex = index;
      vm.showLoader();
      var currentTab = $( '#tab_'+index);
      $('md-tab-item').each(function () {
        if( $(this).hasClass('md-active') ){
          $(this).addClass('md-previous');
          $(this).prevAll().addClass('md-previous');
        }
      });
    };
    vm.previousStep = function () {
      var index = (vm.selectedIndex === 0) ? 0 : vm.selectedIndex - 1;
      vm.selectedIndex = index;
      var currentTab = $( '#tab_'+index);
      angular.element('md-tab-item').each(function(){
        if( angular.element(this).hasClass('md-active') ){
          angular.element(this).removeClass('md-previous');
          angular.element(this).prev().removeClass('md-previous')
        }
      });
    };
    vm.b64toBlob = function (b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    };
    vm.parseMe = function(value){
      return value;
    };
    //Verify add Payee if payee amount is less then balance
    vm.verifyRecord = verifyRecord;
    function verifyRecord(stepAmount, price) {
      return price && +parseFloating(stepAmount) > 0 && +parseFloating(stepAmount) < +parseFloating(price) || "";
    };
    function parseFloating(value, s){
      var the_amount = +value;
      if(typeof value === 'string'){
        the_amount = value.split(',').join('');
        the_amount = parseFloat(the_amount).toFixed(2);
      }

      if(typeof value === 'number'){
        the_amount = parseFloat(the_amount).toFixed(2);
      }

      return the_amount;
    };
    // vm.verifyAddPayee = function(stepAmount, totalBalance){
    //   return +vm.parseFloating(stepAmount) > 0 && +vm.parseFloating(stepAmount) < +totalBalance ;
    // }
    vm.verifyFunding= function(stepAmount, totalBalance) {
      var step4Amount = stepAmount || vm.step4.amount ;
      // var step4Balanse = totalBalance || vm.beneficiary && vm.beneficiary.sell;
      return +vm.parseFloating(stepAmount) >= 0 && +vm.parseFloating(stepAmount) < +vm.parseFloating(totalBalance) ;
    };
    //Next
    vm.next = function (stepAmount, totalBalance) {
      return +vm.parseFloating(stepAmount) >= +totalBalance;
    }
    vm.expires_in = 60;
    vm.timer = function(){
      vm.duration = 60;
      vm.percent_duration = 0;
      vm.interval_func = $interval(function(){
        vm.expires_in -= 1;
        //vm.duration -= 1;
        vm.percent_duration = (100/(vm.duration/vm.expires_in)).toFixed(2);
        if(vm.expires_in < 1){
          vm.duration = 60;
          vm.expires_in = 60;
          vm.percent_duration = 0;
          $interval.cancel(vm.interval_func);
        }
      }, 1000);
    };
    vm.showLoader = function () {

      if (vm.selectedIndex === 1) {
        vm.determinateValue = 100;
        vm.expire_in = 60;
        vm.interval_ = $interval(function () {

          vm.determinateValue -= 2;
          vm.expire_in -= 1;
          if (vm.determinateValue < 1) {
            // vm.selectedIndex = 0;
            vm.determinateValue = 100;
            vm.expire_in = 60;
            $interval.cancel(vm.interval_);
          }

        }, 1000);
      } else {
        vm.determinateValue = 100;
        vm.expire_in = 60;
        $interval.cancel(vm.interval_);
        vm.interval_ = null;
      }
    };
    $scope.showHelloMe = false;
    $scope.collapseAll = function(data) {
      for(var i in $scope.accordianData) {
        if($scope.accordianData[i] != data) {
          $scope.accordianData[i].expanded = false;
          $scope.showHelloMe = false

        }
      }
      data.expanded = !data.expanded;
      $scope.showHelloMe= true
    };
    vm.routingFilter = function(payee){
      if(vm.payeeTable){
        if(
          (payee.internal_reference.toLowerCase().indexOf(vm.payeeTable.routing || '') !== -1)
        ){
          return true;
        }else{
          return false;
        }
      }else{
        return true;
      }
    };
    vm.payeeNameFilter = function(payee){
      if(vm.payeeTable){
        if(
          (payee.name.toLowerCase().indexOf(vm.payeeTable.payee_payor || '') !== -1)
        ){
          return true;
        }else{
          return false;
        }
      }else{
        return true;
      }
    };
    vm.payeeBankFilter = function(payee){
      if(vm.payeeTable){
        if(
          (payee.beneficiarybank_name.toLowerCase().indexOf(vm.payeeTable.bank_name || '') !== -1)
        ){
          return true;
        }else{
          return false;
        }
      }else{
        return true;
      }
    };
    vm.payeeCountryFilter = function(payee){
      if(vm.payeeTable){
        if(
          (payee.country_name.toLowerCase().indexOf(vm.payeeTable.country || '') !== -1)
        ){
          return true;
        }else{
          return false;
        }
      }else{
        return true;
      }
    };
    vm.payeeMethodFilter = function(payee){
      if(vm.payeeTable){
        if(
          (payee.type.toLowerCase().indexOf(vm.payeeTable.method || '') !== -1)
        ){
          return true;
        }else{
          return false;
        }
      }else{
        return true;
      }
    };
    vm.getAccountName = function(account_id){
      var account_object = _.filter(vm.accounts, {id: account_id});
    };
    vm.getMethodName = function(method_id){
      var method_object = _.filter(vm.methods, {id: method_id});
      return method_object[0].name;
    };
    vm.printDeal = function(){
      vm.showCircleLoader = false;
      vm.GetDealReport(vm.finalDeal).then(function(data){
        data = data[0];
        vm.showCircleLoader = true;
        if(data.status == 'TRUE'){
          var blob = vm.b64toBlob(data.value, 'application/pdf');
          saveAs(blob, data.recordid);
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">No report found.</md-toast>',
            hideDelay: 5000,
            position: 'bottom right'
          });
        }
      });
    };
    // vm.check = function(){
    //   vm.step1.i_want_to = "ehsan"
    // }
    vm.printSettlement = function(settlement){
      if(settlement.settlement_id != null && settlement.settlement_id != ''){
        vm.showCircleLoader = false;
        vm.GetWireReport(settlement.settlement_id).then(function(data){
          data = data[0];
          vm.showCircleLoader = true;
          if(data.status == 'TRUE'){
            var blob = vm.b64toBlob(data.value, 'application/pdf');
            saveAs(blob, data.recordid);
          }else{
            $mdToast.show({
              template: '<md-toast class="md-toast error">No report found.</md-toast>',
              hideDelay: 5000,
              position: 'bottom right'
            });
          }
        });
      }
    };

    vm.fakeAPI = function(){
      // $http({
      //   method:'get',
      //   url:'https://jsonplaceholder.typicode.com/posts'
      // }).then(function(resp){
      //   console.log(resp);
      // })
      // soap_api.Get
    }

  }
})();
