(function () {
  'use strict';

  angular
    .module('app.forexdealing', [
      'app.forexdealing.book_spot_deal',
      'app.forexdealing.place_order',
      'app.forexdealing.draw_down',
      'app.forexdealing.book_forward_deal',
      'app.forexdealing.dealing_history'
    ])
    .config(config);

  /** @ngInject */
  function config(msNavigationServiceProvider) {



    msNavigationServiceProvider.saveItem('main_menu.forexdealing', {
      title: 'Forex Dealing',
      icon: 'icon-chart-line',
      weight: 1,
      child_count: 5
    });

    msNavigationServiceProvider.saveItem('main_menu.forexdealing.book_forward_deal', {
      title: 'Book a Forward Deal',
      state: 'app.book_forward_deal',
      weight: 1,
      module_id: 233
    });


    msNavigationServiceProvider.saveItem('main_menu.forexdealing.book_spot_deal', {
      title: 'Book a Spot Deal',
      state: 'app.book_spot_deal',
      weight: 1,
      module_id: 12277
    });


    msNavigationServiceProvider.saveItem('main_menu.forexdealing.place_order', {
      title: 'Place an order',
      state: 'app.place_order', //its important value
      weight: 1,
      module_id: 924
    });

    msNavigationServiceProvider.saveItem('main_menu.forexdealing.draw_down', {
      title: 'Deal Draw Down',
      state: 'app.draw_down', //its important value
      weight: 1,
      module_id: 924
    });

    msNavigationServiceProvider.saveItem('main_menu.forexdealing.book_forward_deal', {
      title: 'Book Forward Deal',
      state: 'app.book_forward_deal', //its important value
      weight: 1,
      module_id: 924
    });

    msNavigationServiceProvider.saveItem('main_menu.forexdealing.dealing_history', {
      title: 'Dealing History',
      state: 'app.dealing_history',
      weight: 1,
      module_id: 11432
    });
  }
})();


