(function () {
  'use strict';

  var app = angular
    .module('app.forexdealing.manage_user', ['datatables'])
    .config(config);

  /** @ngInject */
  function config($stateProvider, msApiProvider) {
    // State
    $stateProvider.state('app.manage_user', {
      url: '/manage_user',
      views: {
        'content@app': {
          templateUrl: 'app/main/forexdealing/manage_user/manage_user.html',
          controller: 'ManageUserController as vm'
        }
      },
      resolve: {
        Invoice: function (msApi) {
          return msApi.resolve('invoice@get');
        },
        access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
      }
    });

    // Api
    msApiProvider.register('invoice', ['app/data/invoice/invoice.json']);
    //msApiProvider.register('invoice', ['app/data/invoice/invoice.json']);
  }
})();
