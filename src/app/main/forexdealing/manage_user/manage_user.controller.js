(function () {
  'use strict';

  angular
    .module('app.forexdealing.manage_user')
    .controller('ManageUserController', ManageUserController);

  /** @ngInject */
  function ManageUserController($state, $timeout, $rootScope, $mdDialog, $scope, $interval, $mdToast, Invoice, $filter, soap_api, $q){
    var vm = this;
    //its compulsory
    //CONSTANTS DECLARATION BEGIN
    $rootScope.headerTitle = 'Manage User';
    vm.circleLoader = true;
    vm.data ={
      group1 : 1
    };
    vm.selectedIndex = 0;
    vm.orderType = new Array();
    vm.buyingCurrency = new Array();
    vm.sellingCurrency = new Array();
    vm.wantTo = new Array();
    vm.orderType = new Array();
    vm.step1 = {};
    //CONSTANTS DECLARATION END
    vm.moveForwardTab = function(){
      vm.selectedIndex++;
    }
    /**
     * getPlaceAnOrderData
     * @desc This function will get all dropdown data for place an order
     * @return void
     */
    vm.getPlaceAnOrderData = function(){
      //ORDER TYPE POLUTING BEGIN
      vm.orderType = [
        {
          id : 1,
          label : 'Market Watch'
        },
        {
          id : 2,
          label : 'Daily Watch'
        },
      ];
      //ORDER TYPE POLUTING BEGIN
      //BUYING CURRENCY BEGIN
      vm.buyingCurrency = [
        {
          id : 1,
          label : 'USD'
        },
        {
          id : 2,
          label : 'CNY'
        },
      ];
      //BUYING CURRENCY END
      //I WANT TO PAY BEGIN
      vm.wantTo =  [
        {
          id : 1,
          label : 'Buy'
        },
        {
          id : 2,
          label : 'Sell'
        },
      ];
      //I WANT TO PAY END
      //SELLING CURRENCY END
      vm.sellingCurrency =  [
        {
          id : 1,
          label : 'USD'
        },
        {
          id : 2,
          label : 'CNY'
        },
      ];
      //SELLING CURRENCY END
    }

    /**
     * submitPlaceAnOrder
     * @param Object
     * @desc This function is used to submit place an order
     */
    vm.submitPlaceAnOrderStep1 = function(data){
      //INCREASING INDEX TO NEXT INDEX

      var isValidated = true;
      if( vm.step1.orderType == undefined || vm.step1.orderType == ''){
        $mdToast.show({
          template: '<md-toast class="md-toast error">Enter Order Type.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        isValidated = false;
      }
      if( vm.step1.currencyBuying == undefined || vm.step1.currencyBuying == ''){
        $mdToast.show({
          template: '<md-toast class="md-toast error">Select Buying Currency.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        isValidated = false;
      }
      if( vm.step1.i_want_to == undefined || vm.step1.i_want_to == ''){
        $mdToast.show({
          template: '<md-toast class="md-toast error">Select I want to pay.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        isValidated = false;
      }
      if( vm.step1.value_date == undefined || vm.step1.value_date == ''){
        $mdToast.show({
          template: '<md-toast class="md-toast error">Select Value Date.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        isValidated = false;
      }
      if( vm.step1.currencySelling == undefined || vm.step1.currencySelling == ''){
        $mdToast.show({
          template: '<md-toast class="md-toast error">Select Selling Currency.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        isValidated = false;
      }
      if( vm.step1.targetRate == undefined || vm.step1.targetRate == ''){
        $mdToast.show({
          template: '<md-toast class="md-toast error">Select Target Rate.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        isValidated = false;
      }
      if( vm.step1.expiryDate == undefined || vm.step1.expiryDate == ''){
        $mdToast.show({
          template: '<md-toast class="md-toast error">Select Expiry Date.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        isValidated = false;
      }
      if(isValidated){
        vm.selectedIndex++;
        vm.step1 = data;
      }


    }

    //INVOKING METHOD BEGIN
    vm.getPlaceAnOrderData();
    // INVOKING METHOD END
  }
})();
