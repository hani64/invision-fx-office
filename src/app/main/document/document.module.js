(function () {
  'use strict';

  angular
    .module('app.document', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) {
    // State
    $stateProvider
      .state('app.document', {
        url: '/document',
        views: {
          'content@app': {
            templateUrl: 'app/main/document/document.html',
            controller: 'DocumentController as vm'
          }
        }
      });

    // Translation
    $translatePartialLoaderProvider.addPart('app/main/document');

    // Api
    //msApiProvider.register('sample', ['app/data/payments/payments.json']);


    msNavigationServiceProvider.saveItem('main_menu.document', {
      title: 'Documents',
      icon: 'icon-document',
      state: 'app.document',
      weight: 1,
      child_count: 2
    });

    msNavigationServiceProvider.saveItem('main_menu.document.personal', {
      title: 'Personal',
      state: 'app.document.personal',
      weight: 1,
      module_id: 11881
    });


    msNavigationServiceProvider.saveItem('main_menu.document.help', {
      title: 'Help',
      state: 'app.document.help',
      weight: 1,
      module_id: 13578
    });



  }
})();
