(function () {
  'use strict';

  angular
    .module('app.beneficiaries.view_beneficiaries')
    .controller('ViewBeneficiaryController', ViewBeneficiaryController);

  /** @ngInject */

  function ViewBeneficiaryController( api, $filter, $location, $rootScope, NgTableParams)
  {
    var vm = this;
    $rootScope.headerTitle = 'View Beneficiaries';
    vm.showDataTable = true;
    vm.enableFilter = false;
    vm.beneficiaries = [];
    vm.filter = {
      name: null,
      address1: null,
      type: null,
      currency: null,
      reference: null,
      accountNumber: null
    };

    //API
    api.GetAllBeneficiaries().then(function(data){
      vm.beneficiaries = data.output.table1;
      vm.showDataTable = false;
      vm.tableParams = new NgTableParams({}, {
        counts: [5, 10, 20, 50, 100],
        dataset : vm.beneficiaries,
        filterOptions: {
          filterFn: vm.filterForTable
        }
      });
    });
    vm.filterForTable = function(data, filterValues){
      if(vm.filter){
        return data.filter(function(item){
          return (item.name.indexOf(filterValues.name || '') !== -1) &&
            (item.paymentmethod_name.indexOf(filterValues.paymentmethod_name || '') !== -1) &&
            (item.currency_name.indexOf(filterValues.currency_name || '') !== -1) &&
            (item.beneficiarybank_address1.indexOf(filterValues.beneficiarybank_address1 || '') !== -1) &&
            (item.beneficiary_accountcode.indexOf(filterValues.beneficiary_accountcode || '') !== -1) &&
            (item.internal_reference.indexOf(filterValues.internal_reference || '') !== -1);
        });
      }else{
        return data;
      }
    };
    vm.filterAll = function(){
      vm.tableParams.filter({
        name: vm.filter.name,
        paymentmethod_name: vm.filter.type,
        currency_name: vm.filter.currency,
        beneficiarybank_address1: vm.filter.address1,
        internal_reference: vm.filter.reference,
        beneficiary_accountcode: vm.filter.accountNumber
      });
    };
    vm.toggleFilters = function(){
      vm.filter = {
        name: null,
        address1: null,
        type: null,
        currency: null,
        reference: null,
        accountNumber: null
      };
      vm.enableFilter = !vm.enableFilter;
    };
    vm.moveFirstToLast = function(firstOrLast){
      if(firstOrLast === 'last')
      {
        var lastPage = updatePageCount();
        vm.tableParams.page(lastPage);
        return;
      }
      vm.tableParams.page(1);
    };
    function updatePageCount(){
      return Math.ceil(vm.tableParams.total()/vm.tableParams.count());
    }
    vm.addNewBeneficiary = function(){
      $location.url('/add_beneficiary');
    };
  }
})();
  /*function ViewBeneficiaryController($scope, soap_api, $location, $mdDialog, $filter, $rootScope, NgTableParams) {
    var vm = this;


    $rootScope.headerTitle = 'View Beneficiaries';

    vm.circleLoader = false;
    vm.circleLoader = false;
    vm.showDataTable = true;
    vm.enableFilter = false;
    vm.ready = false;
    vm.filter = {
      global_search: null,
      name: null,
      address1: null,
      method: null,
      currency: null,
      reference: null
    };
    vm.from_date = new Date(new Date().setFullYear( new Date().getFullYear() - 10 ));
    vm.to_date = new Date(new Date().setFullYear( new Date().getFullYear() + 2 ));
    vm.parseBoolean = function(str){
      return str == 'true' ? true: false;
    };
    vm.parseFloat = function(number){
      return parseFloat(number);
    };
    vm.toLowerCase = function(str){
      str = str || '';
      return str.toLowerCase();
    };
    vm.toUpperCase = function(str){
      str = str || '';
      return str.toUpperCase();
    };
    vm.isXML = function(xml){
      try{
        var xmlDoc = jQuery.parseXML(xml);
        return true;
      }catch(error){
        return false;
      }
    };
    vm.includes = function(string, substring){
      string = string || '';
      substring = substring || ''
      if( string.toLowerCase().indexOf(substring.toLowerCase()) !== -1){
        return true;
      }else{
        return false;
      }
    };
    vm.replaceWith = function(string, separator){
      string = string || '';
      return string.replace(/ +/g, separator);
    };
    vm.floor = function(expression){
      return Math.floor(expression);
    };
    vm.parseDate = function(input){
      if(typeof input !== 'undefined' && input !== null && input !== ''){
        var parts = input.split('-');
        return new Date(parts[2], parts[1]-1, parts[0]);
      }else{
        return input;
      }
    };
    vm.protectAmount = function(event){
      var input = event.key;
      var re = /\d/i;
      if(input != 'Backspace' && input != '.' && input != 'Delete' && input != 'Del' && input != 'Decimal' && input != 'Left' && input != 'Right' && input != 'Tab' && input != 'ArrowLeft' && input != 'ArrowRight'){
        if(input.match(re) == null){
          event.preventDefault();
          return false;
        }
      }else{
        if(input == '.' || input == 'Decimal'){
          re = /\./g;
          if(event.target.value.length > 0){
            if(event.target.value.match(/\./g) != null){
              if(event.target.value.match(/\./g).length > 0){
                event.preventDefault();
                return false;
              }
            }
          }
        }
      }
    };
    vm.onlyWords = function(event){
      var input = event.key;
      var re = /^[a-zA-Z\s]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.noSpecialCharactersFunction = function(event){
      var input = event.key;
      var re = /^[a-zA-Z0-9\-\s]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.toggleFilters = function(){
      vm.filter = {
        global_search: null,
        name: null,
        address1: null,
        method: null,
        currency: null,
        reference: null
      };
      vm.enableFilter = !vm.enableFilter;
    };

    vm.GetAllBeneficiaries = function(){
      return soap_api.GetAllBeneficiaries().then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT Table1').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              name: jQuery(e).find('NAME').text(),
              parent_id: jQuery(e).find('PARENT_ID').text(),
              entity: jQuery(e).find('ENTITY').text(),
              type: jQuery(e).find('TYPE').text(),
              is_non_third_party: jQuery(e).find('IS_NON_THIRD_PARTY').text(),
              payment_type_name: jQuery(e).find('PAYMENT_TYPE_NAME').text(),
              payment_type: jQuery(e).find('PAYMENT_TYPE').text(),
              act_seg_id: jQuery(e).find('ACT_SEG_ID').text(),
              paymentmethod_name: jQuery(e).find('PAYMENTMETHOD_NAME').text(),
              paymentmethod: jQuery(e).find('PAYMENTMETHOD').text(),
              currency_id: jQuery(e).find('CURRENCY_ID').text(),
              currency_name: jQuery(e).find('CURRENCY_NAME').text(),
              beneficiary_accountcode: jQuery(e).find('BENEFICIARY_ACCOUNTCODE').text(),
              country_name: jQuery(e).find('COUNTRY_NAME').text(),
              beneficiary_email: jQuery(e).find('BENEFICIARY_EMAIL').text(),
              beneficiary_instructions: jQuery(e).find('BENEFICIARY_INSTRUCTIONS').text(),
              internal_reference: jQuery(e).find('INTERNAL_REFERENCE').text(),
              beneficiarybank_name: jQuery(e).find('BENEFICIARYBANK_NAME').text(),
              purpose: jQuery(e).find('PURPOSE').text(),
              purpose_of_payment: jQuery(e).find('PURPOSE_OF_PAYMENT').text(),
              beneficiarybank_address1: jQuery(e).find('BENEFICIARYBANK_ADDRESS1').text(),
              beneficiarybank_city: jQuery(e).find('BENEFICIARYBANK_CITY').text(),
              beneficiarybank_country: jQuery(e).find('BENEFICIARYBANK_COUNTRY').text(),
              currency_item_id: jQuery(e).find('CURRENCY_ITEM_ID').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetAllBeneficiaries().then(function(data){
      vm.beneficiaries = data;
      vm.circleLoader = true;
      vm.showDataTable = false;
      vm.ready = true;
      vm.filterForTable = function(data, filterValues){
        if(vm.filter){
          return data.filter(function(item){
            return (item.name.toLowerCase().indexOf(filterValues.name || '') !== -1) &&
              (item.paymentmethod_name.toLowerCase().indexOf(filterValues.paymentmethod_name || '') !== -1) &&
              (item.currency_name.toLowerCase().indexOf(filterValues.currency_name || '') !== -1) &&
              (item.beneficiarybank_address1.toLowerCase().indexOf(filterValues.beneficiarybank_address1 || '') !== -1) &&
              (item.internal_reference.toLowerCase().indexOf(filterValues.internal_reference || '') !== -1);
          });
        }else{
          return data;
        }
      };
//      console.log(vm.beneficiaries)
      vm.tableParams = new NgTableParams({}, {
        counts: [5, 10, 20, 50, 100],
        dataset : vm.beneficiaries,
        filterOptions: {
          filterFn: vm.filterAll
        }
      });

      vm.searchFilterFunction = function(){
        vm.tableParams.filter({ $: vm.filter.global_search });
      };
      vm.dateRangeFilter = function(){
        var filteredData = $filter('dateRangeFilter')(vm.beneficiaries, vm.filter.date_range);
        vm.tableParams = new NgTableParams({}, {
          counts: [5, 10, 20, 50, 100],
          dataset : filteredData
        });
      };
      vm.filterAll = function(){

        vm.tableParams.filter({
          name: vm.filter.name,
          paymentmethod_name: vm.filter.method,
          currency_name: vm.filter.currency,
          beneficiarybank_address1: vm.filter.address1,
          internal_reference: vm.filter.reference,
          beneficiary_accountcode:vm.filter.accountNumber
        });
      };
      updatePageCount();
    });
    vm.moveFirstToLast = function(firstOrLast){
      if(firstOrLast === 'last')
      {
        var lastPage = updatePageCount();
        vm.tableParams.page(lastPage);
        return;
      }
      vm.tableParams.page(1);

    };


    function updatePageCount(){
      return Math.ceil(vm.tableParams.total()/vm.tableParams.count());
    }

    vm.addNewBeneficiary = function(){
      $location.url('/add_beneficiary');
    };

  }
})();*/
