(function ()
{
  'use strict';

  angular
    .module('app.beneficiaries.view_beneficiaries', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider)
  {
    // State
    $stateProvider.state('app.view_beneficiary', {
      url      : '/view_beneficiaries',
      views    : {
        'content@app': {
          templateUrl: 'app/main/beneficiaries/view_beneficiary/view_beneficiary.html',
          controller : 'ViewBeneficiaryController as vm',
          resolve: {
            access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
          },
        }
      }
    });
  }

})();
