(function () {
  'use strict';

  angular
    .module('app.beneficiaries', ['app.beneficiaries.view_beneficiaries', 'app.beneficiaries.add_beneficiary'])
    .config(config);

  /** @ngInject */
  function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) {
    // State
    /*$stateProvider
      .state('app.beneficiaries', {
        url: '/beneficiaries',
        views: {
          'content@app': {
            templateUrl: 'app/main/beneficiaries/beneficiaries.html',
            controller: 'BeneficiariesController as vm'
          }
        }
      });*/

    // Translation
    //$translatePartialLoaderProvider.addPart('app/main/beneficiaries');

    // Api
    //msApiProvider.register('sample', ['app/data/payments/payments.json']);



   msNavigationServiceProvider.saveItem('main_menu.beneficiaries', {
      title: 'Beneficiaries',
      icon: 'icon-chart-pie',
      weight: 1,
      child_count: 3
    });

    msNavigationServiceProvider.saveItem('main_menu.beneficiaries.add_draft_beneficiary', {
      title: 'Add a Draft  Beneficiary',
      state: 'app.add_draft_beneficiary',
      weight: 1,
      module_id: 13576
    });


    msNavigationServiceProvider.saveItem('main_menu.beneficiaries.view_beneficiary', {
      title: 'View Existing Beneficiary',
      state: 'app.view_beneficiary',
      weight: 1,
      module_id: 11406
    });


    msNavigationServiceProvider.saveItem('main_menu.beneficiaries.add_beneficiary', {
      title: 'Add a Beneficiary',
      state: 'app.add_beneficiary',
      weight: 1,
      module_id: 11406
    });
  }
})();
