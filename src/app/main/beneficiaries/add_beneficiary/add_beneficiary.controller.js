(function () {
  'use strict';

  angular
    .module('app.beneficiaries.add_beneficiary')
    .controller('AddBeneficiaryController', AddBeneficiaryController);

  /** @ngInject */
  function AddBeneficiaryController(api, $scope, $mdToast, $mdDialog, messageService, $interval, $rootScope, $q, $compile, soap_api, QueryFactory, XmlJson) {
    var vm = this;
    $rootScope.headerTitle = 'Add Beneficiary';
    vm.circleLoader = false;
    vm.customFullscreen = true;
    vm.disableAllFields = false;
    vm.showApprove = false;
    vm.progressing = false;
    vm.module_id = 11406;
    vm.showUpdate = false;
    vm.step1Invalid = false;
    vm.step1AutoInvalid = false;



    //global methods
    vm.parseBoolean = function(str){
      return str == 'true' ? true: false;
    };
    vm.parseFloat = function(number){
      return parseFloat(number);
    };
    vm.toLowerCase = function(str){
      str = str || '';
      return str.toLowerCase();
    };
    vm.toUpperCase = function(str){
      str = str || '';
      return str.toUpperCase();
    };
    vm.isXML = function(xml){
      try{
        var xmlDoc = jQuery.parseXML(xml);
        return true;
      }catch(error){
        return false;
      }
    };
    vm.includes = function(string, substring){
      string = string || '';
      substring = substring || ''
      if( string.toLowerCase().indexOf(substring.toLowerCase()) !== -1){
        return true;
      }else{
        return false;
      }
    };
    vm.replaceWith = function(string, separator){
      string = string || '';
      return string.replace(/ +/g, separator);
    };
    vm.floor = function(expression){
      return Math.floor(expression);
    };
    vm.protectAmount = function(event){
      var input = event.key;
      var re = /\d/i;
      if(input != 'Backspace' && input != '.' && input != 'Delete' && input != 'Del' && input != 'Decimal' && input != 'Left' && input != 'Right' && input != 'Tab' && input != 'ArrowLeft' && input != 'ArrowRight'){
        if(input.match(re) == null){
          event.preventDefault();
          return false;
        }
      }else{
        if(input == '.' || input == 'Decimal'){
          re = /\./g;
          if(event.target.value.length > 0){
            if(event.target.value.match(/\./g) != null){
              if(event.target.value.match(/\./g).length > 0){
                event.preventDefault();
                return false;
              }
            }
          }
        }
      }
    };
    vm.onlyWords = function(event){
      var input = event.key;
      var re = /^[a-zA-Z\s]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.WordsAndNumbers = function(event){
      var input = event.key;
      var re = /^[a-zA-Z0-9\s]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.OnlyContacts = function(event){
      var input = event.key;
      var re = /^[0-9\s\-]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.noSpecialCharactersFunction = function(event){
      var input = event.key;
      var re = /^[a-zA-Z0-9\-\s]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.validateEmail = function(event){
      var input = event.key;
      var re = /^[a-zA-Z0-9@.]+$/g;
      if(event.key.match(re) == null){
        event.preventDefault();
        return false;
      }
    };
    vm.parseDate = function parseDate(input) {
      console.log(typeof input);
      var parts = input.split('-');
      // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
      return new Date(parts[0], parts[1]-1, parts[2]); // Note: months are 0-based
    };
    vm.pasteLimit = function(event, limit){
      var length = event.originalEvent.clipboardData.getData('text/plain').length + event.originalEvent.target.value.length;
      if(length > limit){
        event.preventDefault();
      }
    };
    vm.stopInputing = function(event){
      event.preventDefault();
    };
    vm.limitLength = function(event, limit){
      if(event.target.value.length == limit){
        event.preventDefault();
      }
    };


    //api method
    api.GetEntity().then(function(data){
      vm.pay_to = data.output.entity.name;
    });
    vm.GetFundingCurrencies = function(){
      return soap_api.GetFundingCurrencies().then(function(success){
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            buy_item_id: jQuery(e).find('BUY_ITEM_ID').text(),
            item_name: jQuery(e).find('ITEM_NAME').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            curr_rate_conversion_type: jQuery(e).find('CURR_RATE_CONVERSION_TYPE').text(),
            currency_price_rounding: jQuery(e).find('CURRENCY_PRICE_ROUNDING').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.GetPurposeOfPayments = function(){
      return soap_api.GetPurposeOfPayments().then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT PURPOSE_OF_PAYMENT').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            name: jQuery(e).find('NAME').text(),
            description: jQuery(e).find('DESCRIPTION').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            is_approved: jQuery(e).find('IS_APPROVED').text(),
            version_no: jQuery(e).find('VERSION_NO').text()
          };
          row.push(temp);
        });
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.GetEntityInformation = function(){
      return soap_api.GetEntityInformation().then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT BENEFICIARY_INFO').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              parent_id: jQuery(e).find('PARENT_ID').text(),
              parent_type: jQuery(e).find('PARENT_TYPE').text(),
              address_type: jQuery(e).find('ADDRESS_TYPE').text(),
              priority: jQuery(e).find('PRIORITY').text(),
              street_address: jQuery(e).find('STREET_ADDRESS').text(),
              post_code: jQuery(e).find('POST_CODE').text(),
              city: jQuery(e).find('CITY').text(),
              province: jQuery(e).find('PROVINCE').text(),
              countryid: jQuery(e).find('COUNTRYID').text(),
              is_default: jQuery(e).find('IS_DEFAULT').text(),
              created_on: jQuery(e).find('CREATED_ON').text(),
              created_by: jQuery(e).find('CREATED_BY').text(),
              updated_on: jQuery(e).find('UPDATED_ON').text(),
              updated_by: jQuery(e).find('UPDATED_BY').text(),
              is_deleted: jQuery(e).find('IS_DELETED').text(),
              is_approved: jQuery(e).find('IS_APPROVED').text(),
              is_web_approved: jQuery(e).find('IS_WEB_APPROVED').text(),
              version_no: jQuery(e).find('VERSION_NO').text(),
              country_name: jQuery(e).find('COUNTRY_NAME').text(),
              name: jQuery(e).find('NAME').text(),
              phone_number: jQuery(e).find('PHONE_NUMBER').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetCountries = function(){
      return soap_api.GetCountries().then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT COUNTRY').each(function(i,e){
            var temp = {
              country_id: jQuery(e).find('COUNTRY_ID').text(),
              country_name: jQuery(e).find('COUNTRY_NAME').text(),
              continent_id: jQuery(e).find('CONTINENT_ID').text(),
              country_code: jQuery(e).find('COUNTRY_CODE').text(),
              is_watch_list: jQuery(e).find('IS_WATCH_LIST').text(),
              created_on: jQuery(e).find('CREATED_ON').text(),
              created_by: jQuery(e).find('CREATED_BY').text(),
              updated_on: jQuery(e).find('UPDATED_ON').text(),
              updated_by: jQuery(e).find('UPDATED_BY').text(),
              is_deleted: jQuery(e).find('IS_DELETED').text(),
              is_approved: jQuery(e).find('IS_APPROVED').text(),
              version_no: jQuery(e).find('VERSION_NO').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetAllCurrencies = function(){
      return soap_api.GetAllCurrencies().then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT CURRENCY').each(function(i,e){
            var temp = {
              currency_id: jQuery(e).find('CURRENCY_ID').text(),
              currency_name: jQuery(e).find('CURRENCY_NAME').text(),
              currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
              currency_detail: jQuery(e).find('CURRENCY_DETAIL').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetIndividualIdentificationTypes = function(){
      return soap_api.GetIndividualIdentificationTypes().then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT INDIVIDUAL_IDENTIFICATION_TYPE').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              name: jQuery(e).find('NAME').text(),
              description: jQuery(e).find('DESCRIPTION').text(),
              is_corporate: jQuery(e).find('IS_CORPORATE').text(),
              is_individual: jQuery(e).find('IS_INDIVIDUAL').text(),
              created_on: jQuery(e).find('CREATED_ON').text(),
              created_by: jQuery(e).find('CREATED_BY').text(),
              updated_on: jQuery(e).find('UPDATED_ON').text(),
              updated_by: jQuery(e).find('UPDATED_BY').text(),
              is_deleted: jQuery(e).find('IS_DELETED').text(),
              is_approved: jQuery(e).find('IS_APPROVED').text(),
              version_no: jQuery(e).find('VERSION_NO').text(),
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetRoutingCodeList = function(filter, type, page_number, page_size){

      return soap_api.GetRoutingCodeList(filter, type, page_number, page_size).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT PAGING').each(function(i,e){
            var temp = {
              page_number: jQuery(e).find('PAGE_NUMBER').text(),
              page_size: jQuery(e).find('PAGE_SIZE').text(),
              record_count: jQuery(e).find('RECORD_COUNT').text(),
            };
            row.push(temp);
          });
          jQuery(response).find('OUTPUT ROUTINGCODE').each(function(i,e){
            if(jQuery(e).children().length > 0){
              var temp = {
                rownumber: jQuery(e).find('ROWNUMBER').text(),
                bank_code: jQuery(e).find('BANK_CODE').text(),
                name: jQuery(e).find('NAME').text(),
                routingcode: jQuery(e).find('ROUTINGCODE').text(),
                routingtype: jQuery(e).find('ROUTINGTYPE').text(),
                country: jQuery(e).find('COUNTRY').text(),
                country_name: jQuery(e).find('COUNTRY_NAME').text(),
                country_code: jQuery(e).find('COUNTRY_CODE').text(),
                is_approved: jQuery(e).find('IS_APPROVED').text(),
                routingcodeid: jQuery(e).find('ROUTINGCODEID').text(),
                address1: jQuery(e).find('ADDRESS1').text(),
                city: jQuery(e).find('CITY').text(),
                country_id: jQuery(e).find('COUNTRY_ID').text(),
                province: jQuery(e).find('PROVINCE').text(),
                postcode: jQuery(e).find('POSTCODE').text(),
                id: jQuery(e).find('ID').text()
              };
              row.push(temp);
            }
          });
          return row;
        }
      });
    };
    vm.GetRoutingTypesList = function(){
      return soap_api.GetRoutingTypesList().then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT ROUTINGTYPE').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              name: jQuery(e).find('NAME').text(),
              is_bank_code_enabled: jQuery(e).find('IS_BANK_CODE_ENABLED').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetBeneficiaryAccountTypes = function(){
      return soap_api.GetBeneficiaryAccountTypes().then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT ACCOUNT_TYPE').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              name: jQuery(e).find('NAME').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetProvinceByCityName = function(city){
      return soap_api.GetProvinceByCityName(city).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT PROVINCE').each(function(i,e){
            var temp = {
              province_name: jQuery(e).find('Province_Name').text(),
              country_id: jQuery(e).find('Country_ID').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.CreateWire = function(beneficiary, beneficiary_bank, intermediary_bank, further_credit_detail, by_order_of, additional, saveOrUpdate){
      if(saveOrUpdate === 'save'){
        return soap_api.CreateWire(beneficiary, beneficiary_bank, intermediary_bank, further_credit_detail, by_order_of, additional).then(function(success){
          if(vm.isXML(success.data)){
            var response = jQuery.parseXML(success.data)
            var row = [];
            jQuery(response).find('OUTPUT RESULT').each(function(i,e){
              var temp = {
                status: jQuery(e).find('STATUS').text(),
                recordid: jQuery(e).find('RECORDID').text(),
                error: jQuery(e).find('ERROR').text(),
                recordnumber: jQuery(e).find('RECORDNUMBER').text()
              };
              row.push(temp);
            });
            return row;
          }
        });

      }
      return soap_api.updateWire(beneficiary, beneficiary_bank, intermediary_bank, further_credit_detail, by_order_of, additional).then(function(success){
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data)
          var row = [];
          jQuery(response).find('OUTPUT RESULT').each(function(i,e){
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              recordid: jQuery(e).find('RECORDID').text(),
              error: jQuery(e).find('ERROR').text(),
              recordnumber: jQuery(e).find('RECORDNUMBER').text(),

            };
            temp.method = 'update';
            row.push(temp);
          });
          return row;
        }
      });


    };
    vm.GetAllCitiesByCountryAndProvince = function(country, province){
      return soap_api.GetAllCitiesByCountryAndProvince(country, province).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT CITY').each(function(i,e){
            var temp = {
              city_id: jQuery(e).find('CITY_ID').text(),
              city_name: jQuery(e).find('CITY_NAME').text(),
              country_id: jQuery(e).find('COUNTRY_ID').text(),
              province_id: jQuery(e).find('PROVINCE_ID').text(),
              is_approved: jQuery(e).find('IS_APPROVED').text(),
              continent_id: jQuery(e).find('CONTINENT_ID').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetAllProvinces = function(){
      return soap_api.GetAllProvinces().then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT PROVINCE').each(function(i,e){
            var temp = {
              province_id: jQuery(e).find('PROVINCE_ID').text(),
              province_name: jQuery(e).find('PROVINCE_NAME').text()
            };
            row.push(temp);
          });
          return row;
        }
      });
    };
    vm.GetUserApprovalRights = function(deal, module){
      return soap_api.GetUserApprovalRights(deal, module).then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text()
          };
          row.push(temp);
        });
        return row;
      });
    };
    vm.GetSavedApprovalsByModule = function(module){
      return soap_api.GetSavedApprovalsByModule(module).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT ADM_WEB_APPROVAL_POLICY').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              entity_name: jQuery(e).find('ENTITY_NAME').text(),
              entity_id: jQuery(e).find('ENTITY_ID').text(),
              module_id: jQuery(e).find('MODULE_ID').text(),
              approval_level: jQuery(e).find('APPROVAL_LEVEL').text(),
              created_on: jQuery(e).find('CREATED_ON').text(),
              created_by: jQuery(e).find('CREATED_BY').text(),
              updated_on: jQuery(e).find('UPDATED_ON').text(),
              updated_by: jQuery(e).find('UPDATED_BY').text(),
              is_deleted: jQuery(e).find('IS_DELETED').text(),
              is_approved: jQuery(e).find('IS_APPROVED').text(),
              version_no: jQuery(e).find('VERSION_NO').text()
            };
            row.push(temp);
          });
          jQuery(response).find('OUTPUT ADM_WEB_USER_APPROVAL').each(function(i,e){
            var temp = {
              id: jQuery(e).find('ID').text(),
              approval_policy_id: jQuery(e).find('APPROVAL_POLICY_ID').text(),
              user_id: jQuery(e).find('USER_ID').text(),
              user_name: jQuery(e).find('USER_NAME').text(),
              approval_type_id: jQuery(e).find('APPROVAL_TYPE_ID').text(),
              approval_type_name: jQuery(e).find('APPROVAL_TYPE_NAME').text(),
              created_on: jQuery(e).find('CREATED_ON').text(),
              created_by: jQuery(e).find('CREATED_BY').text(),
              updated_on: jQuery(e).find('UPDATED_ON').text(),
              updated_by: jQuery(e).find('UPDATED_BY').text(),
              is_deleted: jQuery(e).find('IS_DELETED').text(),
              is_approved: jQuery(e).find('IS_APPROVED').text(),
              version_no: jQuery(e).find('VERSION_NO').text()
            };
            row.push(temp);
          });
        }else{
          console.log(success.data);
        }
        return row;
      });
    };
    vm.GetSavedApprovalsByModule(vm.module_id).then(function(data){
      vm.approvalPolicy = data;
      if(data[0].approval_level == 0 || data[0].approval_level == ''){
        vm.skipApprovalStep = true;
      }else{
        vm.skipApprovalStep = false;
      }
    });
    vm.GetApprovalHistory = function(deal){
      return soap_api.GetApprovalHistory(deal).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT APPROVAL_HISTORY').each(function(i,e){
            var temp = {
              trans_approval_id: jQuery(e).find('TRANS_APPROVAL_ID').text(),
              org_approval_id: jQuery(e).find('ORG_APPROVAL_ID').text(),
              transaction_id: jQuery(e).find('TRANSACTION_ID').text(),
              user_id: jQuery(e).find('USER_ID').text(),
              user_name: jQuery(e).find('USER_NAME').text(),
              approval_num: jQuery(e).find('APPROVAL_NUM').text(),
              approval_date: jQuery(e).find('APPROVAL_DATE').text(),
              approval_level: jQuery(e).find('APPROVAL_LEVEL').text(),
              created_on: jQuery(e).find('CREATED_ON').text(),
              created_by: jQuery(e).find('CREATED_BY').text(),
              updated_on: jQuery(e).find('UPDATED_ON').text(),
              updated_by: jQuery(e).find('UPDATED_BY').text(),
              version_no: jQuery(e).find('VERSION_NO').text(),
              is_deleted: jQuery(e).find('IS_DELETED').text()
            };
            row.push(temp);
          });
        }else{
          console.log(success.data);
        }
        return row;
      });
    };
    vm.SetDealApprovalWithID = function(deal){
      return soap_api.SetDealApprovalWithID(deal).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function(i,e){
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              error: jQuery(e).find('ERROR').text()
            };
            row.push(temp);
          });
        }else{
          console.log(success.data);
        }
        return row;
      });
    };
    vm.SetWireApproval = function(wire){
      return soap_api.SetWireApproval(wire).then(function(success){
        var row = [];
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function(i,e){
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              error: jQuery(e).find('ERROR').text()
            };
            row.push(temp);
          });
        }else{
          console.log(success.data);
        }
        return row;
      });
    };
    vm.entity = [];
    vm.step1_cities = [];
    vm.step2_cities = [];
    vm.step3_cities = [];
    vm.step4_cities = [];
    vm.step5_cities = [];
    vm.entityInformation = [];
    vm.selectedIndex = 0;
    vm.max = 3;
    vm.activated = true;
    vm.step2_provinces = [];
    vm.provinces = [];
    vm.step2_intermediary_provinces = [];
    vm.currencies = [];
    vm.identifications = [];
    vm.routings = [];
    vm.beneficiary_account_types = [];
    //step1
    vm.step1 = {
      beneficiary_name : null,
      account_type: null,
      account_no: null,
      beneficiary_instruction: null,
      auto_by_order_of: false,
      beneficiary_is_person: null,
      pay_to: false,
      currency: null,
      beneficiary_email: null,
      send_bank_instruction: null,
      address1: null,
      city: null,
      country: null,
      address2: null,
      province: null,
      postal_code: null,
    };
    vm.step1_is_person = {
      contact_number: null,
      occupation: null,
      identification_number: null,
      date_of_birth: null,
      identification_type: null
    };
    vm.step1_auto_by_order_of = {
      name: null,
      address1: null,
      city: null,
      country: null,
      address2: null,
      province: null,
      postal_code: null
    };
    //step2
    vm.step2 = {
      name: null,
      routing_code: null,
      routing_code_id: null,
      bank_code: null,
      address1: null,
      city: null,
      country: null,
      address2: null,
      province: null,
      postal_code: null,
      intermediary_bank: false
    };
    vm.step2_intermediary = {
      routing_code: null,
      routing_code_id: null,
      name: null,
      address1: null,
      city: null,
      country: null,
      account_no: null,
      routing_type: null,
      bank_code: null,
      address2: null,
      province: null,
      postal_code: null
    };
    //step3
    vm.step3 = {
      payment_purpose: null,
      payment_purpose_detail: null
    };
    //step4
    vm.step4 = {
      name: null,
      address1: null,
      city: null,
      country: null,
      address2: null,
      province: null,
      postal_code: null,
      account_number: null
    };
    vm.currencies = [];
    vm.GetCountries().then(function(data){
      vm.countries = data;
      vm.circleLoader = true;
    });
    vm.GetAllProvinces().then(function(data){
      vm.provinces = data;
      /*vm.step2_provinces = vm.provinces;
      vm.step2_intermediary_provinces = vm.step2_provinces;*/
      vm.circleLoader = true;
    });
    vm.GetFundingCurrencies().then(function(data){
      vm.currencies = data;
    });
    vm.fetchAccountTypes = function(){
      if(vm.beneficiary_account_types.length == 0){
        vm.GetBeneficiaryAccountTypes().then(function(data){
          vm.beneficiary_account_types = data;
        });
      }
    };
    vm.fetchIdentification = function(){
      if(vm.identifications.length == 0){
        vm.GetIndividualIdentificationTypes().then(function(data){
          vm.identifications = data;
        });
      }
    };
    vm.GetRoutingTypesList().then(function(data){
      vm.routings = data;
    });
    vm.GetRoutingCodeList(true, false, 1, 50).then(function(data){

      vm.routing_pagination = data.shift();
      vm.routing_codes = data;
    });
    vm.GetBeneficiaryAccountTypes().then(function(data){
      vm.beneficiary_account_types = data;
    });
    vm.GetPurposeOfPayments().then(function(data){
      vm.paymentPurposes = data;
    });
    vm.GetEntityInformation().then(function(data){
      vm.entity = data[0];
    });
    /*vm.GetFundingCurrencies().then(function(data){
      vm.currencies = data;
    });
    vm.GetPurposeOfPayments().then(function(data){
      vm.paymentPurposes = data;
    });
    vm.GetEntityInformation().then(function(data){
      vm.entity = data[0];
    });
    vm.GetCountries().then(function(data){
      vm.countries = data;
      vm.circleLoader = true;
    });
    vm.GetAllProvinces().then(function(data){
      vm.provinces = data;
      vm.step2_provinces = data;
      vm.step2_intermediary_provinces = data;
      vm.circleLoader = true;
    });
    vm.GetAllCurrencies().then(function(data){
      vm.currencies = data;
    });
    vm.GetIndividualIdentificationTypes().then(function(data){
      vm.identifications = data;
    });
    vm.GetRoutingTypesList().then(function(data){
      vm.routings = data;
    });
    vm.GetRoutingCodeList(true, false, 1, 1000).then(function(data){
      vm.routing_pagination = data.shift();
      vm.routing_codes = data;
    });
    vm.GetBeneficiaryAccountTypes().then(function(data){
      vm.beneficiary_account_types = data;
    });*/
    //fetch cities based upon country and cities
    vm.fetchCitiesStep1_1 = function(data){

      if(data.country != null && data.province != null){
        vm.step1_cities = [];
        vm.GetAllCitiesByCountryAndProvince(data.country, data.province).then(function(data){
          vm.step1_cities = data;
          if(data.length == 0){
            $mdToast.show({
              template: '<md-toast class="md-toast error">Cities not found.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
          }
        });
      }
      else if(data.country == null && data.province == null){
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please select country and province.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
      }
    };
    vm.fetchCitiesStep1_2 = function(data){
      if(data.country != null && data.province != null){
        vm.step2_cities = [];
        vm.GetAllCitiesByCountryAndProvince(data.country, data.province).then(function(data){
          vm.step2_cities = data;
          if(data.length == 0){
            $mdToast.show({
              template: '<md-toast class="md-toast error">Cities not found.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
          }
        });
      }
      else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please select country and province.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
      }
    };
    vm.fetchCitiesStep2_1 = function(data){
      if(data.country != null && data.province != null){
        vm.step3_cities = [];
        vm.GetAllCitiesByCountryAndProvince(data.country, data.province).then(function(data){
          vm.step3_cities = data;
          if(data.length == 0){
            $mdToast.show({
              template: '<md-toast class="md-toast error">Cities not found.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
          }
        });
      }else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please select country and province.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
      }
    };
    vm.fetchCitiesStep2_2 = function(data){
      if(data.country != null && data.province != null){
        vm.step4_cities = [];
        vm.GetAllCitiesByCountryAndProvince(data.country, data.province).then(function(data){
          vm.step4_cities = data;
          if(data.length == 0){
            $mdToast.show({
              template: '<md-toast class="md-toast error">Cities not found.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
          }
        });
      }else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please select country and province.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
      }
    };
    vm.fetchCitiesStep4_1 = function(data){
      if(data.country != null && data.province != null){
        vm.step5_cities = [];
        vm.GetAllCitiesByCountryAndProvince(data.country, data.province).then(function(data){
          vm.step5_cities = data;
          if(data.length == 0){
            $mdToast.show({
              template: '<md-toast class="md-toast error">Cities not found.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
          }
        });
      }else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please select country and province.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
      }
    };
    //fetch province name based on city
    vm.fetchProvinceStep1_1 = function(data){
      var city = _.filter(vm.cities, {city_id: data.city});
      if(city.length > 0){
        vm.step1_province = [];
        vm.GetProvinceByCityName(city[0].city_name).then(function(data){
          vm.step1_province = data;
          if(data.length == 0){
            $mdToast.show({
              template: '<md-toast class="md-toast error">Cities not found.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
          }
        });
      }else{
        vm.step1_province = [];
      }
    };
    vm.fetchProvinceStep1_2 = function(data){
      if(vm.step1_province.length == 0){
        var city = _.filter(vm.cities, {city_id: data.city});
        if(city.length > 0){
          vm.step1_province = [];
          vm.GetProvinceByCityName(city[0].city_name).then(function(data){
            vm.step1_province = data;
            if(data.length == 0){
              $mdToast.show({
                template: '<md-toast class="md-toast error">Cities not found.</md-toast>',
                hideDelay: 4000,
                position: 'bottom right'
              });
            }
          });
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">City name is not selected.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.step1_province = [];
        }
      }
    };
    vm.fetchProvinceStep1AutoByOrderOf_1 = function(data){
      var city = _.filter(vm.cities, {city_id: data.city});
      if(city.length > 0){
        vm.step1_auto_by_order_of_province = [];
        vm.GetProvinceByCityName(city[0].city_name).then(function(data){
          vm.step1_auto_by_order_of_province = data;
        });
      }else{
        vm.step1_auto_by_order_of_province = [];
      }
    };
    vm.fetchProvinceStep1AutoByOrderOf_2 = function(data){
      if(vm.step1_auto_by_order_of_province.length == 0){
        var city = _.filter(vm.cities, {city_id: data.city});
        if(city.length > 0){
          vm.step1_auto_by_order_of_province = [];
          vm.GetProvinceByCityName(city[0].city_name).then(function(data){
            vm.step1_auto_by_order_of_province = data;
          });
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">City name is not selected.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.step1_auto_by_order_of_province = [];
        }
      }
    };
    vm.fetchProvinceStep2_1 = function(data){
      var city = _.filter(vm.cities, {city_id: data.city});
      if(city.length > 0){
        vm.step2_province = [];
        vm.GetProvinceByCityName(city[0].city_name).then(function(data){
          vm.step2_province = data;
        });
      }else{
        vm.step2_province = [];
      }
    };
    vm.fetchProvinceStep2_2 = function(data){
      if(vm.step2_province.length == 0){
        var city = _.filter(vm.cities, {city_id: data.city});
        if(city.length > 0){
          vm.step2_province = [];
          vm.GetProvinceByCityName(city[0].city_name).then(function(data){
            vm.step2_province = data;
          });
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">City name is not selected.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.step2_province = [];
        }
      }
    };
    vm.fetchProvinceIntermediary_1 = function(data){
      var city = _.filter(vm.cities, {city_id: data.city});
      if(city.length > 0){
        vm.step2_intermediary_province = [];
        vm.GetProvinceByCityName(city[0].city_name).then(function(data){
          vm.step2_intermediary_province = data;
        });
      }else{
        vm.step2_intermediary_province = [];
      }
    };
    vm.fetchProvinceIntermediary_2 = function(data){
      if(vm.step2_intermediary_province.length == 0){
        var city = _.filter(vm.cities, {city_id: data.city});
        if(city.length > 0){
          vm.step2_intermediary_province = [];
          vm.GetProvinceByCityName(city[0].city_name).then(function(data){
            vm.step2_intermediary_province = data;
          });
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">City name is not selected.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.step2_intermediary_province = [];
        }
      }
    };
    vm.fetchProvinceStep4_1 = function(data){
      var city = _.filter(vm.cities, {city_id: data.city});
      if(city.length > 0){
        vm.step4_province = [];
        vm.GetProvinceByCityName(city[0].city_name).then(function(data){
          vm.step4_province = data;
        });
      }else{
        vm.step4_province = [];
      }
    };
    vm.fetchProvinceStep4_2 = function(data){
      if(vm.step4_province.length === 0){
        var city = _.filter(vm.cities, {city_id: data.city});
        if(city.length > 0){
          vm.step4_province = [];
          vm.GetProvinceByCityName(city[0].city_name).then(function(data){
            vm.step4_province = data;
          });
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">City name is not selected.</md-toast>',
            hideDelay: 4000,
            position: 'bottom right'
          });
          vm.step4_province = [];
        }
      }
    };

    vm.fillEntityFields = function(){
      if(vm.step1.pay_to === false){
        vm.GetEntityInformation().then(function(data){
          vm.entityInformation = data[0];
          vm.step1.beneficiary_name = vm.entityInformation.name;
          vm.step1.postal_code = vm.entityInformation.post_code;
          vm.step1.country = vm.entityInformation.country_name;
          vm.step1.province = vm.entityInformation.province;
          vm.step1.city = vm.entityInformation.city;
          console.log("City");
          console.log(vm.entityInformation.city);
          console.log("City!");
          console.log(vm.step1.city);
          vm.step1_cities.push({city_name: vm.entityInformation.city});
          console.log("Cities");
          console.log(vm.step1_cities);
          vm.step1.address1 = vm.entityInformation.street_address;
          vm.step1_is_person.contact_number = vm.entityInformation.phone_number;
        });
      }else{
        vm.step1.beneficiary_name = null;
        vm.step1.postal_code = null;
        vm.step1.country = null;
        vm.step1.province = null;
        vm.step1.city = null;
        vm.step1.address1 = null;
        vm.step1_is_person.contact_number = null;
      }
    };
    vm.autoFillOrderOf  = function(){
      if(vm.step1.auto_by_order_of === false){
        vm.GetEntityInformation().then(function(data){
          vm.entityInformation = data[0];
          vm.step1_auto_by_order_of.name = vm.entityInformation.name;
          vm.step1_auto_by_order_of.postal_code = vm.entityInformation.post_code;
          vm.step1_auto_by_order_of.country = vm.entityInformation.country_name;
          vm.step1_auto_by_order_of.province = vm.entityInformation.province;
          vm.step1_auto_by_order_of.city = vm.entityInformation.city;
          vm.step2_cities.push({city_name: vm.entityInformation.city});
          vm.step1_auto_by_order_of.address1 = vm.entityInformation.street_address;
        });
      }else{
        vm.step1_auto_by_order_of.name = null;
        vm.step1_auto_by_order_of.postal_code = null;
        vm.step1_auto_by_order_of.country = null;
        vm.step1_auto_by_order_of.province = null;
        vm.step1_auto_by_order_of.city = null;
        vm.step1_auto_by_order_of.address1 = null;
        vm.step2_cities = [];
      }
    };
    //save beneficiary data
    vm.saveBeneficiary = function(){
      var validation = true;
      vm.circleLoader = false;

    };
    vm.saveWire = function(){
      createUpdateWire(vm.entity.parent_id, 'save');
      //validateWire();
    };
    //update wire
    vm.updateWire = function(){
      createUpdateWire(vm.beneficiary_deal.recordid, 'update')
    };
    function createUpdateWire(id, saveUpdate){
      var saveOrUpdateId = id;
      var saveOrUpdate = saveUpdate;
      var validation = true;
      if(vm.step1.beneficiary_name == null || typeof vm.step1.beneficiary_name === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please provide beneficiary name</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });

      }
      if(vm.step1.address1 == null || typeof vm.step1.address1 === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please provide beneficiary address</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      if(vm.step1.city == null || typeof vm.step1.city === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please select city</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      if(vm.step1.country == null || typeof vm.step1.country === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please select country</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      if(vm.step1.account_no == null || typeof vm.step1.account_no === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please type account no.</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      if(vm.step1.currency == null || typeof vm.step1.currency === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please select currency</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      if(vm.step1.beneficiary_is_person == true){
        if(vm.step1_is_person.contact_number == null || typeof vm.step1_is_person.contact_number === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please type contact number.</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }

        if(vm.step1_is_person.occupation == null || typeof vm.step1_is_person.occupation === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please type person occupation.</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }

        if(vm.step1_is_person.identification_number == null || typeof vm.step1_is_person.identification_number === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please type identification number.</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }

        if(vm.step1_is_person.date_of_birth == null || typeof vm.step1_is_person.date_of_birth === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please pick date of birth.</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }

        if(vm.step1_is_person.identification_type == null || typeof vm.step1_is_person.identification_type === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please select identification type.</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }
      }
      // if(vm.step1.auto_by_order_of == true){
      if(vm.step1.auto_by_order_of == false){
        if(vm.step1_auto_by_order_of.name == null || typeof vm.step1_auto_by_order_of.name === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please type name</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }
        if(vm.step1_auto_by_order_of.address1 == null || typeof vm.step1_auto_by_order_of.address1 === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please type address</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }
        if(vm.step1_auto_by_order_of.city == null || typeof vm.step1_auto_by_order_of.city === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please select city</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }
        if(vm.step1_auto_by_order_of.country == null || typeof vm.step1_auto_by_order_of.country === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please select country</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }
        if(vm.step1_auto_by_order_of.province == null || typeof vm.step1_auto_by_order_of.province === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please select province</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }
      }
      //step2
      if(vm.step2.routing_code == null || typeof vm.step2.routing_code === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please type routing code</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      if(vm.step2.name == null || typeof vm.step2.name === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please type beneficiary bank name</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      if(vm.step2.address1 == null || typeof vm.step2.address1 === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please type beneficiary bank address 1</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      if(vm.step2.city == null || typeof vm.step2.city === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please select beneficiary bank city</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      if(vm.step2.country == null || typeof vm.step2.country === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please select beneficiary bank country</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      if(vm.step2.routing_type == null || typeof vm.step2.routing_type === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please type beneficiary bank routing type</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      if(vm.step2.intermediary_bank == true){
        if(vm.step2_intermediary.routing_code == null || typeof vm.step2_intermediary.routing_code === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please pick intermediary routing code</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }
        if(vm.step2_intermediary.name == null || typeof vm.step2_intermediary.name === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please type intermediary name</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }
        if(vm.step2_intermediary.address1 == null || typeof vm.step2_intermediary.address1 === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please type intermediary address 1</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }
        if(vm.step2_intermediary.city == null || typeof vm.step2_intermediary.city === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please select intermediary city</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }
        if(vm.step2_intermediary.country == null || typeof vm.step2_intermediary.country === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please select intermediary country</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }
        if(vm.step2_intermediary.account_no == null || typeof vm.step2_intermediary.account_no === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please select intermediary account no</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }
        if(vm.step2_intermediary.routing_type == null || typeof vm.step2_intermediary.routing_type === 'undefined'){
          validation = false;
          $mdToast.show({
            template: '<md-toast class="md-toast error">Please select intermediary routing type</md-toast>',
            hideDelay: 2500,
            position: 'bottom right'
          });
        }
      }
      //step3
      if(vm.step3.payment_purpose == null || typeof vm.step3.payment_purpose === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please select purpose of payment</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      /*if(vm.step3.payment_purpose_detail == null || typeof vm.step3.payment_purpose_detail === 'undefined'){
       validation = false;
       $mdToast.show({
       template: '<md-toast class="md-toast error">Please type purpose details</md-toast>',
       hideDelay: 2500,
       position: 'bottom right'
       });
       }*/
      //step4
      if(vm.step4.name == null || typeof vm.step4.name === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please type credit detail name</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      if(vm.step4.country == null || typeof vm.step4.country === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please select credit detail country</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      if(vm.step4.province == null || typeof vm.step4.province === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please select credit detail province</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      if(vm.step4.city == null || typeof vm.step4.city === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please select credit detail city</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }
      if(vm.step4.address1 == null || typeof vm.step4.address1 === 'undefined'){
        validation = false;
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please type credit detail address 1</md-toast>',
          hideDelay: 2500,
          position: 'bottom right'
        });
      }

      if(validation == true){

        /*var city_name = _.filter(vm.cities, { city_id : vm.step1.city });
         city_name = city_name[0].city_name;*/

        /*var type = _.filter(vm.beneficiary_account_types, { id : vm.step1.account_type });
         if(type.length == 0){
         type = '';
         }else{
         type = type[0].name;
         }*/
        var beneficiary = '';

        if(saveOrUpdate === 'update'){
          beneficiary += '<WIRE_ID>' + saveOrUpdateId + '</WIRE_ID>';
        }
        else{
          beneficiary += '<ENTITY_ID>' + saveOrUpdateId + '</ENTITY_ID>';
        }


        // beneficiary += '<ENTITY_ID>' + saveOrUpdateId + '</ENTITY_ID>';
        beneficiary += '<INTERNAL_REFERENCE>Internal</INTERNAL_REFERENCE>';
        beneficiary += '<BENEFICIARY_NAME>' + vm.step1.beneficiary_name + '</BENEFICIARY_NAME>';
        beneficiary += '<BENEFICIARY_NICKNAME>' + vm.entity.name + '</BENEFICIARY_NICKNAME>';
        beneficiary += '<IS_NON_THIRD_PARTY>0</IS_NON_THIRD_PARTY>';
        beneficiary += '<BENEFICIARY_ADDRESS1>' + vm.step1.address1 + '</BENEFICIARY_ADDRESS1>';
        beneficiary += '<BENEFICIARY_ADDRESS2>' + vm.step1.address2 + '</BENEFICIARY_ADDRESS2>';
        beneficiary += '<BENEFICIARY_CITY>' + vm.step1.city + '</BENEFICIARY_CITY>';
        beneficiary += '<BENEFICIARY_PROVINCE>' + vm.step1.province + '</BENEFICIARY_PROVINCE>';
        beneficiary += '<BENEFICIARY_COUNTRY><Value>' + vm.step1.country + '</Value></BENEFICIARY_COUNTRY>';
        beneficiary += '<BENEFICIARY_POSTCODE>' + vm.step1.postal_code + '</BENEFICIARY_POSTCODE>';
        beneficiary += '<BENEFICIARY_CURRENCY><ID>' + vm.step1.currency + '</ID></BENEFICIARY_CURRENCY>';
        beneficiary += '<BENEFICIARY_ACCOUNTCODE>' + vm.step1.account_no + '</BENEFICIARY_ACCOUNTCODE>';
        beneficiary += '<BENEFICIARY_EMAIL>' + vm.step1.beneficiary_email + '</BENEFICIARY_EMAIL>';
        beneficiary += '<BENEFICIARY_INSTRUCTIONS>' + vm.step1.beneficiary_instruction + '</BENEFICIARY_INSTRUCTIONS>';
        beneficiary += '<TYPE><Value>Outgoing</Value></TYPE>';
        beneficiary += '<STATUS><ID>1</ID></STATUS>';
        beneficiary += '<BENEFICIARY_SENDINGBANK_INSTRUCTIONS>' + vm.step1.send_bank_instruction + '</BENEFICIARY_SENDINGBANK_INSTRUCTIONS>';

        if(vm.step1.beneficiary_is_person == true){
          beneficiary += '<BENEFICIARY_CONTACT_NUMBER>'+vm.step1_is_person.contact_number+'</BENEFICIARY_CONTACT_NUMBER>';
          var value_date = new Date(Date.parse(vm.step1_is_person.date_of_birth));
          value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
          beneficiary += '<BENEFICIARY_INDIVIDUAL_DOB>'+value_date+'</BENEFICIARY_INDIVIDUAL_DOB>';
          beneficiary += '<BENEFICIARY_INDIVIDUAL_OCCUPATION>'+vm.step1_is_person.occupation+'</BENEFICIARY_INDIVIDUAL_OCCUPATION>';
          beneficiary += '<BENEFICIARY_INDIVIDUAL_ID_TYPE><ID>'+vm.step1_is_person.identification_type+'</ID></BENEFICIARY_INDIVIDUAL_ID_TYPE>';
          beneficiary += '<BENEFICIARY_INDIVIDUAL_ID_NUMBER>'+vm.step1_is_person.identification_number+'</BENEFICIARY_INDIVIDUAL_ID_NUMBER>';
        }

        var beneficiary_bank = '';
        beneficiary_bank += '<BENEFICIARYBANK_ROUTINGCODE>' + vm.step2.routing_code + '</BENEFICIARYBANK_ROUTINGCODE>';
        beneficiary_bank += '<BENEFICIARYBANK_ROUTINGCODE_ID>' + vm.step2.routing_code_id + '</BENEFICIARYBANK_ROUTINGCODE_ID>';
        beneficiary_bank += '<BENEFICIARYBANK_ROUTINGTYPE><Value>' + vm.step2.routing_type + '</Value></BENEFICIARYBANK_ROUTINGTYPE>';
        beneficiary_bank += '<BENEFICIARYBANK_BANK_CODE>' + vm.step2.bank_code + '</BENEFICIARYBANK_BANK_CODE>';
        beneficiary_bank += '<BENEFICIARYBANK_NAME>' + vm.step2.name + '</BENEFICIARYBANK_NAME>';
        beneficiary_bank += '<BENEFICIARYBANK_ADDRESS1>' + vm.step2.address1 + '</BENEFICIARYBANK_ADDRESS1>';
        beneficiary_bank += '<BENEFICIARYBANK_ADDRESS2>' + vm.step2.address2 + '</BENEFICIARYBANK_ADDRESS2>';
        beneficiary_bank += '<BENEFICIARYBANK_CITY>' + vm.step2.city + '</BENEFICIARYBANK_CITY>';
        beneficiary_bank += '<BENEFICIARYBANK_PROVINCE>' + vm.step2.province + '</BENEFICIARYBANK_PROVINCE>';
        beneficiary_bank += '<BENEFICIARYBANK_COUNTRY><Value>'+vm.step2.country+'</Value></BENEFICIARYBANK_COUNTRY>';
        beneficiary_bank += '<BENEFICIARYBANK_POSTCODE>'+vm.step2.postal_code+'</BENEFICIARYBANK_POSTCODE>';

        //if auto by order of is enabled
        var auto_by_order_of = '';
        if(vm.step1.auto_by_order_of == true){
          auto_by_order_of += '<BYORDEROF>';
          auto_by_order_of += '<BYORDEROF_ISBYORDEROF>1</BYORDEROF_ISBYORDEROF>';
          auto_by_order_of += '<BYORDEROF_NAME>'+vm.step1_auto_by_order_of.name+'</BYORDEROF_NAME>';
          auto_by_order_of += '<BYORDEROF_IS_INDIVIDUAL>False</BYORDEROF_IS_INDIVIDUAL>';
          auto_by_order_of += '<BYORDEROF_ADDRESS1>'+vm.step1_auto_by_order_of.address1+'</BYORDEROF_ADDRESS1>';
          auto_by_order_of += '<BYORDEROF_ADDRESS2>'+vm.step1_auto_by_order_of.address2+'</BYORDEROF_ADDRESS2>';
          auto_by_order_of += '<BYORDEROF_CITY>'+vm.step1_auto_by_order_of.city+'</BYORDEROF_CITY>';
          auto_by_order_of += '<BYORDEROF_PROVINCE>'+vm.step1_auto_by_order_of.province+'</BYORDEROF_PROVINCE>';
          auto_by_order_of += '<BYORDEROF_COUNTRY><Value>'+vm.step1_auto_by_order_of.country+'</Value></BYORDEROF_COUNTRY>';
          auto_by_order_of += '<BYORDEROF_POSTCODE>'+vm.step1_auto_by_order_of.postal_code+'</BYORDEROF_POSTCODE>';
          auto_by_order_of += '</BYORDEROF>';
        }
        //if intermediary is enabled
        var intermediary_bank = '';
        if(vm.step2.intermediary_bank == true){
          intermediary_bank += '<INTERMEDIARYBANK>'
          intermediary_bank += '<INTERBANK1_ROUTINGCODE>'+ vm.step2_intermediary.routing_code +'</INTERBANK1_ROUTINGCODE>';
          intermediary_bank += '<INTERBANK1_ROUTINGCODE_ID>'+ vm.step2_intermediary.routing_code_id +'</INTERBANK1_ROUTINGCODE_ID>';
          intermediary_bank += '<INTERBANK1_ROUTINGTYPE><Value>'+ vm.step2_intermediary.routing_type +'</Value></INTERBANK1_ROUTINGTYPE>';
          intermediary_bank += '<INTERBANK1_BANK_CODE>'+ vm.step2_intermediary.bank_code +'</INTERBANK1_BANK_CODE>';
          intermediary_bank += '<INTERBANK1_NAME>'+ vm.step2_intermediary.name +'</INTERBANK1_NAME>';
          intermediary_bank += '<INTERBANK1_ADDRESS1>'+ vm.step2_intermediary.address1 +'</INTERBANK1_ADDRESS1>';
          intermediary_bank += '<INTERBANK1_ADDRESS2>'+ vm.step2_intermediary.address2 +'</INTERBANK1_ADDRESS2>';
          intermediary_bank += '<INTERBANK1_CITY>'+ vm.step2_intermediary.city +'</INTERBANK1_CITY>';
          intermediary_bank += '<INTERBANK1_PROVINCE>'+ vm.step2_intermediary.name +'</INTERBANK1_PROVINCE>';
          intermediary_bank += '<INTERBANK1_COUNTRY><Value>'+vm.step2_intermediary.country+'</Value></INTERBANK1_COUNTRY>';
          intermediary_bank += '<INTERBANK1_POSTCODE>'+ vm.step2_intermediary.postal_code +'</INTERBANK1_POSTCODE>';
          intermediary_bank += '</INTERMEDIARYBANK>';
        }

        //additional details
        var additional_details = '';
        additional_details += '<IS_INDIVIDUAL>1</IS_INDIVIDUAL>';
        additional_details += '<PURPOSE_OF_PAYMENT_ID><ID>'+vm.step3.payment_purpose+'</ID></PURPOSE_OF_PAYMENT_ID>';
        additional_details += '<PURPOSE_OF_PAYMENT_DETAILS>'+vm.step3.payment_purpose_detail+'</PURPOSE_OF_PAYMENT_DETAILS>';




        //further credit details
        var further_credit_detail = '';
        further_credit_detail += '<FURTHER_NAME>'+vm.step4.name+'</FURTHER_NAME>';
        further_credit_detail += '<FURTHER_ADDRESS1>'+vm.step4.address1+'</FURTHER_ADDRESS1>';
        further_credit_detail += '<FURTHER_ADDRESS2>'+vm.step4.address2+'</FURTHER_ADDRESS2>';
        further_credit_detail += '<FURTHER_CITY>'+vm.step4.city+'</FURTHER_CITY>';
        further_credit_detail += '<FURTHER_PROVINCE>'+vm.step4.province+'</FURTHER_PROVINCE>';
        further_credit_detail += '<FURTHER_COUNTRY><Value>'+vm.step4.country+'</Value></FURTHER_COUNTRY>';
        further_credit_detail += '<FURTHER_POSTCODE>'+vm.step4.postal_code+'</FURTHER_POSTCODE>';
        further_credit_detail += '<FURTHER_ACCOUNTCODE>'+vm.step4.account_number+'</FURTHER_ACCOUNTCODE>';

        vm.circleLoader = false;
        //now create wire
        vm.CreateWire(beneficiary, beneficiary_bank, intermediary_bank, further_credit_detail, auto_by_order_of, additional_details, saveOrUpdate).then(function(data, saveOrUpdate){
          vm.circleLoader = true;

            showSuccess(data, saveOrUpdate)


        });
      }
    }
    function  showSuccess(data, message) {

      if(data[0].status === 'TRUE' && !data[0].method){
        $mdToast.show({
          template: '<md-toast class="md-toast success">Deal Created</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        })
        vm.showUpdate = true;
      }
      else if(data[0].status === 'TRUE' && data[0].method === 'update'){
        $mdToast.show({
          template: '<md-toast class="md-toast success">Deal Updated</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        })
      }
      else{
        $mdToast.show({
          template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        vm.disableAllFields = false;
        return;
      }
      vm.beneficiary_deal = data[0];
      vm.beneficiary_deal.id = vm.beneficiary_deal.recordid;
      vm.showApprove = true;
    }
    vm.nextStep = function(){
      var index = (vm.selectedIndex === vm.max) ? 0 : vm.selectedIndex + 1;
      vm.selectedIndex = index;
    };
    vm.clearIntermediary = function(){
      if(vm.step2.intermediary_bank == false){
        vm.step2_intermediary = {
          routing_code: null,
          routing_code_id: null,
          name: null,
          address1: null,
          city: null,
          country: null,
          account_no: null,
          routing_type: null,
          bank_code: null,
          address2: null,
          province: null,
          postal_code: null
        };
      }
    };
    vm.previousStep = function () {
      var index = (vm.selectedIndex === 0) ? 0 : vm.selectedIndex - 1;
      vm.selectedIndex = index;
    };
    vm.launchThisTab = function(event, index){
      console.log(index);
    };
    vm.pickRouting = function(formData){
      vm.circleLoader = false;
      if(vm.step2.routing_type != null && typeof vm.step2.routing_type !== 'undefined'){
        vm.GetRoutingCodeList(true, vm.step2.routing_type, 1, 100).then(function(data){
          vm.routing_pagination = data.shift();
          vm.routing_codes = data;
          vm.circleLoader = true;
          $mdDialog.show({
            locals:{dataToPass: formData},
            fullscreen: vm.customFullscreen,
            clickOutsideToClose: true,
            scope: $scope,
            preserveScope: true,
            templateUrl: 'app/main/beneficiaries/add_beneficiary/pick_routing.html',
            parent: angular.element(document.body),
            controller: function($scope, $mdDialog){
              vm.circleLoader = true;
              $scope.tableLoader = false;
              $scope.closeDialog = function(){
                $mdDialog.hide();
              };
              vm.GetRoutingCodeList(true, false, 1, 1000).then(function(data){

                vm.routing_pagination = data.shift();
                vm.routing_codes = data;
              });
              $scope.routing_codes = [];
              angular.copy(vm.routing_codes, $scope.routing_codes);
              $scope.search = {
                name: '',
                routing_code: '',
                routing_type: '',
                city: '',
                postal_code: '',
                province: '',
                country: '',
                address: ''
              };
              $scope.selectRoutingPicker = function(pickerData, event){
                var element = event.target;
                var tr = angular.element(element).closest('tr');
                //remove all previous checks
                var table = tr.closest('table');
                table.find('tr').each(function(i, e){
                  angular.element(e).not(tr).find('td:first-child').find('span.pick-it').removeClass('show').addClass('hide');
                });
                var first_td = tr.find('td:first-child');
                var span_tag = first_td.find('span.pick-it');
                if( span_tag.hasClass('hide') == true ){
                  span_tag.removeClass("hide").addClass("show");
                }else{
                  span_tag.removeClass("show").addClass("hide");
                }
              };
              $scope.excludedByFilter = false;
              $scope.applyFilter = function(){

                var nameFilter = $scope.search.name.toLowerCase();
                var routingCodeFilter = $scope.search.routing_code;
                var routingTypeFilter = $scope.search.routing_type.toLowerCase();
                var addressFilter = $scope.search.address.toLowerCase();
                var cityFilter = $scope.search.city.toLowerCase();
                var postalCodeFilter = $scope.search.postal_code;
                var provinceFilter = $scope.search.province.toLowerCase();
                var countryFilter = $scope.search.country.toLowerCase();

                var showAll = 0 === nameFilter.length &&
                  0 === routingCodeFilter.length &&
                  0 === routingTypeFilter.length &&
                  0 === addressFilter.length &&
                  0 === cityFilter.length &&
                  0 === postalCodeFilter.length &&
                  0 === provinceFilter.length &&
                  0 === countryFilter.length;

                angular.forEach(vm.routing_codes, function(picker){
                  if(showAll){
                    picker.excludedByFilter = false;
                  }else{
                    picker.excludedByFilter = (picker.name.toLowerCase().indexOf(nameFilter) === -1) ||
                      (picker.routingcode.indexOf(routingCodeFilter) === -1) ||
                      (picker.routingtype.toLowerCase().indexOf(routingTypeFilter) === -1) ||
                      (picker.address1.toLowerCase().indexOf(addressFilter) === -1) ||
                      //(picker.currency.toLowerCase().indexOf(currencyFilter) === -1) ||
                      (picker.city.toLowerCase().indexOf(cityFilter) === -1) ||
                      (picker.province.toLowerCase().indexOf(provinceFilter) === -1) ||
                      (picker.country_name.toLowerCase().indexOf(countryFilter) === -1) ||
                      (picker.postcode.indexOf(postalCodeFilter) === -1);
                  }
                });
              };
              $scope.selectRow = function(){
                var table = angular.element("#pick_routing");
                var span = table.find('span.pick-it.show');
                var tr = span.closest('tr');
                var pick_id = tr.data('id');
                var picker = [];
                for(var i=0; i<$scope.routing_codes.length; i++){
                  if($scope.routing_codes[i].id == pick_id){
                    picker.push($scope.routing_codes[i]);
                  }
                }
                if( picker.length > 0 ){

                  vm.step2.routing_code = picker[0].routingcode;
                  vm.step2.routing_code_id = picker[0].id;
                  vm.step2.name = picker[0].name;
                  vm.step2.country = picker[0].country_name;
                  vm.step2.province = picker[0].province;
                  vm.step2_provinces.push({province_name: vm.step2.province});
                  vm.step2.city = picker[0].city;
                  vm.step3_cities.push({city_name: picker[0].city});

                  vm.step2.routing_type = picker[0].routingtype;
                  vm.step2.address1 = picker[0].address1;
                  vm.step2.postal_code = picker[0].postcode;
                  $scope.closeDialog();
                }
              };
              $scope.clearFilter = function(){
                $scope.search = {
                  name: null,
                  routing_code: null,
                  routing_type: null,
                  city: null,
                  postal_code: null,
                  province: null,
                  country: null,
                  address: null
                };
              };
            }
          });
        });
      }else{
        $mdDialog.show({
          locals:{dataToPass: formData},
          fullscreen: vm.customFullscreen,
          clickOutsideToClose: true,
          scope: $scope,
          preserveScope: true,
          templateUrl: 'app/main/beneficiaries/add_beneficiary/pick_routing.html',
          parent: angular.element(document.body),
          controller: function($scope, $mdDialog){
            vm.circleLoader = true;
            $scope.closeDialog = function(){
              $mdDialog.hide();
            };
            $scope.routing_codes = [];
            angular.copy(vm.routing_codes, $scope.routing_codes);
            $scope.search = {
              name: '',
              routing_code: '',
              routing_type: '',
              city: '',
              postal_code: '',
              province: '',
              country: '',
              address: ''
            };
            $scope.selectRoutingPicker = function(pickerData, event){

              var element = event.target;
              var tr = angular.element(element).closest('tr');
              //remove all previous checks
              var table = tr.closest('table');
              table.find('tr').each(function(i, e){
                angular.element(e).not(tr).find('td:first-child').find('span.pick-it').removeClass('show').addClass('hide');
              });
              var first_td = tr.find('td:first-child');
              var span_tag = first_td.find('span.pick-it');
              if( span_tag.hasClass('hide') == true ){
                span_tag.removeClass("hide").addClass("show");
              }else{
                span_tag.removeClass("show").addClass("hide");
              }
            };
            $scope.excludedByFilter = false;
            $scope.maxSize = 5;

            $scope.applyFilter = function(){
              vm.circleLoader = false;
              var input = '<INPUT>';
              if($scope.search.name){
                input += '<FILTER><NAME TYPE="STRING" OPERATOR="EQAULS TO">'+$scope.search.name+'</NAME></FILTER>';
              }
              if($scope.search.bank_code){
                input += '<FILTER><BANK_CODE TYPE="STRING" OPERATOR="EQAULS TO">'+$scope.search.bank_code+'</BANK_CODE></FILTER>';
              }
              if($scope.search.routing_code){
                input += '<FILTER><ROUTINGCODE TYPE="STRING" OPERATOR="EQAULS TO">'+$scope.search.routing_code+'</ROUTINGCODE></FILTER>';
              }
              if($scope.search.routing_type){
                input += '<FILTER><ROUTINGTYPE TYPE="STRING" OPERATOR="EQAULS TO">'+$scope.search.routing_type+'</ROUTINGTYPE></FILTER>';
              }
              if($scope.search.routing_type){
                input += '<FILTER><ROUTINGTYPE TYPE="STRING" OPERATOR="EQAULS TO">'+$scope.search.routing_type+'</ROUTINGTYPE></FILTER>';
              }

              if($scope.search.city){
                input += '<FILTER><CITY TYPE="STRING" OPERATOR="EQAULS TO">'+$scope.search.city+'</CITY></FILTER>';
              }
              if($scope.search.province){
                input += '<FILTER><PROVINCE TYPE="STRING" OPERATOR="EQAULS TO">'+$scope.search.province+'</PROVINCE></FILTER>';
              }

              if($scope.search.postal_code){
                input += '<FILTER><POSTCODE TYPE="STRING" OPERATOR="EQAULS TO">'+$scope.search.postal_code+'</POSTCODE></FILTER>';
              }
              if($scope.search.country){
                input += '<FILTER><COUNTRY_NAME TYPE="STRING" OPERATOR="EQAULS TO">'+$scope.search.country+'</COUNTRY_NAME></FILTER>';
              }


              input += '<PAGING><PAGE_NUMBER>1</PAGE_NUMBER><PAGE_SIZE>50</PAGE_SIZE></PAGING>' +
                       '</INPUT>';

              QueryFactory.query('GetRoutingCodeList', input).then(function(response){
                updateRecords(response.data);
                vm.circleLoader = true;
              });

              function updateRecords(data){
                var filterRecords = XmlJson.xmlToJson(data).output.routingcode;

                if(filterRecords && !filterRecords.length){
                  var records = [];
                  records.push(filterRecords);
                  filterRecords = records;
                }
                vm.routing_pagination = filterRecords && filterRecords[0];
                vm.routing_codes = filterRecords;

                $scope.totalPages = vm.routing_codes/$scope.maxSize;

              }


            };
            $scope.selectRow = function(){
              var table = angular.element("#pick_routing");
              var span = table.find('span.pick-it.show');
              var tr = span.closest('tr');
              var pick_id = tr.data('id');
              var picker = [];
              for(var i=0; i<$scope.routing_codes.length; i++){
                if($scope.routing_codes[i].id == pick_id){
                  picker.push($scope.routing_codes[i]);
                }
              }
              if( picker.length > 0 ){
                vm.step2.routing_code = picker[0].routingcode;
                vm.step2.routing_code_id = picker[0].id;
                vm.step2.name = picker[0].name;
                vm.step2.country = picker[0].country_name;
                vm.step2.province = picker[0].province;
                vm.step2_provinces.push({province_name: vm.step2.province});
                vm.step2.city = picker[0].city;
                vm.step3_cities.push({city_name: picker[0].city});
                vm.step2.routing_type = picker[0].routingtype;
                vm.step2.address1 = picker[0].address1;
                vm.step2.postal_code = picker[0].postcode;
               if(picker[0].bank_code != null && picker[0].bank_code != ''){
                  vm.step2_intermediary.bank_code = picker[0].bank_code;
                }
                $scope.closeDialog();
              }
            };
            $scope.clearFilter = function(){
              $scope.search = {
                name: null,
                routing_code: null,
                routing_type: null,
                city: null,
                postal_code: null,
                province: null,
                country: null,
                address: null
              };
            };
          }
        });
      }
    };
    vm.pickIntermediaryRouting = function(formData){
      vm.circleLoader = false;
      if(vm.step2_intermediary.routing_type != null && typeof vm.step2_intermediary.routing_type !== 'undefined'){
        vm.GetRoutingCodeList(true, vm.step2_intermediary.routing_type, 1, 100).then(function(data){

          vm.routing_pagination = data.shift();
          vm.routing_codes = data;
          vm.circleLoader = true;
          $mdDialog.show({
            locals:{dataToPass: formData},
            fullscreen: vm.customFullscreen,
            clickOutsideToClose: true,
            scope: $scope,
            preserveScope: true,
            templateUrl: 'app/main/beneficiaries/add_beneficiary/pick_routing.html',
            parent: angular.element(document.body),
            controller: function($scope, $mdDialog){
              vm.circleLoader = true;
              $scope.closeDialog = function(){
                $mdDialog.hide();
              };
              $scope.routing_codes = [];
              angular.copy(vm.routing_codes, $scope.routing_codes);
              $scope.search = {
                name: '',
                routing_code: '',
                routing_type: '',
                city: '',
                postal_code: '',
                province: '',
                country: '',
                address: ''
              };
              $scope.selectRoutingPicker = function(pickerData, event){
                var element = event.target;
                var tr = angular.element(element).closest('tr');
                //remove all previous checks
                var table = tr.closest('table');
                table.find('tr').each(function(i, e){
                  angular.element(e).not(tr).find('td:first-child').find('span.pick-it').removeClass('show').addClass('hide');
                });
                var first_td = tr.find('td:first-child');
                var span_tag = first_td.find('span.pick-it');
                if( span_tag.hasClass('hide') == true ){
                  span_tag.removeClass("hide").addClass("show");
                }else{
                  span_tag.removeClass("show").addClass("hide");
                }
              };
              $scope.excludedByFilter = false;
              $scope.applyFilter = function(){
                console.log("filter")
                // var nameFilter = $scope.search.name.toLowerCase();
                // var routingCodeFilter = $scope.search.routing_code;
                // var routingTypeFilter = $scope.search.routing_type.toLowerCase();
                // var addressFilter = $scope.search.address.toLowerCase();
                // var cityFilter = $scope.search.city.toLowerCase();
                // var postalCodeFilter = $scope.search.postal_code;
                // var provinceFilter = $scope.search.province.toLowerCase();
                // var countryFilter = $scope.search.country.toLowerCase();
                //
                // var showAll = 0 === nameFilter.length &&
                //   0 === routingCodeFilter.length &&
                //   0 === routingTypeFilter.length &&
                //   0 === addressFilter.length &&
                //   0 === cityFilter.length &&
                //   0 === postalCodeFilter.length &&
                //   0 === provinceFilter.length &&
                //   0 === countryFilter.length;
                //
                // angular.forEach(vm.routing_codes, function(picker){
                //   if(showAll){
                //     picker.excludedByFilter = false;
                //   }else{
                //     picker.excludedByFilter = (picker.name.toLowerCase().indexOf(nameFilter) === -1) ||
                //       (picker.routingcode.indexOf(routingCodeFilter) === -1) ||
                //       (picker.routingtype.toLowerCase().indexOf(routingTypeFilter) === -1) ||
                //       (picker.address1.toLowerCase().indexOf(addressFilter) === -1) ||
                //       //(picker.currency.toLowerCase().indexOf(currencyFilter) === -1) ||
                //       (picker.city.toLowerCase().indexOf(cityFilter) === -1) ||
                //       (picker.province.toLowerCase().indexOf(provinceFilter) === -1) ||
                //       (picker.country_name.toLowerCase().indexOf(countryFilter) === -1) ||
                //       (picker.postcode.indexOf(postalCodeFilter) === -1);
                //   }
                // });
              };
              $scope.selectRow = function(){
                var table = angular.element("#pick_routing");
                var span = table.find('span.pick-it.show');
                var tr = span.closest('tr');
                var pick_id = tr.data('id');
                var picker = [];
                for(var i=0; i<$scope.routing_codes.length; i++){
                  if($scope.routing_codes[i].id == pick_id){
                    picker.push($scope.routing_codes[i]);
                  }
                }
                if( picker.length > 0 ){

                  vm.step2_intermediary.routing_code = picker[0].routingcode;
                  vm.step2_intermediary.routing_code_id = picker[0].id;
                  vm.step2_intermediary.name = picker[0].name;
                  vm.step2_intermediary.country = picker[0].country_name;
                  vm.step2_intermediary.province = picker[0].province;
                  vm.step2_intermediary_provinces.push({province_name: vm.step2_intermediary.province});
                  vm.step2_intermediary.city = picker[0].city;
                  vm.step4_cities.push({city_name: picker[0].city});
                  vm.step2_intermediary.routing_type = picker[0].routingtype;

                  vm.step2_intermediary.address1 = picker[0].address1;
                  vm.step2_intermediary.postal_code = picker[0].postcode;
                  $scope.closeDialog();
                }
              };
              $scope.clearFilter = function(){
                $scope.search = {
                  name: null,
                  routing_code: null,
                  routing_type: null,
                  city: null,
                  postal_code: null,
                  province: null,
                  country: null,
                  address: null
                };
              };
            }
          });
        });
      }else{
        $mdDialog.show({
          locals:{dataToPass: formData},
          fullscreen: vm.customFullscreen,
          clickOutsideToClose: true,
          scope: $scope,
          preserveScope: true,
          templateUrl: 'app/main/beneficiaries/add_beneficiary/pick_routing.html',
          parent: angular.element(document.body),
          controller: function($scope, $mdDialog){
            vm.circleLoader = true;
            $scope.closeDialog = function(){
              $mdDialog.hide();
            };
            $scope.routing_codes = [];
            angular.copy(vm.routing_codes, $scope.routing_codes);
            $scope.search = {
              name: '',
              routing_code: '',
              routing_type: '',
              city: '',
              postal_code: '',
              province: '',
              country: '',
              address: ''
            };
            $scope.selectRoutingPicker = function(pickerData, event){
              var element = event.target;
              var tr = angular.element(element).closest('tr');
              //remove all previous checks
              var table = tr.closest('table');
              table.find('tr').each(function(i, e){
                angular.element(e).not(tr).find('td:first-child').find('span.pick-it').removeClass('show').addClass('hide');
              });
              var first_td = tr.find('td:first-child');
              var span_tag = first_td.find('span.pick-it');
              if( span_tag.hasClass('hide') == true ){
                span_tag.removeClass("hide").addClass("show");
              }else{
                span_tag.removeClass("show").addClass("hide");
              }
            };
            $scope.excludedByFilter = false;
            $scope.applyFilter = function(){

              var nameFilter = $scope.search.name.toLowerCase();
              var routingCodeFilter = $scope.search.routing_code;
              var routingTypeFilter = $scope.search.routing_type.toLowerCase();
              var addressFilter = $scope.search.address.toLowerCase();
              var cityFilter = $scope.search.city.toLowerCase();
              var postalCodeFilter = $scope.search.postal_code;
              var provinceFilter = $scope.search.province.toLowerCase();
              var countryFilter = $scope.search.country.toLowerCase();

              var showAll = 0 === nameFilter.length &&
                0 === routingCodeFilter.length &&
                0 === routingTypeFilter.length &&
                0 === addressFilter.length &&
                0 === cityFilter.length &&
                0 === postalCodeFilter.length &&
                0 === provinceFilter.length &&
                0 === countryFilter.length;

              angular.forEach(vm.routing_codes, function(picker){
                if(showAll){
                  picker.excludedByFilter = false;
                }else{
                  picker.excludedByFilter = (picker.name.toLowerCase().indexOf(nameFilter) === -1) ||
                    (picker.routingcode.indexOf(routingCodeFilter) === -1) ||
                    (picker.routingtype.toLowerCase().indexOf(routingTypeFilter) === -1) ||
                    (picker.address1.toLowerCase().indexOf(addressFilter) === -1) ||
                    //(picker.currency.toLowerCase().indexOf(currencyFilter) === -1) ||
                    (picker.city.toLowerCase().indexOf(cityFilter) === -1) ||
                    (picker.province.toLowerCase().indexOf(provinceFilter) === -1) ||
                    (picker.country_name.toLowerCase().indexOf(countryFilter) === -1) ||
                    (picker.postcode.indexOf(postalCodeFilter) === -1);
                }
              });
            };
            $scope.selectRow = function(){
              var table = angular.element("#pick_routing");
              var span = table.find('span.pick-it.show');
              var tr = span.closest('tr');
              var pick_id = tr.data('id');
              var picker = [];
              for(var i=0; i<$scope.routing_codes.length; i++){
                if($scope.routing_codes[i].id == pick_id){
                  picker.push($scope.routing_codes[i]);
                }
              }
              if( picker.length > 0 ){
                vm.step2_intermediary.routing_code = picker[0].routingcode;
                vm.step2_intermediary.routing_code_id = picker[0].id;
                vm.step2_intermediary.name = picker[0].name;
                vm.step2_intermediary.country = picker[0].country_name;
                vm.step2_intermediary.province = picker[0].province;
                vm.step2_intermediary_provinces.push({province_name: vm.step2_intermediary.province});
                vm.step2_intermediary.city = picker[0].city;
                vm.step4_cities.push({city_name: picker[0].city});
                vm.step2_intermediary.routing_type = picker[0].routingtype;
                vm.step2_intermediary.address1 = picker[0].address1;
                vm.step2_intermediary.postal_code = picker[0].postcode;
                $scope.closeDialog();
              }
            };
            $scope.clearFilter = function(){
              $scope.search = {
                name: null,
                routing_code: null,
                routing_type: null,
                city: null,
                postal_code: null,
                province: null,
                country: null,
                address: null
              };
            };
          }
        });
      }
    };
    //Approvals
    vm.showPopOver = true;
    vm.popover = {
      title: 'List of Approvals',
    };
    vm.Approvals = [];
    vm.ApprovalNext = false;

    vm.approvalCounter = 0;
    vm.showPopOverFunc = function(){
      vm.showPopOver = false;
    };
    vm.showPopLeaveFunc = function(){
      vm.showPopOver = true;
    };
    //approve wire
    vm.approveWire = function(){

      if(!vm.ApprovalNext){
        vm.showUpdate = false;
        vm.circleLoader = false;
        vm.GetUserApprovalRights(vm.beneficiary_deal, vm.module_id).then(function(data){
          if(data[0].status == 'TRUE'){
            vm.SetWireApproval(vm.beneficiary_deal.recordid).then(function(data){
              if(data[0].status == 'TRUE'){
                $mdToast.show({
                  template: '<md-toast class="md-toast info">Deal Approved.</md-toast>',
                  hideDelay: 4000,
                  position: 'bottom right'
                });
                vm.GetApprovalHistory(vm.beneficiary_deal).then(function(data){
                  vm.approvalCounter++;
                  vm.Approvals = data;
                  if(vm.Approvals.length == vm.approvalPolicy[0].approval_level){
                    vm.ApprovalNext = true;
                  }
                  vm.circleLoader = true;
                });
              }else{
                $mdToast.show({
                  template: '<md-toast class="md-toast error">'+data[0].error+'</md-toast>',
                  hideDelay: 2000,
                  position: 'bottom right'
                });
                vm.circleLoader = true;
              }
            });
          }else{
            $mdToast.show({
              template: '<md-toast class="md-toast error">You cannot approve this deal.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
          }
        });
      }
    };
    function validateWire() {
      if(
        vm.step1.beneficiary_name === null ||
        vm.step1.address1 === null ||
        vm.step1.city === null ||
        vm.step1.country === null ||
        vm.step1.currency === null
      ){
        messageService.error('Fill required fields', 5000);
        vm.step1Invalid = true;
      }else{
        if( vm.step1.auto_by_order_of === false ){
          if(
            vm.step1_auto_by_order_of.name === null ||
            vm.step1_auto_by_order_of.country === null ||
            vm.step1_auto_by_order_of.province === null ||
            vm.step1_auto_by_order_of.city === null ||
            vm.step1_auto_by_order_of.address1 === null
          ){
            messageService.error('Fill required fields', 5000);
            vm.step1AutoInvalid = true;
          }
        }
      }
    }
    function createWire() {

    }
  }
})();
