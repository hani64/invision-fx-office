(function ()
{
  'use strict';

  angular
    .module('app.beneficiaries.add_beneficiary', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider)
  {
    // State
    $stateProvider.state('app.add_beneficiary', {
      url      : '/add_beneficiary',
      views    : {
        'content@app': {
          templateUrl: 'app/main/beneficiaries/add_beneficiary/add_beneficiary.html',
          controller : 'AddBeneficiaryController as vm',
          access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
        }
      }
    });
  }

})();
