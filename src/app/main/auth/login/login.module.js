(function ()
{
  'use strict';

  angular
    .module('app.authentication.login', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
  {

    // State
    $stateProvider.state('app.pages_auth_login', {

      url      : '/auth/login',
      views    : {
        'main@'                       : {
          templateUrl: 'app/core/layouts/content-only.html',
          controller : 'MainController as vm'
        },
        'content@app.pages_auth_login': {
          templateUrl: 'app/main/auth/login/login.html',
          controller : 'LoginController as vm',
          resolve    : {
            admin : function ($http) {
              return $http({
                method: 'GET',
                url: 'api/config'
              });
            }
          }
        }
      },
      resolve: {
        DashboardData: function (msApi) {
          return msApi.resolve('dashboard.project@get');
        },
        access: ["AuthenticateFactory", function (AuthenticateFactory) { return !AuthenticateFactory.isAuthenticated(); }],

      },
      bodyClass: 'login'
    });

    // Translation
    $translatePartialLoaderProvider.addPart('app/main/auth');

    // // Navigation
    // msNavigationServiceProvider.saveItem('pages.auth', {
    //   title : 'Authentication',
    //   icon  : 'icon-lock',
    //   weight: 1
    // });
    //
    // msNavigationServiceProvider.saveItem('pages.auth.login', {
    //   title : 'Login',
    //   state : 'app.pages_auth_login',
    //   weight: 1
    // });
  }

})();
