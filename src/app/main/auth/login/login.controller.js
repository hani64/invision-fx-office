(function () {
  'use strict';

  angular
    .module('app.authentication.login')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController($location, $scope, $rootScope, $mdToast, soap_api, vcRecaptchaService, $http, $state, $localStorage, $sessionStorage, $interval, cryptoService, admin, $cookies) {

    var vm = this;

    // vm.config.captcha = admin.data.success;

    vm.showCircleLoader = true;
    // vm.saveUserInfoInCookiesRemeberMe = rememberMe();
    $rootScope.passphrase = btoa('Fx-Office-Web-App');
    $rootScope.salt = cryptoService.crypto.lib.WordArray.random(128/8);
    $rootScope.key = cryptoService.crypto.PBKDF2($rootScope.passphrase, $rootScope.salt, { keySize: 512/32, iterations: 100 });
    $rootScope.passphrase = $rootScope.key.toString();



    vm.form = {
      txtCustomerID: typeof $rootScope.loginForm == 'undefined' ? null: $rootScope.loginForm.CustID,
      username: typeof $rootScope.loginForm == 'undefined' ? null: $rootScope.loginForm.username,
      password: null,
      remember: null
    };
    vm.form.remember = isRememberOrNot();
    console.log(vm.form.remember)

    retriveRememberMe();


    function isRememberOrNot()
    {
      var isRemember = angular.fromJson($localStorage.rememberMe)
      if(isRemember)
      {
        return isRemember.remember ;
      }

    };


    function retriveRememberMe()
    {
      if(vm.form.remember)
      {
        vm.form = $cookies.get('userInfo') && JSON.parse($cookies.get('userInfo')) || vm.form;

        vm.form.remember = angular.fromJson($localStorage.rememberMe).remember ;

      }
      else
      {
        vm.form = {};
      }
    }

    function saveRememberMe(form)
    {
      if(vm.form.remember)
      {
        var cookieObj = {};
        cookieObj['username'] = form['username'];
        cookieObj['txtCustomerID'] = form['txtCustomerID'];
        $cookies.put('userInfo', JSON.stringify(cookieObj));
      }
    }

    /*vm.form = {
     txtCustomerID: null ,
     username: null,
     password: null,
     remember: null
     };*/

    vm.isXML = function(xml){
      try{
        var xmlDoc = jQuery.parseXML(xml);
        return true;
      }catch(error){
        return false;
      }
    };
    vm.result = {};
    vm.reCaptcha = {
      key: '6Lc-SxEUAAAAAOA_H4baTHQUWy3ZaZqlCpD32TBs'
    };
    vm.response = null;
    vm.widgetId = null;

    vm.setWidgetId = function(widgetId){
      console.info('Created widget ID: %s', widgetId);
      vm.widgetId = widgetId;
    };
    vm.setResponse = function(response){
      console.info('Response available');
      vm.response = response;
    };
    vm.cbExpiration = function(){
      console.info('Captcha expired. Resetting response object');
      vcRecaptchaService.reload($scope.widgetId);
      vm.response = null;
    };
    vm.doLogin = function(form){

      /*vm.showCircleLoader = false;
      var remember = vm.form.remember == 1 ? true: false;
      $localStorage.rememberMe = JSON.stringify({remember:remember});
      if(typeof $localStorage.rememberMe == 'string' && remember == true){
        $localStorage.passphrase = $rootScope.passphrase;
        var encrypt = cryptoService.crypto.AES.encrypt(JSON.stringify({username:form.username,password:form.password,OrgID:3,CustID:form.txtCustomerID}), $localStorage.passphrase);
        $localStorage.login = encrypt.toString();

      }else{
        $sessionStorage.passphrase = $rootScope.passphrase;
        var encrypt = cryptoService.crypto.AES.encrypt(JSON.stringify({username:form.username,password:form.password,OrgID:3,CustID:form.txtCustomerID}), $sessionStorage.passphrase);
        $sessionStorage.login = encrypt.toString();

      }
      $location.url('/dashboard');
      $mdToast.show({
        template: '<md-toast class="md-toast success">Logged in successfully.</md-toast>',
        hideDelay: 4000,
        position: 'bottom right'
      });*/
      soap_api.ValidateUserLogin(form.username, form.password, 3, form.txtCustomerID, '').then(function(success){
        if(vm.isXML(success.data)){
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function(i,e){
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              recordid: jQuery(e).find('RECORDID').text(),
              error: jQuery(e).find('ERROR').text()
            };
            angular.copy(temp, vm.result);
            if(vm.result.status === 'TRUE'){
              //store login session in localStorage
              var remember = vm.form.remember;
              $localStorage.rememberMe = JSON.stringify({remember:remember}) ;
              if(typeof $sessionStorage.rememberMe == 'string' && remember){
                // debugger
                $sessionStorage.passphrase = $rootScope.passphrase;
                var encrypt = cryptoService.crypto.AES.encrypt(JSON.stringify({username:form.username,password:form.password,OrgID:3,CustID:form.txtCustomerID}), $sessionStorage.passphrase);
                $sessionStorage.login = encrypt.toString();
                sessionStorage.setItem('ngStorage-login' , encrypt.toString());

              }else{
                $sessionStorage.passphrase = $rootScope.passphrase;
                var encrypt = cryptoService.crypto.AES.encrypt(JSON.stringify({username:form.username,password:form.password,OrgID:3,CustID:form.txtCustomerID}), $sessionStorage.passphrase);
                $sessionStorage.login = encrypt.toString();
                $sessionStorage.passphrase = $rootScope.passphrase;
                $sessionStorage.login = encrypt.toString();
                sessionStorage.setItem('ngStorage-login' , encrypt.toString());
              }
              if(vm.form.remember)
              {
                saveRememberMe(form);
              }

              // $location.url('/dashboard');
              $state.go('app.dashboard');
              $mdToast.show({
                template: '<md-toast class="md-toast success">Logged in successfully.</md-toast>',
                hideDelay: 4000,
                position: 'bottom right'
              });
              vm.form = {
                txtCustomerID: null,
                username: null,
                password: null,
                remember: null
              };
              vm.showCircleLoader = false;
            }else{
              /*if(typeof vm.result.error !== 'undefined'){
                $mdToast.show({
                  template: '<md-toast class="md-toast error">'+vm.result.error+'</md-toast>',
                  hideDelay: 5000,
                  position: 'bottom right'
                });
              }else{
                $mdToast.show({
                  template: '<md-toast class="md-toast error">Login Failed. Please type correct credentials.</md-toast>',
                  hideDelay: 5000,
                  position: 'bottom right'
                });
              }*/
              $mdToast.show({
                template: '<md-toast class="md-toast error">'+vm.result.error+'</md-toast>',
                hideDelay: 5000,
                position: 'bottom right'
              });
              vm.showCircleLoader = true;
            }
          });
        }else{
          $mdToast.show({
            template: '<md-toast class="md-toast error">'+success.data+'</md-toast>',
            hideDelay: 5000,
            position: 'bottom right'
          });
          vm.showCircleLoader = true;
        }
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.goForgotPassword = function(){
      $state.go('app.pages_auth_forgot-password');
    };
  }
})();
