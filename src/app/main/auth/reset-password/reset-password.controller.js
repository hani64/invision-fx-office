(function ()
{
    'use strict';

    angular
        .module('app.authentication.reset-password')
        .controller('ResetPasswordController', ResetPasswordController);

    /** @ngInject */
    function ResetPasswordController($state, $stateParams, QueryFactory, XmlJson, messageService)
    {
      var vm = this;
      vm.resetPassword = verifyForgot;

      console.log($stateParams);
      function verifyForgot()
      {
        var location = document.location.origin + $stateParams.token;
        // var url = getUrl(location);
        var uri;
        if(isResetTokenExist())
        {
          uri = localStorage.getItem("reset-token");
        }

        else
        {
          uri = decodeUri(location);
          localStorage.setItem("reset-token", uri);
        }


        if(!location || !$stateParams.token){
          alert("Url is undefined!")
          return
        }
        var input =
          "<INPUT>" +
          "<REQUEST_URL>" + uri + "</REQUEST_URL>" +
          "</INPUT>";

        QueryFactory.query('VerifyForgotPasswordRequest', input, vm.form , "forgot").then(success, err)
        function success(res)
        {
           var verfiyResponse;
           verfiyResponse = getResponse(res.data, 'user');
          if(!verfiyResponse)
          {
            verfiyResponse =  getResponse(res.data, 'result');
          }

          if(verfiyResponse.status && verfiyResponse.status !== 'TRUE')
          {
            messageService.error(verfiyResponse.error + 'please sent a new password request', 5000);
            removeStorage();
            $state.go('app.pages_auth_forgot-password');
            return $state.go('app.pages_auth_forgot-password')
          }



          resetPasswordAfterVerification(verfiyResponse);

        }
        function err(error)
        {
          console.log(error)
        }
      }
      function resetPasswordAfterVerification(userAfterVerify)
      {
        var input = "<INPUT>" +
                    "<NEW_PASSWORD>" + vm.form.passwordConfirm + "</NEW_PASSWORD>" +
                    "</INPUT>";
      vm.form["username"] = userAfterVerify.user_name;
      vm.form["CustID"] = userAfterVerify.entity_no;

      QueryFactory.query('ResetPassword', input, vm.form, "forgot").then(success, error);
      function success(resetResponse)
      {
        console.log(resetResponse)
        var resetResponse = getResponse(resetResponse.data, 'result');
        if(resetResponse.status === 'TRUE')
        {
          removeStorage();
          $state.go('app.pages_auth_login');
         return messageService.success('Passord Updated', 5000);

        }
      }
      function error(err)
      {
        console.log(err);
      }
      }

      function removeStorage()
      {
        localStorage.removeItem('reset-token');
      }
      function isResetTokenExist()
      {
        return localStorage.getItem("reset-token");
      }
      function getUrl(location)
      {
        var uri = decodeUri(location);
        localStorage.setItem("reset-token", uri);
        return localStorage.getItem("reset-token");
      }
      function decodeUri(Uri)
      {return decodeURIComponent(Uri);}
      function getResponse(data, output)
      {
        return convertDataToJson(data, output);
      }
      function convertDataToJson(xml, name)
      {
        var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
        var actualJson = XmlJson.xmlToJson(xmlDOM);
        return actualJson.output[name];
      }
        // Data

        // Methods

        //////////
    }
})();
