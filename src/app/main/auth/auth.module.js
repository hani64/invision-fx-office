(function () {
  'use strict';

  angular
    .module('app.authentication', [
      'app.authentication.login',
      'app.authentication.forgot-password',
      'app.authentication.reset-password'
    ])
    .config(config);

  /** @ngInject */
  function config() {
  }
})();
