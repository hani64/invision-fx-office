(function ()
{
    'use strict';

    angular
        .module('app.authentication.forgot-password', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider)
    {

        // State
        $stateProvider.state('app.pages_auth_forgot-password', {
            url      : '/auth/forgot-password',
            views    : {
                'main@'                                 : {
                    templateUrl: 'app/core/layouts/content-only.html',
                    controller : 'MainController as vm'
                },
                'content@app.pages_auth_forgot-password': {
                    templateUrl: 'app/main/auth/forgot-password/forgot-password.html',
                    controller : 'ForgotPasswordController as vm'
                }
            },
          resolve: {
            DashboardData: function (msApi) {
              return msApi.resolve('dashboard.project@get');
            },
            access: ["AuthenticateFactory", function (AuthenticateFactory) { return !AuthenticateFactory.isAuthenticated(); }],

          },
            bodyClass: 'forgot-password'
        });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/auth/forgot-password');

        /*// Navigation
        msNavigationServiceProvider.saveItem('pages.auth.forgot-password', {
            title : 'Forgot Password',
            state : 'app.pages_auth_forgot-password',
            weight: 5
        });*/
    }

})();
