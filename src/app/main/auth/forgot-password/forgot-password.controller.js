(function ()
{
    'use strict';

    angular
        .module('app.authentication.forgot-password')
        .controller('ForgotPasswordController', ForgotPasswordController);

    /** @ngInject */
    function ForgotPasswordController(QueryFactory, XmlJson, messageService)
    {
        var vm = this;

        //Reset types
        // 1: email
        // 2: security question

        //send form request
        vm.submitRequest = function(){
         var input =  '<INPUT>' +
           '<SITE_URL>' + document.location.origin + "/auth/reset-password?token" +
           '</SITE_URL>' +
          '</INPUT>';

          // console.log(vm.form);
          //  vm.form["custId"]  = '0004902',
          //  vm.form["name"]    = 'test'

          QueryFactory.query('SendForgotPasswordEmail', input, vm.form).then(success);
          function success(response)
          {


              var sendForgotPasswordResponse = convertDataToJson(response.data, 'result');
              if(sendForgotPasswordResponse.status !== 'TRUE')
              {
                 messageService.error(sendForgotPasswordResponse.error, 5000);
                 return
              }
            messageService.success(sendForgotPasswordResponse.error, 5000);

          }
        };



      function convertDataToJson(xml, name)
      {
        var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
        var actualJson = XmlJson.xmlToJson(xmlDOM);
        return actualJson.output[name];
      }
    }
})();
