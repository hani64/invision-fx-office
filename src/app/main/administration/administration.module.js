(function () {
  'use strict';

  angular
    .module('app.administration', [
      'app.administration.approval_policies',
      'app.administration.manage_users'
      //'app.administration.manage_users',
      // 'app.administration.add_user',
      // 'app.administration.approval_policy',
      //'app.administration.manage_account'
    ])
    .config(config);

  /** @ngInject */
  function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) {
    // Translation
    $translatePartialLoaderProvider.addPart('app/main/administration');

    // Api
    //msApiProvider.register('sample', ['app/data/payments/payments.json']);
    msNavigationServiceProvider.saveItem('main_menu.administration', {
      title: 'Administration',
      icon: 'icon-cog',
      //state: 'app.administration',
      weight: 1,
      child_count: 5
    });

    msNavigationServiceProvider.saveItem('main_menu.administration.manage_users', {
      title: 'Manage Users',
      state: 'app.manage_users',
      weight: 1,
      module_id: 21001
    });

    // msNavigationServiceProvider.saveItem('main_menu.administration.change_password', {
    //   title: 'Change Password',
    //   state: 'app.administration.change_password',
    //   weight: 1,
    //   module_id: 21001
    // });


    // msNavigationServiceProvider.saveItem('main_menu.administration.user_permission', {
    //   title: 'User Permissions',
    //   state: 'app.administration.user_permission',
    //   weight: 1,
    //   module_id: 12
    // });

    // msNavigationServiceProvider.saveItem('main_menu.administration.add_user', {
    //   title: 'Add User',
    //   state: 'app.add_user',
    //   weight: 1,
    //   module_id: 13
    // });

    msNavigationServiceProvider.saveItem('main_menu.administration.approval_policies', {
      title: 'Approval Policies',
      state: 'app.approval_policies',
      weight: 1,
      module_id: 11495
    });
    msNavigationServiceProvider.saveItem('main_menu.administration.manage_account', {
      title: 'Manage Profile',
      state: 'app.manage_account',
      weight: 1,
      module_id: 11495
    });
  }
})();
