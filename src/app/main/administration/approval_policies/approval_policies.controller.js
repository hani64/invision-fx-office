(function () {
  'use strict';

  angular
    .module('app.administration.approval_policies')
    .controller('ApprovalPolicyController', ApprovalPolicyController);

  /** @ngInject */
  function ApprovalPolicyController(api, $rootScope, NgTableParams, messageService)
  {
    var vm  = this;
    $rootScope.headerTitle = 'Approval Policies';
    vm.showDataTable = true;
    vm.enableFilter = false;
    vm.modules = [];
    vm.approvalLevels = [];
    vm.webUsers = [];
    vm.entity = [];
    vm.hideTabs = true;
    vm.step1 = {
      entity: null,
      module: null,
      level: null
    };
    vm.step2 = {
      module: null,
      user: null,
      level: null
    };
    vm.step3 = [];
    vm.step1Invalid = false;
    vm.loader = true;


    //functions
    vm.toggleFilters = function(){
      vm.enableFilter = !vm.enableFilter;
    };
    function filterColumns(data, filterValues) {
      if(vm.filter){
        return data.filter(function(item){
          return (item.deal_number.toLowerCase().indexOf(filterValues.deal_number || '') !== -1) &&
            (item.sell_amount.toString().toLowerCase().indexOf(filterValues.sell_amount || '') !== -1) &&
            (item.beneficiary_name.toLowerCase().indexOf(filterValues.beneficiary_name || '') !== -1);
        });
      }else{
        return data;
      }
    }
    api.GetWebModules().then(function(data){
      vm.showDataTable = false;
      // debugger
      vm.modules = data.output.table1;
      console.log(vm.modules);
      vm.tableParams = new NgTableParams({}, {
        counts: [5, 10, 20, 50, 100],
        dataset : vm.modules,
        filterOptions: {
          filterFn: filterColumns
        }
      });
    });
    api.GetApprovalLevels().then(function(data){
      debugger;
      vm.approvalLevels = data.output.approval_level;
    });
    api.GetWebUsers().then(function(data){
      vm.webUsers = data.output.web_user;
    });
    api.GetEntity().then(function(data){
      vm.entity = data.output;
      if(angular.isDefined(vm.entity) && vm.entity !== null){
        vm.step1.entity = vm.entity.entity.name;
      }
    });
    vm.addApprovalPolicy = function(event){
      event.preventDefault();
      vm.hideTabs = false;
    };
    vm.editApprovalPolicy = function(module){
      vm.hideTabs = false;
      console.log(module);
    };
    vm.backToApprovals = function(event){
      event.preventDefault();
      vm.hideTabs = true;
    };
    vm.assignUser = function(event){
      event.preventDefault();
      if(
        vm.step2.module === null ||
        vm.step2.user === null ||
        vm.step2.level === null
      ){
        messageService.error('Fill required fields.', 4000);
      }else{
        var user = {
          name: vm.step2.user,
          level: vm.step2.level
        };
        vm.step2 = {
          module: null,
          user: null,
          level: null
        };
        vm.step3.push(user);
      }
    };
    vm.deleteRecord = function(user, index){
      vm.step3.splice(index, 1);
    };
    vm.saveApprovalPolicy = function(event){
      event.preventDefault();
      vm.loader = false;
      if(
        vm.step1.level === null ||
        vm.step1.module === null ||
        vm.step3.length <= 0
      ){
        vm.step1Invalid = true;
        vm.loader = true;
        messageService.error('Fill required fields.', 5000);
      }else{
        var detail = '';
        vm.step3.forEach(function(user){
          detail += '<DETAIL><USERS><Value>'+user.name+'</Value></USERS><APPROVAL_TYPE><Value>'+user.level+'</Value></APPROVAL_TYPE></DETAIL>';
        });
        api.SaveApprovalPolicy(vm.step1.module, vm.step1.level, detail).then(function(data){
          vm.approval = data.output.result;
          if(vm.approval.status === 'FALSE'){
            messageService.error(vm.approval.error, 5000);
          }else{
            messageService.success(vm.approval.error, 5000);
          }
          vm.loader = true;
        });
      }
    };
  }
})();
