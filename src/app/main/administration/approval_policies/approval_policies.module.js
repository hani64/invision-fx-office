(function () {
  'use strict';

  var app = angular
    .module('app.administration.approval_policies', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider, msApiProvider) {
    // State
    $stateProvider.state('app.approval_policies', {
      url: '/approval_policy',
      resolve: {
        access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
      },
      views: {
        'content@app': {
          templateUrl: 'app/main/administration/approval_policies/approval_policies.html',
          controller: 'ApprovalPolicyController as vm'
        }
      }
    });

    // Api
    msApiProvider.register('invoice', ['app/data/invoice/invoice.json']);
    //msApiProvider.register('invoice', ['app/data/invoice/invoice.json']);
  }
})();
