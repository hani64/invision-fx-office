(function () {
  'use strict';

  angular
    .module('app.administration.manage_users')
    .controller('ManageUsersController', ManageUsersController);

  /** @ngInject */
  function ManageUsersController(api, $rootScope, NgTableParams, messageService)
  {
    var vm = this;
    $rootScope.headerTitle = 'Manage Users';
    vm.showDataTable = true;
    vm.enableFilter = false;
    vm.modules = [];
    vm.approvalLevels = [];
    vm.webUsers = [];
    vm.entity = [];
    vm.hideTabs = false;
    vm.step1 = {
      entity: null,
      module: null,
      level: null
    };
    vm.step2 = {
      module: null,
      user: null,
      level: null
    };
    vm.step3 = [];
    vm.step1Invalid = false;
    vm.loader = true;


    //functions
    vm.toggleFilters = function(){
      vm.enableFilter = !vm.enableFilter;
    };
    function filterColumns(data, filterValues) {
      if(vm.filter){
        return data.filter(function(item){
          return (item.deal_number.toLowerCase().indexOf(filterValues.deal_number || '') !== -1) &&
            (item.sell_amount.toString().toLowerCase().indexOf(filterValues.sell_amount || '') !== -1) &&
            (item.beneficiary_name.toLowerCase().indexOf(filterValues.beneficiary_name || '') !== -1);
        });
      }else{
        return data;
      }
    }
    api.GetWebModules().then(function(data){
      vm.modules = data.output.table1;
    });
    api.GetWebUsers().then(function(data){
      vm.showDataTable = false;
      vm.webUsers = data.output.web_user;
      vm.tableParams = new NgTableParams({}, {
        counts: [5, 10, 20, 50, 100],
        dataset : vm.webUsers,
        filterOptions: {
          filterFn: filterColumns
        }
      });
    });
  }
})();
