(function () {
  'use strict';

  var app = angular
    .module('app.administration.manage_users', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider, msApiProvider) {
    // State
    $stateProvider.state('app.manage_users', {
      url: '/manage_users',
      views: {
        'content@app': {
          templateUrl: 'app/main/administration/manage_users/manage_users.html',
          controller: 'ManageUsersController as vm'
        }
      }
    });

    // Api
    msApiProvider.register('invoice', ['app/data/invoice/invoice.json']);
    //msApiProvider.register('invoice', ['app/data/invoice/invoice.json']);
  }
})();
