(function () {
  'use strict';

  angular
    .module('app.myaccount.account_statement')
    .controller('AccountStatementController', AccountStatementController);

  /** @ngInject */
  function AccountStatementController($scope, $window, $filter, NgTableParams,  $timeout, $mdDialog, AccountBalanceService, dateRangeFactory, $state, $rootScope, $mdToast, soap_api, DTColumnDefBuilder, QueryFactory, XmlJson) {
    var vm = this;
    vm.showEntitiesTable = true;
    vm.showAccountLoader = true;
    vm.showEntitiesLoader = true;
    vm.AccountListLoader = true;
    vm.enableFilter = false;
    vm.ready = true;
    vm.filter = {
      global_search: null,
      date_range: null,
      source_no: null,
      withdrawal: null,
      deposit: null,
      balance: null,
      deal_number: null,
      narration: null
    };
    vm.from_date = new Date(new Date().setFullYear( new Date().getFullYear() - 10 ));
    vm.to_date = new Date(new Date().setFullYear( new Date().getFullYear() + 2 ));
    vm.entityStatements = [];
    // vm.entityHeader = ['Date', 'Source No', 'Withdrawal', 'Deposit', 'Balance', 'Narration'];
    vm.entityHeader = ['Balance', 'Date', 'Deal Number', 'Deposit', 'Narration', 'Withdrawal'];
    vm.order = ['balance', 'trans_date', 'deal_number', 'deposit', 'narration', 'narration'];


    vm.stopInputing = function(event){
      event.preventDefault();
    };
    vm.entityWithDates = function(elem){
      var date = elem.trans_date.getDate !== undefined &&
        ('0' + elem.trans_date.getDate()).slice(-2) + '/' + ('0' + (elem.trans_date.getMonth()+1)).slice(-2) + '/' + elem.trans_date.getFullYear()
        || elem.trans_date;
      ;
      // elem.trans_date = $filter('date')(date, "dd/MM/yyyy");
      elem.trans_date = date;
      return elem;
    };
    function removeDepositBase(item)
    {
      delete item.deposit_base;
      return item;
    }
    vm.exportEntitiesDateFilter = function(){
      var entityStatements = vm.tableParams.data.map(vm.entityWithDates);
      console.log(entityStatements);
      return entityStatements;
    };
    vm.GetAllCurrencies = function(){
      var row = [];
      soap_api.GetAllCurrencies().then(function(success){
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT CURRENCY').each(function(i,e){
          var temp = {
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            currency_description: jQuery(e).find('CURRENCY_DESCRIPTION').text(),
            currency_detail: jQuery(e).find('CURRENCY_DETAIL').text(),
          };
          row.push(temp);
        });
      }, function(error){
        console.error(error.statusText);
      });
      return row;
    };
    vm.GetAccountList = function(){
      vm.AccountListLoader = false;
      return soap_api.GetAccountList().then(function(success){
        var row = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT Table1').each(function(i,e){
          var temp = {
            act_seg_id: jQuery(e).find('ACT_SEG_ID').text(),
            ent_act_type_name: jQuery(e).find('ENT_ACT_TYPE_NAME').text(),
            id: jQuery(e).find('ID').text(),
            limit: jQuery(e).find('LIMIT').text(),
            margin_factor: jQuery(e).find('MARGIN_FACTOR').text(),
            ent_act_bind_id: jQuery(e).find('ENT_TYPE_ORG_ID').text(),
            opening_date: jQuery(e).find('OPENING_DATE').text(),
            account_no: jQuery(e).find('ACCOUNT_NO').text(),
            investment_class: jQuery(e).find('INVESTMENT_CLASS').text(),
            interest_rate: jQuery(e).find('INTEREST_RATE').text(),
            pay_frequency: jQuery(e).find('PAY_FREQUENCY').text(),
            compounded: jQuery(e).find('COMPOUNDED').text(),
            branch_code: jQuery(e).find('BRANCH_CODE').text(),
            account_value: jQuery(e).find('ACCOUNT_VALUE').text(),
            ent_act_status: jQuery(e).find('ENT_ACT_STATUS').text(),
            curr_master_id: jQuery(e).find('CURR_MASTER_ID').text(),
            ent_act_type_id: jQuery(e).find('ENT_ACT_TYPE_ID').text(),
            currency_name: jQuery(e).find('CURRENCY_NAME').text(),
            currency_id: jQuery(e).find('CURRENCY_ID').text(),
            accountandcurrency: jQuery(e).find('ACCOUNTANDCURRENCY').text(),
            is_default: jQuery(e).find('IS_DEFAULT').text(),
            is_deposit: jQuery(e).find('IS_DEPOSIT').text(),
            is_withdrawal: jQuery(e).find('IS_WITHDRAWAL').text(),
            balance_act: jQuery(e).find('BALANCE_ACT').text(),
            balance_base: jQuery(e).find('BALANCE_BASE').text(),
            available_amount: jQuery(e).find('AVAILABLE_AMOUNT').text(),
            base_availabile_amount: jQuery(e).find('BASE_AVAILABILE_AMOUNT').text()
          };
          row.push(temp);
        });
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.GetEntity = function(){
      var row = [];
      vm.showAccountLoader = false;
      return soap_api.GetEntity().then(function(success){
        var temp1 = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT ENTITY').each(function(i,e){
          var temp = {
            name: jQuery(e).find('NAME').text(),
            entity_id: jQuery(e).find('ENTITY_ID').text(),
            entity_no: jQuery(e).find('ENTITY_NO').text(),
            entity_type_id: jQuery(e).find('ENTITY_TYPE_ID').text(),
            type_name: jQuery(e).find('TYPE_NAME').text(),
            status: jQuery(e).find('STATUS').text(),
            value: jQuery(e).find('VALUE').text(),
            entity_class_id: jQuery(e).find('ENTITY_CLASS_ID').text(),
            name1: jQuery(e).find('NAME1').text(),
            entity_org_id: jQuery(e).find('ENTITY_ORG_ID').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            relationship_manager_id: jQuery(e).find('RELATIONSHIP_MANAGER_ID').text(),
            relationship_manager_name: jQuery(e).find('RELATIONSHIP_MANAGER_NAME').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            version_no: jQuery(e).find('VERSION_NO').text(),
            is_deleted1: jQuery(e).find('IS_DELETED1').text()
          };
          temp1['entity'] = temp;
        });
        jQuery(response).find('OUTPUT ADDRESS').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            parent_type: jQuery(e).find('PARENT_TYPE').text(),
            address_type: jQuery(e).find('ADDRESS_TYPE').text(),
            address_type_value: jQuery(e).find('ADDRESS_TYPE_VALUE').text(),
            priority: jQuery(e).find('PRIORITY').text(),
            street_address: jQuery(e).find('STREET_ADDRESS').text(),
            post_code: jQuery(e).find('POST_CODE').text(),
            city: jQuery(e).find('CITY').text(),
            province: jQuery(e).find('PROVINCE').text(),
            countryid: jQuery(e).find('COUNTRYID').text(),
            country_value: jQuery(e).find('COUNTRY_VALUE').text(),
            is_default: jQuery(e).find('IS_DEFAULT').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            is_approved: jQuery(e).find('IS_APPROVED').text(),
            is_web_approved: jQuery(e).find('IS_WEB_APPROVED').text(),
            version_no: jQuery(e).find('VERSION_NO').text(),
            country_code: jQuery(e).find('COUNTRY_CODE').text()
          };
          temp1['address'] = temp;
        });
        jQuery(response).find('OUTPUT PHONE').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            parent_type: jQuery(e).find('PARENT_TYPE').text(),
            phone_type: jQuery(e).find('PHONE_TYPE').text(),
            phone_type_value: jQuery(e).find('PHONE_TYPE_VALUE').text(),
            phone_number: jQuery(e).find('PHONE_NUMBER').text(),
            priority: jQuery(e).find('PRIORITY').text(),
            is_default: jQuery(e).find('IS_DEFAULT').text(),
            created_on: jQuery(e).find('CREATED_ON').text(),
            created_by: jQuery(e).find('CREATED_BY').text(),
            updated_on: jQuery(e).find('UPDATED_ON').text(),
            updated_by: jQuery(e).find('UPDATED_BY').text(),
            is_deleted: jQuery(e).find('IS_DELETED').text(),
            is_approved: jQuery(e).find('IS_APPROVED').text(),
            version_no: jQuery(e).find('VERSION_NO').text(),
            is_web_approved: jQuery(e).find('IS_WEB_APPROVED').text()
          };
          temp1['phone'] = temp;
        });
        var temp2 = [];
        jQuery(response).find('OUTPUT WEB').each(function(i,e){
          var temp = {
            id: jQuery(e).find('ID').text(),
            parent_id: jQuery(e).find('PARENT_ID').text(),
            PARENT_TYPE_VALUE: jQuery(e).find('PARENT_TYPE_VALUE').text(),
            PARENT_TYPE: jQuery(e).find('PARENT_TYPE').text(),
            WEB_TYPE: jQuery(e).find('WEB_TYPE').text(),
            WEB_TYPE_VALUE: jQuery(e).find('WEB_TYPE_VALUE').text(),
            WEB_ADDRESS: jQuery(e).find('WEB_ADDRESS').text(),
            PRIORITY: jQuery(e).find('PRIORITY').text(),
            IS_DEFAULT: jQuery(e).find('IS_DEFAULT').text(),
            CREATED_ON: jQuery(e).find('CREATED_ON').text(),
            CREATED_BY: jQuery(e).find('CREATED_BY').text(),
            UPDATED_ON: jQuery(e).find('UPDATED_ON').text(),
            UPDATED_BY: jQuery(e).find('UPDATED_BY').text(),
            IS_DELETED: jQuery(e).find('IS_DELETED').text(),
            IS_APPROVED: jQuery(e).find('IS_APPROVED').text(),
            VERSION_NO: jQuery(e).find('VERSION_NO').text(),
            IS_WEB_APPROVED: jQuery(e).find('IS_WEB_APPROVED').text()
          };
          temp2.push(temp);
        });
        temp1['web'] = temp2;
        row.push(temp1);
        return row;
      }, function(error){
        console.error(error.statusText);
      });
    };
    vm.GetEntityAccountStatement = function(entity, from_date, to_date){
      return soap_api.GetEntityAccountStatement(entity, from_date, to_date).then(function(success){
        vm.showEntitiesLoader = false;
        var result = [];
        var response = jQuery.parseXML(success.data);
        jQuery(response).find('OUTPUT ENTITY_ACCOUNT_STATEMENT').each(function(i,e){
          var temp = {
            trans_date: jQuery(e).find('TRANS_DATE').text(),
            source_no: jQuery(e).find('SOURCE_NO').text(),
            withdrawal: jQuery(e).find('WITHDRAWAL').text(),
            deposit: jQuery(e).find('DEPOSIT').text(),
            deposit_base: jQuery(e).find('DEPOSIT_BASE').text(),
            deal_number: jQuery(e).find('DEAL_NUMBER').text(),
            narration: jQuery(e).find('NARRATION').text(),
            balance: jQuery(e).find('BALANCE').text()
          };
          result.push(temp);
        });
        jQuery(response).find('OUTPUT RESULT').each(function(i,e){
          var temp = {
            status: jQuery(e).find('STATUS').text(),
            recordid: jQuery(e).find('RECORDID').text(),
            error: jQuery(e).find('ERROR').text(),
            recordnumber: jQuery(e).find('RECORDNUMBER').text()
          };
          result.push(temp);
        });
        return result;
      });
    };
    vm.toLowerCase = function(value){
      return value.toLowerCase();
    };
    vm.toUpperCase = function(value){
      value = value || '';
      return value.toUpperCase();
    };
    vm.parseInt = function(value){
      return parseInt(value);
    };
    vm.parseFloat = function(value){
      if(value.length > 0)
        return parseFloat(value);
      else
        return parseFloat(0);
    };
    vm.replaceCommaWith = function(string, separator){
      string = string || '';
      return string.replace(/,+/g, separator);
    };
    vm.parseCurrency = function(value){
      if(typeof value != 'undefined'){
        return vm.parseFloat(vm.replaceCommaWith(value, ''))
      }
    };
    vm.toggleFilters = function(){
      vm.enableFilter = !vm.enableFilter;
    };

    $rootScope.headerTitle = 'Account Statement';
    vm.accountBalance = {};
    vm.accountBalance = AccountBalanceService.getAccountBalance();
    vm.GetAccountList().then(function(data){
      vm.accounts = data;
    });
    vm.GetEntity().then(function(data){
      var from = new Date(new Date().setFullYear( new Date().getFullYear() - 59 ));
      var to = new Date(new Date().setFullYear( new Date().getFullYear() + 10 ));

      from =  ('0' + (from.getMonth()+1)).slice(-2) + '/' + ('0' + from.getDate()).slice(-2) + '/' +from.getFullYear();
      to = ('0' + (to.getMonth()+1)).slice(-2) + '/' + ('0' + to.getDate()).slice(-2) + '/' +to.getFullYear();

      vm.account = {
        entity: data[0].entity.name || null,
        account: vm.accountBalance.accountandcurrency || null,
        date_from: null,
        date_to: null
      };
      if(_.isEmpty(vm.accountBalance) == false){
        vm.GetEntityAccountStatement(vm.accountBalance.id, from, to).then(function(data){
          //add new functionality
          for(var i=0; i<data.length; i++){

            if(data[i].trans_date == ''){
              data[i].trans_date = null;
            }else{
              data[i].trans_date = new Date(Date.parse(data[i].trans_date));
            }
            data[i].balance = vm.parseFloat(data[i].balance);
            data[i].deposit_base = vm.parseFloat(data[i].deposit_base);
            data[i].withdrawal = vm.parseFloat(data[i].withdrawal);
            vm.entityStatements.push(data[i]);

          }
          vm.tableParams = new NgTableParams({}, {
            counts: [5, 10, 20, 50, 100],
            dataset : vm.entityStatements,
            filterOptions: {
              filterFn: vm.filterForTable
            }
          });
          vm.searchFilterFunction = function(){
            vm.tableParams.filter({ $: vm.filter.global_search })
          };
          vm.dateRangeFilter = function(){
            //var filteredData = dateRangeFactory.filterData(vm.entityStatements, vm.filter.date_range)
            // $filter('dateRangeFilter')(vm.entityStatements, vm.filter.date_range);
            console.log(vm.filter.date_range)
            var filteredData = dateRangeFactory.filterData(vm.entityStatements, vm.filter.date_range);
            // $filter('dateRangeFilter')(vm.entityStatements, vm.filter.date_range);
            // dateRangeFactory.filterData(vm.entityStatements, vm.filter.date_range);
            vm.tableParams = new NgTableParams({}, {
              counts: [5, 10, 20, 50, 100],
              dataset : filteredData
            });
            if(filteredData.length >0){
              vm.entitySearch = {
                min_value : searchValue("top", filteredData[0] ),
                max_value : searchValue("last", filteredData[data.length -1]),
              };
            }
            updatePageCount();
          };
          vm.entitySearch = {
            min_value : searchValue("top", data[0] ),
            max_value : searchValue("last", data[data.length -1]),
          };
          vm.showEntitiesTable = true;
          $timeout(function(){
            vm.showAccountLoader = true;
            vm.showEntitiesTable = false;
          }, 500);
          if(vm.entityStatements.length <= 0){
            $mdToast.show({
              template: '<md-toast class="md-toast info">Records not found.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
          }
        });
      }else{

        vm.showAccountLoader = true;
        vm.AccountListLoader = true;
      }
    });
    vm.filterAll = function(){
      vm.tableParams.filter({
        source_no: vm.filter.source_no,
        withdrawal: vm.filter.withdrawal,
        deposit: vm.filter.deposit,
        balance: vm.filter.balance,
        deal_number: vm.filter.deal_number,
        narration:  vm.filter.narration
      });
    };
    vm.filterForTable = function(data, filterValues){
      if(angular.isDefined(vm.filter)){
        var returnedData = data.filter(function(item){
          return (item.source_no.toString().indexOf(filterValues.source_no || '') !== -1) &&
            (item.withdrawal.toString().indexOf(filterValues.withdrawal || '') !== -1) &&
            (item.deposit.toString().indexOf(filterValues.deposit || '') !== -1) &&
            (item.balance.toString().indexOf(filterValues.balance || '') !== -1) &&
            (item.deal_number.toString().indexOf(filterValues.deal_number || '') !== -1) &&
            (item.narration.toString().indexOf(filterValues.narration || '') !== -1);
        });
        if(returnedData.length === 0){
          returnedData.push({message: 'No match found'});
        }
        return returnedData;
      }else{
        return data;
      }
    };
    vm.search = {
      currency : '',
      available_balance: '',
      closing_balance: '',
      title: ''
    };
    vm.printNow = function(){
      $window.print();
    };
    vm.currencies = vm.GetAllCurrencies();
    vm.showTable = true;
    vm.showTableFunc = function(event){
      vm.showTable = false;
      vm.search = {
        currency : '',
        balance: '',

        closing_balance: '',
        title: ''
      };
      vm.GetAccountList().then(function(data){
        vm.accounts = data;
        vm.AccountListLoader = true;
      });
    };
    vm.hideTableFunc = function(event){
      vm.showTable = true;
    };
    var printAccountData;
    vm.selectAccount = function(account, event){
      printAccountData = account;
      vm.account.account = account.accountandcurrency;
      vm.account.entity = vm.account.entity;
      vm.hideTableFunc(event);
    };
    vm.entitySearch = {
      min_value : null,
      max_value : null
    };
    vm.printRow = function(){
      vm.printLoader = true;

      var date_from = vm.searchRecord.date_from && vm.searchRecord.date_from || backtoFiftyNineYears();
      var date_to   = vm.searchRecord.date_to   && vm.searchRecord.date_to   || goTenYearsAhead();
     var input = '<INPUT>' +
      '<FROM_DATE>'+ convertDateFormat(date_from) +'</FROM_DATE>' +
      '<TO_DATE>'+ convertDateFormat(date_to) +'</TO_DATE>'+
      '<ENT_ACT_ID>'+
      '<ID>'+printAccountData.id+'</ID>'+
      '</ENT_ACT_ID>'+
      '</INPUT>';

      QueryFactory.query('GetEntityStatmentReport', input ).then(success, error);
        function success(response){
          var xml = response.data;
          var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
          var data = XmlJson.xmlToJson(xmlDOM);
          var blob = vm.b64toBlob(data.output.result.value, 'application/pdf');
         saveAs(blob, data.recordid);
        }
        function error(err){
          console.log(err);
        }

      // vm.GetWireReport(data.settlement_id).then(function(data){
      //   data = data[0];
      //   vm.printLoader = false;
      //   if(data.status == 'TRUE'){
      //     var blob = vm.b64toBlob(data.value, 'application/pdf');
      //     saveAs(blob, data.recordid);
      //   }else{
      //     $mdToast.show({
      //       template: '<md-toast class="md-toast error">No report found.</md-toast>',
      //       hideDelay: 3000,
      //       position: 'bottom right'
      //     });
      //   }
      // });
    };
    vm.searchRecords = function(account){
      vm.searchRecord = account;

      // vm.filter.date_range = null;
      vm.filter = {};
      vm.entityStatements = [];
      vm.enableFilter = false;
      if(document.getElementById('drp_autogen1') !== null && document.getElementById('drp_autogen1').childNodes[0].textContent !== "By Date"){
        document.getElementById('drp_autogen1').childNodes[0].textContent = "By Date";
        $rootScope.$broadcast('clearFilter', "clearFIlter");
      }

      // jQuery("#date_picker").daterangepicker();

      vm.showEntitiesTable = true;
      vm.showAccountLoader = false;
      if(account.entity === null || account.account === null ){
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please fill required fields.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        vm.showAccountLoader = true;
        vm.showEntitiesTable = true;
        return true;
      }else{
        var original_account = _.filter(vm.accounts, {accountandcurrency: account.account});
        original_account = original_account[0];

        var date_from = account.date_from && account.date_from || backtoFiftyNineYears();
        var date_to   = account.date_to   && account.date_to   || goTenYearsAhead();
        vm.GetEntityAccountStatement(original_account.id, date_from, date_to).then(function(data){
          if(data.length > 0){
            vm.showAccountLoader = true;
            vm.showEntitiesTable = false;
            for(var i=0; i<data.length; i++){
              if(data[i].trans_date == ''){
                data[i].trans_date = null;
              }else{
                data[i].trans_date = new Date(Date.parse(data[i].trans_date));
              }
              data[i].balance = vm.parseFloat(data[i].balance);
              data[i].deposit_base = vm.parseFloat(data[i].deposit_base);
              data[i].withdrawal = vm.parseFloat(data[i].withdrawal);
              vm.entityStatements.push(data[i]);
              // vm.filter.date_range = "BY DATE"
            }
            vm.tableParams = new NgTableParams({}, {
              counts: [5, 10, 20, 50, 100],
              dataset : vm.entityStatements,
              filterOptions: {
                filterFn: vm.filterForTable
              }
            });
            vm.searchFilterFunction = function(){

              vm.tableParams.filter({ $: vm.filter.global_search });
            };
            vm.dateRangeFilter = function(){

              //var filteredData = dateRangeFactory.filterData(vm.entityStatements, vm.filter.date_range)
              // $filter('dateRangeFilter')(vm.entityStatements, vm.filter.date_range);
              var filteredData = dateRangeFactory.filterData(vm.entityStatements, vm.filter.date_range);
              // $filter('dateRangeFilter')(vm.entityStatements, vm.filter.date_range);
              vm.tableParams = new NgTableParams({}, {
                counts: [5, 10, 20, 50, 100],
                dataset : filteredData
              });

              if(filteredData.length >0){
                vm.entitySearch = {
                  min_value : searchValue("top", filteredData[0] ),
                  max_value : searchValue("last", filteredData[filteredData.length -1]),
                };
              }
              updatePageCount();
            };
            vm.entitySearch = {
              min_value : searchValue("top", data[0] ),
              max_value : searchValue("last", data[data.length -1]),
            };
          }else{
            vm.showAccountLoader = true;
            vm.showEntitiesTable = true;
            $mdToast.show({
              template: '<md-toast class="md-toast info">Records not found.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
            return true;
          }
        });
      }

    };
    vm.searchEntityRecord = function(account){
      vm.entityStatements = [];
      vm.showEntitiesTable = true;
      vm.showAccountLoader = false;
      if(account.entity == null || account.account == null || account.date_from == null || account.date_to == null){
        $mdToast.show({
          template: '<md-toast class="md-toast error">Please fill required fields.</md-toast>',
          hideDelay: 4000,
          position: 'bottom right'
        });
        vm.showAccountLoader = true;
        return;
      }else{
        vm.showEntitiesLoader = false;
        //vm.showEntitiesTable = false;
        var original_account = _.filter(vm.accounts, {accountandcurrency: account.account});
        vm.GetEntityAccountStatement(original_account[0].id, account.date_from, account.date_to).then(function(data){
          if(data.length === 0 ){
            vm.entityStatements = null;
            vm.showEntitiesTable = true;
            vm.showEntitiesLoader = true;
            vm.showAccountLoader = true;
            vm.AccountListLoader = true;
            $mdToast.show({
              template: '<md-toast class="md-toast info">Records not found.</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
            return;
          }
          if(data[0].status === 'TRUE'){
            vm.entityStatements = null;
            vm.showEntitiesTable = true;
            vm.showEntitiesLoader = true;
            $mdToast.show({
              template: '<md-toast class="md-toast info">'+data[0].error+'</md-toast>',
              hideDelay: 4000,
              position: 'bottom right'
            });
            vm.showAccountLoader = true;
          }else{
            $mdToast.show({
              template: '<md-toast class="md-toast success">Records found.</md-toast>',
              hideDelay: 2000,
              position: 'bottom right'
            });
            for(var i=0; i<data.length; i++){
              vm.ready = true;
              if(data[i].trans_date === ''){
                data[i].trans_date = null;
              }else{
                data[i].trans_date = new Date(Date.parse(data[i].trans_date));
              }
              data[i].balance = vm.parseFloat(data[i].balance);
              data[i].deposit_base = vm.parseFloat(data[i].deposit_base);
              data[i].withdrawal = vm.parseFloat(data[i].withdrawal);
              vm.entityStatements.push(data[i]);
            }

            vm.tableParams = new NgTableParams({}, {
              counts: [5, 10, 20, 50, 100],
              dataset : vm.entityStatements
            });
            vm.entitySearch = {
              min_value : original_account[0].available_amount,
              max_value : original_account[0].balance_act
            };
            vm.showEntitiesTable = true;
            vm.showEntitiesLoader = false;
            vm.showAccountLoader = true;
            $timeout(function(){
              vm.showEntitiesTable = false;
              vm.showAccountLoader = true;
            }, 500);
          }
        });
      }
    };
    vm.openDealDialog= function(statement){
      console.log(statement);
    };
    vm.searchFilterFunction = function(){
      vm.tableParams.filter({ $: vm.filter.global_search })
    };
    vm.dateRangeFilter = function(){

      var filteredData = $filter('dateRangeFilter')(vm.entityStatements, vm.filter.date_range);
      vm.tableParams = new NgTableParams({}, {
        counts: [5, 10, 20, 50, 100],
        dataset : filteredData
      });
    };
    vm.b64toBlob = function (b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    };
    vm.isXML = function(xml){
      try{
        var xmlDoc = jQuery.parseXML(xml);
        return true;
      }catch(error){
        return false;
      }
    };
    vm.openDealWindow = function(data) {
      vm.showAccountLoader = false;
      getReport(data).then(function (data) {
        console.log(data)
        data = data[0];
        vm.showCircleLoader = true;
        if (data.status == 'TRUE') {
          var blob = vm.b64toBlob(data.value, 'application/pdf');
          // saveAs(blob, data.recordid);
          window.open(URL.createObjectURL(blob));
          // saveAs(blob, data.recordid);
          // window.open(URL.createObjectURL(blob));
        } else {
          $mdToast.show({
            template: '<md-toast class="md-toast error">No report found.</md-toast>',
            hideDelay: 3000,
            position: 'bottom right'
          });
        }
        vm.showAccountLoader = true;
      });
    }
    function getReport(deal){
      return soap_api.GetDealReport(deal).then(function(success){
        if(vm.isXML(success.data)){
          var row = [];
          var response = jQuery.parseXML(success.data);
          jQuery(response).find('OUTPUT RESULT').each(function(i,e){
            var temp = {
              status: jQuery(e).find('STATUS').text(),
              recordid: jQuery(e).find('RECORDID').text(),
              error: jQuery(e).find('ERROR').text(),
              value: jQuery(e).find('VALUE').text()
            };
            row.push(temp);
          });
          return row;
        }
      });

      // $mdDialog.show({
      //   fullscreen: true,
      //   clickOutsideToClose: true,
      //   scope: $scope,
      //   preserveScope: true,
      //   templateUrl: 'app/main/myaccount/account_statement/accountdeal_information.html',
      //   parent: angular.element(document.body),
      //   controller: function dialogController($scope, $mdDialog){
      //     $scope.showPayeeTable  = true;
      //     $scope.dealInformation = data;
      //     $scope.row = data;
      //     $scope.closeDialog = function() {
      //       $mdDialog.hide();
      //     };
      //   }
      // });
    }
    vm.moveFirstToLast = function(firstOrLast){
      if(firstOrLast === 'last')
      {
        var lastPage = updatePageCount();
        vm.tableParams.page(lastPage);
        return;
      }
      vm.tableParams.page(1);
    };
    function updatePageCount(){
      return Math.ceil(vm.tableParams.total()/vm.tableParams.count());
    }
    function backtoFiftyNineYears(){
      var date = new Date(new Date().setFullYear(new Date().getFullYear() - 59));
      return convertDateFormat(date);
    }
    function goTenYearsAhead(){
      var date = new Date(new Date().setFullYear(new Date().getFullYear() + 10));
      return convertDateFormat(date);
    }
    function convertDateFormat(date){
      var date = new Date(date);
      return date.getFullYear() + '-' + ('0' + (date.getMonth()+1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2)
    }
    function searchValue(topOrLast, data){
      if(!data){
        return ;
      }
      return topOrLast === "top" && ((data.balance + data.withdrawal) - parseInt(data.deposit)) ||
        ((data.balance ));
      // - data.withdrawal)  - parseInt(data.deposit)
    }
  }
})();
