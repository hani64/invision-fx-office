(function ()
{
  'use strict';

  angular
    .module('app.myaccount.account_statement', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider)
  {
    // State
    $stateProvider.state('app.account_statement', {
      url      : '/account_statement',
      views    : {
        'content@app': {
          templateUrl: 'app/main/myaccount/account_statement/account_statement.html',
          controller : 'AccountStatementController as vm',
          resolve: {
            access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
          },

        }
      }
    });

    // Api

  }

})();
