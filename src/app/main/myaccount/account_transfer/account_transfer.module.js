(function ()
{
  'use strict';

  angular
    .module('app.myaccount.account_transfer', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider)
  {
    // State
    $stateProvider.state('app.account_transfer', {
      url      : '/account_transfer',
      views    : {
        'content@app': {
          templateUrl: 'app/main/myaccount/account_transfer/account_transfer.html',
          controller : 'AccountTransferController as vm',
          resolve: {
            access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
          },
        }
      }
    });

    // Api

  }

})();
