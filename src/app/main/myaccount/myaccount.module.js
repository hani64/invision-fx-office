(function () {
  'use strict';

  angular
    .module('app.myaccount', [
      'app.myaccount.accountbalance',
      'app.myaccount.account_statement',
      'app.myaccount.account_transfer'
    ])
    .config(config);

  /** @ngInject */
  function config(msNavigationServiceProvider) {

    msNavigationServiceProvider.saveItem('main_menu.myaccount', {
      title: 'My Account',
      icon: 'icon-account-box-outline',
      weight: 1,
      child_count: 3
    });

    msNavigationServiceProvider.saveItem('main_menu.myaccount.account_balances', {
      title: 'Account Balance',
      state: 'app.accountbalance',
      weight: 1,
      module_id: 11411
    });


    msNavigationServiceProvider.saveItem('main_menu.myaccount.account_statement', {
      title: 'Account Statement',
      state: 'app.account_statement',
      weight: 1,
      module_id: 1141
    });


    msNavigationServiceProvider.saveItem('main_menu.myaccount.account_transfer', {
      title: 'Account Transfer',
      state: 'app.account_transfer',
      weight: 1,
      module_id: 1139
    });

  }
})();
