(function ()
{
  'use strict';

  angular
    .module('app.myaccount.accountbalance', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider)
  {
    // State
    $stateProvider.state('app.accountbalance', {
      url      : '/accountbalance',
      views    : {
        'content@app': {
          templateUrl: 'app/main/myaccount/accountbalance/accountbalance.html',
          controller : 'AccountBalanceController as vm',
          resolve: {
            access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
          },
        }
      }
    });

    // Api

  }

})();
