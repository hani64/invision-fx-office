(function () {
  'use strict';

  angular
    .module('app.myaccount.accountbalance')
    .controller('AccountBalanceController', AccountBalanceController);

  /** @ngInject */
  function AccountBalanceController(api, NgTableParams, $rootScope, AccountBalanceService, $state) {

    var vm = this;
    vm.module_id = 11411;
    $rootScope.headerTitle = 'Account Balance';
    vm.hideFilters = true;
    vm.loader = false;
    vm.accounts = [];
    // vm.filter = {
    //   account_title: null,
    //   available_balance: null,
    //   closing_balance: null
    // };

    vm.toggleFilters = function(){
      vm.hideFilters = !vm.hideFilters;
    };
    api.GetAccountList().then(function(data){
      vm.accounts = data.output.table1;
      vm.tableParams = new NgTableParams({}, {
        counts: [5, 10, 20, 50, 100],
        dataset : vm.accounts,
        filterOptions: {
          filterFn: vm.filterForTable
        }
      });
      vm.loader = true;
    });
    vm.filterAll = function(){
      vm.tableParams.filter({
        account_title: vm.filter.account_title,
        available_balance: vm.filter.available_balance,
        closing_balance: vm.filter.closing_balance
      });
    };
    vm.filterForTable = function(data, filterValues){
      if(angular.isDefined(vm.filter)){
        var returnedData = data.filter(function(item){
            return (item.accountandcurrency.toString().toLowerCase().indexOf(filterValues.account_title || '') !== -1) &&
              (item.available_amount.toString().indexOf(filterValues.available_balance || '') !== -1) &&
              (item.balance_act.toString().indexOf(filterValues.closing_balance || '') !== -1);

        });
        if(returnedData.length === 0){
          returnedData.push({message:'No match found'});
        }
        return returnedData;
      }else{
        return data;
      }
    };
    vm.clearAll = function(){
      vm.filter = {
        account_title: null,
        available_balance: null,
        closing_balance: null
      };
      vm.filterAll();
    };
    AccountBalanceService.setAccountBalance({});
    vm.openStatement = function (account, event) {
      event.preventDefault();
      AccountBalanceService.setAccountBalance(account);
      $state.go('app.account_statement');
    };
  }
})();
