(function () {
  'use strict';

  angular
    .module('app.mail',
      []
    )
    .config(config);

  /** @ngInject */
  function config($stateProvider, msApiProvider) {
    // State
    $stateProvider
      .state('app.mail', {
        url: '/book_a_spot_deal',
        views: {
          'content@app': {
            templateUrl: 'app/main/mail/mail.html',
            controller: 'MailController as vm'
          }
        },
        bodyClass: 'mail',
        resolve: {
          access: ["AuthenticateFactory", function (AuthenticateFactory) { return AuthenticateFactory.isAuthenticated(); }],
          Invoice: function (msApi) {
            return msApi.resolve('invoice@get');
          }
        }

      });

    // Api
    msApiProvider.register('invoice', ['app/data/invoice/invoice.json']);
  }
})();
