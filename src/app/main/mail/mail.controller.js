(function () {
  'use strict';

  angular
    .module('app.mail')
    .controller('MailController', MailController);

  /** @ngInject */
  function MailController($scope, $rootScope, $document, $mdDialog, $state, $filter, $interval, $mdToast, Invoice) {
    var vm = this;

    vm.colors = ['blue-bg', 'blue-grey-bg', 'orange-bg', 'pink-bg', 'purple-bg'];


    vm.currentFilter = {
      type: $state.params.type,
      filter: $state.params.filter
    };

    vm.views = {
      outlook: 'app/main/mail/views/outlook/outlook-view.html'
    };


    vm.composeDialog = composeDialog;
    vm.replyDialog = replyDialog;


    /**
     * Open compose dialog
     *
     * @param ev
     */

    function composeDialog(ev) {
      $mdDialog.show({
        controller: 'ComposeDialogController',
        controllerAs: 'vm',
        locals: {
          selectedMail: undefined
        },
        templateUrl: 'app/main/apps/mail/dialogs/compose/compose-dialog.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: true
      });
    }

    /**
     * Open reply dialog
     *
     * @param ev
     */
    function replyDialog(ev) {
      $mdDialog.show({
        controller: 'ComposeDialogController',
        controllerAs: 'vm',
        locals: {
          selectedMail: vm.selectedMail
        },
        templateUrl: 'app/main/apps/mail/dialogs/compose/compose-dialog.html',
        parent: angular.element($document.body),
        targetEvent: ev,
        clickOutsideToClose: true
      });
    }


    //----------------------------------------------------------------


    /**
     * Steps of book a spot
     * @type {*[]}
     */
    vm.steps = [
      {
        id: 1,
        title: "BOOK A SPOT DEAL",
        description: "Step 1 - Please select the Currency and the Amount then click Get Quote"
      },
      {
        id: 2,
        title: "CONFIRM DEAL",
        description: "Step 2 - Review the Rate and Click Accept to book the Deal"
      },
      {
        id: 3,
        title: "BENEFICIARY",
        description: "Step 3 - Click add Payee"
      },
      {
        id: 4,
        title: "FUNDING",
        description: "Step 4 - How will you be paying for the Deal ?"
      },
      {
        id: 5,
        title: "CONFIRMATION",
        description: "Step 5 - Confirmation"
      }
    ];


    /**
     * Currencies
     * @type {*[]}
     */
    vm.currencies = [
      {
        "value": "CAD",
        "name": "CAD"
      },
      {
        "value": "AUS",
        "name": "AUS"
      },

      {
        "value": "USD",
        "name": "USD"
      }
    ];


    /**
     * Beneficiaries
     * @type {Array}
     */
    vm.beneficiaries = [];

    /**
     * Fundings Array
     * @type {Array}
     */
    vm.fundings = [];


    /**
     * Invoice Data
     */
    vm.invoice = Invoice.data;

    /**
     * Intililize first step as current step
     * @type {*}
     */
    vm.currentStep = vm.steps[0];

    /**
     * Data table init
     * @type {{dom: string, pagingType: string, autoWidth: boolean, responsive: boolean, search: boolean}}
     */
    vm.dtOptions = {
      //dom: '<"top"f>rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
      dom: '',
      pagingType: 'none',
      autoWidth: false,
      responsive: true,
      search: false
    };
    /**
     * For step 2.
     * @type {number}
     */
    vm.determinateValue = 100;
    vm.interval_ = null;


    /**
     * Show 50 second preloader for
     * step 2
     */
    vm.showLoader = function () {

      if (vm.currentStep.id === 2) {

        vm.determinateValue = 100;
        vm.expire_in = 50;
        vm.interval_ = $interval(function () {

          vm.determinateValue -= 2;
          vm.expire_in -= 1;
          if (vm.determinateValue < 1) {
            vm.currentStep = vm.steps[0];
            vm.determinateValue = 100;
            vm.expire_in = 50;
            $interval.cancel(vm.interval_);
          }

        }, 1000);
      } else {
        vm.determinateValue = 100;
        vm.expire_in = 50;
        $interval.cancel(vm.interval_);
        vm.interval_ = null;
      }

    };


    /**
     * Check if the step is selected
     * @param step
     */
    vm.isSelectedStep = function (step) {
      return vm.currentStep == step;
    };


    /**
     * Open the step
     * @param step
     */
    vm.openStep = function (step) {
      vm.currentStep = step;
    };


    /**
     * Move to next step.
     */
    vm.nextStep = function () {

      for (var i = 0; i < vm.steps.length; i++) {
        if (vm.steps[i].id === vm.currentStep.id)
          vm.steps[i].is_completed = true;
      }
      vm.currentStep = $filter('getById')(vm.steps, vm.currentStep.id + 1);
      vm.showLoader();
    };


    /**
     * Move to next step.
     */
    vm.previousStep = function () {

      for (var i = 0; i < vm.steps.length; i++) {
        if (vm.steps[i].id === vm.currentStep.id || vm.steps[i].id === vm.currentStep.id - 1)
          vm.steps[i].is_completed = false;
      }
      vm.currentStep = $filter('getById')(vm.steps, vm.currentStep.id - 1);
      vm.showLoader();
    };


    /**
     * Checks if the step is completed.
     * @param step
     * @returns {boolean}
     */
    vm.isStepComplete = function (step) {
      return typeof step.is_completed !== 'undefined' && step.is_completed;
    };


    /**
     * Add beneficiary
     * @param beneficiary
     */
    vm.addBeneficiary = function (beneficiary_) {
      console.log(beneficiary_);
      var beneficiary = beneficiary_;
      var id = vm.beneficiaries.length + 1;

      beneficiary.id = id;
      beneficiary.method = "Wire";
      beneficiary.currency = "CAD";
      beneficiary.fee = "1.66";


      if (beneficiary.amount != '' && beneficiary.payee_ != '') {
        vm.beneficiaries.push(beneficiary);
        vm.beneficiary_ = {};
      } else {


        $mdToast.show(
          $mdToast.simple()
            .textContent('Please Fill in all the required Fields.')
            .position("bottom right")
            .hideDelay(3000)
        );
      }

    };

    /**
     * Add deal to the array.
     * @param deal
     */
    vm.add_funds = function (fund) {
      console.log(fund);
      fund.currency = "USD";

      if (fund.amount != '' && fund.method != '') {
        vm.fundings.push(fund);
      } else {


        $mdToast.show(
          $mdToast.simple()
            .textContent('Please Fill in all the required Fields.')
            .position("bottom right")
            .hideDelay(3000)
        );
      }

    };

  }
})();
