(function ()
{
  'use strict';

  angular
    .module('fuse')
    .factory('api', apiService)
    .service('soap_api',function($http, $state, $q, $mdToast, $localStorage, $sessionStorage, cryptoService, $rootScope, $location){
      if(typeof $localStorage.rememberMe == 'undefined'){
        $location.url('/auth/login');
      }
      if(typeof $localStorage.rememberMe == 'string'){
        var rememberMe = angular.fromJson($localStorage.rememberMe);
        if(rememberMe.remember == false) {
          this.storage = $sessionStorage
          // this.storage = $sessionStorage;
        }else{
          this.storage = $sessionStorage
        }
      }
      this.baseUrl = 'http://68.68.5.189:7070/fxoffice.asmx/CallService';
      this.UserCredentials = {
        userName: null,
        passWord: null,
        OrgID: null,
        custID: null,
        email: null
      };
      this.method = '';
      this.input = '';
      this.result = [];
      this.ValidateUserLogin = function(username, password, OrgID, custID, email){
        this.UserCredentials = {
          userName: username,
          passWord: password,
          OrgID: OrgID,
          custID: custID,
          email: email
        };
        this.method = 'ValidateUserLogin';
        this.input = '' +
          '<INPUT>' +
          '<USER>' +
          '<Value>'+username+'</Value>' +
          '</USER>' +
          '<CUSTOMER_ID>'+custID+'</CUSTOMER_ID>' +
          '<PASSWORD>'+password+'</PASSWORD>' +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: this.UserCredentials.userName,
              password: this.UserCredentials.passWord,
              OrgID: this.UserCredentials.OrgID,
              CustID: this.UserCredentials.custID,
              email: this.UserCredentials.email
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetAllCurrencies = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }
        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetAllCurrencies';
        this.input = '';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetPayeesByCurrencyAndPaymentMethod = function(currency, method){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetAllPayeeTemplates';
        currency = currency.toUpperCase();
        method = method.toUpperCase();
        //for settlements pass payment_type ID as 696 which is incoming.
        this.input = '<INPUT>' +
          '<PAYEE>' +
          '<PAYMENT_TYPE><ID>696</ID></PAYMENT_TYPE>' +
          '<PAYMENT_METHOD><ID>'+method+'</ID></PAYMENT_METHOD>' +
          '<CURRENCY><Value>'+currency+'</Value></CURRENCY>' +
          '</PAYEE></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetRate = function(buy_currency, buy_amount, sell_currency, sell_amount, value_date, client_name){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        value_date = typeof value_date == 'object' ? value_date : new Date();
        value_date = (value_date.getMonth() + 1) + '/' + value_date.getDate() + '/' + value_date.getFullYear();
        buy_currency = buy_currency.toUpperCase();
        sell_currency = sell_currency.toUpperCase();
        this.method = 'GetRate';
        this.input = '<Data>' +
          '<ENTITY>' +
          '<Value>'+client_name+'</Value>' +
          '</ENTITY>' +
          '<BUY_ITEM>' +
          '<Value>'+buy_currency+'</Value>' +
          '</BUY_ITEM>' +
          '<SELL_ITEM>' +
          '<Value>'+sell_currency+'</Value>' +
          '</SELL_ITEM>' +
          '<VALUE_DATE>'+value_date+'</VALUE_DATE>' +
          '<BUY_AMOUNT>'+buy_amount+'</BUY_AMOUNT>' +
          '<SELL_AMOUNT>'+sell_amount+'</SELL_AMOUNT>' +
          '</Data>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetRateByIDS = function(buy_currency, buy_amount, sell_currency, sell_amount, value_date, client_name){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        value_date = typeof value_date == 'object' ? value_date : new Date();
        value_date = (value_date.getMonth() + 1) + '/' + value_date.getDate() + '/' + value_date.getFullYear();

        this.method = 'GetRate';
        this.input = '<Data>' +
          '<ENTITY>' +
          '<Value>'+client_name+'</Value>' +
          '</ENTITY>' +
          '<BUY_ITEM>' +
          '<ID>'+buy_currency+'</ID>' +
          '</BUY_ITEM>' +
          '<SELL_ITEM>' +
          '<ID>'+sell_currency+'</ID>' +
          '</SELL_ITEM>' +
          '<VALUE_DATE>'+value_date+'</VALUE_DATE>' +
          '<BUY_AMOUNT>'+buy_amount+'</BUY_AMOUNT>' +
          '<SELL_AMOUNT>'+sell_amount+'</SELL_AMOUNT>' +
          '</Data>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetPurposeOfPayments = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetPurposeOfPayments';
        this.input = '';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetAllPayeeTemplates = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetAllPayeeTemplates';
        this.input = '<INPUT><PAYEE></PAYEE></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetCountries = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetCountries';
        this.input = '';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.ValidateUserOnlineTradingHours = function(date){

        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));
        date = typeof date == 'object' ? date : new Date();
        date = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        this.method = 'ValidateUserOnlineTradingHours';
        this.input = '<INPUT><TIME>'+date+'</TIME></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetAccountList = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetAccountList';
        this.input = '';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetFeeByMethod = function(method){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetFeeByMethod';
        this.input = '<INPUT><METHOD><Value>'+method+'</Value></METHOD></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetFeeByMethodByID = function(ID){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(!rememberMe.remember) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetFeeByMethod';
        this.input = '<INPUT><METHOD><ID>'+ID+'</ID></METHOD></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetWebModules = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(!rememberMe.remember) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetUserWebModules';
        this.input = '';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetEntity = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetEntity';
        this.input = '<INPUT><ENTITY_NO>'+login.CustID+'</ENTITY_NO></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetFundingCurrencies = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetCurrencyForDeal';
        this.input = '<INPUT><CURRENCY_ID>2</CURRENCY_ID><IS_BUY>true</IS_BUY></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetFundingCurrenciesFilterByCurrency = function(currency_id){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetCurrencyForDeal';
        this.input = '<INPUT><CURRENCY_ID>'+currency_id+'</CURRENCY_ID><IS_BUY>true</IS_BUY></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetFundingCurrenciesWithoutFilter = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage

          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetCurrencyForDeal';
        this.input = '<INPUT><IS_BUY>true</IS_BUY></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetPaymentCurrencies = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetCurrencyForDeal';
        this.input = '<INPUT><CURRENCY_ID>0</CURRENCY_ID><IS_BUY>false</IS_BUY></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetPaymentCurrenciesFilterByCurrency = function(currency_id){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetCurrencyForDeal';
        this.input = '<INPUT><CURRENCY_ID>'+currency_id+'</CURRENCY_ID><IS_BUY>false</IS_BUY></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetPaymentCurrenciesWithoutFilter = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetCurrencyForDeal';
        this.input = '<INPUT><IS_BUY>false</IS_BUY></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetEntityAccountStatement = function(entity, from_date, to_date){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        from_date = typeof from_date == 'object' ? from_date: new Date(Date.parse(from_date));
        from_date = from_date.getFullYear() + '-' + ('0' + (from_date.getMonth()+1)).slice(-2) + '-' + ('0' + from_date.getDate()).slice(-2);

        //console.log(to_date);
        to_date = typeof to_date == 'object' ? to_date : new Date(Date.parse(to_date));
        to_date = to_date.getFullYear() + '-' + ('0' + (to_date.getMonth()+1)).slice(-2) + '-' + ('0' + to_date.getDate()).slice(-2);

        this.method = 'GetEntityAccountStatement';
        this.input = '<INPUT>' +
          '<FROM_DATE>'+from_date+'</FROM_DATE>' +
          '<TO_DATE>'+to_date+'</TO_DATE>' +
          '<ENT_ACT_ID><ID>'+entity+'</ID></ENT_ACT_ID>' +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.SendForgotPasswordEmail = function(customerID, orgID, username, password, email, site_url){

        this.method = 'SendForgotPasswordEmail';
        this.input = '<INPUT><SITE_URL>'+site_url+'</SITE_URL></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: username,
              password: password,
              OrgID: orgID,
              CustID: customerID,
              email: email
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetEntityWebDefaults = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetEntityWebDefaults';
        this.input = '';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.CreateWebDeal = function(client, trader, notes, internal_remarks, value_date, is_bach, deal_transaction){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'CreateWebDeal';
        this.input = '<INPUT> ' +
          '<DETAIL> ' +
          '<CLIENT> ' +
          '<Value>'+client+'</Value> ' +
          '</CLIENT> ' +
          '<TRADER> ' +
          '<ID>'+trader+'</ID> ' +
          '</TRADER> ' +
          '<NOTES>'+notes+'</NOTES> ' +
          '<INTERNAL_REMARKS>'+internal_remarks+'</INTERNAL_REMARKS> ' +
          '</DETAIL> ' +
          '<DEAL> ' +
          '<VALUE_DATE>'+value_date+'</VALUE_DATE> ' +
          '<IS_BATCH>'+is_bach+'</IS_BATCH> ' +
          '</DEAL>' +
          deal_transaction +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.CreateWebDealWithSettlementAndPayments = function(client, trader, notes, internal_remarks, value_date, is_bach, deal_transaction, payments, settlements){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'CreateWebDeal';
        this.input = '<INPUT> ' +
          '<DETAIL> ' +
          '<CLIENT> ' +
          '<ID>'+client+'</ID> ' +
          '</CLIENT> ' +
          '<TRADER> ' +
          '<ID>'+trader+'</ID> ' +
          '</TRADER> ' +
          '<NOTES>'+notes+'</NOTES> ' +
          '<INTERNAL_REMARKS>'+internal_remarks+'</INTERNAL_REMARKS> ' +
          '</DETAIL> ' +
          '<DEAL> ' +
          '<VALUE_DATE>'+value_date+'</VALUE_DATE> ' +
          '<IS_BATCH>'+is_bach+'</IS_BATCH> ' +
          '</DEAL>' +
          deal_transaction +
          payments +
          settlements +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.updateWebDealWithPayments = function(deal, value_date, deal_transaction, payments){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        var deal = {
          number: deal,
          value_date: value_date
        };
        deal.value_date = deal.value_date.getFullYear() + '-' + ('0' + (deal.value_date.getMonth()+1)).slice(-2) + '-' + ('0' + deal.value_date.getDate()).slice(-2);


        this.method = 'UpdateWebDeal';
        this.input = '<INPUT>' +
          '<DEAL>' +
          '<RECORD><Value>'+deal.number+'</Value></RECORD>' +
          '<VALUE_DATE>'+deal.value_date+'</VALUE_DATE>' +
          '</DEAL>' +
          deal_transaction +
          payments +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.updateWebDeal = function(deal, value_date, deal_transaction){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        var deal = {
          ID: deal,
          value_date: value_date
        };

        this.method = 'UpdateWebDeal';
        this.input = '<INPUT>' +
          '<DEAL>' +
          '<RECORD><ID>'+deal.ID+'</ID></RECORD>' +
          '<VALUE_DATE>'+deal.value_date+'</VALUE_DATE>' +
          '</DEAL>' +
          deal_transaction +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.updateWebDealWithSettlementsPaymentsTransactions  = function(deal, value_date, deal_transaction, payments, settlements){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        value_date = value_date == '' ? new Date() : new Date(Date.parse(value_date));
        var deal = {
          number: deal,
          value_date: value_date
        };
        deal.value_date = deal.value_date.getFullYear() + '-' + ('0' + (deal.value_date.getMonth()+1)).slice(-2) + '-' + ('0' + deal.value_date.getDate()).slice(-2);


        this.method = 'UpdateWebDeal';
        this.input = '<INPUT>' +
          '<DEAL>' +
          '<RECORD><ID>'+deal.number+'</ID></RECORD>' +
          '<VALUE_DATE>'+deal.value_date+'</VALUE_DATE>' +
          '</DEAL>' +
          deal_transaction +
          payments +
          settlements +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.SendSettlements  = function(deal, value_date, settlements){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));
        value_date = value_date == '' ? new Date() : new Date(Date.parse(value_date));
        value_date = value_date.getFullYear() + '-' + ('0' + (value_date.getMonth()+1)).slice(-2) + '-' + ('0' + value_date.getDate()).slice(-2);
        var deal = {
          number: deal,
          value_date: value_date
        };


        this.method = 'UpdateWebDeal';
        this.input = '<INPUT>' +
          '<DEAL>' +
          '<RECORD><Value>'+deal.number+'</Value></RECORD>' +
          '<VALUE_DATE>'+deal.value_date+'</VALUE_DATE>' +
          '</DEAL>' +
          settlements +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.SendPayments  = function(deal, value_date, payments){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        var deal = {
          number: deal,
          value_date: value_date
        };
        deal.value_date = deal.value_date.getFullYear() + '-' + ('0' + (deal.value_date.getMonth()+1)).slice(-2) + '-' + ('0' + deal.value_date.getDate()).slice(-2);


        this.method = 'UpdateWebDeal';
        this.input = '<INPUT>' +
          '<DEAL>' +
          '<RECORD><Value>'+deal.number+'</Value></RECORD>' +
          '<VALUE_DATE>'+deal.value_date+'</VALUE_DATE>' +
          '</DEAL>' +
          payments +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetWire = function(wire){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetWire';
        this.input = wire;
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.PostWebDeal = function(deal){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'PostWebDeal';
        this.input = '<INPUT><DEAL><ID>'+deal.id+'</ID></DEAL></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.FinalizeDealPaymentSettlement = function(deal){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'FinalizeDealPaymentSettlement';
        this.input = '<INPUT><DEAL><ID>'+deal.id+'</ID></DEAL></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetPaymentsPayees = function (methods) {
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetAllPayeeTemplates';
        this.input = '<INPUT>' +
          '<PAYEE></PAYEE>' +
          '<FILTER><PAYMENTMETHOD OPERATOR="IN" TYPE="STRING">'+methods+'</PAYMENTMETHOD></FILTER>' +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetSettlementsPayees = function (methods) {
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetAllPayeeTemplates';
        //for settlements pass payment_type ID as 697 which is outgoing.
        this.input = '<INPUT>' +
          '<PAYEE></PAYEE>' +
          '<PAYMENT_TYPE><ID>697</ID></PAYMENT_TYPE>' +
          '<FILTER><PAYMENTMETHOD OPERATOR="IN" TYPE="STRING">'+methods+'</PAYMENTMETHOD></FILTER>' +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetSettlementsPayeesByMethodAndCurrency = function (methods, currency) {
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetAllPayeeTemplates';
        //for settlements pass payment_type ID as 697 which is outgoing.
        this.input = '<INPUT>' +
          '<PAYEE>' +
          '<CURRENCY><ID>'+currency+'</ID></CURRENCY>' +
          '</PAYEE>' +
          //'<PAYMENT_TYPE><ID>697</ID></PAYMENT_TYPE>' +
          '<FILTER><PAYMENTMETHOD OPERATOR="IN" TYPE="STRING">'+methods+'</PAYMENTMETHOD></FILTER>' +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetSettlementsPayeesByCurrency = function (currency) {
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetAllPayeeTemplates';
        //for settlements pass payment_type ID as 697 which is outgoing.
        this.input = '<INPUT>' +
          '<PAYEE>' +
          '<PAYMENT_TYPE><ID>697</ID></PAYMENT_TYPE>' +
          '<CURRENCY><ID>'+currency+'</ID></CURRENCY>' +
          '</PAYEE>' +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      //for book a spot module
      this.GetSettlementsByMethodsAndCurrencyInBookASpot = function(methods, currency){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetOutgoingBeneficiaries';
        //for settlements pass payment_type ID as 697 which is outgoing.
        this.input = '<INPUT>' +
          '<PAYEE>' +
          '<CURRENCY><ID>'+currency+'</ID></CURRENCY>' +
          '</PAYEE>' +
          //'<PAYMENT_TYPE><ID>697</ID></PAYMENT_TYPE>' +
          '<FILTER><PAYMENTMETHOD OPERATOR="IN" TYPE="STRING">'+methods+'</PAYMENTMETHOD></FILTER>' +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetPaymentsByMethodAndCurrencyInBookASpot = function(method, currency){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetIncomingBeneficiaries';
        //for settlements pass payment_type ID as 697 which is outgoing.
        this.input = '<INPUT>' +
          '<PAYEE>' +
          '<PAYMENT_METHOD><ID>'+method+'</ID></PAYMENT_METHOD>' +
          '<CURRENCY><ID>'+currency+'</ID></CURRENCY>' +
          '</PAYEE>' +
          //'<PAYMENT_TYPE><ID>697</ID></PAYMENT_TYPE>' +
          //'<FILTER><PAYMENTMETHOD OPERATOR="IN" TYPE="STRING">'+methods+'</PAYMENTMETHOD></FILTER>' +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetEntitySpreadTemplate = function(entity_id){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetEntitySpreadTemplate';
        this.input = '<INPUT><ENTITY><ID>'+entity_id+'</ID></ENTITY></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetAllElectronicMethods = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetAllElectronicMethods';
        this.input = '';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      //for make a payment module
      this.GetPaymentsByCurrenyAndMethodInMakeAPaymentTop = function(currency_id, method_id){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetIncomingBeneficiaries';
        this.input = '<INPUT>' +
          '<PAYEE>' +
          '<PAYMENT_METHOD><ID>'+method_id+'</ID></PAYMENT_METHOD>' +
          '<CURRENCY><ID>'+currency_id+'</ID></CURRENCY>' +
          '</PAYEE>' +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetPaymentsByCurrenyAndMethodsInMakeAPaymentSinglePayee = function(currency_id, methods){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetOutgoingBeneficiaries';
        //for settlements pass payment_type ID as 697 which is outgoing.
        this.input = '<INPUT>' +
          '<PAYEE>' +
          '<CURRENCY><ID>'+currency_id+'</ID></CURRENCY>' +
          '</PAYEE>' +
          '<FILTER><PAYMENTMETHOD OPERATOR="IN" TYPE="STRING">'+methods+'</PAYMENTMETHOD></FILTER>' +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetPaymentsByCurreniesAndMethodsInMakeAPaymentMultiplePayee = function(currencies, methods){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetOutgoingBeneficiaries';
        //for settlements pass payment_type ID as 697 which is outgoing.
        this.input = '<INPUT>' +
          '<PAYEE></PAYEE>' +
          '<FILTER>' +
          '<CURRENCY_ID OPERATOR="IN" TYPE="STRING">'+currencies+'</CURRENCY_ID>' +
          '<PAYMENTMETHOD OPERATOR="IN" TYPE="STRING">'+methods+'</PAYMENTMETHOD>' +
          '</FILTER>' +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetWebPaymentMethods = function(module){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetWebPaymentMethod';
        this.input = '<INPUT><MODULE_ID>'+module+'</MODULE_ID></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetFundingMethod = function(module){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetFundingMethod';
        this.input = '<INPUT><MODULE_ID>'+module+'</MODULE_ID></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetSavedApprovalsByModule = function(module){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetSavedApprovalsByModule';
        this.input = '<INPUT><MODULE><ID>'+module+'</ID></MODULE></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetApprovalHistory = function(deal){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        var id = deal.id;
        this.method = 'GetApprovalHistory';
        this.input = '<INPUT><DEAL><ID>'+id+'</ID></DEAL></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.SetDealApproval = function(deal){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        var deal_value = deal;
        this.method = 'SetDealApproval';
        this.input = '<INPUT><DEAL><Value>'+deal_value+'</Value></DEAL></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.SetDealApprovalWithID = function(deal){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        var deal_value = deal;
        this.method = 'SetDealApproval';
        this.input = '<INPUT><DEAL><ID>'+deal_value+'</ID></DEAL></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.SetWireApproval = function(wire_value){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'SetWireApproval';
        this.input = '<INPUT><WIRE><ID>'+wire_value+'</ID></WIRE></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetDefaultDate = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetDefaultDate';
        this.input = '<INPUT><IS_FORWARD>false</IS_FORWARD></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetUserApprovalRights = function(deal, module){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        var id = deal.id;

        this.method = 'GetUserApprovalRights';
        this.input = '<INPUT><DETAIL><RECORD_ID>'+id+'</RECORD_ID><MODULE_ID>'+module+'</MODULE_ID></DETAIL></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetPaymentTracker = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetPaymentTracker';
        this.input = '';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetFeeForSettlements = function(input_xml){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetFeeForSettlements';
        this.input = '<INPUT>'+input_xml+'</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetSystemConfiguration = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetSystemConfiguration';
        this.input = '';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetWebDeals = function(module){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetWebDeals';
        this.input = '<INPUT><FILTER ><IS_BATCH TYPE="STRING" OPERATOR="EQUALS TO">'+module+'</IS_BATCH ></FILTER></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetWebDealPaymentAndSettlement = function(deal){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        var deal = deal.id;
        this.method = 'GetWebDealPaymentAndSettlement';
        this.input = '<INPUT><DEAL><ID>'+deal+'</ID></DEAL></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetWireReport = function(settlement_id){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        this.method = 'GetWireReport';
        this.input = '<?xml version="1.0" encoding="UTF-8"?><INPUT><REPORT><SETTLEMENT_ID>'+settlement_id+'</SETTLEMENT_ID></REPORT></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetDealReport = function(deals) {
        if (typeof $localStorage.rememberMe == 'string') {
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if (rememberMe.remember == false) {
            this.storage = $sessionStorage;
            // this.storage = $sessionStorage
          } else {
            // this.storage = $sessionStorage
            this.storage = $sessionStorage;
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));
        var deal = deals.id || deals.deal_number;
        this.method = 'GetDealReport';
        if (deals.id)
        {
          this.input = '<?xml version="1.0" encoding="UTF-8"?><INPUT><Report><BATCH><ID>'+deal+'</ID></BATCH></Report></INPUT>';
        }
        else{
          this.input = '<?xml version="1.0" encoding="UTF-8"?><INPUT><Report><DEAL><Value>'+deal+'</Value></DEAL></Report></INPUT>';
        }
        // <INPUT>
        // <Report>
        // <DEAL>
        // <Value>DEMO-DEAL-0049164</Value>
        // </DEAL>
        // </Report>
        // </INPUT>
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      //methods for My Account > Account Transfer
      this.GetTransferAccountFrom = function(module){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage;
            // this.storage = $sessionStorage
          }else{
            // this.storage = $sessionStorage
            this.storage = $sessionStorage;
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetTransferAccountFrom';
        this.input = '<INPUT><MODULE><ID>'+module+'</ID></MODULE></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetTransferAccountTo = function(module){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage;
            // this.storage = $sessionStorage
          }else{
            // this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetTransferAccountTo';
        this.input = '<INPUT><MODULE><ID>'+module+'</ID></MODULE></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetAccountBalanceAmount = function(){

      };
      //methods for Payments > Payment History
      this.GetPaymentHistory = function(beneficiary_name, from_date, to_date, method){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage;
            // this.storage = $sessionStorage
          }else{
            // this.storage = $sessionStorage
            this.storage = $sessionStorage;
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        var from = from_date.getFullYear() + '-' + ('0' + (from_date.getMonth()+1)).slice(-2) + '-' + ('0' + from_date.getDate()).slice(-2);
        var to   = to_date.getFullYear() + '-' + ('0' + (to_date.getMonth()+1)).slice(-2) + '-' + ('0' + to_date.getDate()).slice(-2);
        this.method = 'GetPaymentHistory';
        this.input = '<INPUT>' +
          '<BENE_NAME>'+beneficiary_name+'</BENE_NAME>' +
          '<FROM_DATE>'+from+'</FROM_DATE>' +
          '<TO_DATE>'+to+'</TO_DATE>' +
          '<METHOD_NAME>'+method+'</METHOD_NAME>' +
          '</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      //methods for Payments > Pay From Your Account
      //methods for Beneficiaries modules
      this.GetAllBeneficiaries = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage;
            // this.storage = $sessionStorage
          }else{
            // this.storage = $sessionStorage
            this.storage = $sessionStorage;
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetAllBeneficiaries';
        this.input = '<INPUT><PAYEE></PAYEE></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetEntityInformation = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage
            this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
            this.storage = $sessionStorage;
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetEntityInformation';
        this.input = '<INPUT></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetAllCities = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage
            this.storage = $sessionStorage;
          }else{
            // this.storage = $sessionStorage
            this.storage = $sessionStorage;
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetAllCities';
        this.input = '<INPUT></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetCountries = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage
            this.storage = $sessionStorage;
          }else{
            // this.storage = $sessionStorage
            this.storage = $sessionStorage;
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetCountries';
        this.input = '<INPUT></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetProvinceByCityName = function(city){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;

          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetProvinceByCityName';
        this.input = '<INPUT><CITY>'+city+'</CITY></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetBeneficiaryAccountTypes = function(city){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetBeneficiaryAccountTypes';
        this.input = '<INPUT><CITY>'+city+'</CITY></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetIndividualIdentificationTypes = function(city){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetIndividualIdentificationTypes';
        this.input = '<INPUT></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetRoutingCodeList = function(filter, type, page_number, page_size){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        var input = '';
        if(filter){
          if(typeof type === 'string')
            input += '<FILTER><ROUTINGTYPE TYPE="STRING" OPERATOR="EQAULS TO">'+type+'</ROUTINGTYPE></FILTER>';
          input += '<PAGING><PAGE_NUMBER>'+page_number+'</PAGE_NUMBER><PAGE_SIZE>'+page_size+'</PAGE_SIZE></PAGING>';
        }
        this.method = 'GetRoutingCodeList';
        this.input = '<INPUT>'+input+'</INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetRoutingTypesList = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetRoutingTypesList';
        this.input = '<INPUT></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetBeneficiaryAccountTypes = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetBeneficiaryAccountTypes';
        this.input = '<INPUT></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      function createOrUpdateWire(beneficiary, beneficiary_bank, intermediary_bank, further_credit_detail, by_order_of, additional, createUpdate, self){
        var createOrUpdate = createUpdate;

        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            self.storage = $sessionStorage;
          }else{
            self.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(self.storage.login, self.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        self.method = createUpdate;
        self.input = '<INPUT>' +
          '<BENEFICIARY>'+beneficiary+'</BENEFICIARY>' +
          '<BENEFICIARYBANK>'+beneficiary_bank+'</BENEFICIARYBANK>' +
          intermediary_bank+
          '<FURTHERCREDITDETAIL>'+further_credit_detail+'</FURTHERCREDITDETAIL>' +
          by_order_of +
          '<ADDITIONAL>'+additional+'</ADDITIONAL>' +
          '</INPUT>';
        self.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: self.method,
              input: self.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: self.baseUrl,
          data: JSON.stringify(self.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });

      }
      this.updateWire = function(beneficiary, beneficiary_bank, intermediary_bank, further_credit_detail, by_order_of, additional){
        createOrUpdateWire(beneficiary, beneficiary_bank, intermediary_bank, further_credit_detail, by_order_of, additional, "UpdateWire", this);
      };
      this.CreateWire = function(beneficiary, beneficiary_bank, intermediary_bank, further_credit_detail, by_order_of, additional){
        createOrUpdateWire(beneficiary, beneficiary_bank, intermediary_bank, further_credit_detail, by_order_of, additional, "CreateWire", this);
        var defer = $q.defer();
        createOrUpdateWire(beneficiary, beneficiary_bank, intermediary_bank, further_credit_detail, by_order_of, additional, "UpdateWire", this).then(success, error);

        function success(data){

          defer.resolve(data)
        }
        function error(err){
          defer.reject(err)
        }
        return defer.promise;

      };
      this.GetAllCitiesByCountryAndProvince = function(country, province){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetAllCities';
        this.input = '<INPUT><FILTER>' +
          '<COUNTRY_ID  TYPE="STRING" OPERATOR="EQUALS TO">'+country+'</COUNTRY_ID>' +
          '<PROVINCE_ID  TYPE="STRING" OPERATOR="EQUALS TO">'+province+'</PROVINCE_ID>' +
          '</FILTER></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      this.GetAllProvinces = function(){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetAllProvinces';
        this.input = '<INPUT></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      //User Rights method
      this.GetUserRights = function(){


          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe && !rememberMe.remember)
          {
            // this.storage = $sessionStorage;
            this.storage = $sessionStorage
          }
          else
          {
            this.storage = $sessionStorage
          }


        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));


        this.method = 'GetUserRights';
        this.input = '<INPUT></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
      //methods for all modules
      this.ValidateBusinessDate = function(date){
        if(typeof $localStorage.rememberMe == 'string'){
          var rememberMe = angular.fromJson($localStorage.rememberMe);
          if(rememberMe.remember == false) {
            this.storage = $sessionStorage
            // this.storage = $sessionStorage;
          }else{
            this.storage = $sessionStorage
          }
        }

        var decrypt = cryptoService.crypto.AES.decrypt(this.storage.login, this.storage.passphrase);
        var login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));

        date = typeof date == 'object' ? date: new Date(Date.parse(date));
        date = date.getFullYear() + '-' + ('0' + (date.getMonth()+1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);


        this.method = 'ValidateBusinessDate';
        this.input = '<INPUT><DATE>'+date+'</DATE></INPUT>';
        this.data = {
          'data': {
            'headers':{
              username: login.username,
              password: login.password,
              OrgID: login.OrgID,
              CustID: login.CustID
            },
            'body':{
              method: this.method,
              input: this.input
            }
          }
        };
        return $http({
          method: 'POST',
          url: this.baseUrl,
          data: JSON.stringify(this.data),
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
        });
      };
    })

  /** @ngInject */
  function apiService(QueryFactory, XmlJson, cryptoService, $sessionStorage)
  {
    /* Global Methods */
    function GetEntity(customerId)
    {
      var decrypt, login;
      if($sessionStorage.login)
      {
        decrypt = cryptoService.crypto.AES.decrypt($sessionStorage.login, $sessionStorage.passphrase);
        login = angular.fromJson(decrypt.toString(cryptoService.crypto.enc.Utf8));
      }

      if(login)
      {
        var input = '<INPUT><ENTITY_NO>'+login.CustID+'</ENTITY_NO></INPUT>';
        return QueryFactory.query('GetEntity', input).then(function(data){
          return XmlJson.xmlToJson(data.data)
        });
      }


    }
    function GetEntityWebDefaults() {
      return QueryFactory.query('GetEntityWebDefaults', '').then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetEntitySpreadTemplate(entity_id) {
      var input = '<INPUT><ENTITY><ID>'+entity_id+'</ID></ENTITY></INPUT>';
      return QueryFactory.query('GetEntitySpreadTemplate', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetDefaultDate(is_forward) {

      var choice = is_forward && is_forward || false;
      var input = '<INPUT><IS_FORWARD>'+choice+'</IS_FORWARD></INPUT>';
      return QueryFactory.query('GetDefaultDate', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetUserApprovalRights(deal_id, module){
      var input = '<INPUT><DETAIL><RECORD_ID>'+deal_id+'</RECORD_ID><MODULE_ID>'+module+'</MODULE_ID></DETAIL></INPUT>';
      return QueryFactory.query('GetUserApprovalRights', input).then(function(data){
        var userRights = convertDataToJson(data.data, 'result')
        return userRights
      });
    }
    function GetWebPaymentMethod(module){
      var input = '<INPUT><MODULE_ID>'+module+'</MODULE_ID></INPUT>';
      return QueryFactory.query('GetWebPaymentMethod', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetSavedApprovalsByModule(module) {
      var input = '<INPUT><MODULE><ID>'+ module +'</ID></MODULE></INPUT>';
      return QueryFactory.query('GetSavedApprovalsByModule', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function SetDealApproval(deal_id) {
      var input = '<INPUT><DEAL><ID>'+deal_id+'</ID></DEAL></INPUT>';
      return QueryFactory.query('SetDealApproval', input).then(function(data){
        var dealApprovals = convertDataToJson(data.data, 'result')
        return dealApprovals;
      });
    }
    function GetApprovalHistory(deal_id) {
      var input = '<INPUT><DEAL><ID>'+deal_id+'</ID></DEAL></INPUT>';
      return QueryFactory.query('GetApprovalHistory', input).then(function(data){

        var approvaLApprovals = convertDataToJson(data.data, 'approval_history')
        return approvaLApprovals;

      });
    }
    function GetAllCurrencies() {
      return QueryFactory.query('GetAllCurrencies', '').then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetPurposeOfPayments() {
      return QueryFactory.query('GetPurposeOfPayments', '').then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetFeeByMethod(ID) {
      var input = '<INPUT><METHOD><ID>'+ID+'</ID></METHOD></INPUT>';
      return QueryFactory.query('GetFeeByMethod', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }

    /* Make A Payment Methods*/
    function GetFundingCurrencies() {
      var input = '<INPUT><CURRENCY_ID>2</CURRENCY_ID><IS_BUY>true</IS_BUY></INPUT>';
      return QueryFactory.query('GetCurrencyForDeal', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetFundingCurrenciesFilterByCurrencyID(currency_id) {
      var input = '<INPUT><CURRENCY_ID>'+currency_id+'</CURRENCY_ID><IS_BUY>true</IS_BUY></INPUT>';
      return QueryFactory.query('GetCurrencyForDeal', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetFundingCurrenciesWithoutFilter() {
      var input = '<INPUT><IS_BUY>true</IS_BUY></INPUT>';
      return QueryFactory.query('GetCurrencyForDeal', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetPaymentCurrencies() {
      var input = '<INPUT><CURRENCY_ID>0</CURRENCY_ID><IS_BUY>false</IS_BUY></INPUT>';
      return QueryFactory.query('GetCurrencyForDeal', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetPaymentsByCurrencyAndMethodInMakeAPaymentTop(method_id, currency_id) {
      var input = '<INPUT>' +
        '<PAYEE>' +
        '<PAYMENT_METHOD><ID>'+method_id+'</ID></PAYMENT_METHOD>' +
        '<CURRENCY><ID>'+currency_id+'</ID></CURRENCY>' +
        '</PAYEE>' +
        '</INPUT>';
      return QueryFactory.query('GetIncomingBeneficiaries', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    /* Book A Spot Methods*/
    function GetSettlementsByMethodsAndCurrencyInBookASpot(currency, methods) {
      var input = '<INPUT>' +
        '<PAYEE>' +
        '<CURRENCY><ID>'+currency+'</ID></CURRENCY>' +
        '</PAYEE>' +
        //'<PAYMENT_TYPE><ID>697</ID></PAYMENT_TYPE>' +
        '<FILTER><PAYMENTMETHOD OPERATOR="IN" TYPE="STRING">'+methods+'</PAYMENTMETHOD></FILTER>' +
        '</INPUT>';
      return QueryFactory.query('GetOutgoingBeneficiaries', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetFundingMethod(module){
      var input = '<INPUT><MODULE_ID>'+module+'</MODULE_ID></INPUT>';
      return QueryFactory.query('GetFundingMethod', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetPaymentsByMethodAndCurrencyInBookASpot(currency, method) {
      var input = '<INPUT>' +
        '<PAYEE>' +
        '<PAYMENT_METHOD><ID>'+method+'</ID></PAYMENT_METHOD>' +
        '<CURRENCY><ID>'+currency+'</ID></CURRENCY>' +
        '</PAYEE>' +
        '</INPUT>';
      return QueryFactory.query('GetIncomingBeneficiaries', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }

    /*Account Balance*/
    function GetAccountList() {
      return QueryFactory.query('GetAccountList', '').then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }

    /*Deal History*/
    function GetWebDeals() {
      return QueryFactory.query('GetWebDeals', '').then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetSettlementsByMethodsAndCurrencyInDealHistory(currency, methods) {
      var input = '<INPUT>' +
        '<PAYEE>' +
        '<CURRENCY><Value>'+currency+'</Value></CURRENCY>' +
        '</PAYEE>' +
        '<FILTER><PAYMENTMETHOD OPERATOR="IN" TYPE="STRING">'+methods+'</PAYMENTMETHOD></FILTER>' +
        '</INPUT>';
      return QueryFactory.query('GetOutgoingBeneficiaries', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }

    /*Payment Tracker*/
    function GetPaymentTracker() {
      return QueryFactory.query('GetPaymentTracker', '').then(function(data){

        return data;
      });
    }

    /*Approval Policies*/
    function GetWebModules() {
      return QueryFactory.query('GetUserWebModules', '').then(function(data){

        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetApprovalLevels() {
      return QueryFactory.query('GetApprovalLevels', '').then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetWebUsers() {
      return QueryFactory.query('GetWebUsers', '').then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function SaveApprovalPolicy(module, level, detail) {
      detail = detail === false ? '':detail;
      var input = '<INPUT><APPROVAL>' +
        '<MODULE_ID>'+module+'</MODULE_ID>' +
        '<APPROVAL_LEVEL>'+level+'</APPROVAL_LEVEL></APPROVAL>';
      if(detail !== '')
        input += detail;
      input += '</INPUT>';
      return QueryFactory.query('SaveApprovalPolicy', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }

    /*Pay From Your Account*/
    function GetTransferAccountFrom(module) {
      var input = '<INPUT><MODULE><ID>'+module+'</ID></MODULE></INPUT>';
      return QueryFactory.query('GetTransferAccountFrom', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }
    function GetTransferAccountTo(module) {
      var input = '<INPUT><MODULE><ID>'+module+'</ID></MODULE></INPUT>';
      return QueryFactory.query('GetTransferAccountTo', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }

    /*Beneficiary*/
    function GetAllBeneficiaries() {
      var input = '<INPUT><PAYEE></PAYEE></INPUT>';
      return QueryFactory.query('GetAllBeneficiaries', input).then(function(data){
        return XmlJson.xmlToJson(data.data)
      });
    }

    var apiset = {
      GetEntity: GetEntity,
      GetEntityWebDefaults: GetEntityWebDefaults,
      GetEntitySpreadTemplate: GetEntitySpreadTemplate,
      GetDefaultDate: GetDefaultDate,
      GetUserApprovalRights: GetUserApprovalRights,
      GetFundingCurrencies: GetFundingCurrencies,
      GetFundingCurrenciesFilterByCurrencyID: GetFundingCurrenciesFilterByCurrencyID,
      GetFundingCurrenciesWithoutFilter: GetFundingCurrenciesWithoutFilter,
      GetPaymentCurrencies: GetPaymentCurrencies,
      GetSettlementsByMethodsAndCurrencyInBookASpot: GetSettlementsByMethodsAndCurrencyInBookASpot,
      GetSettlementsByMethodsAndCurrencyInDealHistory: GetSettlementsByMethodsAndCurrencyInDealHistory,
      GetWebPaymentMethod: GetWebPaymentMethod,
      GetFundingMethod: GetFundingMethod,
      GetPaymentsByMethodAndCurrencyInBookASpot: GetPaymentsByMethodAndCurrencyInBookASpot,
      GetSavedApprovalsByModule: GetSavedApprovalsByModule,
      SetDealApproval: SetDealApproval,
      GetApprovalHistory: GetApprovalHistory,
      GetAccountList: GetAccountList,
      GetAllCurrencies: GetAllCurrencies,
      GetPurposeOfPayments: GetPurposeOfPayments,
      GetWebDeals: GetWebDeals,
      GetPaymentsByCurrencyAndMethodInMakeAPaymentTop: GetPaymentsByCurrencyAndMethodInMakeAPaymentTop,
      GetPaymentTracker: GetPaymentTracker,
      GetWebModules: GetWebModules,
      GetApprovalLevels: GetApprovalLevels,
      GetWebUsers: GetWebUsers,
      SaveApprovalPolicy: SaveApprovalPolicy,
      GetTransferAccountFrom: GetTransferAccountFrom,
      GetTransferAccountTo: GetTransferAccountTo,
      GetFeeByMethod: GetFeeByMethod,
      GetAllBeneficiaries: GetAllBeneficiaries
    };

    function convertDataToJson(xml, name){
      var xmlDOM = new DOMParser().parseFromString(xml, 'text/xml');
      var actualJson = XmlJson.xmlToJson(xmlDOM);
      return actualJson.output[name];
    }
    return apiset;
    /**
     * You can use this service to define your API urls. The "api" service
     * is designed to work in parallel with "apiResolver" service which you can
     * find in the "app/core/services/api-resolver.service.js" file.
     *
     * You can structure your API urls whatever the way you want to structure them.
     * You can either use very simple definitions, or you can use multi-dimensional
     * objects.
     *
     * Here's a very simple API url definition example:
     *
     *      api.getBlogList = $resource('http://api.example.com/getBlogList');
     *
     * While this is a perfectly valid $resource definition, most of the time you will
     * find yourself in a more complex situation where you want url parameters:
     *
     *      api.getBlogById = $resource('http://api.example.com/blog/:id', {id: '@id'});
     *
     * You can also define your custom methods. Custom method definitions allow you to
     * add hardcoded parameters to your API calls that you want to sent every time you
     * make that API call:
     *
     *      api.getBlogById = $resource('http://api.example.com/blog/:id', {id: '@id'}, {
         *         'getFromHomeCategory' : {method: 'GET', params: {blogCategory: 'home'}}
         *      });
     *
     * In addition to these definitions, you can also create multi-dimensional objects.
     * They are nothing to do with the $resource object, it's just a more convenient
     * way that we have created for you to packing your related API urls together:
     *
     *      api.blog = {
         *                   list     : $resource('http://api.example.com/blog'),
         *                   getById  : $resource('http://api.example.com/blog/:id', {id: '@id'}),
         *                   getByDate: $resource('http://api.example.com/blog/:date', {id: '@date'}, {
         *                       get: {
         *                            method: 'GET',
         *                            params: {
         *                                getByDate: true
         *                            }
         *                       }
         *                   })
         *       }
     *
     * If you look at the last example from above, we overrode the 'get' method to put a
     * hardcoded parameter. Now every time we make the "getByDate" call, the {getByDate: true}
     * object will also be sent along with whatever data we are sending.
     *
     * All the above methods are using standard $resource service. You can learn more about
     * it at: https://docs.angularjs.org/api/ngResource/service/$resource
     *
     * -----
     *
     * After you defined your API urls, you can use them in Controllers, Services and even
     * in the UIRouter state definitions.
     *
     * If we use the last example from above, you can do an API call in your Controllers and
     * Services like this:
     *
     *      function MyController (api)
     *      {
         *          // Get the blog list
         *          api.blog.list.get({},
         *
         *              // Success
         *              function (response)
         *              {
         *                  console.log(response);
         *              },
         *
         *              // Error
         *              function (response)
         *              {
         *                  console.error(response);
         *              }
         *          );
         *
         *          // Get the blog with the id of 3
         *          var id = 3;
         *          api.blog.getById.get({'id': id},
         *
         *              // Success
         *              function (response)
         *              {
         *                  console.log(response);
         *              },
         *
         *              // Error
         *              function (response)
         *              {
         *                  console.error(response);
         *              }
         *          );
         *
         *          // Get the blog with the date by using custom defined method
         *          var date = 112314232132;
         *          api.blog.getByDate.get({'date': date},
         *
         *              // Success
         *              function (response)
         *              {
         *                  console.log(response);
         *              },
         *
         *              // Error
         *              function (response)
         *              {
         *                  console.error(response);
         *              }
         *          );
         *      }
     *
     * Because we are directly using $resource service, all your API calls will return a
     * $promise object.
     *
     * --
     *
     * If you want to do the same calls in your UI Router state definitions, you need to use
     * "apiResolver" service we have prepared for you:
     *
     *      $stateProvider.state('app.blog', {
         *          url      : '/blog',
         *          views    : {
         *               'content@app': {
         *                   templateUrl: 'app/main/apps/blog/blog.html',
         *                   controller : 'BlogController as vm'
         *               }
         *          },
         *          resolve  : {
         *              Blog: function (apiResolver)
         *              {
         *                  return apiResolver.resolve('blog.list@get');
         *              }
         *          }
         *      });
     *
     *  You can even use parameters with apiResolver service:
     *
     *      $stateProvider.state('app.blog.show', {
         *          url      : '/blog/:id',
         *          views    : {
         *               'content@app': {
         *                   templateUrl: 'app/main/apps/blog/blog.html',
         *                   controller : 'BlogController as vm'
         *               }
         *          },
         *          resolve  : {
         *              Blog: function (apiResolver, $stateParams)
         *              {
         *                  return apiResolver.resolve('blog.getById@get', {'id': $stateParams.id);
         *              }
         *          }
         *      });
         *
         *  And the "Blog" object will be available in your BlogController:
         *
         *      function BlogController(Blog)
         *      {
         *          var vm = this;
         *
         *          // Data
         *          vm.blog = Blog;
         *
         *          ...
         *      }
         */

    // this.api = {};
  }
})();
