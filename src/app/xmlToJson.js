/**
 * Created by Hp on 5/18/2017.
 */
(function(){
  angular
    .module('fuse')
    .factory('XmlJson' , XmlJson);
})();
function XmlJson(){
  var service ={
    xmlToJson : xmlToJson
  }
  var abc = [];
  return service;
  function xmlToJson(xml) {
    if (typeof xml === 'string') {
      xml = new DOMParser().parseFromString(xml, 'text/xml');
    }
    // Create the return object
    var obj = {};

    if (xml.nodeType === 1) { // element
      // do attributes
      if (xml.attributes.length > 0) {
        obj["@attributes"] = {};
        for (var j = 0; j < xml.attributes.length; j++) {
          var attribute = xml.attributes.item(j);
          if (attribute.nodeValue) {
            obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
          }

        }
      }
    } else if (xml.nodeType === 3) { // text
      obj = xml.nodeValue;
    }

    // do children
    // If just one text node inside
    if (xml.hasChildNodes() && xml.childNodes.length === 1 && xml.childNodes[0].nodeType === 3) {
      obj = xml.childNodes[0].nodeValue;
    }
    else if (xml.hasChildNodes()) {
      for (var i = 0; i < xml.childNodes.length; i++) {
        var item = xml.childNodes.item(i);
        var nodeName = item.nodeName;
        if (typeof(obj[nodeName]) === "undefined") {
          obj[nodeName] = xmlToJson(item);
        } else {
          if (typeof(obj[nodeName].push) === "undefined") {
            var old = obj[nodeName];
            obj[nodeName] = [];
            obj[nodeName].push(old);
          }
          obj[nodeName].push(xmlToJson(item));
        }
      }
    }
    return toLowerKeys(obj);

  }

  function toLowerKeys(obj) {
    if (!typeof(obj) === "object" || typeof(obj) === "string" || typeof(obj) === "number" || typeof(obj) === "boolean") {

      return obj;
    }

    // delete obj["#text"]
    // console.log(Object.getOwnPropertyNames(obj))
    var length = abc.push(Object.getOwnPropertyNames(obj));
    // console.log(length);
    var keys = Object.keys(obj);
    // for(var i =0; i < keys.length; i++)
    // {
    //   console.log(keys[i])
    // }

    var n = keys.length;
    var lowKey;
    while (n--) {
      var key = keys[n];
      if (key === (lowKey = key.toLowerCase()))
        continue;
      obj[lowKey] = toLowerKeys(obj[key]);
      delete obj[key];
    }
    return (obj);
  }

  // function toLowerKeys(obj){
  //   console.log(_.mapKeys)
  //   obj = _.mapKeys(obj, function (v, k) { return k.toLowerCase(); });
  //   console.log(obj)
  //   // Object.keys(obj).forEach(function(key) {
  //   //   var k = key.toLowerCase();
  //   //   if (k != key) {
  //   //     var v = obj[key]
  //   //     obj[k] = v;
  //   //     delete obj[key];
  //   //
  //   //     if (typeof v == 'object') {
  //   //       toLowerKeys(v);
  //   //     }
  //   //   }
  //   // });
  //   return obj;
  // }
}
