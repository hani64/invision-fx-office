(function () {
  'use strict';

  angular
    .module('app.core')
    .filter('toTrusted', toTrustedFilter)
    .filter('htmlToPlaintext', htmlToPlainTextFilter)
    .filter('nospace', nospaceFilter)
    .filter('getById', getById)
    .filter('humanizeDoc', humanizeDocFilter);


  /** @ngInject */
  function toTrustedFilter($sce) {
    return function (value) {
      return $sce.trustAsHtml(value);
    };
  }


  /** @ngInject */
  function getById() {
    return function (input, id) {
      var i = 0, len = input.length;
      for (; i < len; i++) {
        if (+input[i].id == +id) {
          return input[i];
        }
      }
      return null;
    }
  }

  /** @ngInject */
  function htmlToPlainTextFilter() {
    return function (text) {
      return String(text).replace(/<[^>]+>/gm, '');
    };
  }

  /** @ngInject */
  function nospaceFilter() {
    return function (value) {
      return (!value) ? '' : value.replace(/ /g, '');
    };
  }

  /** @ngInject */
  function humanizeDocFilter() {
    return function (doc) {
      if (!doc) {
        return;
      }
      if (doc.type === 'directive') {
        return doc.name.replace(/([A-Z])/g, function ($1) {
          return '-' + $1.toLowerCase();
        });
      }
      return doc.label || doc.name;
    };
  }

})();
