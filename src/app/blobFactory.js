/**
 * Created by Tech N Choice on 6/5/2017.
 */
(function(){
  angular
    .module('fuse')
    .factory('BlobFactory' , BlobFactory);
})();
BlobFactory.$inject = [];
function BlobFactory($http, $q, $localStorage, $sessionStorage, cryptoService){
  function b64toBlob(b64Data, contentType, sliceSize)
  {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }
  var service ={
    b64toBlob : b64toBlob

  }
  return service;
}
