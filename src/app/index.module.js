(function () {
  'use strict';

  /**
   * Main module of the Fuse
   */
  var fuse = angular
    .module('fuse', [
      // Core
      'app.core',

      // Navigation
      'app.navigation',

      // Toolbar
      'app.toolbar',

      'app.mail',

      //Dashboard
      'app.dashboard',
     // 'app.components',

      // my account
      'app.myaccount',

      //Payments
      'app.payments',

      //Forex Dealing
      'app.forexdealing',

      //Beneficiaries
      'app.beneficiaries',

      //Company Info
      'app.companyinfo',

      //Document
      'app.document',

      //Manage Profile
      'app.administration',

      //Authentication
      'app.authentication',

      //humanize-duration
      'timer',

      //recaptcha
      'vcRecaptcha',

      //ngStorage
      'ngStorage',

      //ngTable
      'ngTable',

      'ngCookies',

      'ui.bootstrap',

      'perfect_scrollbar',

      'ngSanitize',

      'ngCsv',

      'angularUtils.directives.dirPagination'
    ]);
  fuse.directive('jqDataTable', [function () {
    return {
      restrict: 'A',
      scope:{
          'model': '='
      },
      link: function (scope, elem, attrs) {
        var element = $(elem);
        element.DataTable({
          "paging":   false,
          "ordering": false,
          "info":     false,
          columnDefs: [
            {
              targets: [ 0, 1, 2 ],
              className: 'mdl-data-table__cell--non-numeric'
            }
          ]
        });
      }
    };
  }]);
  fuse.factory('beforeUnload', function($rootScope, $window, $localStorage, $sessionStorage){
    // function clearUserBrowserClose(){
    //   window.onbeforeunload = function (e) {
    //     var confirmation = {};
    //     var event = $rootScope.$broadcast('onBeforeUnload', confirmation);
    //     var rememberMe = angular.fromJson($localStorage.rememberMe);
    //     if(rememberMe.remember == false) {
    //       var storage = $sessionStorage;
    //     }else{
    //       var storage = $localStorage;
    //       storage.previousLogin = storage.login;
    //       localStorage.clear();
    //     }
    //     delete storage.login;
    //     if (event.defaultPrevented) {
    //       return confirmation.message;
    //     }
    //   };
    //   window.onunload = function () {
    //     $rootScope.$broadcast('onUnload');
    //   };
    // }


    return {
      // clearUserBrowserClose : clearUserBrowserClose
    };
  });
  fuse.directive('jqTimeCircles', [function () {
    return {
      restrict: 'AEC',
      link: function (scope, elem, attrs) {
        elem.TimeCircles({
          "animation": "smooth",
          "bg_width": 1,
          "fg_width": 0.21,
          "circle_bg_color": "#AAAAAA",
          "direction":'Counter-clockwise',
          "start_angle": 160,
          "time": {
            "Days": {
              "text": "Days",
              "color": "#FFCC66",
              "show": false
            },
            "Hours": {
              "text": "Hours",
              "color": "#99CCFF",
              "show": false
            },
            "Minutes": {
              "text": "Minutes",
              "color": "#BBFFBB",
              "show": false
            },
            "Seconds": {
              "text": "Expires in",
              "color": "#FFA834",
              "show": true
            }
          }
        }).addListener(function(unit, value, total){
          if(unit == 'Seconds'){
            var text_box = elem.find('.textDiv_Seconds');
            text_box.find('.title').remove();
            var new_value = value + 'Secs';
            text_box.append('<span class="title">'+new_value+'</span>');
          }
        });
      }
    };
  }]);
  fuse.directive('jqDatePicker', [function () {
    return {
      restrict: 'AEC',
      link: function (scope, elem, attrs) {
       if(typeof attrs.minDate == 'undefined'){
         elem.datepicker({
           dateFormat: 'mm/dd/yy'
         });
       }else if(attrs.minDate == 'today'){
         elem.datepicker({
           dateFormat: 'mm/dd/yy',
           minDate: new Date()
         });
       }
      }
    };
  }]);
  fuse.directive('validNumber', function() {
    return {
      require: '?ngModel',
      link: function(scope, element, attrs, ngModelCtrl) {
        if(!ngModelCtrl) {
          return;
        }

        ngModelCtrl.$parsers.push(function(val) {
          if (angular.isUndefined(val)) {
            var val = '';
          }

          var clean = val.replace(/[^-0-9\.]/g, '');
          var negativeCheck = clean.split('-');
          var decimalCheck = clean.split('.');
          if(!angular.isUndefined(negativeCheck[1])) {
            negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
            clean ="";


          }

          if(!angular.isUndefined(decimalCheck[1])) {
            decimalCheck[1] = decimalCheck[1].slice(0);
            clean =decimalCheck[0] + '.' + decimalCheck[1];
          }

          if (val !== clean) {
            ngModelCtrl.$setViewValue(clean);
            ngModelCtrl.$render();
          }
          return clean;
        });

        element.bind('keypress', function(event) {
          if(event.keyCode === 32) {
            event.preventDefault();
          }
        });
      }
    };
  });
  fuse.directive('totalRecord', totalRecord);
  function totalRecord() {
    return {
      restrict : 'E',
      scope : {
        records: '='
      },
      template: "<div flex='15' flex-md='100'>"  +
      " <p style='width: 140px;'>Total Records : <strong>{{records.length}}</strong></p>" +
    " </div>"


    };
  }
  fuse.directive('jqUIDateRangePicker', [function () {
    return {
      restrict: 'AEC',
      scope: true,
      link: function (scope, elem, attrs) {

        if(typeof attrs.pickerTitle !== 'undefined'){
          elem.daterangepicker({

            initialText : attrs.pickerTitle,
            dateFormat: 'm/d/yy',
            datepickerOptions: {
              autoclose: false,
              minDate: null,
              maxDate: null,
              dateFormat: 'yy d, M'
            }
          });
        }
        scope.$on('clearFilter', function(event, clear)
          {

          if(clear){elem.daterangepicker("clearRange");}
          });
      }
    };
  }]);
  function close(elem, scope){
    scope.$on('clearFilter', function(event, mass) {
      elem.on('daterangepickerclose', function(event, data) { log('close'); }); }
    );
  }
  fuse.filter('dateRangeFilter', function () {
    return function(items, date){

      if(date === null){

        return items;
      }else{


        if(date !== ''){
          var date_object = JSON.parse(date);
          var df = Date.parse(date_object.start);
          var dt = Date.parse(date_object.end);
          var result = [];
          for(var i=0; i<items.length; i++){
            if(typeof items[i].post_on == 'object'){
              if(items[i].post_on !== null)
                var date_bet = Date.parse(items[i].post_on);
              else
                var date_bet = null;
            }else if(typeof items[i].value_date == 'object'){
              if(items[i].value_date !== null)
                var date_bet = Date.parse(items[i].value_date);
              else
                var date_bet = null;
            }else if(typeof items[i].trans_date == 'object'){
              if(items[i].trans_date !== null)
                var date_bet = Date.parse(items[i].trans_date);
              else
                var date_bet = null;
            }
            if (date_bet >= df && dt >= date_bet) {
              result.push(items[i]);
            }
          }
          return result;
        }else{
          return items;
        }
      }
    };
  });
  fuse.filter('dropDigits', function() {
    return function(floatNum) {
      return String(floatNum)
        .split('.')
        .map(function (d, i) { return i ? d.substr(0, 2) : d; })
        .join('.');
    };
  });
  fuse.directive('perfectScrollBar', [function () {
    return {
      restrict: 'AEC',
      link: function (scope, elem, attrs) {
        elem.perfectScrollbar();
      }
    };
  }]);
  fuse.directive('format', ['$filter', function($filter){
    return {
      require: '?ngModel',
      link: function(scope, elem, attrs, ctrl){
        if (!ctrl) return;

        ctrl.$formatters.unshift(function (a) {
          return $filter(attrs.format)(ctrl.$modelValue, '')
        });

        elem.bind('blur', function(event) {
          var plainNumber = elem.val().replace(/[^\d|\-+|\.+]/g, '');
          elem.val($filter(attrs.format)(plainNumber, ''));
        });
      }
    };
  }]);
  fuse.directive('onlyAlphabates',onlyAlphabates);
  function onlyAlphabates() {

    return {
      restrict: 'A',
      require: '?ngModel',
      link: function (scope, element, attrs, modelCtrl) {
        modelCtrl.$parsers.push(function (inputValue) {
          if (inputValue === undefined) return '';
          var transformedInput = inputValue.replace(/[^A-Za-z ]/g, '');
          if (transformedInput !== inputValue) {
            modelCtrl.$setViewValue(transformedInput);
            modelCtrl.$render();
          }
          return transformedInput;
        });
      }
    };
  }
  fuse.directive('onlyNumbers',onlyNumbers);
  function onlyNumbers() {

    return {
      restrict: 'A',
      require: '?ngModel',
      link: function (scope, element, attrs, modelCtrl) {
        modelCtrl.$parsers.push(function (inputValue) {
          if (inputValue === undefined) return '';
          var transformedInput =   inputValue.replace(/[^0-9]/g, '');
          if (transformedInput !== inputValue) {
            modelCtrl.$setViewValue(transformedInput);
            modelCtrl.$render();
          }
          return transformedInput;
        });
      }
    };
  }


  fuse.factory('ApiService', function(){
    return {
      //xml2json function
      xml2json:function(xml, tab){
      var X = {
        toObj: function(xml) {
          var o = {};
          if (xml.nodeType==1) {   // element node ..
            if (xml.attributes.length)   // element with attributes  ..
              for (var i=0; i<xml.attributes.length; i++)
                o["@"+xml.attributes[i].nodeName] = (xml.attributes[i].nodeValue||"").toString();
            if (xml.firstChild) { // element has child nodes ..
              var textChild=0, cdataChild=0, hasElementChild=false;
              for (var n=xml.firstChild; n; n=n.nextSibling) {
                if (n.nodeType==1) hasElementChild = true;
                else if (n.nodeType==3 && n.nodeValue.match(/[^ \f\n\r\t\v]/)) textChild++; // non-whitespace text
                else if (n.nodeType==4) cdataChild++; // cdata section node
              }
              if (hasElementChild) {
                if (textChild < 2 && cdataChild < 2) { // structured element with evtl. a single text or/and cdata node ..
                  X.removeWhite(xml);
                  for (var n=xml.firstChild; n; n=n.nextSibling) {
                    if (n.nodeType == 3)  // text node
                      o["#text"] = X.escape(n.nodeValue);
                    else if (n.nodeType == 4)  // cdata node
                      o["#cdata"] = X.escape(n.nodeValue);
                    else if (o[n.nodeName]) {  // multiple occurence of element ..
                      if (o[n.nodeName] instanceof Array)
                        o[n.nodeName][o[n.nodeName].length] = X.toObj(n);
                      else
                        o[n.nodeName] = [o[n.nodeName], X.toObj(n)];
                    }
                    else  // first occurence of element..
                      o[n.nodeName] = X.toObj(n);
                  }
                }
                else { // mixed content
                  if (!xml.attributes.length)
                    o = X.escape(X.innerXml(xml));
                  else
                    o["#text"] = X.escape(X.innerXml(xml));
                }
              }
              else if (textChild) { // pure text
                if (!xml.attributes.length)
                  o = X.escape(X.innerXml(xml));
                else
                  o["#text"] = X.escape(X.innerXml(xml));
              }
              else if (cdataChild) { // cdata
                if (cdataChild > 1)
                  o = X.escape(X.innerXml(xml));
                else
                  for (var n=xml.firstChild; n; n=n.nextSibling)
                    o["#cdata"] = X.escape(n.nodeValue);
              }
            }
            if (!xml.attributes.length && !xml.firstChild) o = null;
          }
          else if (xml.nodeType==9) { // document.node
            o = X.toObj(xml.documentElement);
          }
          else
            alert("unhandled node type: " + xml.nodeType);
          return o;
        },
        toJson: function(o, name, ind) {
          var json = name ? ("\""+name+"\"") : "";
          if (o instanceof Array) {
            for (var i=0,n=o.length; i<n; i++)
              o[i] = X.toJson(o[i], "", ind+"\t");
            json += (name?":[":"[") + (o.length > 1 ? ("\n"+ind+"\t"+o.join(",\n"+ind+"\t")+"\n"+ind) : o.join("")) + "]";
          }
          else if (o == null)
            json += (name&&":") + "null";
          else if (typeof(o) == "object") {
            var arr = [];
            for (var m in o)
              arr[arr.length] = X.toJson(o[m], m, ind+"\t");
            json += (name?":{":"{") + (arr.length > 1 ? ("\n"+ind+"\t"+arr.join(",\n"+ind+"\t")+"\n"+ind) : arr.join("")) + "}";
          }
          else if (typeof(o) == "string")
            json += (name&&":") + "\"" + o.toString() + "\"";
          else
            json += (name&&":") + o.toString();
          return json;
        },
        innerXml: function(node) {
          var s = ""
          if ("innerHTML" in node)
            s = node.innerHTML;
          else {
            var asXml = function(n) {
              var s = "";
              if (n.nodeType == 1) {
                s += "<" + n.nodeName;
                for (var i=0; i<n.attributes.length;i++)
                  s += " " + n.attributes[i].nodeName + "=\"" + (n.attributes[i].nodeValue||"").toString() + "\"";
                if (n.firstChild) {
                  s += ">";
                  for (var c=n.firstChild; c; c=c.nextSibling)
                    s += asXml(c);
                  s += "</"+n.nodeName+">";
                }
                else
                  s += "/>";
              }
              else if (n.nodeType == 3)
                s += n.nodeValue;
              else if (n.nodeType == 4)
                s += "<![CDATA[" + n.nodeValue + "]]>";
              return s;
            };
            for (var c=node.firstChild; c; c=c.nextSibling)
              s += asXml(c);
          }
          return s;
        },
        escape: function(txt) {
          return txt.replace(/[\\]/g, "\\\\")
            .replace(/[\"]/g, '\\"')
            .replace(/[\n]/g, '\\n')
            .replace(/[\r]/g, '\\r');
        },
        removeWhite: function(e) {
          e.normalize();
          for (var n = e.firstChild; n; ) {
            if (n.nodeType == 3) {  // text node
              if (!n.nodeValue.match(/[^ \f\n\r\t\v]/)) { // pure whitespace text node
                var nxt = n.nextSibling;
                e.removeChild(n);
                n = nxt;
              }
              else
                n = n.nextSibling;
            }
            else if (n.nodeType == 1) {  // element node
              X.removeWhite(n);
              n = n.nextSibling;
            }
            else                      // any other node
              n = n.nextSibling;
          }
          return e;
        }
      };
      if (xml.nodeType == 9) // document node
        xml = xml.documentElement;
      var json = X.toJson(X.toObj(X.removeWhite(xml)), xml.nodeName, "\t");
      return "{\n" + tab + (tab ? json.replace(/\t/g, tab) : json.replace(/\t|\n/g, "")) + "\n}";
    }
    };
  });
  fuse.service('AccountBalanceService', function(){
    this.accountBalance = {};
    this.setAccountBalance = function(account){
      this.accountBalance = account;
    };
    this.getAccountBalance = function(){
      return this.accountBalance;
    };
  });
  fuse.service('cryptoService', function(){
    this.crypto = CryptoJS;
  });
  fuse.factory('dateRangeFactory', dateRangeFactory );

  function dateRangeFactory($filter){
    var dateRange ={filterData : filterData};
         return dateRange;

         function filterData(data, filter){
            var filteredDataByDate;
            if(!filter){
            filteredDataByDate = data;
              }
            else if (data && data.length > 0 && filter)
              {
        var date = JSON.parse(filter);
        var start_date = date.start;
        var end_date =   date.end;
          filteredDataByDate = data.reduce(function(start, elem){
           var filterResult;
           var dateKey;
               for(var key in elem){
                 if(elem[key] instanceof Date){
                   dateKey = key;
                   break;
                 }
                }

                // console.log(dateKey)
              var date = $filter('date')(elem[dateKey], "yyyy-MM-dd");
              if(start_date === end_date){
                 if(date === start_date){
                   filterResult = elem;
                   start.push(filterResult);
                 }
              }
              else if( date >= start_date && date <= end_date){
                filterResult = elem;
                start.push(filterResult);
              }
            return start;
          }, [])

        }
        return filteredDataByDate;
         }
  }
  fuse.service('messageService', function($mdToast){
    this.success = function(message, miliseconds){
      $mdToast.show({
        template: '<md-toast class="md-toast success">'+message+'</md-toast>',
        hideDelay: miliseconds,
        position: 'bottom right'
      });
    };
    this.error = function(message, miliseconds){
      $mdToast.show({
        template: '<md-toast class="md-toast error">'+message+'</md-toast>',
        hideDelay: miliseconds,
        position: 'bottom right'
      });
    };
  });
  fuse.service('appUtility', function(){
    this.isXML = function(xml){
      try{
        var xmlDoc = jQuery.parseXML(xml);
        return true;
      }catch(error){
        return false;
      }
    };
    this.parseDate = function(input){
      var parts = input.split('-');
      // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
      return new Date(parts[0], parts[1]-1, parts[2]); // Note: months are 0-based
    };
    this.textPastingLimit = function(event, limit){
      var length = event.originalEvent.clipboardData.getData('text/plain').length + event.originalEvent.target.value.length;
      if(length > limit){
        event.preventDefault();
      }
    };
    this.mergeObjects = function(obj1,obj2){
      var obj3 = {};
      for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
      for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
      return obj3;
    };
    this.toFixed = function(floating, to){
      floating = floating || 0.0;
      return parseFloat(floating).toFixed(to);
    };
    this.parseFloat = parseFloat;
    this.parseInt = parseInt;
    this.isNaN = isNaN;
    this.typeof = function(value){
      return typeof value;
    };
    this.toLowerCase = function(str){
      return str.toLowerCase();
    };
    this.replaceWith = function(string, separator){
      string = string || '';
      return string.replace(/ +/g, separator);
    };
    this.includes = function(string, substring){
      string = string || '';
      substring = substring || '';
      if( string.toLowerCase().indexOf(substring.toLowerCase()) !== -1){
        return true;
      }else{
        return false;
      }
    };
    this.protectAmount = function(event){
      var regExp = new RegExp('[a-zA-Z]'),
        inputVal = '';
      var input = event.key;
      var re = /\d/i;
      if(input != 'Backspace' && input != '.' && input != 'Delete' && input != 'Del' && input != 'Decimal' && input != 'Left' && input != 'Right' && input != 'Tab' && input != 'ArrowLeft' && input != 'ArrowRight'){
        if(input.match(re) == null){
          event.preventDefault();
          return false;
        }
      }else{
        if(input == '.' || input == 'Decimal'){
          re = /\./g;
          if(event.target.value.length > 0){
            if(event.target.value.match(/\./g) != null){
              if(event.target.value.match(/\./g).length > 0){
                event.preventDefault();
                return false;
              }
            }
          }
        }
      }
    };
    this.stopInput = function(event){
      event.preventDefault();
    };
  });
  fuse.filter('dashes', function(){
    return function(str){
      return str.replace(/ +/g, '-');
    };
  });
  fuse.filter('filterObject', function(){
    return function(str){
      console.log(typeof str);
      console.log(str);
      return str;
    };
  });
})();
