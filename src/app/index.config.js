(function ()
{
    'use strict';

    angular
        .module('fuse')
        .config(config);

    /** @ngInject */
    function config($translateProvider, $httpProvider, $compileProvider)
    {
      $httpProvider.defaults.headers.common = {};
      $httpProvider.defaults.headers.post = {};
      $httpProvider.defaults.headers.put = {};
      $httpProvider.defaults.headers.patch = {};
      //performance improvement
      $compileProvider.debugInfoEnabled(false);
      $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension|data):/);
      //vcRecaptchaServiceProvider
        // Put your common app configurations here
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/xml';
       /* vcRecaptchaServiceProvider.setDefaults({
          key: '6Lc-SxEUAAAAAOA_H4baTHQUWy3ZaZqlCpD32TBs',
          theme: 'light',
          size: 'normal',
          type: 'image',
          lang: 'en'
        });*/
        // angular-translate configuration
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: '{part}/i18n/{lang}.json'
        });
        $translateProvider.preferredLanguage('en');
        $translateProvider.useSanitizeValueStrategy('sanitize');
    }

})();
